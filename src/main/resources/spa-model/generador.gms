***** SIMULADOR PATRONES DE CORTE DE TRONCOS
**Generador de patrones laterales: se considera que las tablas laterales solo pueden tomar como ancho el valor maximo permitido para cada tabla.

OPTION MIP =  cplex;
OPTION iterlim = 10000000;
OPTION reslim  = 180 ;
option limrow  = 50000000;
OPTION OPTCR   = 0.0001;
OPTION OPTCA   = 0.01 ;
option threads = 6 ;

$call gdxxrw.exe Input=%inputxls% Output=%inputgdx% index=Index!A1
$gdxin %inputgdx%

Set
         d          diametro
         a          ancho de corte
         e          espesor de corte
         L          largos
         i          producto
         P_lat      patrones laterales /pl_0,pl_1*pl_1000/
         P_cen      patrones laterales /pc_0,pc_1*pc_5000/
         P_sup      patrones superiores /ps_0,ps_1*ps_1000/
         p          esquemas/p1*p20000/
         REA(e,a) combinacion espesor ancho
         Rel(i,e,a) realacion producto espesor y ancho
         RDL(d,l) combinacion diametros y largos
         RIL(i,l) combinacion producto intermedio y largos
         zpr       producto rendimiento

         x(d,a,p_lat)
         x_cen(d,a,p_CEN)
         x_sup(d,a,p_sup)
         xe(d,p,i)

         Rel_E(p,P_lat,P_cen,P_sup)
         Rel_p(d,p_lat,p_cen,p_sup)
         Rel_pval(d,p_lat,p_cen,p_sup)

$load d a e L i REA Rel RDL RIL zpr

*solo valido para la impresion
         zd          espesor de corte /Dimension/
         s      seccion  /Lateral, Central, Superior, Rendimiento/
*solo valido para la impresion
;


Alias
         (d,dd)
         (a,aA,aB,aC,aD,aE)
         (e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP,eQ,eR,eS,eT)
         (i,iA,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,IL,IM,IN,IO,IP,IQ,IR,IS,IT)
         (p,pp)
         (p_lat,p_lat2)
         (p_cen,p_cen2)
         (p_sup,p_sup2)
;

x(d,a,p_lat) =NO;
x_cen(d,a,p_cen) =NO;
x_sup(d,a,p_sup) =NO;
Rel_E(p,P_lat,P_cen,P_sup) =NO;
Rel_p(d,p_lat,p_cen,p_sup) =NO;
Rel_pval(d,p_lat,p_cen,p_sup) =NO;
xe(d,p,i) =NO;

display Rel, Rea, zpr, i;

Parameters
         DiametroCM(d) diametros [centimetro]
         ancho(a) ancho de corte [pulgadas]
         espesor(e) espesor de corte [pulgadas]
         Largos(l) largos
         Tlimite_Lat(d)      Cantidad de tablas maxima admitida en el costero lateral
         Tlimite(d)    Cantidad de tablas maxima admitida en el Bloque Central
         Tlimite_Sup(d)    Cantidad de tablas maxima admitida en el costero superior
         Rendi_Diametro(d)    Cantidad de tablas maxima admitida en el costero superior
$load DiametroCM ancho espesor Largos Tlimite_Lat Tlimite Tlimite_Sup Rendi_Diametro
;

Scalars
         Kerf_paralela      kerf sierras paralelas
         Kerf_multi      kerf sierras multiples
         Kerf_reaserr      kerf sierras reaserrado
         UnidadD  unidad original diametro
         UnidadL  unidad original largo
         UnidadE  unidad original espesor
         UnidadA  unidad original ancho
$load Kerf_paralela  Kerf_multi Kerf_reaserr UnidadD UnidadL UnidadA UnidadE
;

$gdxin

$ontext
////////////////////////////////////////////////////////////////////////////////
/                                                                              /
/                           Calculo dimensiones                                /
/                                                                              /
////////////////////////////////////////////////////////////////////////////////
$offtext

Parameters
         Area(a,e)   Area de producto [pulgadas]
         Area_P(i)   Area de producto [pulgadas]
         Radio(d)    Radio de tronco [pulgadas]
         Aread(d)    Area de tronco [pulgadas]
         Diametro(d) Diametro de tronco [pulgadas]
         Mancho(a)   Mitad del ancho del Bloque Central [pulgadas]
         Alto (d,a)          Alto del Bloque Central
         Alfa_lat(d,a)
         Alfa_sup(d,a)
         AreaLat(d,a)        Area Costero Izquierdo-Derecho (lateral)
         AreaSup(d,a)        Area Costero Superior-Inferior (superior)
         AreaBC(d,a)         Area Bloque Central
         Sag_lat(d,a)        Sagita Costero Lateral
         Sag_sup(d,a)        Sagita Costero Superior
         Dim_producto(i,l) Dimension de los productos
         Diametro_Largo(d,zd)      muestra el tamanio del diametro y las combinaciones de diametro y largos permitidos
;
Diametro(d)= DiametroCM(d) ;
Radio(d)= Diametro(d)/2;
Area(a,e)=ancho(a)*espesor(e);
Mancho(a)=ancho(a)/2;

Loop(a$(ord(a)>1),
Loop(e$(ord(e)>1),
Loop(i$(Rel(i,e,a)),
Area_P(i)=Area(a,e);
);););
Aread(d) =pi* Radio(d)**2;


** Calculo de las dimensiones del bloque central
Loop (d,
         Loop(a$(ord(a)>1),
         If ((Radio(d)>Mancho(a)),
                 Alto (d,a) = 2* SQRT (((Radio(d))**2)- (Mancho(a))**(2) )  ;
                 AreaBC(d,a) = Alto (d,a) * ancho(a);
                 Sag_lat(d,a)=Radio(d)-Mancho(a)-Kerf_paralela;
                 Sag_sup(d,a)=Radio(d)- Alto(d,a)/2 - Kerf_multi ;
                 Alfa_lat(d,a) = 2* arcsin(Alto(d,a)/(2*Radio(d)));
                 AreaLat(d,a) = 0.5 * (Alfa_lat(d,a) - sin(Alfa_lat(d,a)))*(Radio(d)**2);
                 Alfa_sup(d,a) = 2* arcsin(ancho(a)/(2*Radio(d)));
                 AreaSup(d,a) = 0.5 * (Alfa_sup(d,a) - sin(Alfa_sup(d,a)))*(Radio(d)**2);
         );

         );
);
display Sag_lat, espesor ,Sag_sup, AreaLat, AreaSup


$ontext
////////////////////////////////////////////////////////////////////////////////
/                                                                              /
/                            PATRONES LATERALES                                /
/                                                                              /
////////////////////////////////////////////////////////////////////////////////
$offtext

Scalar
         aux_lat  auxiliar para determinar el indice de los patrones /2/
         Aux1     auxiliar para el calculo del ancho de la 1 tabla lateral
         Aux2     auxiliar para el calculo del ancho de la 2 tabla lateral
         Aux3     auxiliar para el calculo del ancho de la 3 tabla lateral
         Aux4     auxiliar para el calculo del ancho de la 4 tabla lateral
         Aux5     auxiliar para el calculo del ancho de la 5 tabla lateral
;

Parameters
         Ancho1(d,a,e)                        ancho de la 1 tabla lateral
         Ancho2(d,a,e,eB)                     ancho de la 2 tabla lateral
         Ancho3(d,a,e,eB,eC)                  ancho de la 3 tabla lateral
         Ancho4(d,a,e,eB,eC,eD)               ancho de la 4 tabla lateral
         Ancho5(d,a,e,eB,eC,eD,eE)            ancho de la 5 tabla lateral

         Anchotabla1(d,a,e)                   ancho real de la 1 tabla lateral
         Anchotabla2(d,a,e)                   ancho real de la 2 tabla lateral
         Anchotabla3(d,a,e)                   ancho real de la 3 tabla lateral
         Anchotabla4(d,a,e)                   ancho real de la 4 tabla lateral
         Anchotabla5(d,a,e)                   ancho real de la 5 tabla lateral

         Sag_Lat1(d,a,e)                      sagita del lateral restante cuando la 1 tabla lateral es de espesor "e"
         Sag_Lat2(d,a,e,eB)                   sagita del lateral restante cuando la 2 tabla lateral es de espesor "eB"
         Sag_Lat3(d,a,e,eB,eC)                sagita del lateral restante cuando la 3 tabla lateral es de espesor "eC"
         Sag_Lat4(d,a,e,eB,eC,eD)             sagita del lateral restante cuando la 4 tabla lateral es de espesor "eD"
         Sag_Lat5(d,a,e,eB,eC,eD,eE)          sagita del lateral restante cuando la 5 tabla lateral es de espesor "eE"

         y(d,a,e,aA,eB,aB,eC,aC,eD,aD,eE,aE)  Vale 1 si las tablas e-aA +eB-aB+eC-aC+eD-aD+eE-aE se hace
         y_P(d,a,iA,iB,iC,iD,iE)              Vale 1 si los productos se hacen
* y e y_P determinan los productos que se hacen por patron

         ww(d,a,p_lat,i)    cuenta los productos por patron
;

*Radio(d)= Diametro(d)/2;
y(d,a,e,aA,eB,aB,eC,aC,eD,aD,eE,aE) =0;
ww(d,a,p_lat,i)=0;
*////////////////////////////////////////////////////////////////////////

Loop(d,
         Loop((a)$((ord(a) >1)),
*         If((ancho(a) <=  Alto (d,a)),
**  Calculo de las dimensiones de 1 tabla lateral
                 Loop(e$(espesor(e)< Sag_lat(d,a) and ord(e)>1 and 1 <= Tlimite_Lat(d)),
                         Sag_Lat1(d,a,e) =  Sag_lat(d,a)- espesor(e) - Kerf_reaserr;
                         ancho1(d,a,e) = 2* SQRT (((Radio(d))**2)- (Radio(d)-Sag_Lat1(d,a,e) - Kerf_reaserr)**(2) )  ;
                         Aux1 = ancho1(d,a,e);
                         anchotabla1(d,a,e)  = smax((aA)$(ancho(aA)<=Aux1 and REA(e,aA)),ancho(aA)) ;
                         Loop(aA$(ord(aA) >1 AND REA(e,aA)),
                                 If((anchotabla1(d,a,e)=ancho(aA) and Sag_Lat1(d,a,e) >0),
                                         y(d,a,e,aA,'e0','a0','e0','a0','e0','a0','e0','a0') = 1;
                                         Loop(i$(Rel(i,e,aA)),
                                                 y_P(d,a,i,'i0','i0','i0','i0')=y(d,a,e,aA,'e0','a0','e0','a0','e0','a0','e0','a0')  ;
                                         );
                                 );
                         );

**  Calculo de las dimensiones de 2 tabla lateral
                         Loop(eB$(espesor(eB)<Sag_Lat1(d,a,e)and ord(eB) >1 and 2 <= Tlimite_Lat(d)),
                                 Sag_Lat2(d,a,e,eB) =  Sag_Lat1(d,a,e)-espesor(eB) - Kerf_reaserr;
                                 ancho2(d,a,e,eB) = 2* SQRT (((Radio(d))**2)- (Radio(d)-Sag_Lat2(d,a,e,eB)- Kerf_reaserr)**(2) )  ;
                                 Aux2 = ancho2(d,a,e,eB);
                                 anchotabla2(d,a,eB)  = smax((aA,aB)$((ancho(aB)<=Aux2) and (ancho(aB)<= ancho(aA)) and REA(eB,aB)),ancho(aB)) ;
                                 Loop((aA,aB)$(ord(aA) >1 AND ord(aB) >1 AND REA(eB,aB)),
                                         If((anchotabla2(d,a,eB)=ancho(aB) and Sag_Lat2(d,a,e,eB)>0 and y(d,a,e,aA,'e0','a0','e0','a0','e0','a0','e0','a0') = 1),
                                                 y(d,a,e,aA,eB,aB,'e0','a0','e0','a0','e0','a0')=1;
                                                 Loop(i$(Rel(i,e,aA)),
                                                 Loop(iB$(Rel(iB,eB,aB)),
                                                         y_P(d,a,i,iB,'i0','i0','i0')=y(d,a,e,aA,eB,aB,'e0','a0','e0','a0','e0','a0')  ;
                                                 ););
                                         );
                                 );

**  Calculo de las dimensiones de 3 tabla lateral
                                 Loop(eC$(espesor(eC)<Sag_Lat2(d,a,e,eB)and ord(eC) >1 and 3 <= Tlimite_Lat(d)),
                                         Sag_Lat3(d,a,e,eB,eC) = Sag_Lat2(d,a,e,eB)-espesor(eC) - Kerf_reaserr;
                                         ancho3(d,a,e,eB,eC)= 2* SQRT (((Radio(d))**2)- (Radio(d)-Sag_Lat3(d,a,e,eB,eC)- Kerf_reaserr )**(2) )  ;
                                         Aux3 = ancho3(d,a,e,eB,eC);
                                         anchotabla3(d,a,eC)  = smax((aA,aB,aC)$((ancho(aC)<=Aux3) and (ancho(aC)<= ancho(aB)) and REA(eC,aC)),ancho(aC)) ;
                                         Loop((aA,aB,aC)$(ord(aA) >1 AND ord(aB) >1 AND ord(aC) >1 AND REA(eC,aC)),
                                                 If((anchotabla3(d,a,eC)=ancho(aC) and Sag_Lat3(d,a,e,eB,eC)>0 and y(d,a,e,aA,eB,aB,'e0','a0','e0','a0','e0','a0')=1),
                                                         y(d,a,e,aA,eB,aB,eC,aC,'e0','a0','e0','a0')= 1;
                                                         Loop(i$(Rel(i,e,aA)),
                                                         Loop(iB$(Rel(iB,eB,aB)),
                                                         Loop(iC$(Rel(iC,eC,aC)),
                                                                 y_P(d,a,i,iB,iC,'i0','i0')=y(d,a,e,aA,eB,aB,eC,aC,'e0','a0','e0','a0')  ;
                                                         );););
                                                 );
                                         );

**  Calculo de las dimensiones de 4 tabla lateral
                                         Loop(eD$(espesor(eD)<Sag_Lat3(d,a,e,eB,eC)and ord(eD) >1 and 4 <= Tlimite_Lat(d)),
                                                 Sag_Lat4(d,a,e,eB,eC,eD) = Sag_Lat3(d,a,e,eB,eC)-espesor(eD) - Kerf_reaserr;
                                                 ancho4(d,a,e,eB,eC,eD)= 2* SQRT (((Radio(d))**2)- (Radio(d)-Sag_Lat4(d,a,e,eB,eC,eD)- Kerf_reaserr)**(2) )  ;
                                                 Aux4 = ancho4(d,a,e,eB,eC,eD);
                                                 anchotabla4(d,a,eD)  = smax((aA,aB,aC,aD)$((ancho(aD)<=Aux4) AND (ancho(aD)<= ancho(aC))and REA(eD,aD)),ancho(aD)) ;
                                                 Loop((aA,aB,aC,aD)$(ord(aA) >1 AND ord(aB) >1 AND ord(aC) >1 AND ord(aD) >1 AND REA(eD,aD)),
                                                         If((anchotabla4(d,a,eD)=ancho(aD) and Sag_Lat4(d,a,e,eB,eC,eD) >0 and y(d,a,e,aA,eB,aB,eC,aC,'e0','A0','e0','A0')= 1),
                                                                 y(d,a,e,aA,eB,aB,eC,aC,eD,aD,'e0','a0')= 1;
                                                                 Loop(i$(Rel(i,e,aA)),
                                                                 Loop(iB$(Rel(iB,eB,aB)),
                                                                 Loop(iC$(Rel(iC,eC,aC)),
                                                                 Loop(iD$(Rel(iD,eD,aD)),
                                                                         y_P(d,a,i,ib,iC,iD,'i0')=y(d,a,e,aA,eB,aB,eC,aC,eD,aD,'e0','a0')  ;
                                                                 ););););
                                                         );
                                                 );

**  Calculo de las dimensiones de 5 tabla lateral
                                                 Loop(eE$(espesor(eE)<Sag_Lat4(d,a,e,eB,eC,eD)and ord(eE) >1 and 5 <= Tlimite_Lat(d)),
                                                         Sag_Lat5(d,a,e,eB,eC,eD,eE) = Sag_Lat4(d,a,e,eB,eC,eD)-espesor(eE) - Kerf_reaserr;
                                                         ancho5(d,a,e,eB,eC,eD,eE)= 2* SQRT (((Radio(d))**2)- (Radio(d)-Sag_Lat5(d,a,e,eB,eC,eD,eE)- Kerf_reaserr)**(2) )  ;
                                                         Aux5 = ancho5(d,a,e,eB,eC,eD,eE);
                                                         anchotabla5(d,a,eE)  = smax((aA,aB,aC,aD,aE)$((ancho(aE)<Aux5) and (ancho(aE)<= ancho(aD))and REA(eE,aE)) ,ancho(aE)) ;
                                                         Loop((aA,aB,aC,aD,aE)$(ord(aA) >1 AND ord(aB) >1 AND ord(aC) >1 AND ord(aD) >1 AND ord(aE) >1 AND REA(eE,aE)),
                                                                 If((anchotabla5(d,a,eE)=ancho(aE) and Sag_Lat5(d,a,e,eB,eC,eD,eE)>0 and y(d,a,e,aA,eB,aB,eC,aC,eD,aD,'e0','a0')= 1),
                                                                         y(d,a,e,aA,eB,aB,eC,aC,eD,aD,eE,aE) = 1;
                                                                         Loop(i$(Rel(i,e,aA)),
                                                                         Loop(iB$(Rel(iB,eB,aB)),
                                                                         Loop(iC$(Rel(iC,eC,aC)),
                                                                         Loop(iD$(Rel(iD,eD,aD)),
                                                                         Loop(iE$(Rel(iE,eE,aE)),
                                                                         y_P(d,a,i,iB,iC,iD,iE)=y(d,a,e,aA,eB,aB,eC,aC,eD,aD,eE,aE)  ;
                                                                         );););););
                                                                 );
                                                         );
                                                 );
                                         );
                                 );
                         );
                 );
         );
*);
);

Display       Sag_Lat1, ancho1, anchotabla1, y ;

** Se calcula el rendimiento del costero lateral
Parameters
         rendimiento_lat(d,a,i,ib,iC,iD,iE)
         rendimientos_lat(p_lat,d,a)
;

Loop((d,a,i,iB,iC,iD,iE)$(y_P(d,a,i,iB,iC,iD,iE)ne 0),
rendimiento_lat(d,a,i,iB,iC,iD,iE) =   (y_P(d,a,i,iB,iC,iD,iE)*Area_P(i)+
                                        y_P(d,a,i,iB,iC,iD,iE)*Area_P(ib)+
                                        y_P(d,a,i,iB,iC,iD,iE)*Area_P(ic)+
                                        y_P(d,a,i,iB,iC,iD,iE)*Area_P(id)+
                                        y_P(d,a,i,iB,iC,iD,iE)*Area_P(ie))
                                        / AreaLat(d,a);

);
Display rendimiento_lat;

**Se determina la cantidad de producto de cada tipo hay por cada patron lateral
Loop((d,a,i,iB,iC,iD,iE)$(y_P(d,a,i,iB,iC,iD,iE)ne 0),
         If (rendimiento_lat(d,a,i,iB,iC,iD,iE) > 0,
         x(d,a,p_lat) $[ord(P_lat) = aux_lat]=yes  ;
                         iF((ORD (i) >1),
                         ww(d,a,p_lat,i)$(ord(P_lat) = aux_lat)=ww(d,a,p_lat,i)+1;
                         );
                         iF((ORD (iB) >1),
                         ww(d,a,p_lat,iB)$(ord(P_lat) = aux_lat)=ww(d,a,p_lat,iB)+1;
                         );
                         iF((ORD (iC) >1),
                         ww(d,a,p_lat,iC)$(ord(P_lat) = aux_lat)=ww(d,a,p_lat,iC)+1;
                         );
                         iF((ORD (iD) >1),
                         ww(d,a,p_lat,iD)$(ord(P_lat) = aux_lat)=ww(d,a,p_lat,iD)+1;
                         );
                         iF((ORD (iE) >1),
                         ww(d,a,p_lat,iE)$(ord(P_lat) = aux_lat)=ww(d,a,p_lat,iE)+1;
                         );
         aux_lat= aux_lat +1;
         );
);

Loop((d,a,p_lat)$(x(d,a,p_lat) and ord(p_lat) <aux_lat ),
 rendimientos_lat(p_lat,d,a) = sum((i),  ww(d,a,p_lat,i) *Area_P(i)) / AreaLat(d,a);
);


Parameters
         Pro_lat_val(d,a,p_lat,i) da la cantidad por cada producto que genera cada PC
         pat_lat_val(d,a,p_lat)
;
Pro_lat_val(d,a,p_lat,i)=ww(d,a,p_lat,i);

Loop((d,a,p_lat)$(x(d,a,p_lat) and ord(p_lat) <aux_lat),
If( sum(i,Pro_lat_val(d,a,p_lat,i)) ne 0,
   pat_lat_val(d,a,p_lat) =1;
););

Display aux_lat, x,pat_lat_val,  Pro_lat_val;





$ontext
////////////////////////////////////////////////////////////////////////////////
/                                                                              /
/                            PATRONES CENTRALES                                /
/                                                                              /
////////////////////////////////////////////////////////////////////////////////
$offtext

parameters
      aux_cen  auxiliar para los indices de los patrones /2/
      ww_cen(d,a,p_cen,i)
      espmin
      resto_obj(d,a)

      restoOA(d,a,e)
      restoOB(d,a,e,eB)
      restoOC(d,a,e,eB,eC)
      restoOD(d,a,e,eB,eC,eD)
      restoOE(d,a,e,eB,eC,eD,eE)
      restoOF(d,a,e,eB,eC,eD,eE,eF)
      restoOG(d,a,e,eB,eC,eD,eE,eF,eG)
      restoOH(d,a,e,eB,eC,eD,eE,eF,eG,eH)
      restoOI(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI)
      restoOJ(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ)
      restoOK(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK)
      restoOL(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL)
      restoOM(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM)
      restoON(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN)
      restoOO(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO)
      restoOP(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP)
      restoOQ(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP,eQ)


      y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP,eQ)
      y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)
;

espmin  = smin((e)$(ORD(e)>1), espesor(e)) ;
resto_obj(d,a)=Alto (d,a)   ;
ww_cen(d,a,p_cen,i)=0;


Loop (d,
**  Calculo de las dimensiones de 1 tabla central
 Loop(a$(ORD(a)>1),
*  If((ancho(a) <=  Alto (d,a)),
   loop(e$(resto_obj(d,a)>espmin and (ORD(e)>1) and REA(e,a) and 1 <= Tlimite(d) ),
    restoOA(d,a,e)= resto_obj(d,a) - espesor(e) - Kerf_multi;
    If( restoOA(d,a,e)>0,
     y_Central(d,a,e,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0')=1;
     Loop(i$(Rel(i,e,a)),
      y_Central_p(d,a,i,'i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0');
     );
    );

**  Calculo de las dimensiones de 2 tabla central
    Loop(eB$(restoOA(d,a,e) >espmin and  ORD(eB)>=ORD(e) and REA(eB,a) and 2 <= Tlimite(d) ),
     restoOB(d,a,e,eB)= restoOA(d,a,e)  - espesor(eB) - Kerf_multi;
     If(restoOB(d,a,e,eB)>0,
      y_Central(d,a,e,eB,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0')=1;
      Loop(i$(Rel(i,e,a)),
      Loop(iB$(Rel(iB,eB,a)),
       y_Central_p(d,a,i,iB,'i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0');
      ););
     );

**  Calculo de las dimensiones de 3 tabla central
     Loop(eC$(restoOB(d,a,e,eB)>espmin and  ORD(eC)>=ORD(eB)and REA(eC,a) and 3 <= Tlimite(d) ),
      restoOC(d,a,e,eB,eC)= restoOB(d,a,e,eB)  - espesor(eC) - Kerf_multi;
      If(restoOC(d,a,e,eB,eC)>0,
       y_Central(d,a,e,eB,eC,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0')=1;
       Loop(i$(Rel(i,e,a)),
       Loop(iB$(Rel(iB,eB,a)),
       Loop(iC$(Rel(iC,eC,a)),
        y_Central_p(d,a,i,iB,iC,'i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0');
       );););
      );

**  Calculo de las dimensiones de 4 tabla central
      Loop(eD$(restoOC(d,a,e,eB,eC)>espmin and  ORD(eD)>=ORD(eC) and REA(eD,a) and 4 <= Tlimite(d) ),
       restoOD(d,a,e,eB,eC,eD)= restoOC(d,a,e,eB,eC)  - espesor(eD) - Kerf_multi;
       If(restoOD(d,a,e,eB,eC,eD)>0,
        y_Central(d,a,e,eB,eC,eD,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0')=1;
        Loop(i$(Rel(i,e,a)),
        Loop(iB$(Rel(iB,eB,a)),
        Loop(iC$(Rel(iC,eC,a)),
        Loop(iD$(Rel(iD,eD,a)),
         y_Central_p(d,a,i,iB,iC,iD,'i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0');
        ););););
       );

**  Calculo de las dimensiones de 5 tabla central
       Loop(eE$(restoOD(d,a,e,eB,eC,eD)>espmin and  ORD(eE)>=ORD(eD) and REA(eE,a) and 5 <= Tlimite(d) ),
        restoOE(d,a,e,eB,eC,eD,eE)= restoOD(d,a,e,eB,eC,eD)  - espesor(eE) - Kerf_multi;
        If(restoOE(d,a,e,eB,eC,eD,eE)>0,
         y_Central(d,a,e,eB,eC,eD,eE,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0')=1;
         Loop(i$(Rel(i,e,a)),
         Loop(iB$(Rel(iB,eB,a)),
         Loop(iC$(Rel(iC,eC,a)),
         Loop(iD$(Rel(iD,eD,a)),
         Loop(iE$(Rel(iE,eE,a)),
          y_Central_p(d,a,i,iB,iC,iD,iE,'i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0');
         );););););
        );

**  Calculo de las dimensiones de 6 tabla central
        Loop(eF$(restoOE(d,a,e,eB,eC,eD,eE)>espmin and  ORD(eF)>=ORD(eE) and REA(eF,a) and 6 <= Tlimite(d) ),
         restoOF(d,a,e,eB,eC,eD,eE,eF)= restoOE(d,a,e,eB,eC,eD,eE)  - espesor(eF) - Kerf_multi;
         If(restoOF(d,a,e,eB,eC,eD,eE,eF)>0,
          y_Central(d,a,e,eB,eC,eD,eE,eF,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0')=1;
          Loop(i$(Rel(i,e,a)),
          Loop(iB$(Rel(iB,eB,a)),
          Loop(iC$(Rel(iC,eC,a)),
          Loop(iD$(Rel(iD,eD,a)),
          Loop(iE$(Rel(iE,eE,a)),
          Loop(iFF$(Rel(iFF,eF,a)),
           y_Central_p(d,a,i,iB,iC,iD,iE,iFF,'i0','i0','i0','i0','i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0','e0');
          ););););););
         );

**  Calculo de las dimensiones de 7 tabla central
         Loop(eG$(restoOF(d,a,e,eB,eC,eD,eE,eF)>espmin and  ORD(eG)>=ORD(eF) and REA(eG,a) and 7 <= Tlimite(d) ),
          restoOG(d,a,e,eB,eC,eD,eE,eF,eG)= restoOF(d,a,e,eB,eC,eD,eE,eF)  - espesor(eG) - Kerf_multi;
          If(restoOG(d,a,e,eB,eC,eD,eE,eF,eG)>0,
           y_Central(d,a,e,eB,eC,eD,eE,eF,eG,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0')=1;
           Loop(i$(Rel(i,e,a)),
           Loop(iB$(Rel(iB,eB,a)),
           Loop(iC$(Rel(iC,eC,a)),
           Loop(iD$(Rel(iD,eD,a)),
           Loop(iE$(Rel(iE,eE,a)),
           Loop(iFF$(Rel(iFF,eF,a)),
           Loop(iG$(Rel(iG,eG,a)),
            y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,'i0','i0','i0','i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,'e0','e0','e0','e0','e0','e0','e0','e0','e0','e0');
           );););););););
          );

**  Calculo de las dimensiones de 8 tabla central
          Loop(eH$(restoOG(d,a,e,eB,eC,eD,eE,eF,eG)>espmin and  ORD(eH)>=ORD(eG) and REA(eH,a) and 8 <= Tlimite(d) ),
           restoOH(d,a,e,eB,eC,eD,eE,eF,eG,eH)= restoOG(d,a,e,eB,eC,eD,eE,eF,eG)  - espesor(eH) - Kerf_multi;
           If(restoOH(d,a,e,eB,eC,eD,eE,eF,eG,eH)>0,
            y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,'e0','e0','e0','e0','e0','e0','e0','e0','e0')=1;
            Loop(i$(Rel(i,e,a)),
            Loop(iB$(Rel(iB,eB,a)),
            Loop(iC$(Rel(iC,eC,a)),
            Loop(iD$(Rel(iD,eD,a)),
            Loop(iE$(Rel(iE,eE,a)),
            Loop(iFF$(Rel(iFF,eF,a)),
            Loop(iG$(Rel(iG,eG,a)),
            Loop(iH$(Rel(iH,eH,a)),
             y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,'i0','i0','i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,'e0','e0','e0','e0','e0','e0','e0','e0','e0');
            ););););););););
           );

**  Calculo de las dimensiones de 9 tabla central
           Loop(eI$(restoOH(d,a,e,eB,eC,eD,eE,eF,eG,eH)>espmin and  ORD(eI)>=ORD(eH) and REA(eI,a) and 9 <= Tlimite(d) ),
            restoOI(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI)= restoOH(d,a,e,eB,eC,eD,eE,eF,eG,eH)- espesor(eI) - Kerf_multi;
            If(restoOI(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI)>0,
             y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,'e0','e0','e0','e0','e0','e0','e0','e0')=1;
             Loop(i$(Rel(i,e,a)),
             Loop(iB$(Rel(iB,eB,a)),
             Loop(iC$(Rel(iC,eC,a)),
             Loop(iD$(Rel(iD,eD,a)),
             Loop(iE$(Rel(iE,eE,a)),
             Loop(iFF$(Rel(iFF,eF,a)),
             Loop(iG$(Rel(iG,eG,a)),
             Loop(iH$(Rel(iH,eH,a)),
             Loop(iI$(Rel(iI,eI,a)),
              y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,'i0','i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,'e0','e0','e0','e0','e0','e0','e0','e0');
             );););););););););
            );

**  Calculo de las dimensiones de 10 tabla central
            Loop(eJ$(restoOI(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI)>espmin and  ORD(eJ)>=ORD(eI) and REA(eJ,a) and 10 <= Tlimite(d) ),
             restoOJ(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ)= restoOI(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI)- espesor(eJ) - Kerf_multi;
             If(restoOJ(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ)>0,
              y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,'e0','e0','e0','e0','e0','e0','e0')=1;
              Loop(i$(Rel(i,e,a)),
              Loop(iB$(Rel(iB,eB,a)),
              Loop(iC$(Rel(iC,eC,a)),
              Loop(iD$(Rel(iD,eD,a)),
              Loop(iE$(Rel(iE,eE,a)),
              Loop(iFF$(Rel(iFF,eF,a)),
              Loop(iG$(Rel(iG,eG,a)),
              Loop(iH$(Rel(iH,eH,a)),
              Loop(iI$(Rel(iI,eI,a)),
              Loop(iJ$(Rel(iJ,eJ,a)),
               y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,'i0','i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,'e0','e0','e0','e0','e0','e0','e0');
              ););););););););););
             );

**  Calculo de las dimensiones de 11 tabla central
             Loop(eK$(restoOJ(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ)>espmin and  ORD(eK)>=ORD(eJ) and REA(eK,a) and 11 <= Tlimite(d) ),
              restoOK(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK)= restoOJ(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ)- espesor(eK) - Kerf_multi;
              If(restoOK(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK)>0,
               y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,'e0','e0','e0','e0','e0','e0')=1;
               Loop(i$(Rel(i,e,a)),
               Loop(iB$(Rel(iB,eB,a)),
               Loop(iC$(Rel(iC,eC,a)),
               Loop(iD$(Rel(iD,eD,a)),
               Loop(iE$(Rel(iE,eE,a)),
               Loop(iFF$(Rel(iFF,eF,a)),
               Loop(iG$(Rel(iG,eG,a)),
               Loop(iH$(Rel(iH,eH,a)),
               Loop(iI$(Rel(iI,eI,a)),
               Loop(iJ$(Rel(iJ,eJ,a)),
               Loop(iK$(Rel(iK,eK,a)),
                y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,'i0','i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,'e0','e0','e0','e0','e0','e0');
               );););););););););););
              );

**  Calculo de las dimensiones de 12 tabla central
              Loop(eL$(restoOK(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK)>espmin and  ORD(eL)>=ORD(eK) and REA(eL,a) and 12 <= Tlimite(d) ),
               restoOL(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL)= restoOK(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK)- espesor(eL) - Kerf_multi;
               If(restoOL(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL)>0,
                y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,'e0','e0','e0','e0','e0')=1;
                Loop(i$(Rel(i,e,a)),
                Loop(iB$(Rel(iB,eB,a)),
                Loop(iC$(Rel(iC,eC,a)),
                Loop(iD$(Rel(iD,eD,a)),
                Loop(iE$(Rel(iE,eE,a)),
                Loop(iFF$(Rel(iFF,eF,a)),
                Loop(iG$(Rel(iG,eG,a)),
                Loop(iH$(Rel(iH,eH,a)),
                Loop(iI$(Rel(iI,eI,a)),
                Loop(iJ$(Rel(iJ,eJ,a)),
                Loop(iK$(Rel(iK,eK,a)),
                Loop(iL$(Rel(iL,eL,a)),
                 y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,'i0','i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,'e0','e0','e0','e0','e0');
                ););););););););););););
               );

**  Calculo de las dimensiones de 13 tabla central
               Loop(eM$(restoOL(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL)>espmin and  ORD(eM)>=ORD(eL) and REA(eM,a) and 13 <= Tlimite(d) ),
                restoOM(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM)= restoOL(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL)- espesor(eM) - Kerf_multi;
                If(restoOM(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM)>0,
                 y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,'e0','e0','e0','e0')=1;
                 Loop(i$(Rel(i,e,a)),
                 Loop(iB$(Rel(iB,eB,a)),
                 Loop(iC$(Rel(iC,eC,a)),
                 Loop(iD$(Rel(iD,eD,a)),
                 Loop(iE$(Rel(iE,eE,a)),
                 Loop(iFF$(Rel(iFF,eF,a)),
                 Loop(iG$(Rel(iG,eG,a)),
                 Loop(iH$(Rel(iH,eH,a)),
                 Loop(iI$(Rel(iI,eI,a)),
                 Loop(iJ$(Rel(iJ,eJ,a)),
                 Loop(iK$(Rel(iK,eK,a)),
                 Loop(iL$(Rel(iL,eL,a)),
                 Loop(iM$(Rel(iM,eM,a)),
                  y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,'i0','i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,'e0','e0','e0','e0');
                 );););););););););););););
                );

**  Calculo de las dimensiones de 14 tabla central
                Loop(eN$(restoOM(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM)>espmin and  ORD(eN)>=ORD(eM) and REA(eN,a) and 14 <= Tlimite(d) ),
                 restoON(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN)= restoOM(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM)- espesor(eN) - Kerf_multi;
                 If(restoON(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN)>0,
                  y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,'e0','e0','e0')=1;
                  Loop(i$(Rel(i,e,a)),
                  Loop(iB$(Rel(iB,eB,a)),
                  Loop(iC$(Rel(iC,eC,a)),
                  Loop(iD$(Rel(iD,eD,a)),
                  Loop(iE$(Rel(iE,eE,a)),
                  Loop(iFF$(Rel(iFF,eF,a)),
                  Loop(iG$(Rel(iG,eG,a)),
                  Loop(iH$(Rel(iH,eH,a)),
                  Loop(iI$(Rel(iI,eI,a)),
                  Loop(iJ$(Rel(iJ,eJ,a)),
                  Loop(iK$(Rel(iK,eK,a)),
                  Loop(iL$(Rel(iL,eL,a)),
                  Loop(iM$(Rel(iM,eM,a)),
                  Loop(iN$(Rel(iN,eN,a)),
                   y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,IN,'i0','i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,'e0','e0','e0');
                  ););););););););););););););
                 );

**  Calculo de las dimensiones de 15 tabla central
                 Loop(eO$(restoON(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN)>espmin and  ORD(eO)>=ORD(eN) and REA(eO,a) and 15 <= Tlimite(d) ),
                  restoOO(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO)= restoON(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN)- espesor(eO) - Kerf_multi
                  If(restoOO(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO)>0,
                   y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,'e0','e0')=1;
                   Loop(i$(Rel(i,e,a)),
                   Loop(iB$(Rel(iB,eB,a)),
                   Loop(iC$(Rel(iC,eC,a)),
                   Loop(iD$(Rel(iD,eD,a)),
                   Loop(iE$(Rel(iE,eE,a)),
                   Loop(iFF$(Rel(iFF,eF,a)),
                   Loop(iG$(Rel(iG,eG,a)),
                   Loop(iH$(Rel(iH,eH,a)),
                   Loop(iI$(Rel(iI,eI,a)),
                   Loop(iJ$(Rel(iJ,eJ,a)),
                   Loop(iK$(Rel(iK,eK,a)),
                   Loop(iL$(Rel(iL,eL,a)),
                   Loop(iM$(Rel(iM,eM,a)),
                   Loop(iN$(Rel(iN,eN,a)),
                   Loop(iO$(Rel(iO,eO,a)),
                    y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,IN,iO,'i0','i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,'e0','e0');
                   );););););););););););););););
                  );

**  Calculo de las dimensiones de 16 tabla central
                  Loop(eP$(restoOO(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO)>espmin and  ORD(eP)>=ORD(eO) and REA(eP,a) and 16 <= Tlimite(d) ),
                   restoOP(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP)= restoOO(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO)- espesor(eP) - Kerf_multi;
                   If(restoOP(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP)>0,
                    y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP,'e0')=1;
                    Loop(i$(Rel(i,e,a)),
                    Loop(iB$(Rel(iB,eB,a)),
                    Loop(iC$(Rel(iC,eC,a)),
                    Loop(iD$(Rel(iD,eD,a)),
                    Loop(iE$(Rel(iE,eE,a)),
                    Loop(iFF$(Rel(iFF,eF,a)),
                    Loop(iG$(Rel(iG,eG,a)),
                    Loop(iH$(Rel(iH,eH,a)),
                    Loop(iI$(Rel(iI,eI,a)),
                    Loop(iJ$(Rel(iJ,eJ,a)),
                    Loop(iK$(Rel(iK,eK,a)),
                    Loop(iL$(Rel(iL,eL,a)),
                    Loop(iM$(Rel(iM,eM,a)),
                    Loop(iN$(Rel(iN,eN,a)),
                    Loop(iO$(Rel(iO,eO,a)),
                    Loop(iP$(Rel(iP,eP,a)),
                     y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,IN,iO,iP,'i0')=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP,'e0');
                    ););););););););););););););););
                   );

**  Calculo de las dimensiones de 17 tabla central
                   Loop(eQ$(restoOP(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP)>espmin and  ORD(eQ)>=ORD(eP) and REA(eQ,a) and 17 <= Tlimite(d) ),
                    restoOQ(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP,eQ)= restoOP(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP)- espesor(eQ) - Kerf_multi;
                    If(restoOQ(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP,eQ)>0,
                     y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP,eQ)=1;
                     Loop(i$(Rel(i,e,a)),
                     Loop(iB$(Rel(iB,eB,a)),
                     Loop(iC$(Rel(iC,eC,a)),
                     Loop(iD$(Rel(iD,eD,a)),
                     Loop(iE$(Rel(iE,eE,a)),
                     Loop(iFF$(Rel(iFF,eF,a)),
                     Loop(iG$(Rel(iG,eG,a)),
                     Loop(iH$(Rel(iH,eH,a)),
                     Loop(iI$(Rel(iI,eI,a)),
                     Loop(iJ$(Rel(iJ,eJ,a)),
                     Loop(iK$(Rel(iK,eK,a)),
                     Loop(iL$(Rel(iL,eL,a)),
                     Loop(iM$(Rel(iM,eM,a)),
                     Loop(iN$(Rel(iN,eN,a)),
                     Loop(iO$(Rel(iO,eO,a)),
                     Loop(iP$(Rel(iP,eP,a)),
                     Loop(iQ$(Rel(iQ,eQ,a)),
                      y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,IN,iO,iP,iQ)=y_Central(d,a,e,eB,eC,eD,eE,eF,eG,eH,eI,eJ,eK,eL,eM,eN,eO,eP,eQ);
                     );););););););););););););););););
                    );
                   );
                  );
                 );
                );
               );
              );
             );
            );
           );
          );
         );
        );
       );
      );
     );
    );
   );
*  );
 );
);



*dISPLAY resto_obj, y_Central_p;
Parameters
         rendimiento(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)
         rendimientos(p_cen,d,a)
;

Loop((d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)$(y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)ne 0),
rendimiento(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ) = (y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(i)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(iB)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(ic)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(id)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(ie)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(iFF)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(ig)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(ih)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(ii)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(ij)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(iK)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(iL)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(iM)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(iN)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(iO)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(iP)+
                                                                       y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)*Area_P(iQ))
                                                                       / AreaBC(d,a)

);

**Cuento los productos centrales
Loop((d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)$(y_Central_p(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ)ne 0),
 If (rendimiento(d,a,i,iB,iC,iD,iE,iFF,iG,iH,iI,iJ,iK,iL,iM,iN,iO,iP,iQ) > 0.0,
 x_cen(d,a,p_cen) $[ord(p_cen) = aux_cen]=yes  ;
  iF((ORD (i) >1),
   ww_cen(d,a,p_cen,i)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,i)+1;
  );
  iF((ORD (iB) >1),
   ww_cen(d,a,p_cen,iB)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iB)+1;
  );
  iF((ORD (iC) >1),
  ww_cen(d,a,p_cen,iC)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iC)+1;
  );
  iF((ORD (iD) >1),
  ww_cen(d,a,p_cen,iD)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iD)+1;
  );
  iF((ORD (iE) >1),
  ww_cen(d,a,p_cen,iE)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iE)+1;
  );
  iF((ORD (iFF) >1),
  ww_cen(d,a,p_cen,iFF)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iFF)+1;
  );
  iF((ORD (iG) >1),
  ww_cen(d,a,p_cen,iG)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iG)+1;
  );
  iF((ORD (iH) >1),
  ww_cen(d,a,p_cen,iH)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iH)+1;
  );
  iF((ORD (iI) >1),
  ww_cen(d,a,p_cen,iI)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iI)+1;
  );
  iF((ORD (iJ) >1),
  ww_cen(d,a,p_cen,iJ)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iJ)+1;
  );
  iF((ORD (iK) >1),
  ww_cen(d,a,p_cen,iK)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iK)+1;
  );
  iF((ORD (iL) >1),
  ww_cen(d,a,p_cen,iL)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iL)+1;
  );
  iF((ORD (iM) >1),
  ww_cen(d,a,p_cen,iM)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iM)+1;
  );
  iF((ORD (iN) >1),
  ww_cen(d,a,p_cen,iN)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iN)+1;
  );
  iF((ORD (iO) >1),
  ww_cen(d,a,p_cen,iO)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iO)+1;
  );
  iF((ORD (iP) >1),
  ww_cen(d,a,p_cen,iP)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iP)+1;
  );
  iF((ORD (iQ) >1),
  ww_cen(d,a,p_cen,iQ)$(ord(P_cen) = aux_cen)= ww_cen(d,a,p_cen,iQ)+1;
  );
  aux_cen= aux_cen +1;
  );
);

Loop((d,a,p_cen)$(x_cen(d,a,p_cen) and ord (p_cen) < aux_cen),
 rendimientos(p_cen,d,a) = sum((i),  ww_cen(d,a,p_cen,i) *Area_P(i)) / AreaBC(d,a);
);


Parameters
         pat_cen_val(d,a,p_cen)
;

Scalars
         aux_cen2
         indCen
;

loop(d,
 Loop(a,
  loop(p_cen$((ord(p_cen) lt aux_cen) and (x_cen(d,a,p_cen))),
   loop(p_cen2$((ord(p_cen2) lt aux_cen) and (x_cen(d,a,p_cen2) and (ord(p_cen2) ne ord(p_cen)))),
    indCen = smin(i,(ww_cen(d,a,p_cen,i)-ww_cen(d,a,p_cen2,i)))  ;
    if (indCen eq 0,
      x_cen(d,a,p_cen2) = no;
    );
   );
  );
 );
);


Display x_cen;


Parameters
         Pro_cen_val(d,a,p_cen,i) da la cantidad por cada producto que genera cada PC
;

Loop((d,a,p_cen)$(x_cen(d,a,p_cen)),
 pat_cen_val(d,a,p_cen)=1
 Loop(i,
         Pro_cen_val(d,a,p_cen,i)=ww_cen(d,a,p_cen,i);
););
display Pro_cen_val, pat_cen_val


$ontext
////////////////////////////////////////////////////////////////////////////////
/                                                                              /
/                            PATRONES SUPERIOR                                 /
/                                                                              /
////////////////////////////////////////////////////////////////////////////////
$offtext

Scalars
         aux_sup  auxiliar para los indices de los patrones /2/
         Aux1_sup     auxiliar para el calculo de la altura de la 1 tabla lateral
         Aux2_sup     auxiliar para el calculo de la altura de la 2 tabla lateral
         Aux3_sup     auxiliar para el calculo de la altura de la 3 tabla lateral
         Aux4_sup     auxiliar para el calculo de la altura de la 4 tabla lateral
         Aux5_sup     auxiliar para el calculo de la altura de la 5 tabla lateral
;

Parameters
    Ancho1_sup(d,a,e)                        ancho de la 1 tabla superior
    Ancho2_sup(d,a,e,eB)                     ancho de la 2 tabla superior
    Ancho3_sup(d,a,e,eB,eC)                  ancho de la 3 tabla superior
    Ancho4_sup(d,a,e,eB,eC,eD)               ancho de la 4 tabla superior
    Ancho5_sup(d,a,e,eB,eC,eD,eE)            ancho de la 5 tabla superior

    Anchotabla1_sup(d,a,e)                   ancho real de la 1 tabla superior
    Anchotabla2_sup(d,a,e)                   ancho real de la 2 tabla superior
    Anchotabla3_sup(d,a,e)                   ancho real de la 3 tabla superior
    Anchotabla4_sup(d,a,e)                   ancho real de la 4 tabla superior
    Anchotabla5_sup(d,a,e)                   ancho real de la 5 tabla superior

    Resto1_sup(d,a,e)                        espesor del superior restante cuando la 1 tabla superior es de espesor "e"
    Resto2_sup(d,a,e,eB)                     espesor del superior restante cuando la 2 tabla superior es de espesor "eB"
    Resto3_sup(d,a,e,eB,eC)                  espesor del superior restante cuando la 3 tabla superior es de espesor "eC"
    Resto4_sup(d,a,e,eB,eC,eD)               eesor del superior restante cuando la 4 tabla superior es de espesor "eD"
    Resto5_sup(d,a,e,eB,eC,eD,eE)            espesor del superior restante cuando la 5 tabla superior es de espesor "eE"

    y_sup(d,a,e,aA,eB,aB,eC,aC,eD,aD,eE,aE)
    y_P_sup(d,a,iA,iB,iC,iD,iE)

    ww_sup(d,a,p_sup,i)  cuenta los productos por patron
;

y_sup(d,a,e,aA,eB,aB,eC,aC,eD,aD,eE,aE) =0;
ww_sup(d,a,p_sup,i)=0;
*////////////////////////////////////////////////////////////////////////

**  Calculo de las dimensiones de tablas laterales
Loop(d,
         Loop((a)$(ord(a) >1 ),
*         If((ancho(a) <=  Alto (d,a)),
**  Calculo de las dimensiones de 1 tabla Superior
                 Loop(e$(espesor(e)< Sag_sup(d,a) and ord(e) >1 and 1 <= Tlimite_Sup(d) ),
                         Resto1_sup(d,a,e) =  Sag_sup(d,a)-espesor(e) - Kerf_reaserr;
                         ancho1_sup(d,a,e) = 2* SQRT (((Radio(d))**2)- (Radio(d)-Resto1_sup(d,a,e)- Kerf_reaserr)**(2) )  ;
                         Aux1_sup = ancho1_sup(d,a,e);
                         anchotabla1_sup(d,a,e)  = smax((aA)$(ancho(aA)<=Aux1_sup and REA(e,aA)),ancho(aA)) ;
                         Loop(aA$(ord(aA) >1 AND REA(e,aA)),
                                 If((anchotabla1_sup(d,a,e)>=ancho(aA)) and Resto1_sup(d,a,e) >0,
                                         y_sup(d,a,e,aA,'e0','a0','e0','a0','e0','a0','e0','a0') = 1;
                                         Loop(i$(Rel(i,e,aA)),
                                                 y_P_sup(d,a,i,'i0','i0','i0','i0')=y_sup(d,a,e,aA,'e0','a0','e0','a0','e0','a0','e0','a0')  ;
                                         );
                                 );
                         );
*$ontext
**  Calculo de las dimensiones de 2 tabla Superior
                         Loop(eB$(espesor(eB)<Resto1_sup(d,a,e)and ord(eB) >1 and 2 <= Tlimite_Sup(d) ),
                                 Resto2_sup(d,a,e,eB) =  Resto1_sup(d,a,e)-espesor(eB) - Kerf_reaserr;
                                 ancho2_sup(d,a,e,eB) = 2* SQRT (((Radio(d))**2)- (Radio(d)-Resto2_sup(d,a,e,eB)- Kerf_reaserr)**(2) )  ;
                                 Aux2_sup = ancho2_sup(d,a,e,eB);
                                 anchotabla2_sup(d,a,eB)  = smax((aA,aB)$((ancho(aB)<=Aux2_sup) and (ancho(aB)<= ancho(aA))and REA(eB,aB)),ancho(aB)) ;
                                 Loop((aA,aB)$(ord(aA) >1 AND ord(aB) >1 AND REA(eB,aB)),
                                         If((anchotabla2_sup(d,a,eB)>=ancho(aB) and Resto2_sup(d,a,e,eB)>0 and y_sup(d,a,e,aA,'e0','a0','e0','a0','e0','a0','e0','a0') = 1),
                                                 y_sup(d,a,e,aA,eB,aB,'e0','a0','e0','a0','e0','a0')=1;
                                                 Loop(i$(Rel(i,e,aA)),
                                                 Loop(iB$(Rel(iB,eB,aB)),
                                                         y_P_sup(d,a,i,iB,'i0','i0','i0')=y_sup(d,a,e,aA,eB,aB,'e0','a0','e0','a0','e0','a0')  ;
                                                 ););
                                         );
                                 );
**  Calculo de las dimensiones de 3 tabla Superior
                                 Loop(eC$(espesor(eC)<Resto2_sup(d,a,e,eB)and ord(eC) >1 and 3 <= Tlimite_Sup(d) ),
                                         Resto3_sup(d,a,e,eB,eC) = Resto2_sup(d,a,e,eB)-espesor(eC) - Kerf_reaserr;
                                         ancho3_sup(d,a,e,eB,eC)= 2* SQRT (((Radio(d))**2)- (Radio(d)-Resto3_sup(d,a,e,eB,eC) - Kerf_reaserr)**(2) )  ;
                                         Aux3_sup = ancho3_sup(d,a,e,eB,eC);
                                         anchotabla3_sup(d,a,eC)  = smax((aA,aB,aC)$((ancho(aC)<=Aux3_sup) and (ancho(aC)<= ancho(aB))and REA(eC,aC)),ancho(aC)) ;
                                         Loop((aA,aB,aC)$(ord(aA) >1 AND ord(aB) >1 AND ord(aC) >1 AND REA(eC,aC)),
                                                 If((anchotabla3_sup(d,a,eC)>=ancho(aC) and Resto3_sup(d,a,e,eB,eC)>0 and y_sup(d,a,e,aA,eB,aB,'e0','a0','e0','a0','e0','a0')=1),
                                                         y_sup(d,a,e,aA,eB,aB,eC,aC,'e0','a0','e0','a0')= 1;
                                                         Loop(i$(Rel(i,e,aA)),
                                                         Loop(iB$(Rel(iB,eB,aB)),
                                                         Loop(iC$(Rel(iC,eC,aC)),
                                                                 y_P_sup(d,a,i,iB,iC,'i0','i0')=y_sup(d,a,e,aA,eB,aB,eC,aC,'e0','a0','e0','a0')  ;
                                                         );););
                                                 );
                                         );
**  Calculo de las dimensiones de 4 tabla Superior
                                         Loop(eD$(espesor(eD)<Resto3_sup(d,a,e,eB,eC)and ord(eD) >1 and 4 <= Tlimite_Sup(d) ),
                                                 Resto4_sup(d,a,e,eB,eC,eD) = Resto3_sup(d,a,e,eB,eC)-espesor(eD) - Kerf_reaserr;
                                                 ancho4_sup(d,a,e,eB,eC,eD)= 2* SQRT (((Radio(d))**2)- (Radio(d)-Resto4_sup(d,a,e,eB,eC,eD)- Kerf_reaserr)**(2) )  ;
                                                 Aux4_sup = ancho4_sup(d,a,e,eB,eC,eD);
                                                 anchotabla4_sup(d,a,eD)  = smax((aA,aB,aC,aD)$((ancho(aD)<=Aux4_sup) AND (ancho(aD)<= ancho(aC))and REA(eD,aD)),ancho(aD)) ;
                                                 Loop((aA,aB,aC,aD)$(ord(aA) >1 AND ord(aB) >1 AND ord(aC) >1 AND ord(aD) >1 AND REA(eD,aD)),
                                                         If((anchotabla4_sup(d,a,eD)>=ancho(aD) and Resto4_sup(d,a,e,eB,eC,eD) >0 and y_sup(d,a,e,aA,eB,aB,eC,aC,'e0','a0','e0','a0')= 1),
                                                                 y_sup(d,a,e,aA,eB,aB,eC,aC,eD,aD,'e0','a0')= 1;
                                                                 Loop(i$(Rel(i,e,aA)),
                                                                 Loop(iB$(Rel(iB,eB,aB)),
                                                                 Loop(iC$(Rel(iC,eC,aC)),
                                                                 Loop(iD$(Rel(iD,eD,aD)),
                                                                         y_P_sup(d,a,i,iB,iC,iD,'i0')=y_sup(d,a,e,aA,eB,aB,eC,aC,eD,aD,'e0','a0')  ;
                                                                 ););););
                                                         );
                                                 );
**  Calculo de las dimensiones de 5 tabla Superior
                                                 Loop(eE$(espesor(eE)<Resto4_sup(d,a,e,eB,eC,eD)and ord(eE) >1 and 5 <= Tlimite_Sup(d) ),
                                                         Resto5_sup(d,a,e,eB,eC,eD,eE) = Resto4_sup(d,a,e,eB,eC,eD)-espesor(eE) - Kerf_reaserr;
                                                         ancho5_sup(d,a,e,eB,eC,eD,eE)= 2* SQRT (((Radio(d))**2)- (Radio(d)-Resto5_sup(d,a,e,eB,eC,eD,eE)- Kerf_reaserr)**(2) )  ;
                                                         Aux5_sup = ancho5_sup(d,a,e,eB,eC,eD,eE);
                                                         anchotabla5_sup(d,a,eE)  = smax((aA,aB,aC,aD,aE)$((ancho(aE)<=Aux5_sup) and (ancho(aE)<= ancho(aD))and REA(eE,aE)) ,ancho(aE)) ;
                                                         Loop((aA,aB,aC,aD,aE)$(ord(aA) >1 AND ord(aB) >1 AND ord(aC) >1 AND ord(aD) >1 AND ord(aE) >1 AND REA(eE,aE)),
                                                                 If((anchotabla5_sup(d,a,eE)>=ancho(aE) and Resto5_sup(d,a,e,eB,eC,eD,eE)>0 and y_sup(d,a,e,aA,eB,aB,eC,aC,eD,aD,'e0','a0')= 1),
                                                                         y_sup(d,a,e,aA,eB,aB,eC,aC,eD,aD,eE,aE) = 1;
                                                                         Loop(i$(Rel(i,e,aA)),
                                                                         Loop(iB$(Rel(iB,eB,aB)),
                                                                         Loop(iC$(Rel(iC,eC,aC)),
                                                                         Loop(iD$(Rel(iD,eD,aD)),
                                                                         Loop(iE$(Rel(iE,eE,aE)),
                                                                         y_P_sup(d,a,i,iB,iC,iD,iE)=y_sup(d,a,e,aA,eB,aB,eC,aC,eD,aD,eE,aE)  ;
                                                                         );););););
                                                                 );
                                                         );
                                                 );
                                         );
                                 );
                         );
*$offtext
                 );
         ););
*);
display Resto1_sup,  ancho1_sup ;

Parameters
         rendimiento_sup(d,a,i,iB,iC,iD,iE)
         rendimientos_sup(p_sup,d,a)
;

Loop((d,a,i,iB,iC,iD,iE)$(y_P_sup(d,a,i,iB,iC,iD,iE)ne 0),
rendimiento_sup(d,a,i,iB,iC,iD,iE) = (y_P_sup(d,a,i,iB,iC,iD,iE)*Area_P(i)+
                                        y_P_sup(d,a,i,iB,iC,iD,iE)*Area_P(iB)+
                                        y_P_sup(d,a,i,iB,iC,iD,iE)*Area_P(ic)+
                                        y_P_sup(d,a,i,iB,iC,iD,iE)*Area_P(id)+
                                        y_P_sup(d,a,i,iB,iC,iD,iE)*Area_P(ie))
                                        / AreaSup(d,a);

);


**Cuento los productos superiores
Loop((d,a,i,iB,iC,iD,iE)$(y_P_sup(d,a,i,iB,iC,iD,iE)ne 0),
         If (rendimiento_sup(d,a,i,iB,iC,iD,iE) > 0,
         x_sup(d,a,p_sup) $[ord(P_sup) = aux_sup]=yes  ;
                         iF((ORD (i) >1),
                         ww_sup(d,a,p_sup,i)$(ord(P_sup) = aux_sup)=ww_sup(d,a,p_sup,i)+1;
                         );
                         iF((ORD (iB) >1),
                         ww_sup(d,a,p_sup,iB)$(ord(P_sup) = aux_sup)=ww_sup(d,a,p_sup,iB)+1;
                         );
                         iF((ORD (iC) >1),
                         ww_sup(d,a,p_sup,iC)$(ord(P_sup) = aux_sup)=ww_sup(d,a,p_sup,iC)+1;
                         );
                         iF((ORD (iD) >1),
                         ww_sup(d,a,p_sup,iD)$(ord(P_sup) = aux_sup)=ww_sup(d,a,p_sup,iD)+1;
                         );
                         iF((ORD (iE) >1),
                         ww_sup(d,a,p_sup,iE)$(ord(P_sup) = aux_sup)=ww_sup(d,a,p_sup,iE)+1;
                         );

         aux_sup= aux_sup +1;
         );
);


Loop((d,a,p_sup)$(x_sup(d,a,p_sup) and ord(p_sup) <aux_sup),
rendimientos_sup(p_sup,d,a) = sum((i),  ww_sup(d,a,p_sup,i) *Area_P(i)) / AreaSup(d,a);
);

display rendimiento_sup, rendimientos_sup, x_sup, aux_sup ;


Parameters
         Pro_sup_val(d,a,p_sup,i) da la cantidad por cada producto que genera cada PC
         pat_sup_val(d,a,p_sup)
;

Pro_sup_val(d,a,p_sup,i)=ww_sup(d,a,p_sup,i);

Loop((d,a,p_sup),
If( sum(i,Pro_sup_val(d,a,p_sup,i)) ne 0,
   pat_sup_val(d,a,p_sup) =1;
););

Display Pro_sup_val, ww_sup, pat_sup_val;




$ontext
////////////////////////////////////////////////////////////////////////////////
/                                                                              /
/                           RELACION DE PATRONES                               /
/                                                                              /
////////////////////////////////////////////////////////////////////////////////
$offtext


Scalars
         relp/1/
         nesq/1/
         nesq1 contador de esquemas
;

Parameters
         pr(d,p,i)    cantidad de productos i que se obtiene de un tronco de diametro d aplicando un esquema p
         prm(d,p,i)   cantidad de productos i que se obtiene de un tronco de diametro d aplicando un esquema p luego de eliminar incluidos
         prodea(d,p_lat,p_cen,p_sup,i)
         prodeb(d,p_lat,p_cen,p_sup,i)
         prodec(d,p_lat,p_cen,p_sup,i)
         proded(d,p_lat,p_cen,p_sup,i)
         AreaOcu(d,p_lat,p_cen,p_sup)
         Rendi(d,p_lat,p_cen,p_sup)
         prodt(d,p_lat,p_cen,p_sup,i)
         RendiP(p,d) indica que el patron P existe para el diametro d
         RendiPM(p,d) indica que el patron P existe para el diametro d luego de eliminar incluidos
;

*$ontext
Loop((d,a)$(ord(a)>1),
         Loop(P_lat$(pat_lat_val(d,a,p_lat)=1 and ord(p_lat) <aux_lat),
                 Loop(P_cen$(pat_cen_val(d,a,p_cen)=1 and ord(p_cen) <aux_cen),
                         Loop(i$(ORD(i)>1),
                                 prodea(d,p_lat,P_cen,'ps_0',i) = 2*Pro_lat_val(d,a,p_lat,i)+ Pro_cen_val(d,a,p_cen,i) ;
                         );
                         relp=relp+1;
                         Rel_p(d,p_lat,p_cen,'ps_0') =yes;
);););
*$offtext
Loop((d,a)$(ord(a)>1),
         Loop(P_sup$(pat_sup_val(d,a,p_sup) = 1 and ord(p_sup) <aux_sup),
                 Loop(P_cen$(pat_cen_val(d,a,p_cen)=1 and ord(p_cen) <aux_cen),
                         Loop(i$(ORD(i)>1),
                                 prodeb(d,'pl_0',P_cen,p_sup,i) =  Pro_cen_val(d,a,p_cen,i) + 2*Pro_sup_val(d,a,p_sup,i);
                         );
                         relp=relp+1;
                         Rel_p(d,'pl_0',p_cen,P_sup) =yes;
);););

Loop((d,a)$(ord(a)>1),
         Loop(P_cen$(pat_cen_val(d,a,p_cen)=1 and ord(p_cen) <aux_cen),
                 Loop(i$(ORD(i)>1),
                         proded(d,'pl_0',P_cen,'ps_0',i) =  Pro_cen_val(d,a,p_cen,i);
                         );
                         relp=relp+1;
                         Rel_p(d,'pl_0',p_cen,'ps_0') =yes;
););


Parameters
         x2_e(d,p_lat,p_cen,p_sup)              vale 1 si el patron se hace
;

Loop((d,a)$(ord(a)>1),
         Loop(P_lat$(pat_lat_val(d,a,p_lat) = 1 and ord(p_lat) <aux_lat),
                 Loop(P_sup$(pat_sup_val(d,a,p_sup) = 1 and ord(p_sup) <aux_sup),
                         Loop(P_cen$(pat_cen_val(d,a,p_cen)=1 and ord(p_cen) <aux_cen),
                                 Loop(i$(ORD(i)>1),
                                         prodec(d,p_lat,P_cen,p_sup,i) = 2*Pro_lat_val(d,a,p_lat,i)+ Pro_cen_val(d,a,p_cen,i) + 2*Pro_sup_val(d,a,p_sup,i);
                                 );
                                 relp=relp+1;
                                 Rel_p(d,P_lat,p_cen,P_sup) =yes;
););););

Loop((d,a)$(ord(a)>1),
         Loop((P_lat,P_sup,P_cen)$(Rel_p(d,P_lat,p_cen,P_sup)),
                 prodt(d,p_lat,p_cen,p_sup,i) =  prodea(d,p_lat,p_cen,p_sup,i) + prodeb(d,p_lat,p_cen,p_sup,i) +prodec(d,p_lat,p_cen,p_sup,i) +proded(d,p_lat,p_cen,p_sup,i);
*                 prodt(d,p_lat,p_cen,p_sup,i) =  + prodeb(d,p_lat,p_cen,p_sup,i) +prodec(d,p_lat,p_cen,p_sup,i);
                 AreaOcu(d,p_lat,P_cen,p_sup) =  sum((i),(prodea(d,p_lat,P_cen,p_sup,i) *Area_P(i)+prodeb(d,p_lat,P_cen,p_sup,i) *Area_P(i)+prodec(d,p_lat,P_cen,p_sup,i) *Area_P(i) +proded(d,p_lat,P_cen,p_sup,i) *Area_P(i)));
*                 AreaOcu(d,p_lat,P_cen,p_sup) =  sum((p),(prodeb(d,p_lat,P_cen,p_sup,i) *Area_P(i)+prodec(d,p_lat,P_cen,p_sup,i) *Area_P(i)));
                 Rendi(d,p_lat,P_cen,p_sup) = AreaOcu(d,p_lat,P_cen,p_sup) / Aread(d);
         );
);


display prodt ,  prodea, prodeb,prodec,proded, AreaOcu, Rendi, Rel_pval;




Parameters
         Rel_DEs(p,d), rendimiento_patron(d,p,i)
         Rel_DEsM(p,d) vale 1 si el patron p esta definido para el diametro d luego de eliminar incluidos
;

Loop((d),
*Loop((P_lat,P_sup,P_cen)$(Rel_pval(d,P_lat,p_cen,P_sup)),
Loop((P_lat,P_sup,P_cen)$(Rel_p(d,P_lat,p_cen,P_sup)),
         If (Rendi(d,p_lat,P_cen,p_sup) >= Rendi_Diametro(d) ,
                 Loop(p$(ord(p) eq nesq),
                         pr(d,p,i)= prodt(d,p_lat,p_cen,p_sup,i);
                         RendiP(p,d)=Rendi(d,p_lat,P_cen,p_sup);
                         Rel_DEs(p,d)=1;
                 );
                 nesq=nesq+1;
         );
););


scalar indi;

loop(d,
 loop(p$((ord(p) lt nesq) and (Rel_DEs(p,d) eq 1)),
  loop(pp$((ord(pp) lt nesq) and (Rel_DEs(pp,d) eq 1) and (ord(pp) ne ord(p))),
   indi = smin(i,(pr(d,p,i)-pr(d,pp,i)))  ;
   if (indi eq 0,
    Rel_DEs(pp,d) = 0;
   ) ;
  ) ;
 ) ;
) ;

nesq1 = 1 ;

loop(d,
 loop(p$((ord(p) lt nesq) and (Rel_DEs(p,d) eq 1)),
  Loop(pp$(ord(pp) eq nesq1),
   prM(d,pp,i)= pr(d,p,i) ;
   RendiPM(pp,d)= RendiP(p,d);
   Rel_DEsM(pp,d)=1;
  );
  nesq1=nesq1+1;
 ) ;
) ;



*Se pretende volver a los valores cargados por el usuario
Parameters
         DiametroOrig(d)
         LargoOrig(l)
         EspesorOrig(e)
         AnchoOrig(a)
;


Loop(d,
 If(UnidadD = 1,
  DiametroOrig(d) = round(DiametroCM(d)/0.393701);
 );
 If(UnidadD = 2,
  DiametroOrig(d) = round(DiametroCM(d)/0.0393701);
 );
 If(UnidadD = 3,
  DiametroOrig(d) = round(DiametroCM(d)/3.93701);
 );
 If(UnidadD = 4,
  DiametroOrig(d) = round(DiametroCM(d)/12);
 );
 If(UnidadD = 5,
  DiametroOrig(d) = round(DiametroCM(d));
 );
);

Loop(l,
 If(UnidadL = 1,
  LargoOrig(l) = Largos(l)/0.393701;
 );
 If(UnidadL = 2,
  LargoOrig(l) = Largos(l)/0.0393701;
 );
 If(UnidadL = 3,
  LargoOrig(l) = Largos(l)/3.93701;
 );
 If(UnidadL = 4,
  LargoOrig(l) = Largos(l)/12;
 );
 If(UnidadL = 5,
  LargoOrig(l) = Largos(l);
 );
);

Loop(e,
 If(UnidadE = 1,
  EspesorOrig(e) = espesor(e)/0.393701;
 );
 If(UnidadE = 2,
  EspesorOrig(e) = espesor(e)/0.0393701;
 );
 If(UnidadE = 3,
  EspesorOrig(e) = espesor(e)/3.93701;
 );
 If(UnidadE = 4,
  EspesorOrig(e) = espesor(e)/12;
 );
 If(UnidadE = 5,
  EspesorOrig(e) = espesor(e);
 );
);

Loop(a,
 If(UnidadA = 1,
  AnchoOrig(a) = ancho(a) /0.393701;
 );
 If(UnidadA = 2,
  AnchoOrig(a) = ancho(a) /0.0393701;
 );
 If(UnidadA = 3,
  AnchoOrig(a) = ancho(a) /3.93701;
 );
 If(UnidadA = 4,
  AnchoOrig(a) = ancho(a) /12;
 );
 If(UnidadA = 5,
  AnchoOrig(a) = ancho(a) ;
 );
);


********************************************************************************
***Util solo para la impresion


Loop((i,e,a)$(Rel(i,e,a)),
 Dim_producto(i,'Ancho')=AnchoOrig(a);
 Dim_producto(i,'Espesor')= EspesorOrig(e) ;
 Loop(l$(RIL(i,l)),
  Dim_producto(i,l)= yes;
 );
);

Diametro_Largo(d,'Dimension')=Diametro(d);

Loop((d,p)$(Rel_DEsM(p,d) eq 1 ),
Loop(i,
 rendimiento_patron(d,p,i)= prM(d,p,i);
);
 rendimiento_patron(d,p,'rendimiento')=round(100*RendiPM(p,d),2);
);
*Loop((d,p,zpr,i)$(Rel_DEsM(p,d) eq 1),
* rendimiento_patron(p,'rendimiento')= RendiPM(p,d);
*);

display Aread, Area_P, Dim_producto, Sag_lat, espesor, DiametroOrig, rendimiento_patron;

Parameter
         Tseccion(d,s)
         RenMin(d)
;

Loop(d,
 Tseccion(d,'Lateral') = Tlimite_Lat(d);
 Tseccion(d,'Central') = Tlimite(d);
 Tseccion(d,'Superior') = Tlimite_Sup(d);
 RenMin(d) = 100 * Rendi_Diametro(d);
);

********************************************************************************


**Para mostrar al usuario
execute_unload "%generadorgdx%" DiametroOrig Dim_producto  rendimiento_patron prM  LargoOrig Tseccion RenMin
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=RenMin rng=ClasesDiametricas-Largos!e3  dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=ClasesDiametricas-Largos!e2 par=Tseccion rng=ClasesDiametricas-Largos!h2 text="Maxima Cantidad de Tablas por Seccion" rng=ClasesDiametricas-Largos!i1 '

 If(UnidadD = 1,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=DiametroOrig rng=ClasesDiametricas-Largos!b3 dim=1 rdim=1  text="Diametro Menor [cm]" rng=ClasesDiametricas-Largos!b2'
 );
 If(UnidadD = 2,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=DiametroOrig rng=ClasesDiametricas-Largos!b3 dim=1 rdim=1  text="Diametro Menor [mm]" rng=ClasesDiametricas-Largos!b2'
 );
 If(UnidadD = 3,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=DiametroOrig rng=ClasesDiametricas-Largos!b3 dim=1 rdim=1  text="Diametro Menor [m]" rng=ClasesDiametricas-Largos!b2'
 );
 If(UnidadD = 4,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=DiametroOrig rng=ClasesDiametricas-Largos!b3 dim=1 rdim=1  text="Diametro Menor [pie]" rng=ClasesDiametricas-Largos!b2'
 );
 If(UnidadD = 5,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=DiametroOrig rng=ClasesDiametricas-Largos!b3 dim=1 rdim=1  text="Diametro Menor [pulgada]" rng=ClasesDiametricas-Largos!b2'
 );

 If(UnidadL = 1,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=LargoOrig rng=ClasesDiametricas-Largos!m3 dim=1 rdim=1  text="Largo[cm]" rng=ClasesDiametricas-Largos!m2'
 );
 If(UnidadL = 2,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=LargoOrig rng=ClasesDiametricas-Largos!m3 dim=1 rdim=1  text="Largo [mm]" rng=ClasesDiametricas-Largos!m2'
 );
 If(UnidadL = 3,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=LargoOrig rng=ClasesDiametricas-Largos!m3 dim=1 rdim=1  text="Largo [m]" rng=ClasesDiametricas-Largos!m2'
 );
 If(UnidadL = 4,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=LargoOrig rng=ClasesDiametricas-Largos!m3 dim=1 rdim=1  text="Largo [pie]" rng=ClasesDiametricas-Largos!m2'
 );
 If(UnidadL = 5,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=LargoOrig rng=ClasesDiametricas-Largos!m3 dim=1 rdim=1  text="Largo [pulgada]" rng=ClasesDiametricas-Largos!m2'
 );


execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=Dim_producto rng=Productos_Intermedios!b2 text="Largos Posibles" rng=Productos!e1 '
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Productos" rng=Productos_Intermedios!b2 text="Dimension Nominal" rng=Productos!c1 '
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% par=rendimiento_patron  rng=EsquemasyRendimiento!b2 '

 If(UnidadE = 1,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Espesor [cm]" rng=Productos_Intermedios!c2'
 );
 If(UnidadE = 2,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Espesor [mm]" rng=Productos_Intermedios!c2'
 );
 If(UnidadE = 3,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Espesor [m]" rng=Productos_Intermedios!c2'
 );
 If(UnidadE = 4,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Espesor [pie]" rng=Productos_Intermedios!c2'
 );
 If(UnidadE = 5,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Espesor  [pulgadas]" rng=Productos_Intermedios!c2'
 );

 If(UnidadA = 1,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Ancho [cm]" rng=Productos_Intermedios!d2'
 );
 If(UnidadA = 2,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Ancho [mm]" rng=Productos_Intermedios!d2'
 );
 If(UnidadA = 3,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Ancho [m]" rng=Productos_Intermedios!d2'
 );
 If(UnidadA = 4,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Ancho [pie]" rng=Productos_Intermedios!d2'
 );
 If(UnidadA = 5,
execute 'gdxxrw.exe Input=%generadorgdx% Output=%generadorxlsx% text="Ancho  [pulgadas]" rng=Productos_Intermedios!d2'
 );




execute_unload "%resgeneradorgdx%" Pro_lat_val Pro_cen_val  Pro_sup_val prM RendiPM Rel_DEsM DiametroCM  Diametro_Largo  Dim_producto RDL RIL rendimiento_patron Largos  Diametro  Area_P Tseccion

execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% par=prM      rng=PC-Primario!b2 '
execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% text="p0"    rng=PC-Primario!c2 '
execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% par=RendiPM  rng=Rendimiento!b2'
execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% par=Rel_DEsM rng=Rel-D-Es!b2'

execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% par=Diametro rng=AreaDiametro!b2'
execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% par=Largos rng=Largos!b2'
execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% par=Diametro_Largo rng=ClasesDiametricas!b1 set=RDL  rng=ClasesDiametricas!d1 '

execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% par=Dim_producto rng=Productos!b2  '
execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% text="Productos" rng=Productos!b2 text="Espesor" rng=Productos!c2  text="Ancho" rng=Productos!d2 text="Dimension Nominal " rng=Productos!c1 '
execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% set=RIL rng=Productos!e2'
execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% text="Largos Posibles" rng=Productos!f1'

execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% par=Area_P rng=AreaProductos!b2 dim=1 rdim=1'
execute 'gdxxrw.exe Input=%resgeneradorgdx% Output=%resgeneradorxlsx% par=Tseccion rng=TablasMAx!b2'
