$ontext
####.###bitacora del capitan###########
FECHA 2.5.2017
Modelo basado en JAIIO. Partimos como base
$offtext

$onecho > %simuladorXC%
dset=p                           rng=PC-Primario!C2              rdim=1 cdim=0
dset=l                           rng=Productos!c2                rdim=0 cdim=1
dset=d                           rng=ClasesDiametricas!b2        rdim=1 cdim=0
par=RDL                          rng=ClasesDiametricas!d1        rdim=1 cdim=1
par=RIL                          rng=Productos!e2                rdim=1 cdim=1
par=RelacionTAyTronco            rng=PC-Primario!B2              rdim=2 cdim=1
par=Rendi                        rng=Rendimiento!B2              dim=2 rdim=1
par=Rel_DP                       rng=Rel-D-Es!B2                 dim=2 rdim=1
par=Area                         rng=AreaDiametro!b2             cdim=1
par=Largo                        rng=Largos!b2                   cdim=1
par=diametro                     rng=ClasesDiametricas!b2        dim=1 rdim=1
par=Dim_producto                 rng=Productos!b2                dim=2 rdim=1
par=Area_P                       rng=AreaProductos!b2            dim=1 rdim=1
par=TSeccion                     rng=TablasMAx!b2                dim=2 rdim=1
$offecho
$call gdxxrw.exe Input=%resgeneradorxlsx% Output=%resgeneradorgdx% @%simuladorXC%

$call gdxxrw.exe Input=%inputxls%   Output=%inputgdx%   index=Index!A1



set
         p  define el ESQUEMA de corte PRIMARIO
         d   define los diametros de los troncos
         l   define la longitud de los productos finales y del tronco
         i  define los productos intermedios obtenidos en el aserradero en espesor y ancho
         s  define el PATRON de corte SECUNDARIO
         f  tablas SECUNDARIAS o finales de remanufactura obtenidas por set "s"
         RDL(d,l) combinacion diametros y largos
         RIL(i,l) combinacion producto intermedio y largos
         RNP(p)   patrones no deseados
         pfac (p)
         n numero de tablas /n1*n16/

***********************Arreglo para impresion**********************************
         z
         /Minima, Maxima, Precio/

         zZ
         /Espesor, Ancho/

         ST      seccion
         /Rendimiento, Lateral, Central, Superior /
*******************************************************************************

$gdxin %inputgdx%
$load i f s

$gdxin %resgeneradorgdx%
$load    p l d RDL RIL

$gdxin %inputgdx%
$load RNP

;

alias(p,ep,eep)
alias(d,dd,ddd)
alias(i,ia,iaa)
alias(l,ll,lll)
alias(s,sa,saa)
alias(f,ff,fff);

pfac (p)=no;

display RDL, RIL;


*//////INTPUT//////INTPUT/////INTPUT///////INTPUT/////////INTPUT////INTPUT//////

*-----ESCALARES-------------------------
parameters
BigM                             es un numero suficientemente grande
TSetupmin                        tiempos de setup o reconfiguracion de los equipos (min)
TMaxOperacionAse                 tiempo maximo de operacion de aserradero en un periodo determinado (horas)
CosSetupAse                      costo del setup ($ por min)
CosVarAse                        define el costo variable de produccion por tronco procesado ($ por hora)
UnidadD
UnidadL
UnidadE
FO

$load  BigM TSetupmin TMaxOperacionAse CosSetupAse CosVarAse UnidadD UnidadL UnidadE FO
display BigM, TSetupmin, TMaxOperacionAse, CosSetupAse, CosVarAse  ;


*----------- MATRICES Y VECTORES--------
parameters
CosTonelada(d)            define el costo de un tronco de diametro "d"  ($ por tonelada)
InvTroncoNuevo(l,d)       cantidad de troncos de diametro "d" de longitud "l" disponibles (unidades)
TiempProcAs(d,l)          tiempo de proceso de cada tronco de diametro "d" de longitud "l"  (troncos por minutos)
ajuste(d,n)
pvTF(f)                         es el precio de venta de una tablas final "f" de longitud "l" procesada en remanufactura ($ por unidad)
DemTF(f)                        demanda final de las tablas "f" de longitud "l"
DemTFmin(f)                     demanda final minima de las tablas "f" de longitud "l"
RendiLimite(d)                  Rendimiento limite por diametro
RelacionTAyTF(s,i,l,f)          indica la cantidad de tablas "f" que salen de una tabla "i" por el patron "s"

$load   CosTonelada  InvTroncoNuevo  pvTF DemTF  DemTFmin RendiLimite  TiempProcAs ajuste  RelacionTAyTF
display CosTonelada, InvTroncoNuevo, pvTF, DemTF, DemTFmin, RendiLimite, TiempProcAs, ajuste, RelacionTAyTF;
********************************************************************************


parameters
RelacionTAyTronco(d,p,i)        define la cantidad de productos "i" que se obtiene de un tronco de diametro "d" aplicando un esquema "p" (unidades)
Rendi(p,d)                      rendimiento de cada esquema
Rel_DP(p,d)                     define la relacion diametro esquema primario
Area(d)                         Area transversal del tronco  (pulgadas^2)
Largo(l)                        Define los largos de los distintos troncos (pulgadas)
diametro(d)                     Diametro del tronco (pulgadas)
Dim_producto(i,ZZ)               dimension productos intermedios
Area_P(i)                       Area productos intermedios
TSeccion(d,ST)                  Tablas por seccion

$gdxin %resgeneradorgdx%
$load RelacionTAyTronco Rendi Rel_DP   Area Largo diametro Dim_producto Area_P TSeccion
;
display  RelacionTAyTronco,  Rendi, Rel_DP, Area, Largo, diametro,Dim_producto,Area_P, TSeccion;


********************************************************************************
*calculos y cambios de unidades de parametros

Parameters

CosProcTronco(d,p,l)   costo de proceso en ($ por tronco)
TiempProcTronco(d,p,l) tiempo de proceso (troncos por hora)
TSetup                 tiempo de setup (horas)
CosMPTronco(l,d)       costo del tronco ($ por tronco)
VolumenTronco(l,d)     volumen tronco (m^3)
DiametroMEtro(d)       diametro tronco (m)
Largometro(l)          largo tronco (m)
Area_Pmetro(i)         area de producto (m)
DiametroMedio(d)       diametro medio tronco (m)
Totaltablas(d,p)       Total de tablas por PC (unidad)
ajusteTabla(d,p)

;
*define la cantidad de tablas totales por PC
Totaltablas(d,p)=sum(i,RelacionTAyTronco(d,p,i));
*
Loop((d,p,n)$Rel_DP(p,d),
 If((Totaltablas(d,p) eq ord(n) and ord(n) <= card (n)) ,
  ajusteTabla(d,p) = ajuste(d,n);
 );
 if ((Totaltablas(d,p) > card (n)),
  ajusteTabla(d,p) = ajuste(d,'n16');
 );
);
*cambio de unidad en el tiempo de procesamiento de troncos/min a troncos/hora y por esquema
display Rel_DP, RDL;
Loop((d,l),
 if( TiempProcAs(d,l) <= 0,
  RDL(d,l)=no;;
 );
);
TiempProcTronco(d,p,l)$(Rel_DP(p,d))= 60 * TiempProcAs(d,l) * ajusteTabla(d,p);
*convierto el costo de proceso de $/hora a $/tronco
CosProcTronco(d,p,l)$(Rel_DP(p,d) and  RDL(d,l))= CosVarAse / TiempProcTronco(d,p,l);
*convierto tiempo de setup de min a horas
TSetup= TSetupmin /60;
*convierto diametro de pulgada a m
DiametroMEtro(d)= 0.0254*diametro(d);
*convierto largo de pulgada a m
Largometro(l)= 0.0254*Largo(l);
*convierto largo de pulgada a m
Area_Pmetro(i)= (0.0254**2)*Area_P(i);

display TSetup, Rel_DP, RDL;
*calculo del diametro medio de la clase diametrica (m)
Loop(d,
 if(ord(d) eq card(d),
  DiametroMedio(d)=DiametroMEtro(d)+(DiametroMEtro(d)-DiametroMEtro(d-1))/2;
 else
  DiametroMedio(d)= DiametroMEtro(d)+(DiametroMEtro(d+1)-DiametroMEtro(d))/2;
 );
);
*calculo del volumen de tronco (m^3)
VolumenTronco(l,d)=(pi/4)*(((DiametroMedio(d)+0.01*Largometro(l))**2+(DiametroMedio(d))**2)/2)*Largometro(l);
*calculo del costo de MP ($/tronco)
CosMPTronco(l,d)= CosTonelada(d) *VolumenTronco(l,d);

Display Totaltablas, TiempProcTronco, CosProcTronco, TSetup,VolumenTronco, DiametroMEtro, Largometro, DiametroMedio, CosMPTronco, ajusteTabla ;


** se eliminan aquellos patrones que tienen un rendimiento menor al limite
Loop((p,d),
If(Rendi(p,d) > RendiLimite(d),
 pfac (p)=yes;
););


Loop(p$(RNP(p)),
 pfac (p)=no;
);


Parameters
patronTOTAL
DiametroOrig(d)
LargoOrig(l)
Dim_productoOrig(i,l)
Tablas
RendiLimiteRED(d)
LimTablas(d,st)
;

loop((d,p),
 loop((i),
  patronTOTAL(d,p,i) = RelacionTAyTronco(d,p,i);
 );
  patronTOTAL(d,p,'rendimiento') = round(100*Rendi(p,d),2);
);


Loop(d,
 If(UnidadD = 1,
  DiametroOrig(d) = round(diametro(d)/0.393701);
 );
 If(UnidadD = 2,
  DiametroOrig(d) = round(diametro(d)/0.0393701);
 );
 If(UnidadD = 3,
  DiametroOrig(d) = round(diametro(d)/3.93701);
 );
 If(UnidadD = 4,
  DiametroOrig(d) = round(diametro(d)/12);
 );
 If(UnidadD = 5,
  DiametroOrig(d) = round(diametro(d));
 );
);

Loop(l,
 If(UnidadL = 1,
  LargoOrig(l) = Largo(l)/0.393701;
 );
 If(UnidadL = 2,
  LargoOrig(l) = Largo(l)/0.0393701;
 );
 If(UnidadL = 3,
  LargoOrig(l) = Largo(l)/3.93701;
 );
 If(UnidadL = 4,
  LargoOrig(l) = Largo(l)/12;
 );
 If(UnidadL = 5,
  LargoOrig(l) = Largo(l);
 );
);

Loop((i),
 If(UnidadE = 1,
  Dim_productoOrig(i,'Espesor') = Dim_producto(i,'Espesor')/0.393701;
  Dim_productoOrig(i,'Ancho') = Dim_producto(i,'Ancho')/0.393701;
 );
 If(UnidadE = 2,
  Dim_productoOrig(i,'Espesor') = Dim_producto(i,'Espesor')/0.0393701;
  Dim_productoOrig(i,'Ancho') = Dim_producto(i,'Ancho')/0.0393701;
 );
 If(UnidadE = 3,
  Dim_productoOrig(i,'Espesor') = Dim_producto(i,'Espesor')/3.93701;
  Dim_productoOrig(i,'Ancho') = Dim_producto(i,'Ancho')/3.93701;
 );
 If(UnidadE = 4,
  Dim_productoOrig(i,'Espesor') = Dim_producto(i,'Espesor')/12;
  Dim_productoOrig(i,'Ancho') = Dim_producto(i,'Ancho')/12;
 );
 If(UnidadE = 5,
  Dim_productoOrig(i,'Espesor') = Dim_producto(i,'Espesor');
  Dim_productoOrig(i,'Ancho') = Dim_producto(i,'Ancho');
 );
);

Loop((i,l)$(RIL(i,l)),
 Dim_productoOrig(i,l)= yes;
);

Loop(f,
 Tablas(f,'minima') = DemTFmin(f);
 Tablas(f,'maxima') = DemTF(f);
 Tablas(f,'Precio') = pvTF(f);
);

RendiLimiteRED(d) = max (TSeccion(d,'rendimiento'),(100*RendiLimite(d)));

Loop(d,
LimTablas(d,'Lateral') = TSeccion(d,'Lateral') ;
LimTablas(d,'Central') = TSeccion(d,'Central') ;
LimTablas(d,'Superior') = TSeccion(d,'Superior') ;
);

display  RendiLimiteRED, LimTablas;

*-------------------VARIABLES-----------
free variable
z_Objetivo1                      variable objetivo que maximiza el ingreso del aserradero y remanufactura ($)
z_Objetivo2                      variable objetivo que min el costo
z_Objetivo3                      variable objetivo que max el rendimeinto
z_Objetivo4                      variable objetivo que min la demanda insatisfecha


*Parte de la FO aserradero
CosVarTOTALAse                   es el costo total variable del aserradero ($)
CosTroncoTOTALAse                define el costo de comprar los troncos ($)
TiempoSetupTOTALAse              es el tiempo de setup total o configuracion de equipos (horas)
CostoSetupTOTALAse               costo total del setup ($)
*Parte de la FO remanufactura
IngBrutoTOTALRema                ingreso bruto total del remanufactura ($)
CosTOTALDemInsatisfTF            costo total de demanda insatisfecha ($)
CosDemIns
;


positive variable
NumTroncoProceAs(p,d,l)         define la cantidad de tronco de diametro "d" y longitud "l" que se le aplicara el patron "p" (unidades)
VentaTA(l,i)                    venta de productos finales "i" de longitud "l" (unidades)
ProdTFRema(f)                   define la cantidad de productos "f" de longitud "l" que se va a producir (unidades)
TOTALProdTF(f)                  define la cantidad total de tablas finales "f" de longitud "l" que se obtiene de las "i" (unidades)
VentaTF(f)                      define las ventas de tablas finales "f" entre lo producido y el inventario del periodo anterios (unidades)
TR(s,l,i)                       cantidad de tablas "ta" y longitud "l" que seran cortadas con el patron de corte "s"
NumTA(l,i)                      indica la cantidad de producto final "ta" de longitud "l" que se obtiene por el aprovechamiento de un determinado lote (unidades)
DemTroncoAs(l,d)                cantidad de tronco de diametro "d" y longitud "l" que se demanda para completar el lote para procesar (unidades)
NivelInventTroncoAs(l,d)        es el nivel del inventario de troncos de diametro "d" longitud "l" en un periodos determinado (unidades)
TiemProcLoteTronco(p,d,l)       es el tiempo de procesar un lote de rollos de diametro "d" longitud "l" aplicando el esquema "es" (segundo)
DemInsatisfTA(l,i)              define la demanda insatisfecha como cantidad de producto "ta" de longitud "l" que se requiere completar una demanda dada (unidades)
NivelInventTA(l,i)              define el inventario de producto "i" de lonfgitud "l" en un periodo dado (unidades)
NivelInventTF(f)                es el inventario del periodo actual (unidades)
TieOpRema(f)                    computa el tiempo que se utiliza para procesar un lote "f" (segundos)
DemInsatisfTF(f)                define la demanda insatisfecha de tablas finales (unidades)
TroncosNuevos(l,d)              troncos en inventario (unidades)
TroncosViejosDescartados(l,d)   troncos en inventario que no llegaron a ser procesados y se mancharon (unidades)
CostoTOTALTroncoDescartado      costo por descartar troncos manchados ($)
Rendimiento                     porcentaje de tronco ocupado por tablas intermedias
Perdidas                        porcentaje de tronco sin ocupar por tablas intermedias
DInsTotal                       Total demanda insatisfecha
InvTA(l,i)                      inventario tablas intermediasi
InvTF(f)                        inventario tablas finales f
ITTF                            inventario TOTAL tablas finales f
PTotal                          produccion total de f
TTotal                          Tiempo total empleado
Insttotal
InsttotalVol
ETotal                           Numero total de esquemas

;

binary variable
BinSiAplES(p)                   define se aplica el esquema "p"
x(s)                            define si se aplica el patron s
;

*//////DECLARACION DE ECUACIONE DEL MODELO//////////////////////////////////////





equation
FuncionObjetivo                  funcion objetivo que max el beneficio economico
FO2                              funcion objetivo que min el costo
FO3                              funcion objetivo que max el rendimeinto
FO4                              funcion objetivo que min la demanda insatisfecha

*                     *** PARTE ASERRADERO ***
ProduccionVSInventario(l,d)      busca que la produccion de productos este en funcion del inventario de troncos
GeneradorProductosTabla(l,i)     genera los productos a partir de los esquemas de corte
VentaVSDisponibilidad(l,i)       las ventas deben ser menores o iguales a lo que se produce mas el inventario acumulado
ContarSetup(p)                   defien cuando hay setup
Pedidatotal                      calculo perdidas totales

ContarTroncoNuevo                computa el parametro a variable

TiempoProceso(p,l,d)             tiempo entre rollos - implica tiempo de corte mas makespan entre rollos -
SumaTiempoSetup                  contador del tiempo de setup
TiempoProcesoYSetup              busca que no se supere un tiempo maximo de operacion

InveTA(l,i)                      Inventario de TA

*                     *** PARTE REMANUFACTURA ***
ProduccioRemanufactura(l,i)
GeneradorProductosFinales    busca determinar la procuccion de "f" a partir de "ta"
VentaVSDispProdFinales       alcula la venta de "f" en funcion a la produccion y el inventario acumulado
SatisfDemandaProdFinales     busca satisfacer la demanda de "f" o catiabilizar la insatisfaccion.
Demandamin                   busca satisfacer la demanda minima de "f"
Tproduccion                  Total de produccion
DemIns                       Total demanda insatisfecha
InveTF(f)                    Inventario de f
InvTotal                     Inventario total tablas finales
Tusado
Eusado
Volumentotal

*----- Ecu. Auxiliares aserradero ------
auxAs2                           calcula los costos variables de proceso total
auxAs3                           calcula el costo variable por procesar un tronco dado
auxAs4                           calcula el costo del setup
*----- Ecu. Auxiliares remanufactura ---
auxRem1                          calcula el ingreso bruto de remanufactura
auxRem2                          costo demanda insatisfecha
;

*//////ECUACIONE DEL MODELO/////////////////////////////////////////////////////
*----- Funcion objetivo ----------------
*beneficio
FuncionObjetivo..  z_Objetivo1 =e= IngBrutoTOTALRema
                                   - CosVarTOTALAse
                                   - CosTroncoTOTALAse
;

*costos totales
Fo2..             z_Objetivo2 =e= (CosVarTOTALAse + CosTroncoTOTALAse )  ;
*rendimiento
Fo3..             z_Objetivo3 =e= Perdidas;
*demanda insatisfecha
Fo4..             z_Objetivo4 =e= CosDemIns;


***Ecuaciones auxiliares del serradero
*costo de proceso
auxAs2..                         sum((p,d,l)$(pfac (p)), NumTroncoProceAs(p,d,l)*CosProcTronco(d,p,l))  =e= CosVarTOTALAse;
*costo de Materia prima
auxAs3..                         sum((p,d,l)$(pfac (p)), NumTroncoProceAs(p,d,l)*CosMPTronco(l,d))       =e= CosTroncoTOTALAse;
*costo de setup
auxAs4..                         CosSetupAse*sum((p), BinSiAplES(p)*TSetup)                   =e= CostoSetupTOTALAse;
***Ecuaciones auxiliares de la remanufactura
*Ingresos
auxRem1..                        sum((f),pvTF(f)*VentaTF(f))                    =e= IngBrutoTOTALRema;
*demanda insatisfecha
auxRem2..                        CosDemIns =e= sum(f,DemInsatisfTF(f) * pvTF(f));

*----- Restricciones -------------------
*                     *** PARTE ASERRADERO ***
*determina la cantidad de troncos por producto
ProduccionVSInventario(l,d)..     sum((p)$(pfac (p)), NumTroncoProceAs(p,d,l)*Rel_DP(p,d) ) =e= TroncosNuevos(l,d) ;
*cantidad de troncos utilizado no debe superar la disponibilidad de troncos
ContarTroncoNuevo(l,d)..          TroncosNuevos(l,d)                  =l= InvTroncoNuevo(l,d);
*determina la cantidad de TI que se generaan usando un PC
GeneradorProductosTabla(l,i)..   sum((p,d)$(pfac (p)), NumTroncoProceAs(p,d,l)*RelacionTAyTronco(d,p,i)*Rel_DP(p,d) ) =e= NumTA(l,i) ;
*determina la cantidad de tablas intermedias que envian a remanufactura
VentaVSDisponibilidad(l,i)..     VentaTA(l,i) =l= NumTA(l,i);
*inventario de tablas intermedias
InveTA(l,i)..                    InvTA(l,i) =e= NumTA(l,i) - VentaTA(l,i);
*determina los PC que se utilizan
ContarSetup(p)$(pfac (p))..       sum((l,d), NumTroncoProceAs(p,d,l)*Rel_DP(p,d) ) =l= BigM*BinSiAplES(p) ;
*Calcula las perdidas totales
Pedidatotal..                     sum((p,d,l)$(pfac (p)),(1-Rendi(p,d))*NumTroncoProceAs(p,d,l)*VolumenTronco(l,d)*Rel_DP(p,d) ) =e= Perdidas;
*volumen utilizado
Volumentotal..                    sum((p,d,l)$(pfac (p)),Rendi(p,d)*NumTroncoProceAs(p,d,l)*VolumenTronco(l,d)*Rel_DP(p,d) ) =e= Rendimiento;

***Calculos de tiempos de proceso
*tiempo que se emplea por lote de troncos de diametro d y largo l con PC p
TiempoProceso(p,l,d)$(pfac (p) and Rel_DP(p,d) eq 1 and RDL(d,l)).. NumTroncoProceAs(p,d,l)*(1/TiempProcTronco(d,p,l)) =e= TiemProcLoteTronco(p,d,l);
*tiempo utilizado por los cambios de PC
SumaTiempoSetup..                 sum((p)$(pfac (p)), BinSiAplES(p)*TSetup)                 =e= TiempoSetupTOTALAse;
*Tiempo utilizado no puede superar al disponible
TiempoProcesoYSetup..             sum((p)$(pfac (p)), BinSiAplES(p)*TSetup) + sum((p,d,l)$(pfac (p)and Rel_DP(p,d) eq 1 and RDL(d,l)), TiemProcLoteTronco(p,d,l)) =l= TMaxOperacionAse;
*tiempo total utilizado (proceso+setup)
Tusado..                          sum((p)$(pfac (p)), BinSiAplES(p)*TSetup) + sum((p,d,l)$(pfac (p)and Rel_DP(p,d) eq 1 and RDL(d,l)), TiemProcLoteTronco(p,d,l)) =e= TTotal;
*cantidad de Pc utilizados
Eusado..                          sum((p)$(pfac (p)), BinSiAplES(p)) =e= ETotal;


*                     *** PARTE REMANUFACTURA ***
*determina la cantidad de tablas intermedias que se cortaran con el PC secundario
ProduccioRemanufactura(l,i)..    sum(s, TR(s,l,i)) =e= VentaTA(l,i);
*cantida de productos finales que se generan con el PC sec
GeneradorProductosFinales(f).. sum((i,s,l), TR(s,l,i)*RelacionTAyTF(s,i,l,f)) =e= TOTALProdTF(f) ;
*cantidad de productos finales que se utilizan para cubrir la demanda
VentaVSDispProdFinales(f)..    VentaTF(f) =l= TOTALProdTF(f) ;
*satisfacer la demanda
SatisfDemandaProdFinales(f)..  VentaTF(f) + DemInsatisfTF(f) =e= DemTF(f) ;
*satisfacer la demanda
*Demandamin(f)..                VentaTF(f) =g= DemTFmin(f)  ;
Demandamin(f)..                VentaTF(f) =g= DemTFmin(f)  ;
*total tablas demandadas no entregadas
DemIns..                          DInsTotal =e= sum((f),(DemTF(f) - VentaTF(f)));
*total de tablas finales producidas
Tproduccion..                     sum((f),TOTALProdTF(f)) =e= PTotal;

*inventario de tablas finales por tabla
InveTF(f)..                    InvTF(f) =e= TOTALProdTF(f) - VentaTF(f);
*inventario total de tablas finales
InvTotal..                        sum((f),InvTF(f)) =e= ITTF;

VentaTF.up(f) = DemTF(f);

*hace cero aquelos troncos que tengan relacion entre diametro y largo
NumTroncoProceAs.fx(p,d,l)$(not RDL(d,l))=0;
TroncosNuevos.fx(l,d)$(not RDL(d,l))=0;

*Hace cero a los productos intermedios de largo l que no se fabrican
*busca que no se utilicen PC corte en troncos de largo l y que generen prod intermedios que no se consideran para ese largo
VentaTA.fx(l,i)$(not RIL(i,l))=0;
TR.fx(s,l,i)$(not RIL(i,l))=0;
NumTA.fx(l,i)$(not RIL(i,l))=0;



model aserradero
/FuncionObjetivo
Fo2
Fo3
Fo4
auxAs2
auxAs3
auxAs4
auxRem1
auxRem2
ProduccionVSInventario
ContarTroncoNuevo
GeneradorProductosTabla
VentaVSDisponibilidad
InveTA
ContarSetup
Pedidatotal
Volumentotal
TiempoProceso
SumaTiempoSetup
TiempoProcesoYSetup
ProduccioRemanufactura
GeneradorProductosFinales
VentaVSDispProdFinales
SatisfDemandaProdFinales
Demandamin
DemIns
Tproduccion
InveTF
InvTotal
Tusado
Eusado

/;

option optcr = 0.0000 ;
OPTION OPTCA   = 0.0000 ;
*******************************************************************************
*Si se modifica este valor modificar al final el parametro tiempomax
option reslim = 100 ;
******************************************************************************
option mip = cplex ;
Option Limrow=200 ;


If(FO eq 1,
 solve aserradero using MIP max z_Objetivo1;
);
If(FO eq 2,
 solve aserradero using MIP min z_Objetivo2;
);
If(FO eq 3,
 solve aserradero using MIP min z_Objetivo3;
);
If(FO eq 4,
 solve aserradero using MIP min z_Objetivo4;
);

************************ chequeo de infactibilidad  *****************************
*por disponibilidad de troncos
model aserradero2
/FuncionObjetivo
Fo2
Fo3
Fo4
auxAs2
auxAs3
auxAs4
auxRem1
ProduccionVSInventario
GeneradorProductosTabla
VentaVSDisponibilidad
InveTA
ContarSetup
Pedidatotal
Volumentotal
TiempoProceso
SumaTiempoSetup
ProduccioRemanufactura
GeneradorProductosFinales
VentaVSDispProdFinales
SatisfDemandaProdFinales
DemIns
Tproduccion
InveTF
InvTotal
Tusado
Eusado
Demandamin
TiempoProcesoYSetup
/;



File Infactibilidades2 Busca causas de infactibilidades /Infactibilidades2.txt/;
if(aserradero.MODELSTAT EQ 10,
put Infactibilidades2;
 If(FO eq 1,
  solve aserradero2 using MIP max z_Objetivo1;
 );
 If(FO eq 2,
  solve aserradero2 using MIP min z_Objetivo2;
 );
 If(FO eq 3,
  solve aserradero2 using MIP min z_Objetivo3;
 );
 If(FO eq 4,
  solve aserradero2 using MIP min z_Objetivo4;
 );



 if((aserradero2.modelstat eq 1) or (aserradero2.modelstat eq 8) ,
*quiere decir que sacando la restricciÃ³n disponibilidad de troncos da factible

 put /;
 put ' La disponibilidad de troncos dada es insuficiente para cumplir con la demanda minima ' /;
 put /;
 put ' Debe:  '/;
 put '       ** aumentar el numero de troncos de ciertos diametros, o' /;
 put '       ** disminuir la demanda minima, o '/;
 put '       ** incrementar el numero de horas de trabajo '/;



execute_unload "%resultadosgdx%" DiametroOrig RendiLimiteRED LargoOrig Dim_productoOrig patronTOTAL RelacionTAyTF InvTroncoNuevo CosTonelada Tablas TSetupmin TMaxOperacionAse CosVarAse TiempProcAs ajuste LimTablas

********************************************************************************
****                             DATOS                                       ***
********************************************************************************

 If(UnidadD = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [cm]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [mm]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [m]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [pie]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [pulgada]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LimTablas rng=D-Diametro!H2  text="Maxima Cantidad de Tablas por Seccion" rng=D-Diametro!i1'


 If(UnidadL = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo[cm]" rng=D-Largo!b1'
 );
 If(UnidadL = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [mm]" rng=D-Largo!b1'
 );
 If(UnidadL = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [m]" rng=D-Largo!b1'
 );
 If(UnidadL = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [pie]" rng=D-Largo!b1'
 );
 If(UnidadL = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [pulgada]" rng=D-Largo!b1'
 );


 If(UnidadE = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2   text="Dimension [cm]" rng=D-Productos!c1  text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2 text="Dimension [mm]" rng=D-Productos!c1 stext="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2 text="Dimension [m]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2  text="Dimension [pie]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2  text="Dimension [pulgada]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );


execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=patronTOTAL rng=D-PC-primario!b2  text="Patrones de corte Primarios" rng=D-PC-primario!c1 par=RelacionTAyTF rng=D-PC-secundarios!b2  text="Patrones de corte Secundarios" rng=D-PC-secundarios!c1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=InvTroncoNuevo rng=D-MP-disponible!b2  text="Rollos Disponibles" rng=D-MP-disponible!b1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=CosTonelada rng=D-CostoTroncos!b2 dim=1 rdim=1 text="Precio por tonelada de tronco [$/t]" rng=D-CostoTroncos!b1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Demanda de tablas" rng=D-TablasFinales!c1 par=Tablas rng=D-TablasFinales!b2'

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Tiempo de Setup [min]" rng=D-TiemposCostos!a3 par=TSetupmin rng=D-TiemposCostos!c3 text="Tiempo de Turno [hora]" rng=D-TiemposCostos!a4 par=TMaxOperacionAse rng=D-TiemposCostos!c4 text="Costo operacion [$/hora]" rng=D-TiemposCostos!a7 par=CosVarAse rng=D-TiemposCostos!c7'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Rollos procesados por minuto" rng=D-TiempoProceso!A1 par=TiempProcAs rng=D-TiempoProceso!b2'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Ajuste de tiempo de proceso por cantidad de tablas obtenidas por PC" rng=D-CorreccionTiempo!A1 par=ajuste rng=D-CorreccionTiempo!b2'


********************************************************************************
****                             RESUMEN                                     ***
********************************************************************************



execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FECHA DE EJECUCION: %system.date%" rng=Resumen!a1 text="HORA DE EJECUCION: %system.time%" rng=Resumen!e1 text="ESTADO FINAL DE LA RESOLUCION:" rng=Resumen!a3'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="3" rng=Resumen!d3 text="INFACTIBLE" rng=Resumen!e3';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="La disponibilidad de troncos dada es insuficiente para cumplir con la demanda minima" rng=Resumen!b6 text=" Debe:" rng=Resumen!b8  text="** aumentar el numero de troncos de ciertos diametros, o" rng=Resumen!c10 text="** disminuir la demanda minima, o " rng=Resumen!c11 text="** incrementar el numero de horas de trabajo  " rng=Resumen!c12'

If(FO eq 1,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Maximizar Beneficios" rng=Resumen!C5'
);
If(FO eq 2,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Costos" rng=Resumen!C5'
);
If(FO eq 3,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Perdidas" rng=Resumen!C5'
);
If(FO eq 4,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Demanda Insatisfecha" rng=Resumen!C5'
);



 abort ' El problema es infactible pero se detecto la infactibilidad ';
 );
);

**************************************************************************************
*por demanda minima grande
model aserradero3
/FuncionObjetivo
Fo2
Fo3
Fo4
auxAs2
auxAs3
auxAs4
auxRem1
ProduccionVSInventario
GeneradorProductosTabla
VentaVSDisponibilidad
InveTA
ContarSetup
Pedidatotal
Volumentotal
TiempoProceso
SumaTiempoSetup
ProduccioRemanufactura
GeneradorProductosFinales
VentaVSDispProdFinales
SatisfDemandaProdFinales
DemIns
Tproduccion
InveTF
InvTotal
Tusado
Eusado
ContarTroncoNuevo
TiempoProcesoYSetup

/;



File Infactibilidades3 Busca causas de infactibilidades /Infactibilidades3.txt/;
if(aserradero.MODELSTAT EQ 10,
put Infactibilidades3;
 If(FO eq 1,
  solve aserradero3 using MIP max z_Objetivo1;
 );
 If(FO eq 2,
  solve aserradero3 using MIP min z_Objetivo2;
 );
 If(FO eq 3,
  solve aserradero3 using MIP min z_Objetivo3;
 );
 If(FO eq 4,
  solve aserradero3 using MIP min z_Objetivo4;
 );




 if((aserradero3.modelstat eq 1) or (aserradero3.modelstat eq 8) ,
 put /;
 put ' La demanda minima no puede cumplirse en el tiempo de operacion fijado' ; put /;
 put /;
 put ' Debe:  '/;
 put '       ** disminuir la demanda minima, o '/;
 put '       ** incrementar el numero de horas de trabajo '/;

execute_unload "%resultadosgdx%" DiametroOrig RendiLimiteRED LargoOrig Dim_productoOrig patronTOTAL RelacionTAyTF InvTroncoNuevo CosTonelada Tablas TSetupmin TMaxOperacionAse CosVarAse TiempProcAs ajuste LimTablas

********************************************************************************
****                             DATOS                                       ***
********************************************************************************

 If(UnidadD = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [cm]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [mm]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [m]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [pie]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [pulgada]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LimTablas rng=D-Diametro!H2  text="Maxima Cantidad de Tablas por Seccion" rng=D-Diametro!i1'


 If(UnidadL = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo[cm]" rng=D-Largo!b1'
 );
 If(UnidadL = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [mm]" rng=D-Largo!b1'
 );
 If(UnidadL = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [m]" rng=D-Largo!b1'
 );
 If(UnidadL = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [pie]" rng=D-Largo!b1'
 );
 If(UnidadL = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [pulgada]" rng=D-Largo!b1'
 );


 If(UnidadE = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2   text="Dimension [cm]" rng=D-Productos!c1  text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2 text="Dimension [mm]" rng=D-Productos!c1 stext="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2 text="Dimension [m]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2  text="Dimension [pie]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2  text="Dimension [pulgada]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );


execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=patronTOTAL rng=D-PC-primario!b2  text="Patrones de corte Primarios" rng=D-PC-primario!c1 par=RelacionTAyTF rng=D-PC-secundarios!b2  text="Patrones de corte Secundarios" rng=D-PC-secundarios!c1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=InvTroncoNuevo rng=D-MP-disponible!b2  text="Rollos Disponibles" rng=D-MP-disponible!b1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=CosTonelada rng=D-CostoTroncos!b2 dim=1 rdim=1 text="Precio por tonelada de tronco [$/t]" rng=D-CostoTroncos!b1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Demanda de tablas" rng=D-TablasFinales!c1 par=Tablas rng=D-TablasFinales!b2'

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Tiempo de Setup [min]" rng=D-TiemposCostos!a3 par=TSetupmin rng=D-TiemposCostos!c3 text="Tiempo de Turno [hora]" rng=D-TiemposCostos!a4 par=TMaxOperacionAse rng=D-TiemposCostos!c4 text="Costo operacion [$/hora]" rng=D-TiemposCostos!a7 par=CosVarAse rng=D-TiemposCostos!c7'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Rollos procesados por minuto" rng=D-TiempoProceso!A1 par=TiempProcAs rng=D-TiempoProceso!b2'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Ajuste de tiempo de proceso por cantidad de tablas obtenidas por PC" rng=D-CorreccionTiempo!A1 par=ajuste rng=D-CorreccionTiempo!b2'


********************************************************************************
****                             RESUMEN                                     ***
********************************************************************************



execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FECHA DE EJECUCION: %system.date%" rng=Resumen!a1 text="HORA DE EJECUCION: %system.time%" rng=Resumen!e1 text="ESTADO FINAL DE LA RESOLUCION:" rng=Resumen!a3'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="4" rng=Resumen!d3 text="INFACTIBLE" rng=Resumen!e3';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text=" La demanda minima no puede cumplirse en el tiempo de operacion fijado" rng=Resumen!b6 text=" Debe:" rng=Resumen!b8 text="** disminuir la demanda minima, o " rng=Resumen!c11 text="** incrementar el numero de horas de trabajo  " rng=Resumen!c12'

If(FO eq 1,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Maximizar Beneficios" rng=Resumen!C5'
);
If(FO eq 2,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Costos" rng=Resumen!C5'
);
If(FO eq 3,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Perdidas" rng=Resumen!C5'
);
If(FO eq 4,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Demanda Insatisfecha" rng=Resumen!C5'
);

 abort ' El problema es infactible pero se detecto la infactibilidad ';
 );
);


**************************************************************************************
*por abastecimiento insuficiente de troncos
model aserradero4
/FuncionObjetivo
Fo2
Fo3
Fo4
auxAs2
auxAs3
auxAs4
auxRem1
ProduccionVSInventario
GeneradorProductosTabla
VentaVSDisponibilidad
InveTA
ContarSetup
Pedidatotal
Volumentotal
TiempoProceso
SumaTiempoSetup
ProduccioRemanufactura
GeneradorProductosFinales
VentaVSDispProdFinales
SatisfDemandaProdFinales
DemIns
Tproduccion
InveTF
InvTotal
Tusado
Eusado
ContarTroncoNuevo
Demandamin

/;



File Infactibilidades4 Busca causas de infactibilidades /Infactibilidades4.txt/;
if(aserradero.MODELSTAT EQ 10,
put Infactibilidades4;
 If(FO eq 1,
  solve aserradero4 using MIP max z_Objetivo1;
 );
 If(FO eq 2,
  solve aserradero4 using MIP min z_Objetivo2;
 );
 If(FO eq 3,
  solve aserradero4 using MIP min z_Objetivo3;
 );
 If(FO eq 4,
  solve aserradero4 using MIP min z_Objetivo4;
 );



 if((aserradero4.modelstat eq 1) or (aserradero4.modelstat eq 8) ,
 put /;
 put ' El tiempo de operacion no es suficiente para satisfacer la demanda minima ' ; put /;
 put /;
 put ' Debe:  '/;
 put '       ** disminuir la demanda minima, o '/;
 put '       ** incrementar el numero de horas de trabajo '/;


execute_unload "%resultadosgdx%"  DiametroOrig RendiLimiteRED LargoOrig Dim_productoOrig patronTOTAL RelacionTAyTF InvTroncoNuevo CosTonelada Tablas TSetupmin TMaxOperacionAse CosVarAse TiempProcAs ajuste LimTablas

********************************************************************************
****                             DATOS                                       ***
********************************************************************************

 If(UnidadD = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [cm]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [mm]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [m]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [pie]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [pulgada]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LimTablas rng=D-Diametro!H2  text="Maxima Cantidad de Tablas por Seccion" rng=D-Diametro!i1'


 If(UnidadL = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo[cm]" rng=D-Largo!b1'
 );
 If(UnidadL = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [mm]" rng=D-Largo!b1'
 );
 If(UnidadL = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [m]" rng=D-Largo!b1'
 );
 If(UnidadL = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [pie]" rng=D-Largo!b1'
 );
 If(UnidadL = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [pulgada]" rng=D-Largo!b1'
 );


 If(UnidadE = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2   text="Dimension [cm]" rng=D-Productos!c1  text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2 text="Dimension [mm]" rng=D-Productos!c1 stext="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2 text="Dimension [m]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2  text="Dimension [pie]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2  text="Dimension [pulgada]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );


execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=patronTOTAL rng=D-PC-primario!b2  text="Patrones de corte Primarios" rng=D-PC-primario!c1 par=RelacionTAyTF rng=D-PC-secundarios!b2  text="Patrones de corte Secundarios" rng=D-PC-secundarios!c1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=InvTroncoNuevo rng=D-MP-disponible!b2  text="Rollos Disponibles" rng=D-MP-disponible!b1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=CosTonelada rng=D-CostoTroncos!b2 dim=1 rdim=1 text="Precio por tonelada de tronco [$/t]" rng=D-CostoTroncos!b1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Demanda de tablas" rng=D-TablasFinales!c1 par=Tablas rng=D-TablasFinales!b2'

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Tiempo de Setup [min]" rng=D-TiemposCostos!a3 par=TSetupmin rng=D-TiemposCostos!c3 text="Tiempo de Turno [hora]" rng=D-TiemposCostos!a4 par=TMaxOperacionAse rng=D-TiemposCostos!c4 text="Costo operacion [$/hora]" rng=D-TiemposCostos!a7 par=CosVarAse rng=D-TiemposCostos!c7'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Rollos procesados por minuto" rng=D-TiempoProceso!A1 par=TiempProcAs rng=D-TiempoProceso!b2'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Ajuste de tiempo de proceso por cantidad de tablas obtenidas por PC" rng=D-CorreccionTiempo!A1 par=ajuste rng=D-CorreccionTiempo!b2'


********************************************************************************
****                             RESUMEN                                     ***
********************************************************************************

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FECHA DE EJECUCION: %system.date%" rng=Resumen!a1 text="HORA DE EJECUCION: %system.time%" rng=Resumen!e1 text="ESTADO FINAL DE LA RESOLUCION:" rng=Resumen!a3'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="5" rng=Resumen!d3 text="INFACTIBLE" rng=Resumen!e3';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text=" La demanda minima no puede cumplirse en el tiempo de operacion fijado" rng=Resumen!b6 text=" Debe:" rng=Resumen!b8 text="** disminuir la demanda minima, o " rng=Resumen!c11 text="** incrementar el numero de horas de trabajo  " rng=Resumen!c12'

If(FO eq 1,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Maximizar Beneficios" rng=Resumen!C5'
);
If(FO eq 2,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Costos" rng=Resumen!C5'
);
If(FO eq 3,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Perdidas" rng=Resumen!C5'
);
If(FO eq 4,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Demanda Insatisfecha" rng=Resumen!C5'
);

 abort ' El problema es infactible pero se detectÃ³ la infactibilidad ';
 );
);
*******************fin chequeo de infactibilidad  **************************




parameters
Estado, tiempo, GAPrel, tiempomax, TUsados,IntTotal, FinalTotal, TPCpri, TPCsec, Psec(s), PorDemIns,  Renporc,TrocUsados, Tsobrante, NumTA_Ent, VentaTA_ent, InvTA_ent, patron, VentaTF_ent, InvTF_ent, DemInsat_ent, ProdF_ent, PCsecundario , z_Objetivo1RED, IngBrutoTOTALRemaRED, CosVarTOTALAseRED, CosTroncoTOTALAseRED
;
Estado=aserradero.SOLVESTAT;
tiempo = aserradero.RESUSD;
GAPrel = abs(aserradero.objest - aserradero.objval)/abs(aserradero.objest);
tiempomax = 100;
TUsados =  sum((l,d), floor(TroncosNuevos.l(l,d)));
IntTotal = sum((l,i), floor(NumTA.l(l,i)));
FinalTotal = sum((f), floor(TOTALProdTF.l(f)));
TPCpri = sum(p, BinSiAplES.l(p));
Loop((s,l,i),
 If(TR.l(s,l,i) ne 0,
  Psec(s) = 1;
 );
);
TPCsec = sum(s,Psec(s));
PorDemIns  = round(100*DInsTotal.l/sum((f),DemTF(f)),2) ;
Renporc =round(100* sum((l,i)$(RIL(i,l)),(NumTA.l(l,i)*Area_Pmetro(i)*Largometro(l)))/(sum((l,d), (TroncosNuevos.l(l,d)*VolumenTronco(l,d)))),2) ;


TrocUsados(p,d,l) = ceil(NumTroncoProceAs.l(p,d,l));
Tsobrante(d,l)= InvTroncoNuevo(l,d) - floor(TroncosNuevos.l(l,d));

NumTA_Ent(i,l)= floor(NumTA.l(l,i));
VentaTA_ent(i,l) = floor(VentaTA.l(l,i));
InvTA_ent(i,l)= floor(InvTA.l(l,i))  ;




loop((d,p),
If(BinSiAplES.l(p) eq 1,
 loop((i),
  patron(d,p,i) = RelacionTAyTronco(d,p,i);
 );
  patron(d,p,'rendimiento') = round(100*Rendi(p,d),2);

););

VentaTF_ent(f)=floor(VentaTF.l(f));
InvTF_ent(f)=floor(InvTF.l(f)) ;
DemInsat_ent(f)=ceil(DemInsatisfTF.l(f)) ;
ProdF_ent(f)=floor(TOTALProdTF.l(f)) ;
loop((s,i,l,f),
 If(TR.l(s,l,i) > 0,
  PCsecundario(s,i,l,f)=RelacionTAyTF(s,i,l,f);
););

z_Objetivo1RED = round(z_Objetivo1.l);
IngBrutoTOTALRemaRED = round(IngBrutoTOTALRema.l);
CosVarTOTALAseRED = round(CosVarTOTALAse.l);
CosTroncoTOTALAseRED = round(CosTroncoTOTALAse.l);



*Se pretende volver a los valores cargados por el usuario


display PorDemIns, tiempomax, PorDemIns, patron, Dim_productoOrig ;
*$exit
*//////OUTPUT/////////OUTPUT//////OUTPUT///////OUTPUT///////OUTPUT/////OUTPUT///
display z_Objetivo1.l,z_Objetivo2.l,z_Objetivo3.l,z_Objetivo4.l, IngBrutoTOTALRema.l, CosVarTOTALAse.l, CosTroncoTOTALAse.l, CostoSetupTOTALAse.l
display TiempoSetupTOTALAse.l, NumTroncoProceAs.l, Perdidas.l
display VentaTA.l, TOTALProdTF.l, VentaTF.l, TR.l,  NumTA.l, InvTA.l, InvTF.l
display TiemProcLoteTronco.l, DemInsatisfTF.l, BinSiAplES.l, tiempo
display TroncosNuevos.l, TTotal.l, ETotal.l
display NumTroncoProceAs.l


execute_unload "%resultadosgdx%"  DiametroMEtro  RendiLimiteRED Largo Dim_producto RIL RelacionTAyTronco  RelacionTAyTF  CosTonelada tiempo  InvTroncoNuevo DemTF  DemTFmin TUsados TTotal.l IntTotal FinalTotal TPCpri TPCsec PorDemIns z_Objetivo1RED IngBrutoTOTALRemaRED CosTroncoTOTALAseRED CosVarTOTALAseRED  Renporc TrocUsados Tsobrante NumTA_Ent VentaTA_ent InvTA_ent PCsecundario  ProdF_ent  VentaTF_ent InvTF_ent DemInsat_ent patron  TiempProcAs ajuste TSetupmin TMaxOperacionAse CosVarAse  RendiLimite  CosTonelada pvTF  DiametroOrig LargoOrig Dim_productoOrig patronTOTAL Tablas LimTablas

********************************************************************************
****                             DATOS                                       ***
********************************************************************************

 If(UnidadD = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [cm]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [mm]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [m]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [pie]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
 If(UnidadD = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DiametroOrig rng=D-Diametro!b3 dim=1 rdim=1  text="Diametro Menor [pulgada]" rng=D-Diametro!b2 par=RendiLimiteRED rng=D-Diametro!e3 dim=1 rdim=1 text="Rendimiento Minimo [%]" rng=D-Diametro!e2'
 );
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LimTablas rng=D-Diametro!H2  text="Maxima Cantidad de Tablas por Seccion" rng=D-Diametro!i1'


 If(UnidadL = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo[cm]" rng=D-Largo!b1'
 );
 If(UnidadL = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [mm]" rng=D-Largo!b1'
 );
 If(UnidadL = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [m]" rng=D-Largo!b1'
 );
 If(UnidadL = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [pie]" rng=D-Largo!b1'
 );
 If(UnidadL = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=LargoOrig rng=D-Largo!b2 dim=1 rdim=1  text="Largo [pulgada]" rng=D-Largo!b1'
 );


 If(UnidadE = 1,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2   text="Dimension [cm]" rng=D-Productos!c1  text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 2,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2 text="Dimension [mm]" rng=D-Productos!c1 stext="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 3,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2 text="Dimension [m]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 4,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2  text="Dimension [pie]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );
 If(UnidadE = 5,
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Dim_productoOrig rng=D-Productos!b2  text="Dimension [pulgada]" rng=D-Productos!c1 text="Largos permitidos" rng=D-Productos!f1 text="Codigo" rng=D-Productos!b2'
 );


execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=patronTOTAL rng=D-PC-primario!b2  text="Patrones de corte Primarios" rng=D-PC-primario!c1 par=RelacionTAyTF rng=D-PC-secundarios!b2  text="Patrones de corte Secundarios" rng=D-PC-secundarios!c1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=InvTroncoNuevo rng=D-MP-disponible!b2  text="Rollos Disponibles" rng=D-MP-disponible!b1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=CosTonelada rng=D-CostoTroncos!b2 dim=1 rdim=1 text="Precio por tonelada de tronco [$/t]" rng=D-CostoTroncos!b1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Demanda de tablas" rng=D-TablasFinales!c1 par=Tablas rng=D-TablasFinales!b2'

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Tiempo de Setup [min]" rng=D-TiemposCostos!a3 par=TSetupmin rng=D-TiemposCostos!c3 text="Tiempo de Turno [hora]" rng=D-TiemposCostos!a4 par=TMaxOperacionAse rng=D-TiemposCostos!c4 text="Costo operacion [$/hora]" rng=D-TiemposCostos!a7 par=CosVarAse rng=D-TiemposCostos!c7'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Rollos procesados por minuto" rng=D-TiempoProceso!A1 par=TiempProcAs rng=D-TiempoProceso!b2'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Ajuste de tiempo de proceso por cantidad de tablas obtenidas por PC" rng=D-CorreccionTiempo!A1 par=ajuste rng=D-CorreccionTiempo!b2'


********************************************************************************
****                             RESUMEN                                     ***
********************************************************************************


execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="TIEMPO DE EJECUCION:" rng=Resumen!a6 par=tiempo rng=Resumen!c6 text="seg" rng=Resumen!d6'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FECHA DE EJECUCION: %system.date%" rng=Resumen!a1 text="HORA DE EJECUCION: %system.time%" rng=Resumen!e1 text="ESTADO FINAL DE LA RESOLUCION:" rng=Resumen!a3'
If( tiempo < tiempomax,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="1" rng=Resumen!d3 text="Optimo" rng=Resumen!e3';
Else
  execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="2" rng=Resumen!d3 text="Suboptimo - Falta de tiempo de calculo" rng=Resumen!e3';
);

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Ingresos Bruto:" rng=Resumen!a8 par=IngBrutoTOTALRemaRED  rng=Resumen!c8 text="$" rng=Resumen!d8 text="Costo Rollos:" rng=Resumen!a9 par=CosTroncoTOTALAseRED  rng=Resumen!c9 text="$" rng=Resumen!d9 text="Costo Operativo:" rng=Resumen!a10 par=CosVarTOTALAseRED  rng=Resumen!c10 text="$" rng=Resumen!d10 text="Beneficios:" rng=Resumen!a11 par=z_Objetivo1RED  rng=Resumen!c11 text="$" rng=Resumen!d11';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Tiempo de Proceso:" rng=Resumen!a13 var=TTotal.l  rng=Resumen!c13 text="hs" rng=Resumen!d13';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Troncos Usados:" rng=Resumen!a14 par=TUsados rng=Resumen!c14';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Tablas Intermedias Total:" rng=Resumen!a15 par=IntTotal rng=Resumen!c15';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Total PC Primarios Usados:" rng=Resumen!a16 par=TPCpri rng=Resumen!c16';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Tablas Finales Total:" rng=Resumen!a17 par=FinalTotal rng=Resumen!c17';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Total PC Secundarios Usados:" rng=Resumen!a18 par=TPCsec rng=Resumen!c18';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Demanda Insafisfecha:" rng=Resumen!a19 par=PorDemIns rng=Resumen!c19 text="%" rng=Resumen!d19';
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="Rendimiento:" rng=Resumen!a20 par=Renporc rng=Resumen!c20 text="%" rng=Resumen!d20';


********************************************************************************
****                             RESULTADOs                                  ***
********************************************************************************

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=TrocUsados  rng=R-TroncosUsados!b2 '
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=Tsobrante  rng=R-TroncosSobrantes!b2 '

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=NumTA_Ent  rng=R-TablasIntermedias!b2 '
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=VentaTA_ent  rng=R-IntermediasARema!b2 '
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=InvTA_ent  rng=R-InventarioTablasIntermedias!b2 '

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=patron  rng=R-PCPrimarioUsado!b2 text="Estructura Patron de Corte"  rng=R-PCPrimarioUsado!e1'

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=ProdF_ent  rng=R-TablasFInales!b2 dim=1 rdim=1 text="Tablas Producidas"  rng=R-TablasFInales!b1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=VentaTF_ent  rng=R-TablasFInales!e2 dim=1 rdim=1 text="Tablas A entregar"  rng=R-TablasFInales!e1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=InvTF_ent  rng=R-TablasFInales!h2 dim=1 rdim=1 text="Inventario"  rng=R-TablasFInales!h1'
execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=DemInsat_ent  rng=R-TablasFInales!k2 dim=1 rdim=1 text="Demanda insatisfecha"  rng=R-TablasFInales!k1'

execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% par=PCsecundario  rng=R-PCSecundariosUsado!b2'




********************************************************************************
****                             RESUMEN                                     ***
********************************************************************************

If(FO eq 1,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Maximizar Beneficios" rng=Resumen!C5'
);
If(FO eq 2,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Costos" rng=Resumen!C5'
);
If(FO eq 3,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Perdidas" rng=Resumen!C5'
);
If(FO eq 4,
 execute 'gdxxrw.exe Input=%resultadosgdx% Output=%resultadosxlsx% text="FUNCION OBJETIVO" rng=Resumen!a5 text="Minimizar Demanda Insatisfecha" rng=Resumen!C5'
);

