package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.LongGroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LongGroup and its DTO LongGroupDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LongGroupMapper extends EntityMapper<LongGroupDTO, LongGroup> {



    default LongGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        LongGroup longGroup = new LongGroup();
        longGroup.setId(id);
        return longGroup;
    }
}
