package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ConfigProccessTimeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ConfigProccessTime and its DTO ConfigProccessTimeDTO.
 */
@Mapper(componentModel = "spring", uses = {DiameterGroupMapper.class, LongGroupMapper.class})
public interface ConfigProccessTimeMapper extends EntityMapper<ConfigProccessTimeDTO, ConfigProccessTime> {

    @Mapping(source = "diameterGroup.id", target = "diameterGroupId")
    @Mapping(source = "longGroup.id", target = "longGroupId")
    @Mapping(source = "diameterGroup.code", target = "diameterGroupCode")
    @Mapping(source = "longGroup.code", target = "longGroupCode")
    ConfigProccessTimeDTO toDto(ConfigProccessTime configProccessTime);

    @Mapping(source = "diameterGroupId", target = "diameterGroup")
    @Mapping(source = "longGroupId", target = "longGroup")
    ConfigProccessTime toEntity(ConfigProccessTimeDTO configProccessTimeDTO);

    default ConfigProccessTime fromId(Long id) {
        if (id == null) {
            return null;
        }
        ConfigProccessTime configProccessTime = new ConfigProccessTime();
        configProccessTime.setId(id);
        return configProccessTime;
    }
}
