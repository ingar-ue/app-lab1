package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ProductRelationsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ProductRelations and its DTO ProductRelationsDTO.
 */
@Mapper(componentModel = "spring", uses = {FinalProductMapper.class, InterProductMapper.class, LongGroupMapper.class})
public interface ProductRelationsMapper extends EntityMapper<ProductRelationsDTO, ProductRelations> {

    @Mapping(source = "finalProduct1.id", target = "finalProduct1Id")
    @Mapping(source = "finalProduct2.id", target = "finalProduct2Id")
    @Mapping(source = "finalProduct3.id", target = "finalProduct3Id")
    @Mapping(source = "interProduct.id", target = "interProductId")
    @Mapping(source = "longGroup.id", target = "longGroupId")
    @Mapping(source = "finalProduct1.code", target = "finalProduct1Code")
    @Mapping(source = "finalProduct2.code", target = "finalProduct2Code")
    @Mapping(source = "finalProduct3.code", target = "finalProduct3Code")
    @Mapping(source = "interProduct.code", target = "interProductCode")
    @Mapping(source = "longGroup.code", target = "longGroupCode")
    ProductRelationsDTO toDto(ProductRelations productRelations);

    @Mapping(source = "finalProduct1Id", target = "finalProduct1")
    @Mapping(source = "finalProduct2Id", target = "finalProduct2")
    @Mapping(source = "finalProduct3Id", target = "finalProduct3")
    @Mapping(source = "interProductId", target = "interProduct")
    @Mapping(source = "longGroupId", target = "longGroup")
    ProductRelations toEntity(ProductRelationsDTO productRelationsDTO);

    default ProductRelations fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProductRelations productRelations = new ProductRelations();
        productRelations.setId(id);
        return productRelations;
    }
}
