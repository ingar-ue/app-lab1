package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ConfigYieldPlanningDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ConfigYieldPlanning and its DTO ConfigYieldPlanningDTO.
 */
@Mapper(componentModel = "spring", uses = {DiameterGroupMapper.class})
public interface ConfigYieldPlanningMapper extends EntityMapper<ConfigYieldPlanningDTO, ConfigYieldPlanning> {

    @Mapping(source = "diameterGroup.id", target = "diameterGroupId")
    @Mapping(source = "diameterGroup.code", target = "diameterGroupCode")
    ConfigYieldPlanningDTO toDto(ConfigYieldPlanning configYieldPlanning);

    @Mapping(source = "diameterGroupId", target = "diameterGroup")
    ConfigYieldPlanning toEntity(ConfigYieldPlanningDTO configYieldPlanningDTO);

    default ConfigYieldPlanning fromId(Long id) {
        if (id == null) {
            return null;
        }
        ConfigYieldPlanning configYieldPlanning = new ConfigYieldPlanning();
        configYieldPlanning.setId(id);
        return configYieldPlanning;
    }
}
