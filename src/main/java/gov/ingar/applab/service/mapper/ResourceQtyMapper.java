package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ResourceQtyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ResourceQty and its DTO ResourceQtyDTO.
 */
@Mapper(componentModel = "spring", uses = {DiameterGroupMapper.class, LongGroupMapper.class})
public interface ResourceQtyMapper extends EntityMapper<ResourceQtyDTO, ResourceQty> {

    @Mapping(source = "diameterGroup.id", target = "diameterGroupId")
    @Mapping(source = "longGroup.id", target = "longGroupId")
    @Mapping(source = "diameterGroup.code", target = "diameterGroupCode")
    @Mapping(source = "longGroup.code", target = "longGroupCode")
    ResourceQtyDTO toDto(ResourceQty resourceQty);

    @Mapping(source = "diameterGroupId", target = "diameterGroup")
    @Mapping(source = "longGroupId", target = "longGroup")
    ResourceQty toEntity(ResourceQtyDTO resourceQtyDTO);

    default ResourceQty fromId(Long id) {
        if (id == null) {
            return null;
        }
        ResourceQty resourceQty = new ResourceQty();
        resourceQty.setId(id);
        return resourceQty;
    }
}
