package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ConfigTableCorrectionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ConfigTableCorrection and its DTO ConfigTableCorrectionDTO.
 */
@Mapper(componentModel = "spring", uses = {DiameterGroupMapper.class})
public interface ConfigTableCorrectionMapper extends EntityMapper<ConfigTableCorrectionDTO, ConfigTableCorrection> {

    @Mapping(source = "diameterGroup.id", target = "diameterGroupId")
    @Mapping(source = "diameterGroup.code", target = "diameterGroupCode")
    ConfigTableCorrectionDTO toDto(ConfigTableCorrection configTableCorrection);

    @Mapping(source = "diameterGroupId", target = "diameterGroup")
    ConfigTableCorrection toEntity(ConfigTableCorrectionDTO configTableCorrectionDTO);

    default ConfigTableCorrection fromId(Long id) {
        if (id == null) {
            return null;
        }
        ConfigTableCorrection configTableCorrection = new ConfigTableCorrection();
        configTableCorrection.setId(id);
        return configTableCorrection;
    }
}
