package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.PatternExclusionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PatternExclusion and its DTO PatternExclusionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PatternExclusionMapper extends EntityMapper<PatternExclusionDTO, PatternExclusion> {



    default PatternExclusion fromId(Long id) {
        if (id == null) {
            return null;
        }
        PatternExclusion patternExclusion = new PatternExclusion();
        patternExclusion.setId(id);
        return patternExclusion;
    }
}
