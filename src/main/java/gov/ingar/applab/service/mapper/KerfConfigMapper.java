package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.KerfConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity KerfConfig and its DTO KerfConfigDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KerfConfigMapper extends EntityMapper<KerfConfigDTO, KerfConfig> {



    default KerfConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        KerfConfig kerfConfig = new KerfConfig();
        kerfConfig.setId(id);
        return kerfConfig;
    }
}
