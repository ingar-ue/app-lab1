package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.DiameterGroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DiameterGroup and its DTO DiameterGroupDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DiameterGroupMapper extends EntityMapper<DiameterGroupDTO, DiameterGroup> {



    default DiameterGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        DiameterGroup diameterGroup = new DiameterGroup();
        diameterGroup.setId(id);
        return diameterGroup;
    }
}
