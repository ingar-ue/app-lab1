package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.FinalProductDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity FinalProduct and its DTO FinalProductDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FinalProductMapper extends EntityMapper<FinalProductDTO, FinalProduct> {



    default FinalProduct fromId(Long id) {
        if (id == null) {
            return null;
        }
        FinalProduct finalProduct = new FinalProduct();
        finalProduct.setId(id);
        return finalProduct;
    }
}
