package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.InterProductDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity InterProduct and its DTO InterProductDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InterProductMapper extends EntityMapper<InterProductDTO, InterProduct> {



    default InterProduct fromId(Long id) {
        if (id == null) {
            return null;
        }
        InterProduct interProduct = new InterProduct();
        interProduct.setId(id);
        return interProduct;
    }
}
