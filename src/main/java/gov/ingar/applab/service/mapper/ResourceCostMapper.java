package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ResourceCostDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ResourceCost and its DTO ResourceCostDTO.
 */
@Mapper(componentModel = "spring", uses = {DiameterGroupMapper.class})
public interface ResourceCostMapper extends EntityMapper<ResourceCostDTO, ResourceCost> {

    @Mapping(source = "diameterGroup.id", target = "diameterGroupId")
    @Mapping(source = "diameterGroup.code", target = "diameterGroupCode")
    ResourceCostDTO toDto(ResourceCost resourceCost);

    @Mapping(source = "diameterGroupId", target = "diameterGroup")
    ResourceCost toEntity(ResourceCostDTO resourceCostDTO);

    default ResourceCost fromId(Long id) {
        if (id == null) {
            return null;
        }
        ResourceCost resourceCost = new ResourceCost();
        resourceCost.setId(id);
        return resourceCost;
    }
}
