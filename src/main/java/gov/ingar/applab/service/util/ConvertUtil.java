package gov.ingar.applab.service.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ConvertUtil {

    public static final String TOKEN_DEF = ",";

    /**
     * Convert a long string separed by tokens to list
     * @param string aa,bb,cc
     * @return list of "aa","bb","cc"
     */
    public static List<String> strToList(String string) {
        List<String> ret = new ArrayList<>();
        if (StringUtils.isNotBlank(string)) {
            StringTokenizer tokenizer = new StringTokenizer(string, TOKEN_DEF);
            while (tokenizer.hasMoreTokens()) {
                String val = tokenizer.nextToken();
                ret.add(val);
            }
        }

        return ret;
    }

    public static String arrayToString(String[] array) {
        String result = "";
        if (array != null && array.length > 0) {
            result += array[0];
            for (int i = 1; i < array.length; i++) {
                result += TOKEN_DEF;
                result += array[i];
            }
        }
        return result;
    }

    /**
     * Convert a long string separed by tokens to list
     * @param string aa,bb,cc
     * @return list of "aa","bb","cc"
     */
    public static String[] strToArray(String string) {
        String[] result;
        List<String> list = strToList(string);
        if (list == null) {
            result = new String[0];
        } else {
            result = new String[list.size()];
            int i = 0;
            for (String s : list) {
                result[i++] = s;
            }
        }
        return result;
    }

    public static String listToString(List<String> list) {
        String result = "";
        if (list != null && list.size() > 0) {
            result += list.get(0);
            for (int i = 1; i < list.size(); i++) {
                result += TOKEN_DEF;
                result += list.get(i);
            }
        }
        return result;
    }

}
