package gov.ingar.applab.service;

import gov.ingar.applab.domain.DiameterGroup;
import gov.ingar.applab.repository.DiameterGroupRepository;
import gov.ingar.applab.service.dto.DiameterGroupDTO;
import gov.ingar.applab.service.mapper.DiameterGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing DiameterGroup.
 */
@Service
@Transactional
public class DiameterGroupService {

    private final Logger log = LoggerFactory.getLogger(DiameterGroupService.class);

    private final DiameterGroupRepository diameterGroupRepository;

    private final DiameterGroupMapper diameterGroupMapper;

    public DiameterGroupService(DiameterGroupRepository diameterGroupRepository, DiameterGroupMapper diameterGroupMapper) {
        this.diameterGroupRepository = diameterGroupRepository;
        this.diameterGroupMapper = diameterGroupMapper;
    }

    /**
     * Save a diameterGroup.
     *
     * @param diameterGroupDTO the entity to save
     * @return the persisted entity
     */
    public DiameterGroupDTO save(DiameterGroupDTO diameterGroupDTO) {
        log.debug("Request to save DiameterGroup : {}", diameterGroupDTO);
        DiameterGroup diameterGroup = diameterGroupMapper.toEntity(diameterGroupDTO);
        diameterGroup = diameterGroupRepository.save(diameterGroup);
        return diameterGroupMapper.toDto(diameterGroup);
    }

    /**
     * Get all the diameterGroups.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DiameterGroupDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DiameterGroups");
        return diameterGroupRepository.findAll(pageable)
            .map(diameterGroupMapper::toDto);
    }


    /**
     * Get one diameterGroup by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<DiameterGroupDTO> findOne(Long id) {
        log.debug("Request to get DiameterGroup : {}", id);
        return diameterGroupRepository.findById(id)
            .map(diameterGroupMapper::toDto);
    }

    /**
     * Delete the diameterGroup by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DiameterGroup : {}", id);
        diameterGroupRepository.deleteById(id);
    }
}
