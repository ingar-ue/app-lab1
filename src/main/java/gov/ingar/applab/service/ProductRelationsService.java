package gov.ingar.applab.service;

import gov.ingar.applab.domain.ProductRelations;
import gov.ingar.applab.repository.ProductRelationsRepository;
import gov.ingar.applab.service.dto.ProductRelationsDTO;
import gov.ingar.applab.service.mapper.ProductRelationsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;
/**
 * Service Implementation for managing ProductRelations.
 */
@Service
@Transactional
public class ProductRelationsService {

    private final Logger log = LoggerFactory.getLogger(ProductRelationsService.class);

    private final ProductRelationsRepository productRelationsRepository;

    private final ProductRelationsMapper productRelationsMapper;

    public ProductRelationsService(ProductRelationsRepository productRelationsRepository, ProductRelationsMapper productRelationsMapper) {
        this.productRelationsRepository = productRelationsRepository;
        this.productRelationsMapper = productRelationsMapper;
    }

    /**
     * Save a productRelations.
     *
     * @param productRelationsDTO the entity to save
     * @return the persisted entity
     */
    public ProductRelationsDTO save(ProductRelationsDTO productRelationsDTO) {
        log.debug("Request to save ProductRelations : {}", productRelationsDTO);
        ProductRelations productRelations = productRelationsMapper.toEntity(productRelationsDTO);
        productRelations = productRelationsRepository.save(productRelations);
        return productRelationsMapper.toDto(productRelations);
    }

    /**
     * Get all the productRelations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductRelationsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductRelations");
        return productRelationsRepository.findAll(pageable)
            .map(productRelationsMapper::toDto);
    }

    /**
     * Get all the productRelations filter by type containing keyword
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ProductRelationsDTO> findAllBy(Pageable pageable, String type, String keyword) {
        log.debug("Request to get all ProductRelations");
        if (!type.isEmpty() && !keyword.isEmpty()) {
            try {
                switch (type) {
                    case "interProduct":
                        return productRelationsRepository.findAllByInterProductCodeIgnoreCaseContaining(pageable, keyword)
                            .map(productRelationsMapper::toDto);
                    case "longGroup":
                        return productRelationsRepository.findAllByLongGroupCodeIgnoreCaseContaining(pageable, keyword)
                            .map(productRelationsMapper::toDto);
                    case "finalProduct1":
                        return productRelationsRepository.findAllByFinalProduct1CodeIgnoreCaseContaining(pageable, keyword)
                            .map(productRelationsMapper::toDto);
                    case "finalProduct2":
                        return productRelationsRepository.findAllByFinalProduct2CodeIgnoreCaseContaining(pageable, keyword)
                            .map(productRelationsMapper::toDto);
                    case "finalProduct3":
                        return productRelationsRepository.findAllByFinalProduct3CodeIgnoreCaseContaining(pageable, keyword)
                            .map(productRelationsMapper::toDto);
                    default:
                        return productRelationsRepository.findAll(pageable)
                            .map(productRelationsMapper::toDto);
                }
            } catch (NumberFormatException ex) {
                return new PageImpl<>(new ArrayList<>());
            }
        } else {
            return this.findAll(pageable);
        }
    }

    /**
     * Get one productRelations by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ProductRelationsDTO> findOne(Long id) {
        log.debug("Request to get ProductRelations : {}", id);
        return productRelationsRepository.findById(id)
            .map(productRelationsMapper::toDto);
    }

    /**
     * Delete the productRelations by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ProductRelations : {}", id);
        productRelationsRepository.deleteById(id);
    }
}
