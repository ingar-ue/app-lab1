package gov.ingar.applab.service;

import gov.ingar.applab.domain.FinalProduct;
import gov.ingar.applab.repository.FinalProductRepository;
import gov.ingar.applab.service.dto.FinalProductDTO;
import gov.ingar.applab.service.mapper.FinalProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

/**
 * Service Implementation for managing FinalProduct.
 */
@Service
@Transactional
public class FinalProductService {

    private final Logger log = LoggerFactory.getLogger(FinalProductService.class);

    private final FinalProductRepository finalProductRepository;

    private final FinalProductMapper finalProductMapper;

    public FinalProductService(FinalProductRepository finalProductRepository, FinalProductMapper finalProductMapper) {
        this.finalProductRepository = finalProductRepository;
        this.finalProductMapper = finalProductMapper;
    }

    /**
     * Save a finalProduct.
     *
     * @param finalProductDTO the entity to save
     * @return the persisted entity
     */
    public FinalProductDTO save(FinalProductDTO finalProductDTO) {
        log.debug("Request to save FinalProduct : {}", finalProductDTO);
        FinalProduct finalProduct = finalProductMapper.toEntity(finalProductDTO);
        finalProduct = finalProductRepository.save(finalProduct);
        return finalProductMapper.toDto(finalProduct);
    }

    /**
     * Get all the finalProducts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FinalProductDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FinalProducts");
        return finalProductRepository.findAll(pageable)
            .map(finalProductMapper::toDto);
    }

    /**
     * Get all the finalProducts filter by type containing keyword
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FinalProductDTO> findAllBy(Pageable pageable, String type, String keyword) {
        log.debug("Request to get all FinalProducts");
        if (!type.isEmpty() && !keyword.isEmpty()) {
            try {
                switch (type) {
                    case "code":
                        return finalProductRepository.findAllByCodeIgnoreCaseContaining(pageable, keyword)
                            .map(finalProductMapper::toDto);
                    case "name":
                        return finalProductRepository.findAllByNameIgnoreCaseContaining(pageable, keyword)
                            .map(finalProductMapper::toDto);
                    case "price":
                        return finalProductRepository.findAllByPrice(pageable, new BigDecimal(keyword.replaceAll(",", "")))
                            .map(finalProductMapper::toDto);
                    case "minDemand":
                        return finalProductRepository.findAllByMinDemand(pageable, new BigDecimal(keyword.replaceAll(",", "")))
                            .map(finalProductMapper::toDto);
                    case "maxDemand":
                        return finalProductRepository.findAllByMaxDemand(pageable, new BigDecimal(keyword.replaceAll(",", "")))
                            .map(finalProductMapper::toDto);
                    default:
                        return finalProductRepository.findAll(pageable)
                            .map(finalProductMapper::toDto);
                }
            } catch (NumberFormatException ex) {
                return new PageImpl<>(new ArrayList<>());
            }
        } else {
            return this.findAll(pageable);
        }
    }

    /**
     * Get one finalProduct by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<FinalProductDTO> findOne(Long id) {
        log.debug("Request to get FinalProduct : {}", id);
        return finalProductRepository.findById(id)
            .map(finalProductMapper::toDto);
    }

    /**
     * Delete the finalProduct by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete FinalProduct : {}", id);
        finalProductRepository.deleteById(id);
    }

    /**
     * Reset prices of all products
     */
    public void priceReset() {
        finalProductRepository.priceReset();
    }

    /**
     * Reset demand of all products
     */
    public void demandReset() {
        finalProductRepository.demandReset();
    }

    /**
     * Increase prices of all products
     *
     * @param percentage of increase
     */
    public void priceIncrease(Double percentage) {
        finalProductRepository.priceIncrease(BigDecimal.valueOf(1 + (percentage / 100)));
    }
}
