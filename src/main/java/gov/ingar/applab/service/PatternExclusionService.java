package gov.ingar.applab.service;

import gov.ingar.applab.domain.PatternExclusion;
import gov.ingar.applab.repository.PatternExclusionRepository;
import gov.ingar.applab.service.dto.PatternExclusionDTO;
import gov.ingar.applab.service.mapper.PatternExclusionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing PatternExclusion.
 */
@Service
@Transactional
public class PatternExclusionService {

    private final Logger log = LoggerFactory.getLogger(PatternExclusionService.class);

    private final PatternExclusionRepository patternExclusionRepository;

    private final PatternExclusionMapper patternExclusionMapper;

    public PatternExclusionService(PatternExclusionRepository patternExclusionRepository, PatternExclusionMapper patternExclusionMapper) {
        this.patternExclusionRepository = patternExclusionRepository;
        this.patternExclusionMapper = patternExclusionMapper;
    }

    /**
     * Save a patternExclusion.
     *
     * @param patternExclusionDTO the entity to save
     * @return the persisted entity
     */
    public PatternExclusionDTO save(PatternExclusionDTO patternExclusionDTO) {
        log.debug("Request to save PatternExclusion : {}", patternExclusionDTO);
        PatternExclusion patternExclusion = patternExclusionMapper.toEntity(patternExclusionDTO);
        patternExclusion = patternExclusionRepository.save(patternExclusion);
        return patternExclusionMapper.toDto(patternExclusion);
    }

    /**
     * Get all the patternExclusions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PatternExclusionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PatternExclusions");
        return patternExclusionRepository.findAll(pageable)
            .map(patternExclusionMapper::toDto);
    }


    /**
     * Get one patternExclusion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<PatternExclusionDTO> findOne(Long id) {
        log.debug("Request to get PatternExclusion : {}", id);
        return patternExclusionRepository.findById(id)
            .map(patternExclusionMapper::toDto);
    }

    /**
     * Delete the patternExclusion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PatternExclusion : {}", id);
        patternExclusionRepository.deleteById(id);
    }
}
