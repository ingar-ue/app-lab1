package gov.ingar.applab.service;

import gov.ingar.applab.domain.ConfigYieldPlanning;
import gov.ingar.applab.repository.ConfigYieldPlanningRepository;
import gov.ingar.applab.service.dto.ConfigYieldPlanningDTO;
import gov.ingar.applab.service.mapper.ConfigYieldPlanningMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing ConfigYieldPlanning.
 */
@Service
@Transactional
public class ConfigYieldPlanningService {

    private final Logger log = LoggerFactory.getLogger(ConfigYieldPlanningService.class);

    private final ConfigYieldPlanningRepository configYieldPlanningRepository;

    private final ConfigYieldPlanningMapper configYieldPlanningMapper;

    public ConfigYieldPlanningService(ConfigYieldPlanningRepository configYieldPlanningRepository, ConfigYieldPlanningMapper configYieldPlanningMapper) {
        this.configYieldPlanningRepository = configYieldPlanningRepository;
        this.configYieldPlanningMapper = configYieldPlanningMapper;
    }

    /**
     * Save a configYieldPlanning.
     *
     * @param configYieldPlanningDTO the entity to save
     * @return the persisted entity
     */
    public ConfigYieldPlanningDTO save(ConfigYieldPlanningDTO configYieldPlanningDTO) {
        log.debug("Request to save ConfigYieldPlanning : {}", configYieldPlanningDTO);
        ConfigYieldPlanning configYieldPlanning = configYieldPlanningMapper.toEntity(configYieldPlanningDTO);
        configYieldPlanning = configYieldPlanningRepository.save(configYieldPlanning);
        return configYieldPlanningMapper.toDto(configYieldPlanning);
    }

    /**
     * Get all the configYieldPlannings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ConfigYieldPlanningDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ConfigYieldPlannings");
        return configYieldPlanningRepository.findAll(pageable)
            .map(configYieldPlanningMapper::toDto);
    }


    /**
     * Get one configYieldPlanning by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ConfigYieldPlanningDTO> findOne(Long id) {
        log.debug("Request to get ConfigYieldPlanning : {}", id);
        return configYieldPlanningRepository.findById(id)
            .map(configYieldPlanningMapper::toDto);
    }

    /**
     * Delete the configYieldPlanning by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ConfigYieldPlanning : {}", id);
        configYieldPlanningRepository.deleteById(id);
    }
}
