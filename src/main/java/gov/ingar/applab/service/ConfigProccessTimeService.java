package gov.ingar.applab.service;

import gov.ingar.applab.domain.ConfigProccessTime;
import gov.ingar.applab.repository.ConfigProccessTimeRepository;
import gov.ingar.applab.service.dto.ConfigProccessTimeDTO;
import gov.ingar.applab.service.mapper.ConfigProccessTimeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ConfigProccessTime.
 */
@Service
@Transactional
public class ConfigProccessTimeService {

    private final Logger log = LoggerFactory.getLogger(ConfigProccessTimeService.class);

    private final ConfigProccessTimeRepository configProccessTimeRepository;

    private final ConfigProccessTimeMapper configProccessTimeMapper;

    public ConfigProccessTimeService(ConfigProccessTimeRepository configProccessTimeRepository, ConfigProccessTimeMapper configProccessTimeMapper) {
        this.configProccessTimeRepository = configProccessTimeRepository;
        this.configProccessTimeMapper = configProccessTimeMapper;
    }

    /**
     * Save a configProccessTime.
     *
     * @param configProccessTimeDTO the entity to save
     * @return the persisted entity
     */
    public ConfigProccessTimeDTO save(ConfigProccessTimeDTO configProccessTimeDTO) {
        log.debug("Request to save ConfigProccessTime : {}", configProccessTimeDTO);
        ConfigProccessTime configProccessTime = configProccessTimeMapper.toEntity(configProccessTimeDTO);
        configProccessTime = configProccessTimeRepository.save(configProccessTime);
        return configProccessTimeMapper.toDto(configProccessTime);
    }

    /**
     * Get all the configProccessTimes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ConfigProccessTimeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ConfigProccessTimes");
        return configProccessTimeRepository.findAll(pageable)
            .map(configProccessTimeMapper::toDto);
    }

    /**
     *
     * Get all the configProccessTimes.
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ConfigProccessTimeDTO> findAllList() {
        log.debug("Request to get all ConfigProccessTimes");
        return configProccessTimeRepository.findAll()
            .stream()
            .map(configProccessTimeMapper::toDto)
            .collect(Collectors.toList());
    }

    /**
     * Get one configProccessTime by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ConfigProccessTimeDTO> findOne(Long id) {
        log.debug("Request to get ConfigProccessTime : {}", id);
        return configProccessTimeRepository.findById(id)
            .map(configProccessTimeMapper::toDto);
    }

    /**
     * Delete the configProccessTime by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ConfigProccessTime : {}", id);
        configProccessTimeRepository.deleteById(id);
    }
}
