package gov.ingar.applab.service;

import gov.ingar.applab.domain.LongGroup;
import gov.ingar.applab.repository.LongGroupRepository;
import gov.ingar.applab.service.dto.LongGroupDTO;
import gov.ingar.applab.service.mapper.LongGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing LongGroup.
 */
@Service
@Transactional
public class LongGroupService {

    private final Logger log = LoggerFactory.getLogger(LongGroupService.class);

    private final LongGroupRepository longGroupRepository;

    private final LongGroupMapper longGroupMapper;

    public LongGroupService(LongGroupRepository longGroupRepository, LongGroupMapper longGroupMapper) {
        this.longGroupRepository = longGroupRepository;
        this.longGroupMapper = longGroupMapper;
    }

    /**
     * Save a longGroup.
     *
     * @param longGroupDTO the entity to save
     * @return the persisted entity
     */
    public LongGroupDTO save(LongGroupDTO longGroupDTO) {
        log.debug("Request to save LongGroup : {}", longGroupDTO);
        LongGroup longGroup = longGroupMapper.toEntity(longGroupDTO);
        longGroup = longGroupRepository.save(longGroup);
        return longGroupMapper.toDto(longGroup);
    }

    /**
     * Get all the longGroups.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<LongGroupDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LongGroups");
        return longGroupRepository.findAll(pageable)
            .map(longGroupMapper::toDto);
    }

    /**
     * Get all the longGroups.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<LongGroupDTO> findAll() {
        log.debug("Request to get all LongGroups");
        return longGroupMapper.toDto(longGroupRepository.findAllByOrderByCodeAsc());
    }

    /**
     * Get one longGroup by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<LongGroupDTO> findOne(Long id) {
        log.debug("Request to get LongGroup : {}", id);
        return longGroupRepository.findById(id)
            .map(longGroupMapper::toDto);
    }

    /**
     * Delete the longGroup by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete LongGroup : {}", id);
        longGroupRepository.deleteById(id);
    }

}
