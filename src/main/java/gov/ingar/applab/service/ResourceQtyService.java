package gov.ingar.applab.service;

import gov.ingar.applab.domain.ResourceQty;
import gov.ingar.applab.repository.ResourceQtyRepository;
import gov.ingar.applab.service.dto.ResourceQtyDTO;
import gov.ingar.applab.service.mapper.ResourceQtyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing ResourceQty.
 */
@Service
@Transactional
public class ResourceQtyService {

    private final Logger log = LoggerFactory.getLogger(ResourceQtyService.class);

    private final ResourceQtyRepository resourceQtyRepository;

    private final ResourceQtyMapper resourceQtyMapper;

    public ResourceQtyService(ResourceQtyRepository resourceQtyRepository, ResourceQtyMapper resourceQtyMapper) {
        this.resourceQtyRepository = resourceQtyRepository;
        this.resourceQtyMapper = resourceQtyMapper;
    }

    /**
     * Save a resourceQty.
     *
     * @param resourceQtyDTO the entity to save
     * @return the persisted entity
     */
    public ResourceQtyDTO save(ResourceQtyDTO resourceQtyDTO) {
        log.debug("Request to save ResourceQty : {}", resourceQtyDTO);
        ResourceQty resourceQty = resourceQtyMapper.toEntity(resourceQtyDTO);
        resourceQty = resourceQtyRepository.save(resourceQty);
        return resourceQtyMapper.toDto(resourceQty);
    }

    /**
     * Get all the resourceQties.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ResourceQtyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ResourceQties");
        return resourceQtyRepository.findAll(pageable)
            .map(resourceQtyMapper::toDto);
    }


    /**
     * Get one resourceQty by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ResourceQtyDTO> findOne(Long id) {
        log.debug("Request to get ResourceQty : {}", id);
        return resourceQtyRepository.findById(id)
            .map(resourceQtyMapper::toDto);
    }

    /**
     * Delete the resourceQty by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ResourceQty : {}", id);
        resourceQtyRepository.deleteById(id);
    }
}
