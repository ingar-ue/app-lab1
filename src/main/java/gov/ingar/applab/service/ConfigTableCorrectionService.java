package gov.ingar.applab.service;

import gov.ingar.applab.domain.ConfigTableCorrection;
import gov.ingar.applab.repository.ConfigTableCorrectionRepository;
import gov.ingar.applab.service.dto.ConfigTableCorrectionDTO;
import gov.ingar.applab.service.mapper.ConfigTableCorrectionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing ConfigTableCorrection.
 */
@Service
@Transactional
public class ConfigTableCorrectionService {

    private final Logger log = LoggerFactory.getLogger(ConfigTableCorrectionService.class);

    private final ConfigTableCorrectionRepository configTableCorrectionRepository;

    private final ConfigTableCorrectionMapper configTableCorrectionMapper;

    public ConfigTableCorrectionService(ConfigTableCorrectionRepository configTableCorrectionRepository, ConfigTableCorrectionMapper configTableCorrectionMapper) {
        this.configTableCorrectionRepository = configTableCorrectionRepository;
        this.configTableCorrectionMapper = configTableCorrectionMapper;
    }

    /**
     * Save a configTableCorrection.
     *
     * @param configTableCorrectionDTO the entity to save
     * @return the persisted entity
     */
    public ConfigTableCorrectionDTO save(ConfigTableCorrectionDTO configTableCorrectionDTO) {
        log.debug("Request to save ConfigTableCorrection : {}", configTableCorrectionDTO);
        ConfigTableCorrection configTableCorrection = configTableCorrectionMapper.toEntity(configTableCorrectionDTO);
        configTableCorrection = configTableCorrectionRepository.save(configTableCorrection);
        return configTableCorrectionMapper.toDto(configTableCorrection);
    }

    /**
     * Get all the configTableCorrections.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ConfigTableCorrectionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ConfigTableCorrections");
        return configTableCorrectionRepository.findAll(pageable)
            .map(configTableCorrectionMapper::toDto);
    }


    /**
     * Get one configTableCorrection by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ConfigTableCorrectionDTO> findOne(Long id) {
        log.debug("Request to get ConfigTableCorrection : {}", id);
        return configTableCorrectionRepository.findById(id)
            .map(configTableCorrectionMapper::toDto);
    }

    /**
     * Delete the configTableCorrection by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ConfigTableCorrection : {}", id);
        configTableCorrectionRepository.deleteById(id);
    }
}
