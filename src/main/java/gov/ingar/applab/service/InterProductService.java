package gov.ingar.applab.service;

import gov.ingar.applab.domain.InterProduct;
import gov.ingar.applab.repository.InterProductRepository;
import gov.ingar.applab.service.dto.InterProductDTO;
import gov.ingar.applab.service.mapper.InterProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Service Implementation for managing InterProduct.
 */
@Service
@Transactional
public class InterProductService {

    private final Logger log = LoggerFactory.getLogger(InterProductService.class);

    private final InterProductRepository interProductRepository;

    private final InterProductMapper interProductMapper;

    public InterProductService(InterProductRepository interProductRepository, InterProductMapper interProductMapper) {
        this.interProductRepository = interProductRepository;
        this.interProductMapper = interProductMapper;
    }

    /**
     * Save a interProduct.
     *
     * @param interProductDTO the entity to save
     * @return the persisted entity
     */
    public InterProductDTO save(InterProductDTO interProductDTO) {
        log.debug("Request to save InterProduct : {}", interProductDTO);
        InterProduct interProduct = interProductMapper.toEntity(interProductDTO);
        interProduct = interProductRepository.save(interProduct);
        return interProductMapper.toDto(interProduct);
    }

    /**
     * Get all the interProducts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InterProductDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InterProducts");
        return interProductRepository.findAll(pageable)
            .map(interProductMapper::toDto);
    }

    /**
     * Get all the interProducts filter by type containing keyword
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InterProductDTO> findAllBy(Pageable pageable, String type, String keyword) {
        log.debug("Request to get all InterProducts");
        if (!type.isEmpty() && !keyword.isEmpty()) {
            try {
                switch (type) {
                    case "code":
                        return interProductRepository.findAllByCodeIgnoreCaseContaining(pageable, keyword)
                            .map(interProductMapper::toDto);
                    case "longGroups":
                        return interProductRepository.findAllByLongGroupsIgnoreCaseContaining(pageable, keyword)
                            .map(interProductMapper::toDto);
                    case "tickness":
                        return interProductRepository.findAllByTickness(pageable, new BigDecimal(keyword.replaceAll(",", "")))
                            .map(interProductMapper::toDto);
                    case "width":
                        return interProductRepository.findAllByWidth(pageable, new BigDecimal(keyword.replaceAll(",", "")))
                            .map(interProductMapper::toDto);
                    default:
                        return interProductRepository.findAll(pageable)
                            .map(interProductMapper::toDto);
                }
            } catch (NumberFormatException ex) {
                return new PageImpl<>(new ArrayList<>());
            }
        } else {
            return this.findAll(pageable);
        }
    }

    /**
     * Get one interProduct by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<InterProductDTO> findOne(Long id) {
        log.debug("Request to get InterProduct : {}", id);
        return interProductRepository.findById(id)
            .map(interProductMapper::toDto);
    }

    /**
     * Delete the interProduct by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete InterProduct : {}", id);
        interProductRepository.deleteById(id);
    }
}
