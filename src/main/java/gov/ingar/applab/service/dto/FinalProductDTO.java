package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the FinalProduct entity.
 */
public class FinalProductDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 15)
    private String code;

    @NotNull
    private String name;

    @NotNull
    @Min(value = 0)
    private Integer minDemand;

    @NotNull
    @Min(value = 0)
    private Integer maxDemand;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinDemand() {
        return minDemand;
    }

    public void setMinDemand(Integer minDemand) {
        this.minDemand = minDemand;
    }

    public Integer getMaxDemand() {
        return maxDemand;
    }

    public void setMaxDemand(Integer maxDemand) {
        this.maxDemand = maxDemand;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FinalProductDTO finalProductDTO = (FinalProductDTO) o;
        if (finalProductDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), finalProductDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FinalProductDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", minDemand=" + getMinDemand() +
            ", maxDemand=" + getMaxDemand() +
            ", price=" + getPrice() +
            "}";
    }
}
