package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the ResourceCost entity.
 */
public class ResourceCostDTO implements Serializable {

    private Long id;

    @NotNull
    private BigDecimal value;

    private Long diameterGroupId;

    private String diameterGroupCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Long getDiameterGroupId() {
        return diameterGroupId;
    }

    public void setDiameterGroupId(Long diameterGroupId) {
        this.diameterGroupId = diameterGroupId;
    }

    public String getDiameterGroupCode() {
        return diameterGroupCode;
    }

    public void setDiameterGroupCode(String diameterGroupCode) {
        this.diameterGroupCode = diameterGroupCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ResourceCostDTO resourceCostDTO = (ResourceCostDTO) o;
        if (resourceCostDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), resourceCostDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ResourceCostDTO{" +
            "id=" + getId() +
            ", value=" + getValue() +
            ", diameterGroup=" + getDiameterGroupId() +
            "}";
    }
}
