package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ProductRelations entity.
 */
public class ProductRelationsDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer quantity1;

    private Integer quantity2;

    private Integer quantity3;

    private Long finalProduct1Id;

    private Long finalProduct2Id;

    private Long finalProduct3Id;

    private Long interProductId;

    private Long longGroupId;

    private String finalProduct1Code;

    private String finalProduct2Code;

    private String finalProduct3Code;

    private String interProductCode;

    private String longGroupCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity1() {
        return quantity1;
    }

    public void setQuantity1(Integer quantity1) {
        this.quantity1 = quantity1;
    }

    public Integer getQuantity2() {
        return quantity2;
    }

    public void setQuantity2(Integer quantity2) {
        this.quantity2 = quantity2;
    }

    public Integer getQuantity3() {
        return quantity3;
    }

    public void setQuantity3(Integer quantity3) {
        this.quantity3 = quantity3;
    }

    public Long getFinalProduct1Id() {
        return finalProduct1Id;
    }

    public void setFinalProduct1Id(Long finalProductId) {
        this.finalProduct1Id = finalProductId;
    }

    public Long getFinalProduct2Id() {
        return finalProduct2Id;
    }

    public void setFinalProduct2Id(Long finalProductId) {
        this.finalProduct2Id = finalProductId;
    }

    public Long getFinalProduct3Id() {
        return finalProduct3Id;
    }

    public void setFinalProduct3Id(Long finalProductId) {
        this.finalProduct3Id = finalProductId;
    }

    public Long getInterProductId() {
        return interProductId;
    }

    public void setInterProductId(Long interProductId) {
        this.interProductId = interProductId;
    }

    public Long getLongGroupId() {
        return longGroupId;
    }

    public void setLongGroupId(Long longGroupId) {
        this.longGroupId = longGroupId;
    }

    public String getFinalProduct1Code() {
        return finalProduct1Code;
    }

    public void setFinalProduct1Code(String finalProduct1Code) {
        this.finalProduct1Code = finalProduct1Code;
    }

    public String getFinalProduct2Code() {
        return finalProduct2Code;
    }

    public void setFinalProduct2Code(String finalProduct2Code) {
        this.finalProduct2Code = finalProduct2Code;
    }

    public String getFinalProduct3Code() {
        return finalProduct3Code;
    }

    public void setFinalProduct3Code(String finalProduct3Code) {
        this.finalProduct3Code = finalProduct3Code;
    }

    public String getInterProductCode() {
        return interProductCode;
    }

    public void setInterProductCode(String interProductCode) {
        this.interProductCode = interProductCode;
    }

    public String getLongGroupCode() {
        return longGroupCode;
    }

    public void setLongGroupCode(String longGroupCode) {
        this.longGroupCode = longGroupCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductRelationsDTO productRelationsDTO = (ProductRelationsDTO) o;
        if (productRelationsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), productRelationsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProductRelationsDTO{" +
            "id=" + getId() +
            ", quantity1=" + getQuantity1() +
            ", quantity2=" + getQuantity2() +
            ", quantity3=" + getQuantity3() +
            ", finalProduct1=" + getFinalProduct1Id() +
            ", finalProduct2=" + getFinalProduct2Id() +
            ", finalProduct3=" + getFinalProduct3Id() +
            ", interProduct=" + getInterProductId() +
            ", longGroup=" + getLongGroupId() +
            "}";
    }
}
