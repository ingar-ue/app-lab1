package gov.ingar.applab.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PatternExclusion entity.
 */
public class PatternExclusionDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    private LocalDate exclusionDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getExclusionDate() {
        return exclusionDate;
    }

    public void setExclusionDate(LocalDate exclusionDate) {
        this.exclusionDate = exclusionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PatternExclusionDTO patternExclusionDTO = (PatternExclusionDTO) o;
        if (patternExclusionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), patternExclusionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PatternExclusionDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", exclusionDate='" + getExclusionDate() + "'" +
            "}";
    }
}
