package gov.ingar.applab.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import gov.ingar.applab.domain.enumeration.Status;

/**
 * A DTO for the ModelRun entity.
 */
public class ModelRunDTO implements Serializable {

    private Long id;

    @NotNull
    private String modelName;

    @NotNull
    private Integer modelVersion;

    @NotNull
    private String modelObjFunction;

    private LocalDate runDate;

    private String startTime;

    private String endTime;

    private Status status;

    @Min(value = 0)
    @Max(value = 100)
    private Integer progress;

    private String dataSetInput;

    private String dataSetResult;

    private String comments;

    private String user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getModelVersion() {
        return modelVersion;
    }

    public void setModelVersion(Integer modelVersion) {
        this.modelVersion = modelVersion;
    }

    public String getModelObjFunction() {
        return modelObjFunction;
    }

    public void setModelObjFunction(String modelObjFunction) {
        this.modelObjFunction = modelObjFunction;
    }

    public LocalDate getRunDate() {
        return runDate;
    }

    public void setRunDate(LocalDate runDate) {
        this.runDate = runDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getDataSetInput() {
        return dataSetInput;
    }

    public void setDataSetInput(String dataSetInput) {
        this.dataSetInput = dataSetInput;
    }

    public String getDataSetResult() {
        return dataSetResult;
    }

    public void setDataSetResult(String dataSetResult) {
        this.dataSetResult = dataSetResult;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModelRunDTO modelRunDTO = (ModelRunDTO) o;
        if (modelRunDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelRunDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelRunDTO{" +
            "id=" + getId() +
            ", modelName='" + getModelName() + "'" +
            ", modelVersion=" + getModelVersion() +
            ", modelObjFunction='" + getModelObjFunction() + "'" +
            ", runDate='" + getRunDate() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", status='" + getStatus() + "'" +
            ", progress=" + getProgress() +
            ", dataSetInput='" + getDataSetInput() + "'" +
            ", dataSetResult='" + getDataSetResult() + "'" +
            ", comments='" + getComments() + "'" +
            ", user='" + getUser() + "'" +
            "}";
    }
}
