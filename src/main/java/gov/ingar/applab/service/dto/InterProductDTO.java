package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import gov.ingar.applab.domain.enumeration.Unit;
import gov.ingar.applab.service.util.ConvertUtil;

/**
 * A DTO for the InterProduct entity.
 */
public class InterProductDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal tickness;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal width;

    @NotNull
    private Unit valType;

    private String longGroups;

    private String description;

    // field used in front-end
    private String[] longGroupsArray;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getTickness() {
        return tickness;
    }

    public void setTickness(BigDecimal tickness) {
        this.tickness = tickness;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    public Unit getValType() {
        return valType;
    }

    public void setValType(Unit valType) {
        this.valType = valType;
    }

    public String getLongGroups() {
        return longGroups;
    }

    public void setLongGroups(String longGroups) {
        this.longGroups = longGroups;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InterProductDTO interProductDTO = (InterProductDTO) o;
        if (interProductDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), interProductDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InterProductDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", tickness=" + getTickness() +
            ", width=" + getWidth() +
            ", valType='" + getValType() + "'" +
            ", longGroups='" + getLongGroups() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }

    public String[] getLongGroupsArray() {
        //return longGroupsArray;
        return ConvertUtil.strToArray(longGroups);
    }

    public void setLongGroupsArray(String[] longGroupsArray) {
        this.longGroupsArray = longGroupsArray;
        if (longGroupsArray != null && longGroupsArray.length > 0) {
            this.longGroups = ConvertUtil.arrayToString(longGroupsArray);
        }
    }
}
