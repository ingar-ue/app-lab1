package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the KerfConfig entity.
 */
public class KerfConfigDTO implements Serializable {

    private Long id;

    @NotNull
    private String param;

    @NotNull
    private BigDecimal value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        KerfConfigDTO kerfConfigDTO = (KerfConfigDTO) o;
        if (kerfConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), kerfConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KerfConfigDTO{" +
            "id=" + getId() +
            ", param='" + getParam() + "'" +
            ", value=" + getValue() +
            "}";
    }
}
