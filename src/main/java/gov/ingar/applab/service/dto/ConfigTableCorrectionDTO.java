package gov.ingar.applab.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the ConfigTableCorrection entity.
 */
public class ConfigTableCorrectionDTO implements Serializable {

    private Long id;

    private BigDecimal value1;

    private BigDecimal value2;

    private BigDecimal value3;

    private BigDecimal value4;

    private BigDecimal value5;

    private BigDecimal value6;

    private Long diameterGroupId;

    private String diameterGroupCode;

    public String getDiameterGroupCode() {
        return diameterGroupCode;
    }

    public void setDiameterGroupCode(String diameterGroupCode) {
        this.diameterGroupCode = diameterGroupCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue1() {
        return value1;
    }

    public void setValue1(BigDecimal value1) {
        this.value1 = value1;
    }

    public BigDecimal getValue2() {
        return value2;
    }

    public void setValue2(BigDecimal value2) {
        this.value2 = value2;
    }

    public BigDecimal getValue3() {
        return value3;
    }

    public void setValue3(BigDecimal value3) {
        this.value3 = value3;
    }

    public BigDecimal getValue4() {
        return value4;
    }

    public void setValue4(BigDecimal value4) {
        this.value4 = value4;
    }

    public BigDecimal getValue5() {
        return value5;
    }

    public void setValue5(BigDecimal value5) {
        this.value5 = value5;
    }

    public BigDecimal getValue6() {
        return value6;
    }

    public void setValue6(BigDecimal value6) {
        this.value6 = value6;
    }

    public Long getDiameterGroupId() {
        return diameterGroupId;
    }

    public void setDiameterGroupId(Long diameterGroupId) {
        this.diameterGroupId = diameterGroupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConfigTableCorrectionDTO configTableCorrectionDTO = (ConfigTableCorrectionDTO) o;
        if (configTableCorrectionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), configTableCorrectionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConfigTableCorrectionDTO{" +
            "id=" + getId() +
            ", value1=" + getValue1() +
            ", value2=" + getValue2() +
            ", value3=" + getValue3() +
            ", value4=" + getValue4() +
            ", value5=" + getValue5() +
            ", value6=" + getValue6() +
            ", diameterGroup=" + getDiameterGroupId() +
            "}";
    }
}
