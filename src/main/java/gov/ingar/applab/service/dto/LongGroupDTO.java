package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import gov.ingar.applab.domain.enumeration.Unit;

/**
 * A DTO for the LongGroup entity.
 */
public class LongGroupDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private BigDecimal value;

    @NotNull
    private Unit valType;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Unit getValType() {
        return valType;
    }

    public void setValType(Unit valType) {
        this.valType = valType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LongGroupDTO longGroupDTO = (LongGroupDTO) o;
        if (longGroupDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), longGroupDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LongGroupDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", value=" + getValue() +
            ", valType='" + getValType() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
