package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the ConfigProccessTime entity.
 */
public class ConfigProccessTimeDTO implements Serializable {

    private Long id;

    @NotNull
    private BigDecimal value;

    private Long diameterGroupId;

    private Long longGroupId;

    private String longGroupCode;

    private String diameterGroupCode;

    public String getLongGroupCode() {
        return longGroupCode;
    }

    public void setLongGroupCode(String longGroupCode) {
        this.longGroupCode = longGroupCode;
    }

    public String getDiameterGroupCode() {
        return diameterGroupCode;
    }

    public void setDiameterGroupCode(String diameterGroupCode) {
        this.diameterGroupCode = diameterGroupCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Long getDiameterGroupId() {
        return diameterGroupId;
    }

    public void setDiameterGroupId(Long diameterGroupId) {
        this.diameterGroupId = diameterGroupId;
    }

    public Long getLongGroupId() {
        return longGroupId;
    }

    public void setLongGroupId(Long longGroupId) {
        this.longGroupId = longGroupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConfigProccessTimeDTO configProccessTimeDTO = (ConfigProccessTimeDTO) o;
        if (configProccessTimeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), configProccessTimeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConfigProccessTimeDTO{" +
            "id=" + getId() +
            ", value=" + getValue() +
            ", diameterGroup=" + getDiameterGroupId() +
            ", longGroup=" + getLongGroupId() +
            "}";
    }
}
