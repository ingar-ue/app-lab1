package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import gov.ingar.applab.domain.enumeration.Unit;

/**
 * A DTO for the DiameterGroup entity.
 */
public class DiameterGroupDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private BigDecimal minValue;

    @NotNull
    private BigDecimal maxValue;

    @NotNull
    private Unit valType;

    @NotNull
    private String schema;

    @NotNull
    private Integer tablesCentral;

    private Integer tablesSide;

    private Integer tablesTop;

    private String longGroups;

    @NotNull
    @Min(value = 0)
    @Max(value = 100)
    private Integer yieldIndex;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getMinValue() {
        return minValue;
    }

    public void setMinValue(BigDecimal minValue) {
        this.minValue = minValue;
    }

    public BigDecimal getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(BigDecimal maxValue) {
        this.maxValue = maxValue;
    }

    public Unit getValType() {
        return valType;
    }

    public void setValType(Unit valType) {
        this.valType = valType;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public Integer getTablesCentral() {
        return tablesCentral;
    }

    public void setTablesCentral(Integer tablesCentral) {
        this.tablesCentral = tablesCentral;
    }

    public Integer getTablesSide() {
        return tablesSide;
    }

    public void setTablesSide(Integer tablesSide) {
        this.tablesSide = tablesSide;
    }

    public Integer getTablesTop() {
        return tablesTop;
    }

    public void setTablesTop(Integer tablesTop) {
        this.tablesTop = tablesTop;
    }

    public String getLongGroups() {
        return longGroups;
    }

    public void setLongGroups(String longGroups) {
        this.longGroups = longGroups;
    }

    public Integer getYieldIndex() {
        return yieldIndex;
    }

    public void setYieldIndex(Integer yieldIndex) {
        this.yieldIndex = yieldIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DiameterGroupDTO diameterGroupDTO = (DiameterGroupDTO) o;
        if (diameterGroupDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), diameterGroupDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DiameterGroupDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", minValue=" + getMinValue() +
            ", maxValue=" + getMaxValue() +
            ", valType='" + getValType() + "'" +
            ", schema='" + getSchema() + "'" +
            ", tablesCentral=" + getTablesCentral() +
            ", tablesSide=" + getTablesSide() +
            ", tablesTop=" + getTablesTop() +
            ", longGroups='" + getLongGroups() + "'" +
            ", yieldIndex=" + getYieldIndex() +
            "}";
    }
}
