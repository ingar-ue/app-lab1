package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ResourceQty entity.
 */
public class ResourceQtyDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer quantity;

    private Long diameterGroupId;

    private Long longGroupId;

    private String diameterGroupCode;

    private String longGroupCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getDiameterGroupId() {
        return diameterGroupId;
    }

    public void setDiameterGroupId(Long diameterGroupId) {
        this.diameterGroupId = diameterGroupId;
    }

    public Long getLongGroupId() {
        return longGroupId;
    }

    public void setLongGroupId(Long longGroupId) {
        this.longGroupId = longGroupId;
    }

    public String getDiameterGroupCode() {
        return diameterGroupCode;
    }

    public void setDiameterGroupCode(String diameterGroupCode) {
        this.diameterGroupCode = diameterGroupCode;
    }

    public String getLongGroupCode() {
        return longGroupCode;
    }

    public void setLongGroupCode(String longGroupCode) {
        this.longGroupCode = longGroupCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ResourceQtyDTO resourceQtyDTO = (ResourceQtyDTO) o;
        if (resourceQtyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), resourceQtyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ResourceQtyDTO{" +
            "id=" + getId() +
            ", quantity=" + getQuantity() +
            ", diameterGroup=" + getDiameterGroupId() +
            ", longGroup=" + getLongGroupId() +
            "}";
    }
}
