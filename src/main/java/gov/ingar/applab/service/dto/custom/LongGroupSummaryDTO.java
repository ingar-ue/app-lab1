package gov.ingar.applab.service.dto.custom;

import gov.ingar.applab.service.dto.LongGroupDTO;

import java.io.Serializable;
import java.util.List;

public class LongGroupSummaryDTO implements Serializable {

    private List<LongGroupDTO> longs;

    public LongGroupSummaryDTO() {
        // Empty constructor needed for Jackson.
    }

    public List<LongGroupDTO> getLongs() {
        return longs;
    }

    public void setLongs(List<LongGroupDTO> longs) {
        this.longs = longs;
    }

}
