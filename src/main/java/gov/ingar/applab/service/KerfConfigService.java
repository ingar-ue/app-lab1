package gov.ingar.applab.service;

import gov.ingar.applab.domain.KerfConfig;
import gov.ingar.applab.repository.KerfConfigRepository;
import gov.ingar.applab.service.dto.KerfConfigDTO;
import gov.ingar.applab.service.mapper.KerfConfigMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing KerfConfig.
 */
@Service
@Transactional
public class KerfConfigService {

    private final Logger log = LoggerFactory.getLogger(KerfConfigService.class);

    private final KerfConfigRepository kerfConfigRepository;

    private final KerfConfigMapper kerfConfigMapper;

    public KerfConfigService(KerfConfigRepository kerfConfigRepository, KerfConfigMapper kerfConfigMapper) {
        this.kerfConfigRepository = kerfConfigRepository;
        this.kerfConfigMapper = kerfConfigMapper;
    }

    /**
     * Save a kerfConfig.
     *
     * @param kerfConfigDTO the entity to save
     * @return the persisted entity
     */
    public KerfConfigDTO save(KerfConfigDTO kerfConfigDTO) {
        log.debug("Request to save KerfConfig : {}", kerfConfigDTO);
        KerfConfig kerfConfig = kerfConfigMapper.toEntity(kerfConfigDTO);
        kerfConfig = kerfConfigRepository.save(kerfConfig);
        return kerfConfigMapper.toDto(kerfConfig);
    }

    /**
     * Get all the kerfConfigs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<KerfConfigDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KerfConfigs");
        return kerfConfigRepository.findAll(pageable)
            .map(kerfConfigMapper::toDto);
    }


    /**
     * Get one kerfConfig by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<KerfConfigDTO> findOne(Long id) {
        log.debug("Request to get KerfConfig : {}", id);
        return kerfConfigRepository.findById(id)
            .map(kerfConfigMapper::toDto);
    }

    /**
     * Delete the kerfConfig by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete KerfConfig : {}", id);
        kerfConfigRepository.deleteById(id);
    }
}
