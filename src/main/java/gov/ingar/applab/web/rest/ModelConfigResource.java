package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ModelConfigDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ModelConfig.
 */
@RestController
@RequestMapping("/api")
public class ModelConfigResource {

    private final Logger log = LoggerFactory.getLogger(ModelConfigResource.class);

    private static final String ENTITY_NAME = "modelConfig";

    private final ModelConfigService modelConfigService;

    public ModelConfigResource(ModelConfigService modelConfigService) {
        this.modelConfigService = modelConfigService;
    }

    /**
     * POST  /model-configs : Create a new modelConfig.
     *
     * @param modelConfigDTO the modelConfigDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new modelConfigDTO, or with status 400 (Bad Request) if the modelConfig has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/model-configs")
    @Timed
    public ResponseEntity<ModelConfigDTO> createModelConfig(@Valid @RequestBody ModelConfigDTO modelConfigDTO) throws URISyntaxException {
        log.debug("REST request to save ModelConfig : {}", modelConfigDTO);
        if (modelConfigDTO.getId() != null) {
            throw new BadRequestAlertException("A new modelConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModelConfigDTO result = modelConfigService.save(modelConfigDTO);
        return ResponseEntity.created(new URI("/api/model-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /model-configs : Updates an existing modelConfig.
     *
     * @param modelConfigDTO the modelConfigDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated modelConfigDTO,
     * or with status 400 (Bad Request) if the modelConfigDTO is not valid,
     * or with status 500 (Internal Server Error) if the modelConfigDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/model-configs")
    @Timed
    public ResponseEntity<ModelConfigDTO> updateModelConfig(@Valid @RequestBody ModelConfigDTO modelConfigDTO) throws URISyntaxException {
        log.debug("REST request to update ModelConfig : {}", modelConfigDTO);
        if (modelConfigDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ModelConfigDTO result = modelConfigService.save(modelConfigDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, modelConfigDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /model-configs : get all the modelConfigs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of modelConfigs in body
     */
    @GetMapping("/model-configs")
    @Timed
    public ResponseEntity<List<ModelConfigDTO>> getAllModelConfigs(Pageable pageable) {
        log.debug("REST request to get a page of ModelConfigs");
        Page<ModelConfigDTO> page = modelConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/model-configs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /model-configs/:id : get the "id" modelConfig.
     *
     * @param id the id of the modelConfigDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the modelConfigDTO, or with status 404 (Not Found)
     */
    @GetMapping("/model-configs/{id}")
    @Timed
    public ResponseEntity<ModelConfigDTO> getModelConfig(@PathVariable Long id) {
        log.debug("REST request to get ModelConfig : {}", id);
        Optional<ModelConfigDTO> modelConfigDTO = modelConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(modelConfigDTO);
    }

    /**
     * DELETE  /model-configs/:id : delete the "id" modelConfig.
     *
     * @param id the id of the modelConfigDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/model-configs/{id}")
    @Timed
    public ResponseEntity<Void> deleteModelConfig(@PathVariable Long id) {
        log.debug("REST request to delete ModelConfig : {}", id);
        modelConfigService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
