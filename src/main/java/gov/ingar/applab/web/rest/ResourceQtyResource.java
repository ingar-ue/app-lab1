package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ResourceQtyService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ResourceQtyDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ResourceQty.
 */
@RestController
@RequestMapping("/api")
public class ResourceQtyResource {

    private final Logger log = LoggerFactory.getLogger(ResourceQtyResource.class);

    private static final String ENTITY_NAME = "resourceQty";

    private final ResourceQtyService resourceQtyService;

    public ResourceQtyResource(ResourceQtyService resourceQtyService) {
        this.resourceQtyService = resourceQtyService;
    }

    /**
     * POST  /resource-qties : Create a new resourceQty.
     *
     * @param resourceQtyDTO the resourceQtyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new resourceQtyDTO, or with status 400 (Bad Request) if the resourceQty has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/resource-qties")
    @Timed
    public ResponseEntity<ResourceQtyDTO> createResourceQty(@Valid @RequestBody ResourceQtyDTO resourceQtyDTO) throws URISyntaxException {
        log.debug("REST request to save ResourceQty : {}", resourceQtyDTO);
        if (resourceQtyDTO.getId() != null) {
            throw new BadRequestAlertException("A new resourceQty cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ResourceQtyDTO result = resourceQtyService.save(resourceQtyDTO);
        return ResponseEntity.created(new URI("/api/resource-qties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /resource-qties : Updates an existing resourceQty.
     *
     * @param resourceQtyDTO the resourceQtyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated resourceQtyDTO,
     * or with status 400 (Bad Request) if the resourceQtyDTO is not valid,
     * or with status 500 (Internal Server Error) if the resourceQtyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/resource-qties")
    @Timed
    public ResponseEntity<ResourceQtyDTO> updateResourceQty(@Valid @RequestBody ResourceQtyDTO resourceQtyDTO) throws URISyntaxException {
        log.debug("REST request to update ResourceQty : {}", resourceQtyDTO);
        if (resourceQtyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ResourceQtyDTO result = resourceQtyService.save(resourceQtyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resourceQtyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /resource-qties : get all the resourceQties.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of resourceQties in body
     */
    @GetMapping("/resource-qties")
    @Timed
    public ResponseEntity<List<ResourceQtyDTO>> getAllResourceQties(Pageable pageable) {
        log.debug("REST request to get a page of ResourceQties");
        Page<ResourceQtyDTO> page = resourceQtyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/resource-qties");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /resource-qties/:id : get the "id" resourceQty.
     *
     * @param id the id of the resourceQtyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the resourceQtyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/resource-qties/{id}")
    @Timed
    public ResponseEntity<ResourceQtyDTO> getResourceQty(@PathVariable Long id) {
        log.debug("REST request to get ResourceQty : {}", id);
        Optional<ResourceQtyDTO> resourceQtyDTO = resourceQtyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(resourceQtyDTO);
    }

    /**
     * DELETE  /resource-qties/:id : delete the "id" resourceQty.
     *
     * @param id the id of the resourceQtyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/resource-qties/{id}")
    @Timed
    public ResponseEntity<Void> deleteResourceQty(@PathVariable Long id) {
        log.debug("REST request to delete ResourceQty : {}", id);
        resourceQtyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
