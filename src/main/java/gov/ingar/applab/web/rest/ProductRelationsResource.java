package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ProductRelationsService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ProductRelationsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ProductRelations.
 */
@RestController
@RequestMapping("/api")
public class ProductRelationsResource {

    private final Logger log = LoggerFactory.getLogger(ProductRelationsResource.class);

    private static final String ENTITY_NAME = "productRelations";

    private final ProductRelationsService productRelationsService;

    public ProductRelationsResource(ProductRelationsService productRelationsService) {
        this.productRelationsService = productRelationsService;
    }

    /**
     * POST  /product-relations : Create a new productRelations.
     *
     * @param productRelationsDTO the productRelationsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productRelationsDTO, or with status 400 (Bad Request) if the productRelations has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/product-relations")
    @Timed
    public ResponseEntity<ProductRelationsDTO> createProductRelations(@Valid @RequestBody ProductRelationsDTO productRelationsDTO) throws URISyntaxException {
        log.debug("REST request to save ProductRelations : {}", productRelationsDTO);
        if (productRelationsDTO.getId() != null) {
            throw new BadRequestAlertException("A new productRelations cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductRelationsDTO result = productRelationsService.save(productRelationsDTO);
        return ResponseEntity.created(new URI("/api/product-relations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /product-relations : Updates an existing productRelations.
     *
     * @param productRelationsDTO the productRelationsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productRelationsDTO,
     * or with status 400 (Bad Request) if the productRelationsDTO is not valid,
     * or with status 500 (Internal Server Error) if the productRelationsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/product-relations")
    @Timed
    public ResponseEntity<ProductRelationsDTO> updateProductRelations(@Valid @RequestBody ProductRelationsDTO productRelationsDTO) throws URISyntaxException {
        log.debug("REST request to update ProductRelations : {}", productRelationsDTO);
        if (productRelationsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductRelationsDTO result = productRelationsService.save(productRelationsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productRelationsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /product-relations : get all the productRelations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of productRelations in body
     */
    @GetMapping("/product-relations")
    @Timed
    public ResponseEntity<List<ProductRelationsDTO>> getAllProductRelations(Pageable pageable, @RequestParam(required = false, defaultValue="") String type, @RequestParam(required = false, defaultValue="") String keyword) {
        log.debug("REST request to get a page of ProductRelations");
        Page<ProductRelationsDTO> page = productRelationsService.findAllBy(pageable, type, keyword);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/product-relations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /product-relations/:id : get the "id" productRelations.
     *
     * @param id the id of the productRelationsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the productRelationsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/product-relations/{id}")
    @Timed
    public ResponseEntity<ProductRelationsDTO> getProductRelations(@PathVariable Long id) {
        log.debug("REST request to get ProductRelations : {}", id);
        Optional<ProductRelationsDTO> productRelationsDTO = productRelationsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productRelationsDTO);
    }

    /**
     * DELETE  /product-relations/:id : delete the "id" productRelations.
     *
     * @param id the id of the productRelationsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/product-relations/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductRelations(@PathVariable Long id) {
        log.debug("REST request to delete ProductRelations : {}", id);
        productRelationsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
