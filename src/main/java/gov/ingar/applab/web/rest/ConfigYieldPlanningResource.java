package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ConfigYieldPlanningService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ConfigYieldPlanningDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConfigYieldPlanning.
 */
@RestController
@RequestMapping("/api")
public class ConfigYieldPlanningResource {

    private final Logger log = LoggerFactory.getLogger(ConfigYieldPlanningResource.class);

    private static final String ENTITY_NAME = "configYieldPlanning";

    private final ConfigYieldPlanningService configYieldPlanningService;

    public ConfigYieldPlanningResource(ConfigYieldPlanningService configYieldPlanningService) {
        this.configYieldPlanningService = configYieldPlanningService;
    }

    /**
     * POST  /config-yield-plannings : Create a new configYieldPlanning.
     *
     * @param configYieldPlanningDTO the configYieldPlanningDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new configYieldPlanningDTO, or with status 400 (Bad Request) if the configYieldPlanning has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/config-yield-plannings")
    @Timed
    public ResponseEntity<ConfigYieldPlanningDTO> createConfigYieldPlanning(@Valid @RequestBody ConfigYieldPlanningDTO configYieldPlanningDTO) throws URISyntaxException {
        log.debug("REST request to save ConfigYieldPlanning : {}", configYieldPlanningDTO);
        if (configYieldPlanningDTO.getId() != null) {
            throw new BadRequestAlertException("A new configYieldPlanning cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConfigYieldPlanningDTO result = configYieldPlanningService.save(configYieldPlanningDTO);
        return ResponseEntity.created(new URI("/api/config-yield-plannings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /config-yield-plannings : Updates an existing configYieldPlanning.
     *
     * @param configYieldPlanningDTO the configYieldPlanningDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated configYieldPlanningDTO,
     * or with status 400 (Bad Request) if the configYieldPlanningDTO is not valid,
     * or with status 500 (Internal Server Error) if the configYieldPlanningDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/config-yield-plannings")
    @Timed
    public ResponseEntity<ConfigYieldPlanningDTO> updateConfigYieldPlanning(@Valid @RequestBody ConfigYieldPlanningDTO configYieldPlanningDTO) throws URISyntaxException {
        log.debug("REST request to update ConfigYieldPlanning : {}", configYieldPlanningDTO);
        if (configYieldPlanningDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ConfigYieldPlanningDTO result = configYieldPlanningService.save(configYieldPlanningDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, configYieldPlanningDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /config-yield-plannings : get all the configYieldPlannings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of configYieldPlannings in body
     */
    @GetMapping("/config-yield-plannings")
    @Timed
    public ResponseEntity<List<ConfigYieldPlanningDTO>> getAllConfigYieldPlannings(Pageable pageable) {
        log.debug("REST request to get a page of ConfigYieldPlannings");
        Page<ConfigYieldPlanningDTO> page = configYieldPlanningService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/config-yield-plannings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /config-yield-plannings/:id : get the "id" configYieldPlanning.
     *
     * @param id the id of the configYieldPlanningDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the configYieldPlanningDTO, or with status 404 (Not Found)
     */
    @GetMapping("/config-yield-plannings/{id}")
    @Timed
    public ResponseEntity<ConfigYieldPlanningDTO> getConfigYieldPlanning(@PathVariable Long id) {
        log.debug("REST request to get ConfigYieldPlanning : {}", id);
        Optional<ConfigYieldPlanningDTO> configYieldPlanningDTO = configYieldPlanningService.findOne(id);
        return ResponseUtil.wrapOrNotFound(configYieldPlanningDTO);
    }

    /**
     * DELETE  /config-yield-plannings/:id : delete the "id" configYieldPlanning.
     *
     * @param id the id of the configYieldPlanningDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/config-yield-plannings/{id}")
    @Timed
    public ResponseEntity<Void> deleteConfigYieldPlanning(@PathVariable Long id) {
        log.debug("REST request to delete ConfigYieldPlanning : {}", id);
        configYieldPlanningService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
