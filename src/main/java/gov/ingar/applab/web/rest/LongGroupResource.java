package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.LongGroupService;
import gov.ingar.applab.service.dto.custom.LongGroupSummaryDTO;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.LongGroupDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LongGroup.
 */
@RestController
@RequestMapping("/api")
public class LongGroupResource {

    private final Logger log = LoggerFactory.getLogger(LongGroupResource.class);

    private static final String ENTITY_NAME = "longGroup";

    private final LongGroupService longGroupService;

    public LongGroupResource(LongGroupService longGroupService) {
        this.longGroupService = longGroupService;
    }

    /**
     * POST  /long-groups : Create a new longGroup.
     *
     * @param longGroupDTO the longGroupDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new longGroupDTO, or with status 400 (Bad Request) if the longGroup has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/long-groups")
    @Timed
    public ResponseEntity<LongGroupDTO> createLongGroup(@Valid @RequestBody LongGroupDTO longGroupDTO) throws URISyntaxException {
        log.debug("REST request to save LongGroup : {}", longGroupDTO);
        if (longGroupDTO.getId() != null) {
            throw new BadRequestAlertException("A new longGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LongGroupDTO result = longGroupService.save(longGroupDTO);
        return ResponseEntity.created(new URI("/api/long-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /long-groups : Updates an existing longGroup.
     *
     * @param longGroupDTO the longGroupDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated longGroupDTO,
     * or with status 400 (Bad Request) if the longGroupDTO is not valid,
     * or with status 500 (Internal Server Error) if the longGroupDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/long-groups")
    @Timed
    public ResponseEntity<LongGroupDTO> updateLongGroup(@Valid @RequestBody LongGroupDTO longGroupDTO) throws URISyntaxException {
        log.debug("REST request to update LongGroup : {}", longGroupDTO);
        if (longGroupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LongGroupDTO result = longGroupService.save(longGroupDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, longGroupDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /long-groups : get all the longGroups.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of longGroups in body
     */
    @GetMapping("/long-groups")
    @Timed
    public ResponseEntity<List<LongGroupDTO>> getAllLongGroups(Pageable pageable) {
        log.debug("REST request to get a page of LongGroups");
        Page<LongGroupDTO> page = longGroupService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/long-groups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /long-groups/:id : get the "id" longGroup.
     *
     * @param id the id of the longGroupDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the longGroupDTO, or with status 404 (Not Found)
     */
    @GetMapping("/long-groups/{id}")
    @Timed
    public ResponseEntity<LongGroupDTO> getLongGroup(@PathVariable Long id) {
        log.debug("REST request to get LongGroup : {}", id);
        Optional<LongGroupDTO> longGroupDTO = longGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(longGroupDTO);
    }

    /**
     * DELETE  /long-groups/:id : delete the "id" longGroup.
     *
     * @param id the id of the longGroupDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/long-groups/{id}")
    @Timed
    public ResponseEntity<Void> deleteLongGroup(@PathVariable Long id) {
        log.debug("REST request to delete LongGroup : {}", id);
        longGroupService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /long-group-summary : get the summary.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the data
     */
    @GetMapping("/long-group-summary")
    @Timed
    public ResponseEntity<LongGroupSummaryDTO> getSummary() {
        LongGroupSummaryDTO summary = getLongGroupSummaryDTO();

        return new ResponseEntity<>(summary, HttpStatus.OK);
    }

    private LongGroupSummaryDTO getLongGroupSummaryDTO() {
        LongGroupSummaryDTO summary = new LongGroupSummaryDTO();
        // load longs
        log.debug("REST querying longs");
        List<LongGroupDTO> longGroups = longGroupService.findAll();
        if (!longGroups.isEmpty()) {
            summary.setLongs(longGroups);
        }
        return summary;
    }

}
