package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.InterProductService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.InterProductDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InterProduct.
 */
@RestController
@RequestMapping("/api")
public class InterProductResource {

    private final Logger log = LoggerFactory.getLogger(InterProductResource.class);

    private static final String ENTITY_NAME = "interProduct";

    private final InterProductService interProductService;

    public InterProductResource(InterProductService interProductService) {
        this.interProductService = interProductService;
    }

    /**
     * POST  /inter-products : Create a new interProduct.
     *
     * @param interProductDTO the interProductDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new interProductDTO, or with status 400 (Bad Request) if the interProduct has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inter-products")
    @Timed
    public ResponseEntity<InterProductDTO> createInterProduct(@Valid @RequestBody InterProductDTO interProductDTO) throws URISyntaxException {
        log.debug("REST request to save InterProduct : {}", interProductDTO);
        if (interProductDTO.getId() != null) {
            throw new BadRequestAlertException("A new interProduct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InterProductDTO result = interProductService.save(interProductDTO);
        return ResponseEntity.created(new URI("/api/inter-products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /inter-products : Updates an existing interProduct.
     *
     * @param interProductDTO the interProductDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated interProductDTO,
     * or with status 400 (Bad Request) if the interProductDTO is not valid,
     * or with status 500 (Internal Server Error) if the interProductDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/inter-products")
    @Timed
    public ResponseEntity<InterProductDTO> updateInterProduct(@Valid @RequestBody InterProductDTO interProductDTO) throws URISyntaxException {
        log.debug("REST request to update InterProduct : {}", interProductDTO);
        if (interProductDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InterProductDTO result = interProductService.save(interProductDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, interProductDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /inter-products : get all the interProducts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of interProducts in body
     */
    @GetMapping("/inter-products")
    @Timed
    public ResponseEntity<List<InterProductDTO>> getAllInterProducts(Pageable pageable, @RequestParam(required = false, defaultValue="") String type, @RequestParam(required = false, defaultValue ="") String keyword) {
        log.debug("REST request to get a page of InterProducts");
        Page<InterProductDTO> page = interProductService.findAllBy(pageable, type, keyword);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/inter-products");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /inter-products/:id : get the "id" interProduct.
     *
     * @param id the id of the interProductDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the interProductDTO, or with status 404 (Not Found)
     */
    @GetMapping("/inter-products/{id}")
    @Timed
    public ResponseEntity<InterProductDTO> getInterProduct(@PathVariable Long id) {
        log.debug("REST request to get InterProduct : {}", id);
        Optional<InterProductDTO> interProductDTO = interProductService.findOne(id);
        return ResponseUtil.wrapOrNotFound(interProductDTO);
    }

    /**
     * DELETE  /inter-products/:id : delete the "id" interProduct.
     *
     * @param id the id of the interProductDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/inter-products/{id}")
    @Timed
    public ResponseEntity<Void> deleteInterProduct(@PathVariable Long id) {
        log.debug("REST request to delete InterProduct : {}", id);
        interProductService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
