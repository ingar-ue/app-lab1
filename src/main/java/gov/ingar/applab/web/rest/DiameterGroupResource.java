package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.DiameterGroupService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.DiameterGroupDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DiameterGroup.
 */
@RestController
@RequestMapping("/api")
public class DiameterGroupResource {

    private final Logger log = LoggerFactory.getLogger(DiameterGroupResource.class);

    private static final String ENTITY_NAME = "diameterGroup";

    private final DiameterGroupService diameterGroupService;

    public DiameterGroupResource(DiameterGroupService diameterGroupService) {
        this.diameterGroupService = diameterGroupService;
    }

    /**
     * POST  /diameter-groups : Create a new diameterGroup.
     *
     * @param diameterGroupDTO the diameterGroupDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new diameterGroupDTO, or with status 400 (Bad Request) if the diameterGroup has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/diameter-groups")
    @Timed
    public ResponseEntity<DiameterGroupDTO> createDiameterGroup(@Valid @RequestBody DiameterGroupDTO diameterGroupDTO) throws URISyntaxException {
        log.debug("REST request to save DiameterGroup : {}", diameterGroupDTO);
        if (diameterGroupDTO.getId() != null) {
            throw new BadRequestAlertException("A new diameterGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DiameterGroupDTO result = diameterGroupService.save(diameterGroupDTO);
        return ResponseEntity.created(new URI("/api/diameter-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /diameter-groups : Updates an existing diameterGroup.
     *
     * @param diameterGroupDTO the diameterGroupDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated diameterGroupDTO,
     * or with status 400 (Bad Request) if the diameterGroupDTO is not valid,
     * or with status 500 (Internal Server Error) if the diameterGroupDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/diameter-groups")
    @Timed
    public ResponseEntity<DiameterGroupDTO> updateDiameterGroup(@Valid @RequestBody DiameterGroupDTO diameterGroupDTO) throws URISyntaxException {
        log.debug("REST request to update DiameterGroup : {}", diameterGroupDTO);
        if (diameterGroupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DiameterGroupDTO result = diameterGroupService.save(diameterGroupDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, diameterGroupDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /diameter-groups : get all the diameterGroups.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of diameterGroups in body
     */
    @GetMapping("/diameter-groups")
    @Timed
    public ResponseEntity<List<DiameterGroupDTO>> getAllDiameterGroups(Pageable pageable) {
        log.debug("REST request to get a page of DiameterGroups");
        Page<DiameterGroupDTO> page = diameterGroupService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/diameter-groups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /diameter-groups/:id : get the "id" diameterGroup.
     *
     * @param id the id of the diameterGroupDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the diameterGroupDTO, or with status 404 (Not Found)
     */
    @GetMapping("/diameter-groups/{id}")
    @Timed
    public ResponseEntity<DiameterGroupDTO> getDiameterGroup(@PathVariable Long id) {
        log.debug("REST request to get DiameterGroup : {}", id);
        Optional<DiameterGroupDTO> diameterGroupDTO = diameterGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(diameterGroupDTO);
    }

    /**
     * DELETE  /diameter-groups/:id : delete the "id" diameterGroup.
     *
     * @param id the id of the diameterGroupDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/diameter-groups/{id}")
    @Timed
    public ResponseEntity<Void> deleteDiameterGroup(@PathVariable Long id) {
        log.debug("REST request to delete DiameterGroup : {}", id);
        diameterGroupService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
