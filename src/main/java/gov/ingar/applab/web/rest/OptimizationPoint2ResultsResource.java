package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.optimization.OptimizationPoint2ResultsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * REST controller for managing OptimizationPoint1Resource.
 */
@RestController
@RequestMapping("/api")
public class OptimizationPoint2ResultsResource {

    private final OptimizationPoint2ResultsService optimizationPoint2ResultsService;

    public OptimizationPoint2ResultsResource(OptimizationPoint2ResultsService optimizationPoint2ResultsService) {
        this.optimizationPoint2ResultsService = optimizationPoint2ResultsService;
    }

    @GetMapping("/optimization-point-2-results-resume/{modelDefId}")
    @Timed
    public ResponseEntity<String> getOpt2ResultsResume(@PathVariable Long modelDefId) throws IOException {
        return new ResponseEntity<>(optimizationPoint2ResultsService.getResume(modelDefId).toString(), HttpStatus.OK);
    }

    @GetMapping("/optimization-point-2-results-chart1/{modelDefId}")
    @Timed
    public ResponseEntity<String> getOpt2ResultsChart1(@PathVariable Long modelDefId) throws IOException {
        return new ResponseEntity<>(optimizationPoint2ResultsService.getChart1(modelDefId).toString(), HttpStatus.OK);
    }

    @GetMapping("/optimization-point-2-results-production-data/{modelDefId}")
    @Timed
    public ResponseEntity<String> getOpt2ResultsProductionData(@PathVariable Long modelDefId) throws IOException {
        return new ResponseEntity<>(optimizationPoint2ResultsService.getProductionData(modelDefId).toString(), HttpStatus.OK);
    }
}
