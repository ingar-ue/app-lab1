package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ConfigTableCorrectionService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ConfigTableCorrectionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConfigTableCorrection.
 */
@RestController
@RequestMapping("/api")
public class ConfigTableCorrectionResource {

    private final Logger log = LoggerFactory.getLogger(ConfigTableCorrectionResource.class);

    private static final String ENTITY_NAME = "configTableCorrection";

    private final ConfigTableCorrectionService configTableCorrectionService;

    public ConfigTableCorrectionResource(ConfigTableCorrectionService configTableCorrectionService) {
        this.configTableCorrectionService = configTableCorrectionService;
    }

    /**
     * POST  /config-table-corrections : Create a new configTableCorrection.
     *
     * @param configTableCorrectionDTO the configTableCorrectionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new configTableCorrectionDTO, or with status 400 (Bad Request) if the configTableCorrection has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/config-table-corrections")
    @Timed
    public ResponseEntity<ConfigTableCorrectionDTO> createConfigTableCorrection(@RequestBody ConfigTableCorrectionDTO configTableCorrectionDTO) throws URISyntaxException {
        log.debug("REST request to save ConfigTableCorrection : {}", configTableCorrectionDTO);
        if (configTableCorrectionDTO.getId() != null) {
            throw new BadRequestAlertException("A new configTableCorrection cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConfigTableCorrectionDTO result = configTableCorrectionService.save(configTableCorrectionDTO);
        return ResponseEntity.created(new URI("/api/config-table-corrections/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /config-table-corrections : Updates an existing configTableCorrection.
     *
     * @param configTableCorrectionDTO the configTableCorrectionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated configTableCorrectionDTO,
     * or with status 400 (Bad Request) if the configTableCorrectionDTO is not valid,
     * or with status 500 (Internal Server Error) if the configTableCorrectionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/config-table-corrections")
    @Timed
    public ResponseEntity<ConfigTableCorrectionDTO> updateConfigTableCorrection(@RequestBody ConfigTableCorrectionDTO configTableCorrectionDTO) throws URISyntaxException {
        log.debug("REST request to update ConfigTableCorrection : {}", configTableCorrectionDTO);
        if (configTableCorrectionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ConfigTableCorrectionDTO result = configTableCorrectionService.save(configTableCorrectionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, configTableCorrectionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /config-table-corrections : get all the configTableCorrections.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of configTableCorrections in body
     */
    @GetMapping("/config-table-corrections")
    @Timed
    public ResponseEntity<List<ConfigTableCorrectionDTO>> getAllConfigTableCorrections(Pageable pageable) {
        log.debug("REST request to get a page of ConfigTableCorrections");
        Page<ConfigTableCorrectionDTO> page = configTableCorrectionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/config-table-corrections");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /config-table-corrections/:id : get the "id" configTableCorrection.
     *
     * @param id the id of the configTableCorrectionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the configTableCorrectionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/config-table-corrections/{id}")
    @Timed
    public ResponseEntity<ConfigTableCorrectionDTO> getConfigTableCorrection(@PathVariable Long id) {
        log.debug("REST request to get ConfigTableCorrection : {}", id);
        Optional<ConfigTableCorrectionDTO> configTableCorrectionDTO = configTableCorrectionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(configTableCorrectionDTO);
    }

    /**
     * DELETE  /config-table-corrections/:id : delete the "id" configTableCorrection.
     *
     * @param id the id of the configTableCorrectionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/config-table-corrections/{id}")
    @Timed
    public ResponseEntity<Void> deleteConfigTableCorrection(@PathVariable Long id) {
        log.debug("REST request to delete ConfigTableCorrection : {}", id);
        configTableCorrectionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
