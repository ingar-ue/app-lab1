package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.optimization.OptimizationDashboardService;
import gov.ingar.applab.optimization.OptimizationPoint1Service;
import gov.ingar.applab.optimization.dto.OptimizationDashboardDTO;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.ModelObjFunctionService;
import gov.ingar.applab.service.ModelRunDefService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for managing OptimizationPoint1Resource.
 */
@RestController
@RequestMapping("/api")
public class OptimizationSummaryResource {

    private final Logger log = LoggerFactory.getLogger(OptimizationSummaryResource.class);

    private final OptimizationDashboardService optimizationDashboardService;
    private final ModelRunDefService modelRunDefService;

    public OptimizationSummaryResource(OptimizationDashboardService optimizationDashboardService, ModelRunDefService modelRunDefService) {
        this.optimizationDashboardService = optimizationDashboardService;
        this.modelRunDefService = modelRunDefService;
    }

    @GetMapping("/optimization-summary")
    @Timed
    public ResponseEntity<OptimizationDashboardDTO> getOptimizationSummary() {
        return new ResponseEntity<>(this.optimizationDashboardService.getDashboardData(), HttpStatus.OK);
    }

}
