package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.FinalProductService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.FinalProductDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.MathContext;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing FinalProduct.
 */
@RestController
@RequestMapping("/api")
public class FinalProductResource {

    private final Logger log = LoggerFactory.getLogger(FinalProductResource.class);

    private static final String ENTITY_NAME = "finalProduct";

    private final FinalProductService finalProductService;

    public FinalProductResource(FinalProductService finalProductService) {
        this.finalProductService = finalProductService;
    }

    /**
     * POST  /final-products : Create a new finalProduct.
     *
     * @param finalProductDTO the finalProductDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new finalProductDTO, or with status 400 (Bad Request) if the finalProduct has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/final-products")
    @Timed
    public ResponseEntity<FinalProductDTO> createFinalProduct(@Valid @RequestBody FinalProductDTO finalProductDTO) throws URISyntaxException {
        log.debug("REST request to save FinalProduct : {}", finalProductDTO);
        if (finalProductDTO.getId() != null) {
            throw new BadRequestAlertException("A new finalProduct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FinalProductDTO result = finalProductService.save(finalProductDTO);
        return ResponseEntity.created(new URI("/api/final-products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /final-products : Updates an existing finalProduct.
     *
     * @param finalProductDTO the finalProductDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated finalProductDTO,
     * or with status 400 (Bad Request) if the finalProductDTO is not valid,
     * or with status 500 (Internal Server Error) if the finalProductDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/final-products")
    @Timed
    public ResponseEntity<FinalProductDTO> updateFinalProduct(@Valid @RequestBody FinalProductDTO finalProductDTO) throws URISyntaxException {
        log.debug("REST request to update FinalProduct : {}", finalProductDTO);
        if (finalProductDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FinalProductDTO result = finalProductService.save(finalProductDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, finalProductDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /final-products : get all the finalProducts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of finalProducts in body
     */
    @GetMapping("/final-products")
    @Timed
    public ResponseEntity<List<FinalProductDTO>> getAllFinalProducts(Pageable pageable, @RequestParam(required = false, defaultValue="") String type, @RequestParam(required = false, defaultValue="") String keyword) {
        log.debug("REST request to get a page of FinalProducts");
        Page<FinalProductDTO> page = finalProductService.findAllBy(pageable, type, keyword);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/final-products");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /final-products/:id : get the "id" finalProduct.
     *
     * @param id the id of the finalProductDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the finalProductDTO, or with status 404 (Not Found)
     */
    @GetMapping("/final-products/{id}")
    @Timed
    public ResponseEntity<FinalProductDTO> getFinalProduct(@PathVariable Long id) {
        log.debug("REST request to get FinalProduct : {}", id);
        Optional<FinalProductDTO> finalProductDTO = finalProductService.findOne(id);
        return ResponseUtil.wrapOrNotFound(finalProductDTO);
    }

    /**
     * DELETE  /final-products/:id : delete the "id" finalProduct.
     *
     * @param id the id of the finalProductDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/final-products/{id}")
    @Timed
    public ResponseEntity<Void> deleteFinalProduct(@PathVariable Long id) {
        log.debug("REST request to delete FinalProduct : {}", id);
        finalProductService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    /**
     * PUT  /final-products-price-reset : Reset prices of all products
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @PutMapping("/final-products-price-reset")
    @Timed
    public ResponseEntity<Void> priceReset() {
        log.debug("REST request to reset prices");
        finalProductService.priceReset();
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "priceReset")).build();
    }

    /**
     * PUT  /final-products-demand-reset : Reset demand of all products
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @PutMapping("/final-products-demand-reset")
    @Timed
    public ResponseEntity<Void> demandReset() {
        log.debug("REST request to reset prices");
        finalProductService.demandReset();
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "demandReset")).build();
    }

    /**
     * PUT  /final-products-price-increase : Increase prices of all products
     *
     * @param percentage increase
     * @return the ResponseEntity with status 200 (OK)
     */
    @PutMapping("/final-products-price-increase/{percentage}")
    @Timed
    public ResponseEntity<Void> priceIncrease(@PathVariable Double percentage) {
        log.debug("REST request to increase prices");
        finalProductService.priceIncrease(percentage);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "priceIncrease: " + percentage)).build();
    }
}
