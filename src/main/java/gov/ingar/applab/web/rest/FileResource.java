package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.config.WebSocketController;
import gov.ingar.applab.domain.enumeration.Status;
import gov.ingar.applab.optimization.dto.ModelRunDefMessage;
import gov.ingar.applab.optimization.gams.GamsUtil;
import gov.ingar.applab.service.ModelRunDefService;
import gov.ingar.applab.service.dto.ModelRunDefDTO;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class FileResource {

    private final ModelRunDefService modelRunDefService;

//    private final WebSocketController webSocketController;

    public FileResource( ModelRunDefService modelRunDefService/*, WebSocketController webSocketController*/) {
        this.modelRunDefService = modelRunDefService;
//        this.webSocketController = webSocketController;
    }


    @RequestMapping(path = "/download-file", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadFile(String param) throws IOException {
        Optional<ModelRunDefDTO> o = modelRunDefService.findOne(Long.valueOf(param));
        File file = new File(o.get().getDataSetResult());
        HttpHeaders headers = HeaderUtil.createAttachHeaders("Resultado.xlsx");
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
            .headers(headers)
            .contentLength(file.length())
            .contentType(MediaType.parseMediaType("application/octet-stream"))
            .body(resource);
    }

//    @GetMapping("/messages")
//    @Timed
//    public ResponseEntity<Void> checkRunning() throws InterruptedException {
//        while(true){
//            webSocketController.fireMessage("/topic/test", new ModelRunDefMessage(new Long(154),Status.RUNNING, 30));
//            Thread.sleep(5000);
//        }
//    }
}
