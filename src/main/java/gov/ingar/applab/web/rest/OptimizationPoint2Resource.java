package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.optimization.OptimizationPoint2Service;
import gov.ingar.applab.optimization.dto.OptimizationPoint2DTO;
import gov.ingar.applab.optimization.dto.OptimizationPointDTO;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.ModelObjFunctionService;
import gov.ingar.applab.service.ModelRunDefService;
import gov.ingar.applab.service.dto.ModelConfigDTO;
import gov.ingar.applab.service.dto.ModelObjFunctionDTO;
import gov.ingar.applab.service.dto.ModelRunConfigDTO;
import gov.ingar.applab.service.dto.ModelRunDefDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * REST controller for managing OptimizationPoint2Resource.
 */
@RestController
@RequestMapping("/api")
public class OptimizationPoint2Resource {


    private final OptimizationPoint2Service optimizationPoint2Service;
    private final ModelObjFunctionService modelObjFunctionService;
    private final ModelConfigService modelConfigService;
    private final ModelRunDefService modelRunDefService;

    public OptimizationPoint2Resource(OptimizationPoint2Service optimizationPoint2Service, ModelObjFunctionService modelObjFunctionService,
                                      ModelConfigService modelConfigService, ModelRunDefService modelRunDefService) {
        this.optimizationPoint2Service = optimizationPoint2Service;
        this.modelObjFunctionService = modelObjFunctionService;
        this.modelConfigService = modelConfigService;
        this.modelRunDefService = modelRunDefService;
    }

    @GetMapping("/optimization-point-2-load")
    @Timed
    public ResponseEntity<OptimizationPointDTO> getOpt2Input() {
        return new ResponseEntity<>(optimizationPoint2Service.loadCase(), HttpStatus.OK);
    }

    @GetMapping("/optimization-point-2-load-obj-functions/{modelDefId}")
    @Timed
    public ResponseEntity<List<ModelObjFunctionDTO>> getOpt2ObjFunctions(@PathVariable Long modelDefId) {
        return new ResponseEntity<>(modelObjFunctionService.findAllByModelDefId(modelDefId), HttpStatus.OK);
    }

    @GetMapping("/optimization-point-2-load-configs/{modelDefId}")
    @Timed
    public ResponseEntity<List<ModelRunConfigDTO>> getOpt2Configs(@PathVariable Long modelDefId) {
        List<ModelConfigDTO> confs = modelConfigService.findAllByModelDefId(modelDefId);
        List<ModelRunConfigDTO> runConfigs = new ArrayList<>();
        for (ModelConfigDTO conf : confs){
            ModelRunConfigDTO runConfig = new ModelRunConfigDTO();
            runConfig.setValue(conf.getValue());
            runConfig.setParam(conf.getParam());
            runConfigs.add(runConfig);
        }
        return new ResponseEntity<>(runConfigs, HttpStatus.OK);
    }

    @GetMapping("/optimization-point-2-check-running")
    @Timed
    public ResponseEntity<ModelRunDefDTO> checkRunning() {
        return new ResponseEntity<>(modelRunDefService.findRunningModel(), HttpStatus.OK);
    }

    @PutMapping("/optimization-point-2-execute")
    @Timed
    public ResponseEntity<Void> executeOptimizationPoint2(@Valid @RequestBody OptimizationPoint2DTO op2) {
        optimizationPoint2Service.runOptimizationPoint(op2);
        return ResponseEntity.ok().build();
    }

}
