package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.PatternExclusionService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.PatternExclusionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PatternExclusion.
 */
@RestController
@RequestMapping("/api")
public class PatternExclusionResource {

    private final Logger log = LoggerFactory.getLogger(PatternExclusionResource.class);

    private static final String ENTITY_NAME = "patternExclusion";

    private final PatternExclusionService patternExclusionService;

    public PatternExclusionResource(PatternExclusionService patternExclusionService) {
        this.patternExclusionService = patternExclusionService;
    }

    /**
     * POST  /pattern-exclusions : Create a new patternExclusion.
     *
     * @param patternExclusionDTO the patternExclusionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new patternExclusionDTO, or with status 400 (Bad Request) if the patternExclusion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pattern-exclusions")
    @Timed
    public ResponseEntity<PatternExclusionDTO> createPatternExclusion(@Valid @RequestBody PatternExclusionDTO patternExclusionDTO) throws URISyntaxException {
        log.debug("REST request to save PatternExclusion : {}", patternExclusionDTO);
        if (patternExclusionDTO.getId() != null) {
            throw new BadRequestAlertException("A new patternExclusion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PatternExclusionDTO result = patternExclusionService.save(patternExclusionDTO);
        return ResponseEntity.created(new URI("/api/pattern-exclusions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pattern-exclusions : Updates an existing patternExclusion.
     *
     * @param patternExclusionDTO the patternExclusionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated patternExclusionDTO,
     * or with status 400 (Bad Request) if the patternExclusionDTO is not valid,
     * or with status 500 (Internal Server Error) if the patternExclusionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pattern-exclusions")
    @Timed
    public ResponseEntity<PatternExclusionDTO> updatePatternExclusion(@Valid @RequestBody PatternExclusionDTO patternExclusionDTO) throws URISyntaxException {
        log.debug("REST request to update PatternExclusion : {}", patternExclusionDTO);
        if (patternExclusionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PatternExclusionDTO result = patternExclusionService.save(patternExclusionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, patternExclusionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pattern-exclusions : get all the patternExclusions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of patternExclusions in body
     */
    @GetMapping("/pattern-exclusions")
    @Timed
    public ResponseEntity<List<PatternExclusionDTO>> getAllPatternExclusions(Pageable pageable) {
        log.debug("REST request to get a page of PatternExclusions");
        Page<PatternExclusionDTO> page = patternExclusionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pattern-exclusions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pattern-exclusions/:id : get the "id" patternExclusion.
     *
     * @param id the id of the patternExclusionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the patternExclusionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pattern-exclusions/{id}")
    @Timed
    public ResponseEntity<PatternExclusionDTO> getPatternExclusion(@PathVariable Long id) {
        log.debug("REST request to get PatternExclusion : {}", id);
        Optional<PatternExclusionDTO> patternExclusionDTO = patternExclusionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(patternExclusionDTO);
    }

    /**
     * DELETE  /pattern-exclusions/:id : delete the "id" patternExclusion.
     *
     * @param id the id of the patternExclusionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pattern-exclusions/{id}")
    @Timed
    public ResponseEntity<Void> deletePatternExclusion(@PathVariable Long id) {
        log.debug("REST request to delete PatternExclusion : {}", id);
        patternExclusionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
