package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ModelVersionService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ModelVersionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ModelVersion.
 */
@RestController
@RequestMapping("/api")
public class ModelVersionResource {

    private final Logger log = LoggerFactory.getLogger(ModelVersionResource.class);

    private static final String ENTITY_NAME = "modelVersion";

    private final ModelVersionService modelVersionService;

    public ModelVersionResource(ModelVersionService modelVersionService) {
        this.modelVersionService = modelVersionService;
    }

    /**
     * POST  /model-versions : Create a new modelVersion.
     *
     * @param modelVersionDTO the modelVersionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new modelVersionDTO, or with status 400 (Bad Request) if the modelVersion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/model-versions")
    @Timed
    public ResponseEntity<ModelVersionDTO> createModelVersion(@Valid @RequestBody ModelVersionDTO modelVersionDTO) throws URISyntaxException {
        log.debug("REST request to save ModelVersion : {}", modelVersionDTO);
        if (modelVersionDTO.getId() != null) {
            throw new BadRequestAlertException("A new modelVersion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModelVersionDTO result = modelVersionService.save(modelVersionDTO);
        return ResponseEntity.created(new URI("/api/model-versions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /model-versions : Updates an existing modelVersion.
     *
     * @param modelVersionDTO the modelVersionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated modelVersionDTO,
     * or with status 400 (Bad Request) if the modelVersionDTO is not valid,
     * or with status 500 (Internal Server Error) if the modelVersionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/model-versions")
    @Timed
    public ResponseEntity<ModelVersionDTO> updateModelVersion(@Valid @RequestBody ModelVersionDTO modelVersionDTO) throws URISyntaxException {
        log.debug("REST request to update ModelVersion : {}", modelVersionDTO);
        if (modelVersionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ModelVersionDTO result = modelVersionService.save(modelVersionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, modelVersionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /model-versions : get all the modelVersions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of modelVersions in body
     */
    @GetMapping("/model-versions")
    @Timed
    public ResponseEntity<List<ModelVersionDTO>> getAllModelVersions(Pageable pageable) {
        log.debug("REST request to get a page of ModelVersions");
        Page<ModelVersionDTO> page = modelVersionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/model-versions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /model-versions/:id : get the "id" modelVersion.
     *
     * @param id the id of the modelVersionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the modelVersionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/model-versions/{id}")
    @Timed
    public ResponseEntity<ModelVersionDTO> getModelVersion(@PathVariable Long id) {
        log.debug("REST request to get ModelVersion : {}", id);
        Optional<ModelVersionDTO> modelVersionDTO = modelVersionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(modelVersionDTO);
    }

    /**
     * DELETE  /model-versions/:id : delete the "id" modelVersion.
     *
     * @param id the id of the modelVersionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/model-versions/{id}")
    @Timed
    public ResponseEntity<Void> deleteModelVersion(@PathVariable Long id) {
        log.debug("REST request to delete ModelVersion : {}", id);
        modelVersionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
