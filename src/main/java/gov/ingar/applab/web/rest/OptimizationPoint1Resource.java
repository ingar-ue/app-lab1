package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.optimization.dto.OptimizationPointDTO;
import gov.ingar.applab.optimization.gams.GamsOptimizationPoint1Runner;
import gov.ingar.applab.optimization.OptimizationPoint1Service;
import gov.ingar.applab.optimization.dto.OptimizationPoint1DTO;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.ModelObjFunctionService;
import gov.ingar.applab.service.ModelRunDefService;
import gov.ingar.applab.service.dto.ModelConfigDTO;
import gov.ingar.applab.service.dto.ModelObjFunctionDTO;
import gov.ingar.applab.service.dto.ModelRunConfigDTO;
import gov.ingar.applab.service.dto.ModelRunDefDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * REST controller for managing OptimizationPoint1Resource.
 */
@RestController
@RequestMapping("/api")
public class OptimizationPoint1Resource {

    private final Logger log = LoggerFactory.getLogger(OptimizationPoint1Resource.class);

    private final OptimizationPoint1Service optimizationPoint1Service;
    private final ModelObjFunctionService modelObjFunctionService;
    private final ModelConfigService modelConfigService;
    private final ModelRunDefService modelRunDefService;

    public OptimizationPoint1Resource(OptimizationPoint1Service optimizationPoint1Service, ModelObjFunctionService modelObjFunctionService,
                                      ModelConfigService modelConfigService, ModelRunDefService modelRunDefService) {
        this.optimizationPoint1Service = optimizationPoint1Service;
        this.modelObjFunctionService = modelObjFunctionService;
        this.modelConfigService = modelConfigService;
        this.modelRunDefService = modelRunDefService;
    }

    @GetMapping("/optimization-point-1-load")
    @Timed
    public ResponseEntity<OptimizationPointDTO> getOpt1Input() {
        return new ResponseEntity<>(optimizationPoint1Service.loadCase(), HttpStatus.OK);
    }

    @GetMapping("/optimization-point-1-load-obj-functions/{modelDefId}")
    @Timed
    public ResponseEntity<List<ModelObjFunctionDTO>> getOpt1ObjFunctions(@PathVariable Long modelDefId) {
        return new ResponseEntity<>(modelObjFunctionService.findAllByModelDefId(modelDefId), HttpStatus.OK);
    }

    @GetMapping("/optimization-point-1-load-configs/{modelDefId}")
    @Timed
    public ResponseEntity<List<ModelRunConfigDTO>> getOpt1Configs(@PathVariable Long modelDefId) {
        List<ModelConfigDTO> confs = modelConfigService.findAllByModelDefId(modelDefId);
        List<ModelRunConfigDTO> runConfigs = new ArrayList<>();
        for (ModelConfigDTO conf : confs){
            ModelRunConfigDTO runConfig = new ModelRunConfigDTO();
            runConfig.setValue(conf.getValue());
            runConfig.setParam(conf.getParam());
            runConfigs.add(runConfig);
        }
        return new ResponseEntity<>(runConfigs, HttpStatus.OK);
    }

    @GetMapping("/optimization-point-1-check-running")
    @Timed
    public ResponseEntity<ModelRunDefDTO> checkRunning() {
        return new ResponseEntity<>(modelRunDefService.findRunningModel(), HttpStatus.OK);
    }

    @PutMapping("/optimization-point-1-execute")
    @Timed
    public ResponseEntity<Void> executeOptimizationPoint1(@Valid @RequestBody OptimizationPoint1DTO op1) {
        optimizationPoint1Service.runOptimizationPoint(op1);
        return ResponseEntity.ok().build();
    }

}
