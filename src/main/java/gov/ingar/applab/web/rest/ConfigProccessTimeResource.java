package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ConfigProccessTimeService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ConfigProccessTimeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConfigProccessTime.
 */
@RestController
@RequestMapping("/api")
public class ConfigProccessTimeResource {

    private final Logger log = LoggerFactory.getLogger(ConfigProccessTimeResource.class);

    private static final String ENTITY_NAME = "configProccessTime";

    private final ConfigProccessTimeService configProccessTimeService;

    public ConfigProccessTimeResource(ConfigProccessTimeService configProccessTimeService) {
        this.configProccessTimeService = configProccessTimeService;
    }

    /**
     * POST  /config-proccess-times : Create a new configProccessTime.
     *
     * @param configProccessTimeDTO the configProccessTimeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new configProccessTimeDTO, or with status 400 (Bad Request) if the configProccessTime has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/config-proccess-times")
    @Timed
    public ResponseEntity<ConfigProccessTimeDTO> createConfigProccessTime(@Valid @RequestBody ConfigProccessTimeDTO configProccessTimeDTO) throws URISyntaxException {
        log.debug("REST request to save ConfigProccessTime : {}", configProccessTimeDTO);
        if (configProccessTimeDTO.getId() != null) {
            throw new BadRequestAlertException("A new configProccessTime cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConfigProccessTimeDTO result = configProccessTimeService.save(configProccessTimeDTO);
        return ResponseEntity.created(new URI("/api/config-proccess-times/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /config-proccess-times : Updates an existing configProccessTime.
     *
     * @param configProccessTimeDTO the configProccessTimeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated configProccessTimeDTO,
     * or with status 400 (Bad Request) if the configProccessTimeDTO is not valid,
     * or with status 500 (Internal Server Error) if the configProccessTimeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/config-proccess-times")
    @Timed
    public ResponseEntity<ConfigProccessTimeDTO> updateConfigProccessTime(@Valid @RequestBody ConfigProccessTimeDTO configProccessTimeDTO) throws URISyntaxException {
        log.debug("REST request to update ConfigProccessTime : {}", configProccessTimeDTO);
        if (configProccessTimeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ConfigProccessTimeDTO result = configProccessTimeService.save(configProccessTimeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, configProccessTimeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /config-proccess-times : get all the configProccessTimes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of configProccessTimes in body
     */
    @GetMapping("/config-proccess-times")
    @Timed
    public ResponseEntity<List<ConfigProccessTimeDTO>> getAllConfigProccessTimes(Pageable pageable) {
        log.debug("REST request to get a page of ConfigProccessTimes");
        Page<ConfigProccessTimeDTO> page = configProccessTimeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/config-proccess-times");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /config-proccess-times/:id : get the "id" configProccessTime.
     *
     * @param id the id of the configProccessTimeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the configProccessTimeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/config-proccess-times/{id}")
    @Timed
    public ResponseEntity<ConfigProccessTimeDTO> getConfigProccessTime(@PathVariable Long id) {
        log.debug("REST request to get ConfigProccessTime : {}", id);
        Optional<ConfigProccessTimeDTO> configProccessTimeDTO = configProccessTimeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(configProccessTimeDTO);
    }

    /**
     * DELETE  /config-proccess-times/:id : delete the "id" configProccessTime.
     *
     * @param id the id of the configProccessTimeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/config-proccess-times/{id}")
    @Timed
    public ResponseEntity<Void> deleteConfigProccessTime(@PathVariable Long id) {
        log.debug("REST request to delete ConfigProccessTime : {}", id);
        configProccessTimeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
