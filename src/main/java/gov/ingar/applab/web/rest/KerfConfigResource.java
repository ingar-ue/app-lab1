package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.KerfConfigService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.KerfConfigDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing KerfConfig.
 */
@RestController
@RequestMapping("/api")
public class KerfConfigResource {

    private final Logger log = LoggerFactory.getLogger(KerfConfigResource.class);

    private static final String ENTITY_NAME = "kerfConfig";

    private final KerfConfigService kerfConfigService;

    public KerfConfigResource(KerfConfigService kerfConfigService) {
        this.kerfConfigService = kerfConfigService;
    }

    /**
     * POST  /kerf-configs : Create a new kerfConfig.
     *
     * @param kerfConfigDTO the kerfConfigDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new kerfConfigDTO, or with status 400 (Bad Request) if the kerfConfig has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/kerf-configs")
    @Timed
    public ResponseEntity<KerfConfigDTO> createKerfConfig(@Valid @RequestBody KerfConfigDTO kerfConfigDTO) throws URISyntaxException {
        log.debug("REST request to save KerfConfig : {}", kerfConfigDTO);
        if (kerfConfigDTO.getId() != null) {
            throw new BadRequestAlertException("A new kerfConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KerfConfigDTO result = kerfConfigService.save(kerfConfigDTO);
        return ResponseEntity.created(new URI("/api/kerf-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /kerf-configs : Updates an existing kerfConfig.
     *
     * @param kerfConfigDTO the kerfConfigDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated kerfConfigDTO,
     * or with status 400 (Bad Request) if the kerfConfigDTO is not valid,
     * or with status 500 (Internal Server Error) if the kerfConfigDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/kerf-configs")
    @Timed
    public ResponseEntity<KerfConfigDTO> updateKerfConfig(@Valid @RequestBody KerfConfigDTO kerfConfigDTO) throws URISyntaxException {
        log.debug("REST request to update KerfConfig : {}", kerfConfigDTO);
        if (kerfConfigDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KerfConfigDTO result = kerfConfigService.save(kerfConfigDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, kerfConfigDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /kerf-configs : get all the kerfConfigs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of kerfConfigs in body
     */
    @GetMapping("/kerf-configs")
    @Timed
    public ResponseEntity<List<KerfConfigDTO>> getAllKerfConfigs(Pageable pageable) {
        log.debug("REST request to get a page of KerfConfigs");
        Page<KerfConfigDTO> page = kerfConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/kerf-configs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /kerf-configs/:id : get the "id" kerfConfig.
     *
     * @param id the id of the kerfConfigDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the kerfConfigDTO, or with status 404 (Not Found)
     */
    @GetMapping("/kerf-configs/{id}")
    @Timed
    public ResponseEntity<KerfConfigDTO> getKerfConfig(@PathVariable Long id) {
        log.debug("REST request to get KerfConfig : {}", id);
        Optional<KerfConfigDTO> kerfConfigDTO = kerfConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(kerfConfigDTO);
    }

    /**
     * DELETE  /kerf-configs/:id : delete the "id" kerfConfig.
     *
     * @param id the id of the kerfConfigDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/kerf-configs/{id}")
    @Timed
    public ResponseEntity<Void> deleteKerfConfig(@PathVariable Long id) {
        log.debug("REST request to delete KerfConfig : {}", id);
        kerfConfigService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
