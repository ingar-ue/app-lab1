package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ProductRelations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProductRelations entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRelationsRepository extends JpaRepository<ProductRelations, Long> {
    Page<ProductRelations> findAllByInterProductCodeIgnoreCaseContaining(Pageable pageable, String keyword);
    Page<ProductRelations> findAllByLongGroupCodeIgnoreCaseContaining(Pageable pageable, String keyword);
    Page<ProductRelations> findAllByFinalProduct1CodeIgnoreCaseContaining(Pageable pageable, String keyword);
    Page<ProductRelations> findAllByFinalProduct2CodeIgnoreCaseContaining(Pageable pageable, String keyword);
    Page<ProductRelations> findAllByFinalProduct3CodeIgnoreCaseContaining(Pageable pageable, String keyword);
}
