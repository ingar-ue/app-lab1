package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.domain.enumeration.ResultStatus;
import gov.ingar.applab.domain.enumeration.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ModelRunDef entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModelRunDefRepository extends JpaRepository<ModelRunDef, Long> {

    Optional<ModelRunDef> findOneByStatus(Status status);

    ModelRunDef findFirstByNameAndStatusOrderByIdDesc(String name, Status status);

    ModelRunDef findFirstByNameOrderByIdDesc(String name);

    Page<ModelRunDef> findAllByOrderByIdDesc(Pageable pageable);

    List<ModelRunDef> findFirst10ByNameAndStatusOrderByIdDesc(String name, Status status);

    List<ModelRunDef> findAllByNameAndResultStatusAndRunDateBetweenOrderByIdAsc(String name, ResultStatus resultStatus, LocalDate begin, LocalDate end);

    Page<ModelRunDef> findAllByNameAndRunDateBetweenOrderByIdDesc(Pageable pageable, String name, LocalDate begin, LocalDate end);
}
