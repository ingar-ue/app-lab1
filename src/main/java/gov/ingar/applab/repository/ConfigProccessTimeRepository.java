package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ConfigProccessTime;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ConfigProccessTime entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfigProccessTimeRepository extends JpaRepository<ConfigProccessTime, Long> {
    List<ConfigProccessTime> findAll();
}
