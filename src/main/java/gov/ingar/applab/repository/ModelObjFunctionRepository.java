package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ModelObjFunction;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ModelObjFunction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModelObjFunctionRepository extends JpaRepository<ModelObjFunction, Long> {

    List<ModelObjFunction> findAllByModelDefId(Long modelDefId);

    ModelObjFunction findByModelDefIdAndName(Long modelDefId, String name);

}
