package gov.ingar.applab.repository;

import gov.ingar.applab.domain.DiameterGroup;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DiameterGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DiameterGroupRepository extends JpaRepository<DiameterGroup, Long> {

}
