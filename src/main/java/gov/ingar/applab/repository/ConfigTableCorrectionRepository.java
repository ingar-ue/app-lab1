package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ConfigTableCorrection;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ConfigTableCorrection entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfigTableCorrectionRepository extends JpaRepository<ConfigTableCorrection, Long> {

}
