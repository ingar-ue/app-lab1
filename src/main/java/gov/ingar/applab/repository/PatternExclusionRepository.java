package gov.ingar.applab.repository;

import gov.ingar.applab.domain.PatternExclusion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PatternExclusion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatternExclusionRepository extends JpaRepository<PatternExclusion, Long> {

}
