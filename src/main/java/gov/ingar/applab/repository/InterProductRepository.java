package gov.ingar.applab.repository;

import gov.ingar.applab.domain.InterProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;


/**
 * Spring Data  repository for the InterProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InterProductRepository extends JpaRepository<InterProduct, Long> {
    Page<InterProduct> findAllByCodeIgnoreCaseContaining(Pageable pageable, String keyword);
    Page<InterProduct> findAllByLongGroupsIgnoreCaseContaining(Pageable pageable, String keyword);
    Page<InterProduct> findAllByTickness(Pageable pageable, BigDecimal keyword);
    Page<InterProduct> findAllByWidth(Pageable pageable, BigDecimal keyword);
}
