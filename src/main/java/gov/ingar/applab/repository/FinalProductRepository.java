package gov.ingar.applab.repository;

import gov.ingar.applab.domain.FinalProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;


/**
 * Spring Data  repository for the FinalProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FinalProductRepository extends JpaRepository<FinalProduct, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE FROM FinalProduct SET price = 0")
    void priceReset();

    @Transactional
    @Modifying
    @Query("UPDATE FROM FinalProduct SET min_demand = 0, max_demand = 0")
    void demandReset();

    @Transactional
    @Modifying
    @Query("UPDATE FROM FinalProduct SET price = price * ?1")
    void priceIncrease(BigDecimal percentage);

    Page<FinalProduct> findAllByCodeIgnoreCaseContaining(Pageable pageable, String keyword);
    Page<FinalProduct> findAllByNameIgnoreCaseContaining(Pageable pageable, String keyword);
    Page<FinalProduct> findAllByPrice(Pageable pageable, BigDecimal keyword);
    Page<FinalProduct> findAllByMinDemand(Pageable pageable, BigDecimal keyword);
    Page<FinalProduct> findAllByMaxDemand(Pageable pageable, BigDecimal keyword);
}
