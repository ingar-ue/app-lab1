package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ResourceCost;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ResourceCost entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResourceCostRepository extends JpaRepository<ResourceCost, Long> {

    @Query("select AVG(value) from ResourceCost")
    Double findAvgCost();

}
