package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ConfigYieldPlanning;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ConfigYieldPlanning entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfigYieldPlanningRepository extends JpaRepository<ConfigYieldPlanning, Long> {

}
