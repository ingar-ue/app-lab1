package gov.ingar.applab.repository;

import gov.ingar.applab.domain.EnvConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the EnvConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EnvConfigRepository extends JpaRepository<EnvConfig, Long> {

    List<EnvConfig> findByParam(String param);

    EnvConfig findOneByParam(String param);

}
