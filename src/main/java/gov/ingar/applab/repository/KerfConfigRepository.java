package gov.ingar.applab.repository;

import gov.ingar.applab.domain.KerfConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the KerfConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KerfConfigRepository extends JpaRepository<KerfConfig, Long> {

}
