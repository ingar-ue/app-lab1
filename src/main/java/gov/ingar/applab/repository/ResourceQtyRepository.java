package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ResourceQty;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ResourceQty entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResourceQtyRepository extends JpaRepository<ResourceQty, Long> {

    int countByQuantity(int quantity);

}
