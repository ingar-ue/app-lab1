package gov.ingar.applab.repository;

import gov.ingar.applab.domain.LongGroup;
import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.domain.enumeration.Status;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the LongGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LongGroupRepository extends JpaRepository<LongGroup, Long> {

    List<LongGroup> findAllByOrderByCodeAsc();

}
