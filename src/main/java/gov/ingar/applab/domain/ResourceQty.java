package gov.ingar.applab.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ResourceQty.
 */
@Entity
@Table(name = "resource_qty")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ResourceQty implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @ManyToOne
    @JsonIgnoreProperties("")
    private DiameterGroup diameterGroup;

    @ManyToOne
    @JsonIgnoreProperties("")
    private LongGroup longGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public ResourceQty quantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public DiameterGroup getDiameterGroup() {
        return diameterGroup;
    }

    public ResourceQty diameterGroup(DiameterGroup diameterGroup) {
        this.diameterGroup = diameterGroup;
        return this;
    }

    public void setDiameterGroup(DiameterGroup diameterGroup) {
        this.diameterGroup = diameterGroup;
    }

    public LongGroup getLongGroup() {
        return longGroup;
    }

    public ResourceQty longGroup(LongGroup longGroup) {
        this.longGroup = longGroup;
        return this;
    }

    public void setLongGroup(LongGroup longGroup) {
        this.longGroup = longGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ResourceQty resourceQty = (ResourceQty) o;
        if (resourceQty.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), resourceQty.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ResourceQty{" +
            "id=" + getId() +
            ", quantity=" + getQuantity() +
            "}";
    }
}
