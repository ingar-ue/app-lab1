package gov.ingar.applab.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A FinalProduct.
 */
@Entity
@Table(name = "final_product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FinalProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 15)
    @Column(name = "code", length = 15, nullable = false)
    private String code;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Min(value = 0)
    @Column(name = "min_demand", nullable = false)
    private Integer minDemand;

    @NotNull
    @Min(value = 0)
    @Column(name = "max_demand", nullable = false)
    private Integer maxDemand;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "price", precision = 10, scale = 2, nullable = false)
    private BigDecimal price;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public FinalProduct code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public FinalProduct name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinDemand() {
        return minDemand;
    }

    public FinalProduct minDemand(Integer minDemand) {
        this.minDemand = minDemand;
        return this;
    }

    public void setMinDemand(Integer minDemand) {
        this.minDemand = minDemand;
    }

    public Integer getMaxDemand() {
        return maxDemand;
    }

    public FinalProduct maxDemand(Integer maxDemand) {
        this.maxDemand = maxDemand;
        return this;
    }

    public void setMaxDemand(Integer maxDemand) {
        this.maxDemand = maxDemand;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public FinalProduct price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FinalProduct finalProduct = (FinalProduct) o;
        if (finalProduct.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), finalProduct.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FinalProduct{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", minDemand=" + getMinDemand() +
            ", maxDemand=" + getMaxDemand() +
            ", price=" + getPrice() +
            "}";
    }
}
