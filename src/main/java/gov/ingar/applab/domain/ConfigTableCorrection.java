package gov.ingar.applab.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A ConfigTableCorrection.
 */
@Entity
@Table(name = "config_table_correction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ConfigTableCorrection implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "value_1", precision = 10, scale = 2)
    private BigDecimal value1;

    @Column(name = "value_2", precision = 10, scale = 2)
    private BigDecimal value2;

    @Column(name = "value_3", precision = 10, scale = 2)
    private BigDecimal value3;

    @Column(name = "value_4", precision = 10, scale = 2)
    private BigDecimal value4;

    @Column(name = "value_5", precision = 10, scale = 2)
    private BigDecimal value5;

    @Column(name = "value_6", precision = 10, scale = 2)
    private BigDecimal value6;

    @ManyToOne
    @JsonIgnoreProperties("")
    private DiameterGroup diameterGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue1() {
        return value1;
    }

    public ConfigTableCorrection value1(BigDecimal value1) {
        this.value1 = value1;
        return this;
    }

    public void setValue1(BigDecimal value1) {
        this.value1 = value1;
    }

    public BigDecimal getValue2() {
        return value2;
    }

    public ConfigTableCorrection value2(BigDecimal value2) {
        this.value2 = value2;
        return this;
    }

    public void setValue2(BigDecimal value2) {
        this.value2 = value2;
    }

    public BigDecimal getValue3() {
        return value3;
    }

    public ConfigTableCorrection value3(BigDecimal value3) {
        this.value3 = value3;
        return this;
    }

    public void setValue3(BigDecimal value3) {
        this.value3 = value3;
    }

    public BigDecimal getValue4() {
        return value4;
    }

    public ConfigTableCorrection value4(BigDecimal value4) {
        this.value4 = value4;
        return this;
    }

    public void setValue4(BigDecimal value4) {
        this.value4 = value4;
    }

    public BigDecimal getValue5() {
        return value5;
    }

    public ConfigTableCorrection value5(BigDecimal value5) {
        this.value5 = value5;
        return this;
    }

    public void setValue5(BigDecimal value5) {
        this.value5 = value5;
    }

    public BigDecimal getValue6() {
        return value6;
    }

    public ConfigTableCorrection value6(BigDecimal value6) {
        this.value6 = value6;
        return this;
    }

    public void setValue6(BigDecimal value6) {
        this.value6 = value6;
    }

    public DiameterGroup getDiameterGroup() {
        return diameterGroup;
    }

    public ConfigTableCorrection diameterGroup(DiameterGroup diameterGroup) {
        this.diameterGroup = diameterGroup;
        return this;
    }

    public void setDiameterGroup(DiameterGroup diameterGroup) {
        this.diameterGroup = diameterGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConfigTableCorrection configTableCorrection = (ConfigTableCorrection) o;
        if (configTableCorrection.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), configTableCorrection.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConfigTableCorrection{" +
            "id=" + getId() +
            ", value1=" + getValue1() +
            ", value2=" + getValue2() +
            ", value3=" + getValue3() +
            ", value4=" + getValue4() +
            ", value5=" + getValue5() +
            ", value6=" + getValue6() +
            "}";
    }
}
