package gov.ingar.applab.domain.enumeration;

/**
 * The ResultStatus enumeration.
 */
public enum ResultStatus {
    DISCARDED, INANALYSIS, APPLIED
}
