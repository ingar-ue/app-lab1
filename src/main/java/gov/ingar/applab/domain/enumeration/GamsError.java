package gov.ingar.applab.domain.enumeration;

/**
 * The GamsError enumeration.
 */
public enum GamsError {
    ENVIRONMENT_CONFIG,
    FOLDER_PATH_CONFIG,
    EXECUTION,
    COMPILATION,
    GDX_EXCEPTION,
    GAMSX_EXCEPTION,
    OPT_INIT_EXCEPTION,
    OPT_READ_EXCEPTION,
    FILE_HANDLING,
    GENERAL,
    OTHER
}
