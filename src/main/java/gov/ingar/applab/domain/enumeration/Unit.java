package gov.ingar.applab.domain.enumeration;

/**
 * The Unit enumeration.
 */
public enum Unit {
    CM, MM, M, PIE, PULGADA
}
