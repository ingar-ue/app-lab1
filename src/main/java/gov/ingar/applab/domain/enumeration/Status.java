package gov.ingar.applab.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    READY, RUNNING, FINISHED, ERROR
}
