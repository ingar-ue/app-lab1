package gov.ingar.applab.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import gov.ingar.applab.domain.enumeration.Unit;

/**
 * A InterProduct.
 */
@Entity
@Table(name = "inter_product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InterProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "tickness", precision = 10, scale = 2, nullable = false)
    private BigDecimal tickness;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "width", precision = 10, scale = 2, nullable = false)
    private BigDecimal width;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "val_type", nullable = false)
    private Unit valType;

    @Column(name = "long_groups")
    private String longGroups;

    @Column(name = "description")
    private String description;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public InterProduct code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getTickness() {
        return tickness;
    }

    public InterProduct tickness(BigDecimal tickness) {
        this.tickness = tickness;
        return this;
    }

    public void setTickness(BigDecimal tickness) {
        this.tickness = tickness;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public InterProduct width(BigDecimal width) {
        this.width = width;
        return this;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    public Unit getValType() {
        return valType;
    }

    public InterProduct valType(Unit valType) {
        this.valType = valType;
        return this;
    }

    public void setValType(Unit valType) {
        this.valType = valType;
    }

    public String getLongGroups() {
        return longGroups;
    }

    public InterProduct longGroups(String longGroups) {
        this.longGroups = longGroups;
        return this;
    }

    public void setLongGroups(String longGroups) {
        this.longGroups = longGroups;
    }

    public String getDescription() {
        return description;
    }

    public InterProduct description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InterProduct interProduct = (InterProduct) o;
        if (interProduct.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), interProduct.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InterProduct{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", tickness=" + getTickness() +
            ", width=" + getWidth() +
            ", valType='" + getValType() + "'" +
            ", longGroups='" + getLongGroups() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
