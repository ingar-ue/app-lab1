package gov.ingar.applab.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ProductRelations.
 */
@Entity
@Table(name = "product_relations")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProductRelations implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "quantity_1", nullable = false)
    private Integer quantity1;

    @Column(name = "quantity_2")
    private Integer quantity2;

    @Column(name = "quantity_3")
    private Integer quantity3;

    @ManyToOne
    @JsonIgnoreProperties("")
    private FinalProduct finalProduct1;

    @ManyToOne
    @JsonIgnoreProperties("")
    private FinalProduct finalProduct2;

    @ManyToOne
    @JsonIgnoreProperties("")
    private FinalProduct finalProduct3;

    @ManyToOne
    @JsonIgnoreProperties("")
    private InterProduct interProduct;

    @ManyToOne
    @JsonIgnoreProperties("")
    private LongGroup longGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity1() {
        return quantity1;
    }

    public ProductRelations quantity1(Integer quantity1) {
        this.quantity1 = quantity1;
        return this;
    }

    public void setQuantity1(Integer quantity1) {
        this.quantity1 = quantity1;
    }

    public Integer getQuantity2() {
        return quantity2;
    }

    public ProductRelations quantity2(Integer quantity2) {
        this.quantity2 = quantity2;
        return this;
    }

    public void setQuantity2(Integer quantity2) {
        this.quantity2 = quantity2;
    }

    public Integer getQuantity3() {
        return quantity3;
    }

    public ProductRelations quantity3(Integer quantity3) {
        this.quantity3 = quantity3;
        return this;
    }

    public void setQuantity3(Integer quantity3) {
        this.quantity3 = quantity3;
    }

    public FinalProduct getFinalProduct1() {
        return finalProduct1;
    }

    public ProductRelations finalProduct1(FinalProduct finalProduct) {
        this.finalProduct1 = finalProduct;
        return this;
    }

    public void setFinalProduct1(FinalProduct finalProduct) {
        this.finalProduct1 = finalProduct;
    }

    public FinalProduct getFinalProduct2() {
        return finalProduct2;
    }

    public ProductRelations finalProduct2(FinalProduct finalProduct) {
        this.finalProduct2 = finalProduct;
        return this;
    }

    public void setFinalProduct2(FinalProduct finalProduct) {
        this.finalProduct2 = finalProduct;
    }

    public FinalProduct getFinalProduct3() {
        return finalProduct3;
    }

    public ProductRelations finalProduct3(FinalProduct finalProduct) {
        this.finalProduct3 = finalProduct;
        return this;
    }

    public void setFinalProduct3(FinalProduct finalProduct) {
        this.finalProduct3 = finalProduct;
    }

    public InterProduct getInterProduct() {
        return interProduct;
    }

    public ProductRelations interProduct(InterProduct interProduct) {
        this.interProduct = interProduct;
        return this;
    }

    public void setInterProduct(InterProduct interProduct) {
        this.interProduct = interProduct;
    }

    public LongGroup getLongGroup() {
        return longGroup;
    }

    public ProductRelations longGroup(LongGroup longGroup) {
        this.longGroup = longGroup;
        return this;
    }

    public void setLongGroup(LongGroup longGroup) {
        this.longGroup = longGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProductRelations productRelations = (ProductRelations) o;
        if (productRelations.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), productRelations.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProductRelations{" +
            "id=" + getId() +
            ", quantity1=" + getQuantity1() +
            ", quantity2=" + getQuantity2() +
            ", quantity3=" + getQuantity3() +
            "}";
    }
}
