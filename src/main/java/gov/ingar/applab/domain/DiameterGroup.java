package gov.ingar.applab.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import gov.ingar.applab.domain.enumeration.Unit;

/**
 * A DiameterGroup.
 */
@Entity
@Table(name = "diameter_group")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DiameterGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @Column(name = "min_value", precision = 10, scale = 2, nullable = false)
    private BigDecimal minValue;

    @NotNull
    @Column(name = "max_value", precision = 10, scale = 2, nullable = false)
    private BigDecimal maxValue;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "val_type", nullable = false)
    private Unit valType;

    @NotNull
    @Column(name = "jhi_schema", nullable = false)
    private String schema;

    @NotNull
    @Column(name = "tables_central", nullable = false)
    private Integer tablesCentral;

    @Column(name = "tables_side")
    private Integer tablesSide;

    @Column(name = "tables_top")
    private Integer tablesTop;

    @Column(name = "long_groups")
    private String longGroups;

    @NotNull
    @Min(value = 0)
    @Max(value = 100)
    @Column(name = "yield_index", nullable = false)
    private Integer yieldIndex;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public DiameterGroup code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getMinValue() {
        return minValue;
    }

    public DiameterGroup minValue(BigDecimal minValue) {
        this.minValue = minValue;
        return this;
    }

    public void setMinValue(BigDecimal minValue) {
        this.minValue = minValue;
    }

    public BigDecimal getMaxValue() {
        return maxValue;
    }

    public DiameterGroup maxValue(BigDecimal maxValue) {
        this.maxValue = maxValue;
        return this;
    }

    public void setMaxValue(BigDecimal maxValue) {
        this.maxValue = maxValue;
    }

    public Unit getValType() {
        return valType;
    }

    public DiameterGroup valType(Unit valType) {
        this.valType = valType;
        return this;
    }

    public void setValType(Unit valType) {
        this.valType = valType;
    }

    public String getSchema() {
        return schema;
    }

    public DiameterGroup schema(String schema) {
        this.schema = schema;
        return this;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public Integer getTablesCentral() {
        return tablesCentral;
    }

    public DiameterGroup tablesCentral(Integer tablesCentral) {
        this.tablesCentral = tablesCentral;
        return this;
    }

    public void setTablesCentral(Integer tablesCentral) {
        this.tablesCentral = tablesCentral;
    }

    public Integer getTablesSide() {
        return tablesSide;
    }

    public DiameterGroup tablesSide(Integer tablesSide) {
        this.tablesSide = tablesSide;
        return this;
    }

    public void setTablesSide(Integer tablesSide) {
        this.tablesSide = tablesSide;
    }

    public Integer getTablesTop() {
        return tablesTop;
    }

    public DiameterGroup tablesTop(Integer tablesTop) {
        this.tablesTop = tablesTop;
        return this;
    }

    public void setTablesTop(Integer tablesTop) {
        this.tablesTop = tablesTop;
    }

    public String getLongGroups() {
        return longGroups;
    }

    public DiameterGroup longGroups(String longGroups) {
        this.longGroups = longGroups;
        return this;
    }

    public void setLongGroups(String longGroups) {
        this.longGroups = longGroups;
    }

    public Integer getYieldIndex() {
        return yieldIndex;
    }

    public DiameterGroup yieldIndex(Integer yieldIndex) {
        this.yieldIndex = yieldIndex;
        return this;
    }

    public void setYieldIndex(Integer yieldIndex) {
        this.yieldIndex = yieldIndex;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DiameterGroup diameterGroup = (DiameterGroup) o;
        if (diameterGroup.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), diameterGroup.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DiameterGroup{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", minValue=" + getMinValue() +
            ", maxValue=" + getMaxValue() +
            ", valType='" + getValType() + "'" +
            ", schema='" + getSchema() + "'" +
            ", tablesCentral=" + getTablesCentral() +
            ", tablesSide=" + getTablesSide() +
            ", tablesTop=" + getTablesTop() +
            ", longGroups='" + getLongGroups() + "'" +
            ", yieldIndex=" + getYieldIndex() +
            "}";
    }
}
