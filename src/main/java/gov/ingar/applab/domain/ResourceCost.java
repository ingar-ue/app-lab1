package gov.ingar.applab.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A ResourceCost.
 */
@Entity
@Table(name = "resource_cost")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ResourceCost implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "jhi_value", precision = 10, scale = 2, nullable = false)
    private BigDecimal value;

    @ManyToOne
    @JsonIgnoreProperties("")
    private DiameterGroup diameterGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public ResourceCost value(BigDecimal value) {
        this.value = value;
        return this;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public DiameterGroup getDiameterGroup() {
        return diameterGroup;
    }

    public ResourceCost diameterGroup(DiameterGroup diameterGroup) {
        this.diameterGroup = diameterGroup;
        return this;
    }

    public void setDiameterGroup(DiameterGroup diameterGroup) {
        this.diameterGroup = diameterGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ResourceCost resourceCost = (ResourceCost) o;
        if (resourceCost.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), resourceCost.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ResourceCost{" +
            "id=" + getId() +
            ", value=" + getValue() +
            "}";
    }
}
