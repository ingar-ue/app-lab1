package gov.ingar.applab.optimization;

import gov.ingar.applab.optimization.dto.ResultLineChartDTO;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

@Service
@Transactional(readOnly = true)
public class OptimizationPoint1ResultsService {
    private final FileService fileService;
    private final String sheetName = "EsquemasyRendimiento";

    public OptimizationPoint1ResultsService(FileService fileService) {
        this.fileService = fileService;
    }

    /**
     * Funcion que obtiene cantidad de patrones por porcentaje de rendimiento
     *
     * @param modelRunDefId
     * @return ResultLineChartDTO
     * @throws IOException
     */
    public ResultLineChartDTO getChart1(Long modelRunDefId) throws IOException {
        Sheet sheet = this.fileService.getResultSheet(modelRunDefId, sheetName);
        int rowCount = sheet.getLastRowNum();

        List<Double> performances = Arrays.asList(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

        for (int i = 0; i < rowCount + 1; i++) {
            Row row = sheet.getRow(i);
            if (row != null) {
                Cell last = row.getCell(row.getLastCellNum() - 1);
                if (last != null && last.getCellTypeEnum() == CellType.NUMERIC) {
                    //Increase value of performance
                    int position = (int) Math.floor(last.getNumericCellValue() / 10);
                    performances.set(position, performances.get(position) + 1);
                }
            }
        }

        List<String> valuesX = Arrays.asList("0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100");
        return new ResultLineChartDTO("", "% Rendimiento", "Cantidad de patrones", valuesX, performances);
    }

    /**
     * Funcion que obtiene el maximo porcentaje de rendimiento por cada tipo de diametro
     *
     * @param modelRunDefId
     * @return ResultLineChartDTO
     * @throws IOException
     */
    public ResultLineChartDTO getChart2(Long modelRunDefId) throws IOException {
        Sheet sheet = this.fileService.getResultSheet(modelRunDefId, sheetName);
        int rowCount = sheet.getLastRowNum();

        Map<String, Double> maxMap = new LinkedHashMap<>();

        for (int i = 0; i < rowCount + 1; i++) {
            Row row = sheet.getRow(i);
            if (row != null) {
                Cell last = row.getCell(row.getLastCellNum() - 1);
                Cell first = row.getCell(row.getFirstCellNum());
                if (first != null && last != null && last.getCellTypeEnum() == CellType.NUMERIC) {
                    if (maxMap.get(first.getStringCellValue()) == null || maxMap.get(first.getStringCellValue()) <= last.getNumericCellValue())
                        maxMap.put(first.getStringCellValue(), last.getNumericCellValue());
                }
            }
        }

        return new ResultLineChartDTO("", "Diámetro", "% Rendimiento", new ArrayList<>(maxMap.keySet()), new ArrayList<>(maxMap.values()));
    }

    /**
     * Funcion que obtiene cantidad de tablas por cada tipo de corte de todos los patrones evaluados
     *
     * @param modelRunDefId
     * @return ResultLineChartDTO
     * @throws IOException
     */
    public ResultLineChartDTO getChart3(Long modelRunDefId) throws IOException {
        Sheet sheet = this.fileService.getResultSheet(modelRunDefId, sheetName);
        int rowCount = sheet.getLastRowNum();

        Map<String, Double> sumMap = new LinkedHashMap<>();

        int firstRow = -1;

        for (int i = 0; i < rowCount + 1; i++) {
            Row row = sheet.getRow(i);

            if (row != null) {
                if (firstRow == -1)
                    firstRow = row.getFirstCellNum();

                Cell last = row.getCell(row.getLastCellNum() - 1);
                for (int j = firstRow; j < row.getLastCellNum(); j++) {
                    Cell currentCell = row.getCell(j);
                    if (currentCell != null) {
                        if (last != null && last.getCellTypeEnum() == CellType.NUMERIC) {
                            //add col to count type
                            if (currentCell.getCellTypeEnum() == CellType.NUMERIC && j != row.getLastCellNum() - 1)
                                sumMap.put((String) sumMap.keySet().toArray()[j - firstRow], (double) sumMap.values().toArray()[j - firstRow] + currentCell.getNumericCellValue());
                        } else {
                            //initialize count and labels type
                            if (j != row.getLastCellNum() - 1)
                                sumMap.put(currentCell.getStringCellValue(), 0.0);
                        }
                    }
                }
            }
        }

        return new ResultLineChartDTO("", "Cortes", "Cantidad", new ArrayList<>(sumMap.keySet()), new ArrayList<>(sumMap.values()));
    }
}
