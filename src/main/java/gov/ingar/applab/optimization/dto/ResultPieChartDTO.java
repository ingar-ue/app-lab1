package gov.ingar.applab.optimization.dto;

import java.io.Serializable;
import java.util.List;

public class ResultPieChartDTO implements Serializable {
    private String title;
    private List<String> labels;
    private List<Double> values;

    public ResultPieChartDTO(String title, List<String> labels, List<Double> values) {
        this.title = title;
        this.labels = labels;
        this.values = values;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getLabels() {
        return labels;
    }

    public List<Double> getValues() {
        return values;
    }

}
