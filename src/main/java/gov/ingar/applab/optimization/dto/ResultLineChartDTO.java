package gov.ingar.applab.optimization.dto;

import java.io.Serializable;
import java.util.List;

public class ResultLineChartDTO implements Serializable {
    private String title;
    private String labelX;
    private String labelY;
    private List<String> valuesX;
    private List<Double> valuesY;

    public ResultLineChartDTO(String title, String labelX, String labelY, List<String> valuesX, List<Double> valuesY) {
        this.title = title;
        this.labelX = labelX;
        this.labelY = labelY;
        this.valuesX = valuesX;
        this.valuesY = valuesY;
    }

    public String getTitle() {
        return title;
    }

    public String getLabelX() {
        return labelX;
    }

    public String getLabelY() {
        return labelY;
    }

    public List<String> getValuesX() {
        return valuesX;
    }

    public List<Double> getValuesY() {
        return valuesY;
    }

}
