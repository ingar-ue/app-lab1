package gov.ingar.applab.optimization.dto;

import gov.ingar.applab.service.dto.*;

import java.util.List;

public class OptimizationPoint2DTO extends OptimizationPointDTO {

    private List<FinalProductDTO> finalProducts;
    private List<ProductRelationsDTO> productRelations;
    private List<ResourceCostDTO> resourceCosts;
    private List<ResourceQtyDTO> resourceQtys;
    private List<ModelRunConfigDTO> modelRunConfigs;
    private ModelRunDefDTO optimizationPoint1;

    public List<FinalProductDTO> getFinalProducts() {
        return finalProducts;
    }

    public void setFinalProducts(List<FinalProductDTO> finalProducts) {
        this.finalProducts = finalProducts;
    }

    public List<ProductRelationsDTO> getProductRelations() {
        return productRelations;
    }

    public void setProductRelations(List<ProductRelationsDTO> productRelations) {
        this.productRelations = productRelations;
    }

    public List<ResourceCostDTO> getResourceCosts() {
        return resourceCosts;
    }

    public void setResourceCosts(List<ResourceCostDTO> resourceCosts) {
        this.resourceCosts = resourceCosts;
    }

    public List<ResourceQtyDTO> getResourceQtys() {
        return resourceQtys;
    }

    public void setResourceQtys(List<ResourceQtyDTO> resourceQtys) {
        this.resourceQtys = resourceQtys;
    }

    public List<ModelRunConfigDTO> getModelRunConfigs() {
        return modelRunConfigs;
    }

    public void setModelRunConfigs(List<ModelRunConfigDTO> modelRunConfigs) {
        this.modelRunConfigs = modelRunConfigs;
    }

    public ModelRunDefDTO getOptimizationPoint1() {
        return optimizationPoint1;
    }

    public void setOptimizationPoint1(ModelRunDefDTO optimizationPoint1) {
        this.optimizationPoint1 = optimizationPoint1;
    }
}
