package gov.ingar.applab.optimization.dto;

import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.service.dto.DiameterGroupDTO;
import gov.ingar.applab.service.dto.InterProductDTO;
import gov.ingar.applab.service.dto.LongGroupDTO;

import java.io.Serializable;
import java.util.List;

public abstract class OptimizationPointDTO implements Serializable {
    private List<DiameterGroupDTO> diameters;
    private List<LongGroupDTO> longs;
    private List<InterProductDTO> interProducts;
    private ModelRunDef modelRunDef;
    private Long modelDefId;

    public List<InterProductDTO> getInterProducts() {
        return interProducts;
    }

    public void setInterProducts(List<InterProductDTO> interProducts) {
        this.interProducts = interProducts;
    }

    public List<LongGroupDTO> getLongs() {
        return longs;
    }

    public void setLongs(List<LongGroupDTO> longs) {
        this.longs = longs;
    }

    public List<DiameterGroupDTO> getDiameters() {
        return diameters;
    }

    public void setDiameters(List<DiameterGroupDTO> diameters) {
        this.diameters = diameters;
    }

    public ModelRunDef getModelRunDef() {
        return modelRunDef;
    }

    public void setModelRunDef(ModelRunDef modelRunDef) {
        this.modelRunDef = modelRunDef;
    }

    public Long getModelDefId() {
        return modelDefId;
    }

    public void setModelDefId(Long modelDefId) {
        this.modelDefId = modelDefId;
    }

}
