package gov.ingar.applab.optimization.dto;

import gov.ingar.applab.service.dto.*;

import java.io.Serializable;
import java.util.List;

public class OptimizationDashboardDTO implements Serializable {

    private String languageSolver;
    private int modelsCount;
    private int diametersCount;
    private int longsCount;
    private int interProductsCount;
    private int finalProductsCount;
    private int resourceQty;
    private double resourceCostAvg;
    private ModelRunDefDTO lastOpt1Execution;
    private ModelRunDefDTO lastOpt2Execution;
    private List<ResultLineChartDTO> dailyProduction;
    private ResultPieChartDTO resourceCosts;

    public String getLanguageSolver() {
        return languageSolver;
    }

    public void setLanguageSolver(String languageSolver) {
        this.languageSolver = languageSolver;
    }

    public int getModelsCount() {
        return modelsCount;
    }

    public void setModelsCount(int modelsCount) {
        this.modelsCount = modelsCount;
    }

    public int getDiametersCount() {
        return diametersCount;
    }

    public void setDiametersCount(int diametersCount) {
        this.diametersCount = diametersCount;
    }

    public int getLongsCount() {
        return longsCount;
    }

    public void setLongsCount(int longsCount) {
        this.longsCount = longsCount;
    }

    public int getInterProductsCount() {
        return interProductsCount;
    }

    public void setInterProductsCount(int interProductsCount) {
        this.interProductsCount = interProductsCount;
    }

    public int getFinalProductsCount() {
        return finalProductsCount;
    }

    public void setFinalProductsCount(int finalProductsCount) {
        this.finalProductsCount = finalProductsCount;
    }

    public int getResourceQty() {
        return resourceQty;
    }

    public void setResourceQty(int resourceQty) {
        this.resourceQty = resourceQty;
    }

    public double getResourceCostAvg() {
        return resourceCostAvg;
    }

    public void setResourceCostAvg(double resourceCostAvg) {
        this.resourceCostAvg = resourceCostAvg;
    }

    public ModelRunDefDTO getLastOpt1Execution() {
        return lastOpt1Execution;
    }

    public void setLastOpt1Execution(ModelRunDefDTO lastOpt1Execution) {
        this.lastOpt1Execution = lastOpt1Execution;
    }

    public ModelRunDefDTO getLastOpt2Execution() {
        return lastOpt2Execution;
    }

    public void setLastOpt2Execution(ModelRunDefDTO lastOpt2Execution) {
        this.lastOpt2Execution = lastOpt2Execution;
    }

    public List<ResultLineChartDTO> getDailyProduction() {
        return dailyProduction;
    }

    public void setDailyProduction(List<ResultLineChartDTO> dailyProduction) {
        this.dailyProduction = dailyProduction;
    }

    public ResultPieChartDTO getResourceCosts() {
        return resourceCosts;
    }

    public void setResourceCosts(ResultPieChartDTO resourceCosts) {
        this.resourceCosts = resourceCosts;
    }
}
