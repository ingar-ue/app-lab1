package gov.ingar.applab.optimization.dto;

import gov.ingar.applab.domain.enumeration.Status;

public class ModelRunDefMessage {
    private String name;
    private Status status;
    private Integer progress;
    private String result;
    private Long id;

    public ModelRunDefMessage(String name, Long id, Status status, Integer progress) {
        this.name = name;
        this.id = id;
        this.status = status;
        this.progress = progress;
        this.result = "";
    }

    public ModelRunDefMessage(String name, Long id, Status status, Integer progress, String result) {
        this.name = name;
        this.id = id;
        this.status = status;
        this.progress = progress;
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
