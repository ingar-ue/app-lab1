package gov.ingar.applab.optimization.dto;

import gov.ingar.applab.service.dto.KerfConfigDTO;
import java.util.List;

public class OptimizationPoint1DTO extends OptimizationPointDTO {

    private List<KerfConfigDTO> kerfConfigs;

    public List<KerfConfigDTO> getKerfConfigs() {
        return kerfConfigs;
    }

    public void setKerfConfigs(List<KerfConfigDTO> kerfConfigs) {
        this.kerfConfigs = kerfConfigs;
    }



}
