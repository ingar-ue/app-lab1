package gov.ingar.applab.optimization;

import gov.ingar.applab.domain.ModelDef;
import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.optimization.dto.OptimizationPoint1DTO;
import gov.ingar.applab.optimization.dto.OptimizationPointDTO;
import gov.ingar.applab.optimization.gams.GamsOptimizationPoint1Runner;
import gov.ingar.applab.repository.DiameterGroupRepository;
import gov.ingar.applab.repository.InterProductRepository;
import gov.ingar.applab.repository.KerfConfigRepository;
import gov.ingar.applab.repository.LongGroupRepository;
import gov.ingar.applab.security.SecurityUtils;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.ModelDefService;
import gov.ingar.applab.service.dto.DiameterGroupDTO;
import gov.ingar.applab.service.dto.InterProductDTO;
import gov.ingar.applab.service.dto.KerfConfigDTO;
import gov.ingar.applab.service.dto.LongGroupDTO;
import gov.ingar.applab.service.mapper.DiameterGroupMapper;
import gov.ingar.applab.service.mapper.InterProductMapper;
import gov.ingar.applab.service.mapper.KerfConfigMapper;
import gov.ingar.applab.service.mapper.LongGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OptimizationPoint1Service implements OptimizationPointService {

    private final Logger log = LoggerFactory.getLogger(OptimizationPoint1Service.class);

    private final ModelDefService modelDefService;
    private final ModelConfigService modelConfigService;

    private final DiameterGroupRepository diameterGroupRepository;
    private final LongGroupRepository longGroupRepository;
    private final InterProductRepository interProductRepository;
    private final KerfConfigRepository kerfConfigRepository;

    private final DiameterGroupMapper diameterGroupMapper;
    private final LongGroupMapper longGroupMapper;
    private final InterProductMapper interProductMapper;
    private final KerfConfigMapper kerfConfigMapper;

    private final GamsOptimizationPoint1Runner gamsOptimizationPoint1Runner;

    public OptimizationPoint1Service(DiameterGroupRepository diameterGroupRepository,LongGroupRepository longGroupRepository,
                                 InterProductRepository interProductRepository,KerfConfigRepository kerfConfigRepository,
                                 DiameterGroupMapper diameterGroupMapper, LongGroupMapper longGroupMapper,
                                 InterProductMapper interProductMapper, KerfConfigMapper kerfConfigMapper,
                                 ModelDefService modelDefService, ModelConfigService modelConfigService,
                                 GamsOptimizationPoint1Runner gamsOptimizationPoint1Runner) {
        this.modelDefService = modelDefService;
        this.modelConfigService = modelConfigService;

        this.diameterGroupRepository = diameterGroupRepository;
        this.longGroupRepository = longGroupRepository;
        this.interProductRepository = interProductRepository;
        this.kerfConfigRepository = kerfConfigRepository;

        this.diameterGroupMapper = diameterGroupMapper;
        this.longGroupMapper = longGroupMapper;
        this.interProductMapper = interProductMapper;
        this.kerfConfigMapper = kerfConfigMapper;
        this.gamsOptimizationPoint1Runner = gamsOptimizationPoint1Runner;
    }

    @Override
    public OptimizationPointDTO loadCase(){
        ModelRunDef runDef = new ModelRunDef();
        ModelDef def = modelDefService.findOneByName("Pattern Generator");
        OptimizationPoint1DTO op1 = loadCaseInput();

        runDef.setName(def.getName());
        runDef.setLanguage(def.getLanguage());
        runDef.setSolver(def.getSolver());
        runDef.setVersion(def.getCurrentVersion());
        runDef.setProgress(0);
        runDef.setUser(SecurityUtils.getCurrentUsername());

        op1.setModelRunDef(runDef);
        op1.setModelDefId(def.getId());

        return op1;
    }

    private OptimizationPoint1DTO loadCaseInput(){
        OptimizationPoint1DTO dto = new OptimizationPoint1DTO();

        List<DiameterGroupDTO> diameters = this.diameterList();
        List<LongGroupDTO> longs = this.longList();
        List<InterProductDTO> interProducts = this.interProductList();
        List<KerfConfigDTO> kerfConfig = this.kerfList();

        dto.setDiameters(diameters);
        dto.setLongs(longs);
        dto.setInterProducts(interProducts);
        dto.setKerfConfigs(kerfConfig);

        return dto;
    }

    private List<DiameterGroupDTO> diameterList() {
        return diameterGroupMapper.toDto(diameterGroupRepository.findAll());
    }

    private List<LongGroupDTO> longList() {
        return longGroupMapper.toDto(longGroupRepository.findAll());
    }

    private List<InterProductDTO> interProductList() {
        return interProductMapper.toDto(interProductRepository.findAll());
    }

    private List<KerfConfigDTO> kerfList() {
        return kerfConfigMapper.toDto(kerfConfigRepository.findAll());
    }

    @Override
    public void runOptimizationPoint(OptimizationPointDTO optimizationPointDTO) {
        gamsOptimizationPoint1Runner.runOptimizationPoint1((OptimizationPoint1DTO) optimizationPointDTO);
    }

}
