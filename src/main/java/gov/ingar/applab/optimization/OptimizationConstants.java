package gov.ingar.applab.optimization;

public class OptimizationConstants {

    // Configuración General
    // Los nombres deben ser iguales a los parametros de la clase EnvConfig (app_env_config.csv)
    public static final String ENV_MATH_LANGUAGE = "Math-Language";
    public static final String ENV_MATH_SOLVER = "Math-Solver";
    public static final String ENV_MATH_SOFTWARE_PATH = "Folder-Optimizer";
    public static final String ENV_OPT1_EXEC_PATH = "Folder-Opt-Point-1";
    public static final String ENV_OPT2_EXEC_PATH = "Folder-Opt-Point-2";
    public static final String ENV_MODEL_SRC_RESOURCE = "spa-model/";

}
