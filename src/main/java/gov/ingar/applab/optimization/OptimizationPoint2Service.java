package gov.ingar.applab.optimization;

import gov.ingar.applab.domain.ModelDef;
import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.optimization.dto.OptimizationPoint2DTO;
import gov.ingar.applab.optimization.dto.OptimizationPointDTO;
import gov.ingar.applab.optimization.gams.GamsOptimizationPoint2Runner;
import gov.ingar.applab.repository.*;
import gov.ingar.applab.security.SecurityUtils;
import gov.ingar.applab.service.ModelDefService;
import gov.ingar.applab.service.ModelRunDefService;
import gov.ingar.applab.service.dto.*;
import gov.ingar.applab.service.mapper.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OptimizationPoint2Service implements OptimizationPointService {


    private final ModelDefService modelDefService;
    private final ModelRunDefService modelRunDefService;

    private final FinalProductRepository finalProductRepository;
    private final DiameterGroupRepository diameterGroupRepository;
    private final LongGroupRepository longGroupRepository;
    private final InterProductRepository interProductRepository;
    private final ProductRelationsRepository productRelationsRepository;
    private final ResourceQtyRepository resourceQtyRepository;
    private final ResourceCostRepository resourceCostRepository;

    private final FinalProductMapper finalProductMapper;
    private final DiameterGroupMapper diameterGroupMapper;
    private final LongGroupMapper longGroupMapper;
    private final InterProductMapper interProductMapper;
    private final ProductRelationsMapper productRelationsMapper;
    private final ResourceQtyMapper resourceQtyMapper;
    private final ResourceCostMapper resourceCostMapper;

    private final GamsOptimizationPoint2Runner gamsOptimizationPoint2Runner;

    public OptimizationPoint2Service(ModelDefService modelDefService, ModelRunDefService modelRunDefService, FinalProductRepository finalProductRepository,
                                     DiameterGroupRepository diameterGroupRepository, LongGroupRepository longGroupRepository,
                                     InterProductRepository interProductRepository, ProductRelationsRepository productRelationsRepository,
                                     ResourceQtyRepository resourceQtyRepository, ResourceCostRepository resourceCostRepository,
                                     FinalProductMapper finalProductMapper, DiameterGroupMapper diameterGroupMapper,
                                     LongGroupMapper longGroupMapper, InterProductMapper interProductMapper,
                                     ProductRelationsMapper productRelationsMapper, ResourceQtyMapper resourceQtyMapper,
                                     ResourceCostMapper resourceCostMapper, GamsOptimizationPoint2Runner gamsOptimizationPoint2Runner) {
        this.modelDefService = modelDefService;
        this.modelRunDefService = modelRunDefService;

        this.finalProductRepository = finalProductRepository;
        this.diameterGroupRepository = diameterGroupRepository;
        this.longGroupRepository = longGroupRepository;
        this.interProductRepository = interProductRepository;
        this.productRelationsRepository = productRelationsRepository;
        this.resourceQtyRepository = resourceQtyRepository;
        this.resourceCostRepository = resourceCostRepository;

        this.finalProductMapper = finalProductMapper;
        this.diameterGroupMapper = diameterGroupMapper;
        this.longGroupMapper = longGroupMapper;
        this.interProductMapper = interProductMapper;
        this.productRelationsMapper = productRelationsMapper;
        this.resourceQtyMapper = resourceQtyMapper;
        this.resourceCostMapper = resourceCostMapper;

        this.gamsOptimizationPoint2Runner = gamsOptimizationPoint2Runner;
    }

    @Override
    public OptimizationPointDTO loadCase() {
        ModelRunDef runDef = new ModelRunDef();
        ModelDef def = modelDefService.findOneByName("Planner");
        ModelRunDefDTO op1 = modelRunDefService.findLastFinishedOP1();
        OptimizationPoint2DTO op2 = loadCaseInput();

        runDef.setName(def.getName());
        runDef.setLanguage(def.getLanguage());
        runDef.setSolver(def.getSolver());
        runDef.setVersion(def.getCurrentVersion());
        runDef.setProgress(0);
        runDef.setUser(SecurityUtils.getCurrentUsername());

        op2.setModelRunDef(runDef);
        op2.setModelDefId(def.getId());
        op2.setOptimizationPoint1(op1);

        return op2;
    }

    private OptimizationPoint2DTO loadCaseInput(){
        OptimizationPoint2DTO dto = new OptimizationPoint2DTO();

        List<FinalProductDTO> finalProducts = this.finalProductList();
        List<DiameterGroupDTO> diameters = this.diameterList();
        List<LongGroupDTO> longs = this.longList();
        List<InterProductDTO> interProducts = this.interProductList();
        List<ProductRelationsDTO> productRelations = this.productRelationsList();
        List<ResourceCostDTO> resourceCosts = this.resourceCostsList();
        List<ResourceQtyDTO> resourceQtys = this.resourceQtysList();

        dto.setDiameters(diameters);
        dto.setLongs(longs);
        dto.setInterProducts(interProducts);
        dto.setProductRelations(productRelations);
        dto.setResourceQtys(resourceQtys);
        dto.setResourceCosts(resourceCosts);
        dto.setFinalProducts(finalProducts);

        return dto;
    }

    private List<FinalProductDTO> finalProductList() {
        return finalProductMapper.toDto(finalProductRepository.findAll());
    }

    private List<DiameterGroupDTO> diameterList() {
        return diameterGroupMapper.toDto(diameterGroupRepository.findAll());
    }

    private List<LongGroupDTO> longList() {
        return longGroupMapper.toDto(longGroupRepository.findAll());
    }

    private List<InterProductDTO> interProductList() {
        return interProductMapper.toDto(interProductRepository.findAll());
    }

    private List<ProductRelationsDTO> productRelationsList() {
        return productRelationsMapper.toDto(productRelationsRepository.findAll());
    }

    private List<ResourceCostDTO> resourceCostsList() {
        return resourceCostMapper.toDto(resourceCostRepository.findAll());
    }

    private List<ResourceQtyDTO> resourceQtysList(){
        return resourceQtyMapper.toDto(resourceQtyRepository.findAll());
    }

    @Override
    public void runOptimizationPoint(OptimizationPointDTO optimizationPointDTO) {
        gamsOptimizationPoint2Runner.runOptimizationPoint2((OptimizationPoint2DTO) optimizationPointDTO);
    }
}
