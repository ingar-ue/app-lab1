package gov.ingar.applab.optimization.gams;

import gov.ingar.applab.domain.enumeration.Unit;
import gov.ingar.applab.optimization.dto.OptimizationPointDTO;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

abstract class InputExcel {

    /**
     * Declare the basic shape of the resultant excel
     * @return the finised Workbook
     */
    abstract Workbook toExcel(OptimizationPointDTO op);

    /**
     * Generate a sheet with the index used by GAMS to load sets and parameters
     */
    abstract void writeIndex(Sheet sheet,int[] lastRows);

    public Double convertUnitToInch(Unit inputType, Double value){
        Double result = value;
        switch (inputType){
            case PIE:
                result = value * 12;
                break;
            case CM:
                result = value * 0.39370;
                break;
            case M:
                result = value * 39.370;
                break;
            case MM:
                result = value * 0.039370;
                break;
            case PULGADA:
                result = value;
                break;
        }
        return result;
    }

    public Double convertUnitToCode (Unit inputUnit){
        Double code = null;
        switch (inputUnit){
            case PIE:
                code = 4.0;
                break;
            case CM:
                code = 1.0;
                break;
            case M:
                code = 3.0;
                break;
            case MM:
                code = 2.0;
                break;
            case PULGADA:
                code = 5.0;
                break;
        }
        return code;
    }

    public void writeByColumns(Workbook wb, Sheet sheet, List<List<Object>> dataLists, boolean columnTitle, boolean rowTitle){
        int rowNum;
        int lastRow = -1;
        int colNum;
        Row row;
        Cell cell;
        List<Object> colDataList;
        CellStyle title = wb.createCellStyle();
        title.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        title.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        for (colNum = 1; colNum <= dataLists.size(); colNum++) {
            colDataList = dataLists.get(colNum - 1);
            if(colDataList.size() != 0){

                for (rowNum = 1; rowNum <= colDataList.size(); rowNum++) {
                    if(!(colDataList.get(rowNum -1) == null)){
                        if(lastRow < rowNum){
                            row = sheet.createRow(rowNum);
                            lastRow = rowNum;
                        } else{
                            row = sheet.getRow(rowNum);
                        }
                        cell = row.createCell(colNum);
                        if(rowNum == 1 && columnTitle) cell.setCellStyle(title);
                        if(colNum == 1 && rowTitle) cell.setCellStyle(title);
                        if(colDataList.get(rowNum -1).getClass().toString().equals("class java.lang.String")){
                            String value = colDataList.get(rowNum -1).toString();
                            cell.setCellValue(value);
                        }else{
                            double value = (double) colDataList.get(rowNum -1);
                            cell.setCellValue(value);
                        }
                    }
                }
            }
        }

        int columns = sheet.getRow(1).getLastCellNum();
        for (int i =0; i<=columns; i++){
            sheet.autoSizeColumn(i);
        }
    }
}
