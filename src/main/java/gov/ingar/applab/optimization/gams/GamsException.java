package gov.ingar.applab.optimization.gams;

import gov.ingar.applab.domain.enumeration.GamsError;

public class GamsException extends Exception{

    public GamsError code = GamsError.OTHER;

    public GamsException() {
        super();
    }

    public GamsException(String message) {
        super(message);
    }

    public GamsException(GamsError code, String message) {
        super(message);
        this.code = code;
    }

    public GamsException(GamsError code) {
        super();
        this.code = code;
    }

    public GamsException(Throwable cause) {
        super(cause);
    }

    public GamsException(Exception e) {
        super(e);
    }

    public GamsException(String message, Throwable cause) {
        super(message, cause);
    }

    public GamsError getErrorCode() {
        return this.code;
    }
}
