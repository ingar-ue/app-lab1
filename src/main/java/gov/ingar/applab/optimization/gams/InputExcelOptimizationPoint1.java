package gov.ingar.applab.optimization.gams;

import gov.ingar.applab.domain.enumeration.Unit;
import gov.ingar.applab.optimization.dto.OptimizationPoint1DTO;
import gov.ingar.applab.optimization.dto.OptimizationPointDTO;
import gov.ingar.applab.service.dto.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class InputExcelOptimizationPoint1 extends InputExcel {


    public InputExcelOptimizationPoint1() {}

    @Override
    public Workbook toExcel(OptimizationPointDTO op){
        OptimizationPoint1DTO op1 = (OptimizationPoint1DTO) op;
        Workbook wb = new XSSFWorkbook();
        Sheet sheet1 = wb.createSheet("SetsParametros");
        Sheet sheet2 = wb.createSheet("Relaciones");
        Sheet sheet3 = wb.createSheet("Index");

        LinkedHashMap<Object,Object> widthHashMap = new LinkedHashMap<>();
        LinkedHashMap<Object,Object> thicknessHashMap = new LinkedHashMap<>();

        writeSetsParameters(wb, sheet1, op1, widthHashMap, thicknessHashMap);
        writeRelations(wb, sheet2, op1, widthHashMap, thicknessHashMap);
        int lastRows[] = new int[2];
        lastRows[0] = sheet1.getLastRowNum()+1;
        lastRows[1] = sheet2.getLastRowNum()+1;
        writeIndex(sheet3, lastRows);

        return wb;
    }


    private List<List<Object>> diameterData(OptimizationPoint1DTO op1){
        List<DiameterGroupDTO> diameters = op1.getDiameters();

        List<Object> diameterCodeList = new ArrayList<>();
        List<Object> diameterValueList = new ArrayList<>();
        List<Object> diameterPerformanceList = new ArrayList<>();
        List<Object> diameterSuperiorLimitList = new ArrayList<>();
        List<Object> diameterLateralLimitList = new ArrayList<>();
        List<Object> diameterCentralLimitList = new ArrayList<>();

        diameterCodeList.add("Codigo");
        diameterValueList.add("Diámetro [Inch]");
        diameterPerformanceList.add("Rendimiento");
        diameterSuperiorLimitList.add("Superior");
        diameterLateralLimitList.add("Lateral");
        diameterCentralLimitList.add("Central");

        for (DiameterGroupDTO diameter : diameters){
            diameterCodeList.add(diameter.getCode());
            diameterValueList.add(convertUnitToInch(diameter.getValType(), diameter.getMinValue().doubleValue()));
            diameterPerformanceList.add(diameter.getYieldIndex().doubleValue()/100);
            diameterSuperiorLimitList.add(diameter.getTablesTop().doubleValue());
            diameterLateralLimitList.add(diameter.getTablesSide().doubleValue());
            diameterCentralLimitList.add(diameter.getTablesCentral().doubleValue());
        }

        List<List<Object>> lists = new ArrayList<>();
        lists.add(diameterCodeList);
        lists.add(diameterValueList);
        lists.add(diameterPerformanceList);
        lists.add(diameterSuperiorLimitList);
        lists.add(diameterLateralLimitList);
        lists.add(diameterCentralLimitList);

        return lists;
    }

    private List<List<Object>> longData(OptimizationPoint1DTO op1){
        List<LongGroupDTO> longs = op1.getLongs();

        List<Object> longCodeList = new ArrayList<>();
        List<Object> longValueList = new ArrayList<>();

        longCodeList.add("Código");
        longValueList.add("Largo [Inch]");
        longCodeList.add("Ancho");
        longCodeList.add("Espesor");
        longValueList.add(null);
        longValueList.add(null);

        for (LongGroupDTO longGroup : longs){
            longCodeList.add(longGroup.getCode());
            longValueList.add(convertUnitToInch(longGroup.getValType(), longGroup.getValue().doubleValue()));
        }



        List<List<Object>> lists = new ArrayList<>();
        lists.add(longCodeList);
        lists.add(longValueList);

        return lists;
    }

    private List<List<Object>> interProductData(OptimizationPoint1DTO op1, LinkedHashMap<Object,Object> widthHash, LinkedHashMap<Object,Object> thicknessHash){
        List<InterProductDTO> interProducts = op1.getInterProducts();

        List<Object> interProductCodeList = new ArrayList<>();

        widthHash.put("Valor [Inch]", "Ancho");
        thicknessHash.put("Valor [Inch]", "Espesor");
        interProductCodeList.add("Inter Products");
        interProductCodeList.add("i0");

        int widthNumber = 1;
        int thicknessNumber = 1;

        for (InterProductDTO interProduct : interProducts){
            double widthValue = convertUnitToInch(interProduct.getValType(),interProduct.getWidth().doubleValue());

            if(!widthHash.containsKey(widthValue)){
                widthHash.put(widthValue, "a" + widthNumber);
                widthNumber++;
            }

            double thicknessValue = convertUnitToInch(interProduct.getValType(), interProduct.getTickness().doubleValue());
            if(!thicknessHash.containsKey(thicknessValue)){
                thicknessHash.put(thicknessValue, "e" + thicknessNumber);
                thicknessNumber++;
            }

            interProductCodeList.add(interProduct.getCode());
        }

        widthHash.put(null, "a0");
        thicknessHash.put(null, "e0");


        interProductCodeList.add("Rendimiento");

        List<List<Object>> lists = new ArrayList<>();
        lists.add(new ArrayList<>(widthHash.values()));
        lists.add(new ArrayList<>(widthHash.keySet()));
        lists.add(new ArrayList<>());
        lists.add(new ArrayList<>(thicknessHash.values()));
        lists.add(new ArrayList<>(thicknessHash.keySet()));
        lists.add(new ArrayList<>());
        lists.add(interProductCodeList);

        return lists;
    }

    private List<List<Object>> unitData(OptimizationPoint1DTO op1){
        List<Object> unitCodeList = new ArrayList<>();
        List<Object> unitValueList = new ArrayList<>();

        unitCodeList.add("Unidad de: ");
        unitValueList.add("Código");

        unitCodeList.add("Ancho");
        unitCodeList.add("Diametro");
        unitCodeList.add("Espesor");
        unitCodeList.add("Largo");
        unitValueList.add(convertUnitToCode(op1.getInterProducts().get(0).getValType()));
        unitValueList.add(convertUnitToCode(op1.getDiameters().get(0).getValType()));
        unitValueList.add(convertUnitToCode(op1.getInterProducts().get(0).getValType()));
        unitValueList.add(convertUnitToCode(op1.getLongs().get(0).getValType()));

        List<List<Object>> lists = new ArrayList<>();
        lists.add(unitCodeList);
        lists.add(unitValueList);

        return lists;
    }

    private List<List<Object>> kerfData(OptimizationPoint1DTO op1){
        List<KerfConfigDTO> kerfs = op1.getKerfConfigs();

        List<Object> kerfCodeList = new ArrayList<>();
        List<Object> kerfValueList = new ArrayList<>();

        kerfCodeList.add("Tipo Kerf");
        kerfValueList.add("Valor [inch]");

        for (KerfConfigDTO kerf : kerfs){
            kerfCodeList.add(kerf.getParam());
            kerfValueList.add(convertUnitToInch(Unit.MM, kerf.getValue().doubleValue()));
        }

        List<List<Object>> lists = new ArrayList<>();
        lists.add(kerfCodeList);
        lists.add(kerfValueList);

        return lists;
    }

    private List<List<List<Object>>> productRelations (OptimizationPoint1DTO op1, LinkedHashMap<Object,Object> widthHashMap, LinkedHashMap<Object,Object> thicknessHashMap){
        List<InterProductDTO> interProducts = op1.getInterProducts();

        List<List<Object>> widthThickRelation = new ArrayList<>();
        List<List<Object>> productWidthThickRelation = new ArrayList<>();
        List<List<Object>> productLongRelation = new ArrayList<>();

        widthThickRelation.add(Arrays.asList("Espesor", "Ancho"));
        productWidthThickRelation.add(Arrays.asList("Producto", "Espesor", "Ancho"));
        productLongRelation.add(Arrays.asList("Producto", "Largo"));

        for (InterProductDTO interProduct : interProducts){
            String product = interProduct.getCode();
            double width = convertUnitToInch(interProduct.getValType(), interProduct.getWidth().doubleValue());
            double thickness = convertUnitToInch(interProduct.getValType(), interProduct.getTickness().doubleValue());

            List<Object> relation1 = Arrays.asList(product, thicknessHashMap.get(thickness), widthHashMap.get(width));
            if(!productWidthThickRelation.contains(relation1)){
                productWidthThickRelation.add(relation1);
            }

            List<Object> relation2 = Arrays.asList(thicknessHashMap.get(thickness), widthHashMap.get(width));
            if(!widthThickRelation.contains(relation2)){
                widthThickRelation.add(relation2);
            }

            String[] longs = interProduct.getLongGroups().split(",");
            for(String l : longs){
                productLongRelation.add(Arrays.asList(product,l));
            }
        }

        List<List<List<Object>>> relationsList = new ArrayList<>();
        relationsList.add(widthThickRelation);
        relationsList.add(productWidthThickRelation);
        relationsList.add(productLongRelation);

        return relationsList;
    }

    private List<List<Object>> longRelations (OptimizationPoint1DTO op1){
        List<LongGroupDTO> longs = op1.getLongs();
        List<DiameterGroupDTO> diameters = op1.getDiameters();

        List<List<Object>> diameterLongRelation = new ArrayList<>();

        diameterLongRelation.add(Arrays.asList("Diámetro", "Largo"));

        for (DiameterGroupDTO diameter : diameters){
            for (LongGroupDTO longGroup : longs){
                diameterLongRelation.add(Arrays.asList(diameter.getCode(), longGroup.getCode()));
            }
        }

        return diameterLongRelation;
    }


    @Override
    public void writeIndex(Sheet sheet, int[] lastRows){
        List<List<String>> index = new ArrayList<>();

        index.add(Arrays.asList(null,null,null,"rdim","cdim","dim","ignoreColumns"));
        index.add(Arrays.asList("set","d","SetsParametros!B3:B"+lastRows[0],"1"));
        index.add(Arrays.asList("set","a","SetsParametros!L3:L"+lastRows[0],"1"));
        index.add(Arrays.asList("set","e","SetsParametros!O3:O"+lastRows[0],"1"));
        index.add(Arrays.asList("set","L","SetsParametros!I3:I"+lastRows[0],"1"));
        index.add(Arrays.asList("set","i","SetsParametros!R3:R"+lastRows[0],"1"));
        index.add(Arrays.asList("set","REA","Relaciones!B3:C"+lastRows[1],"2"));
        index.add(Arrays.asList("set","Rel","Relaciones!E3:G"+lastRows[1],"3"));
        index.add(Arrays.asList("set","RDL","Relaciones!L3:M"+lastRows[1],"2"));
        index.add(Arrays.asList("set","RIL","Relaciones!I2:J"+lastRows[1],"2"));
        index.add(Arrays.asList("set","zpr","SetsParametros!R4:R"+lastRows[0],"1"));
        index.add(Arrays.asList("par","DiametroCM","SetsParametros!B3:C"+lastRows[0],"1"));
        index.add(Arrays.asList("par","ancho","SetsParametros!L3:M"+lastRows[0],"1"));
        index.add(Arrays.asList("par","espesor","SetsParametros!O3:P"+lastRows[0],"1"));
        index.add(Arrays.asList("par","Largos","SetsParametros!I3:J"+lastRows[0],"1"));
        index.add(Arrays.asList("par","Tlimite_Lat","SetsParametros!B3:F"+lastRows[0],"1",null,null,"C,D,E"));
        index.add(Arrays.asList("par","Tlimite","SetsParametros!B3:G"+lastRows[0],"1",null,null,"C,D,E,F"));
        index.add(Arrays.asList("par","Tlimite_Sup","SetsParametros!B3:E"+lastRows[0],"1",null,null,"C,D"));
        index.add(Arrays.asList("par","Rendi_Diametro","SetsParametros!B3:D"+lastRows[0],"1",null,null,"C"));
        index.add(Arrays.asList("par","Kerf_paralela","SetsParametros!X4",null,null,"0"));
        index.add(Arrays.asList("par","Kerf_multi","SetsParametros!X3",null,null,"0"));
        index.add(Arrays.asList("par","Kerf_reaserr","SetsParametros!X5",null,null,"0"));
        index.add(Arrays.asList("par","UnidadD","SetsParametros!U4",null,null,"0"));
        index.add(Arrays.asList("par","UnidadL","SetsParametros!U6",null,null,"0"));
        index.add(Arrays.asList("par","UnidadA","SetsParametros!U3",null,null,"0"));
        index.add(Arrays.asList("par","UnidadE","SetsParametros!U5",null,null,"0"));

        int rowNum;
        int colNum;
        Row row;
        Cell cell;
        List<String> indexRow;

        for (rowNum = 0; rowNum < index.size(); rowNum++) {
            indexRow = index.get(rowNum);
            row = sheet.createRow(rowNum);
            for (colNum = 0; colNum < indexRow.size(); colNum++) {
                cell = row.createCell(colNum);
                cell.setCellValue(indexRow.get(colNum));
            }
        }

        int columns = sheet.getRow(0).getLastCellNum();
        for (int i =0; i<=columns; i++){
            sheet.autoSizeColumn(i);
        }
    }

    private void writeSetsParameters (Workbook wb, Sheet sheet, OptimizationPoint1DTO op1, LinkedHashMap<Object,Object> widthHashMap, LinkedHashMap<Object,Object> thicknessHashMap){
        List<List<Object>> diameterData = diameterData(op1);
        List<List<Object>> longData = longData(op1);
        List<List<Object>> interProductData = interProductData(op1, widthHashMap, thicknessHashMap);
        List<List<Object>> unitData = unitData(op1);
        List<List<Object>> kerfData = kerfData(op1);

        List<List<Object>> dataLists = new ArrayList<>();
        dataLists.addAll(diameterData);
        dataLists.add(new ArrayList<>());
        dataLists.addAll(longData);
        dataLists.add(new ArrayList<>());
        dataLists.addAll(interProductData);
        dataLists.add(new ArrayList<>());
        dataLists.addAll(unitData);
        dataLists.add(new ArrayList<>());
        dataLists.addAll(kerfData);

        writeByColumns(wb, sheet, dataLists, true, false);
    }

    private void writeRelations(Workbook wb, Sheet sheet, OptimizationPoint1DTO op1, LinkedHashMap<Object,Object> widthHashMap, LinkedHashMap<Object,Object> thicknessHashMap) {

        List<List<List<Object>>> productRelationsList = productRelations(op1, widthHashMap, thicknessHashMap);
        List<List<Object>> longRelationsList = longRelations(op1);

        List<List<List<Object>>> relationsList = new ArrayList<>();
        relationsList.addAll(productRelationsList);
        relationsList.add(longRelationsList);

        int rowNum;
        int lastRow = -1;
        int colNum = 0;
        int lastColumn = 0;
        Row row;
        Cell cell;
        List<Object> relationRow;
        CellStyle title = wb.createCellStyle();
        title.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        title.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        for(List<List<Object>> relation: relationsList){
            for (rowNum = 1; rowNum <= relation.size(); rowNum++) {
                relationRow = relation.get(rowNum-1);
                if(lastRow < rowNum){
                    row = sheet.createRow(rowNum);
                    lastRow = rowNum;
                } else{
                    row = sheet.getRow(rowNum);
                }
                for (colNum = lastColumn+1; colNum < relationRow.size()+lastColumn+1; colNum++) {
                    cell = row.createCell(colNum);
                    if(rowNum == 1) cell.setCellStyle(title);
                    if(relationRow.get(colNum-(lastColumn+1)).getClass().toString().equals("class java.lang.String")){
                        String value = relationRow.get(colNum-(lastColumn+1)).toString();
                        cell.setCellValue(value);
                    }else{
                        double value = (double) relationRow.get(colNum-(lastColumn+1));
                        cell.setCellValue(value);
                    }
                }
            }
            lastColumn = colNum;
        }



        int columns = sheet.getRow(1).getLastCellNum();
        for (int i = 0; i <= columns; i++){
            sheet.autoSizeColumn(i);
        }
    }

}
