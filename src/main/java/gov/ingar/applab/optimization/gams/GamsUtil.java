package gov.ingar.applab.optimization.gams;

import java.io.File;

public class GamsUtil {

    // gams configuration
    public static final String GAMS_OPTDEF = "optgams.def";
    public static final int GAMS_LOGOPTIONS = 2;

    // gams pattern generator & optimizador.gms files
    // logs
    public static final String GAMS_OPT1_LOGFILE = "generador.log";
    public static final String GAMS_OPT2_LOGFILE = "optimizador.log";
    // models
    public static final String GAMS_OPT1_FILENAME = "generador.gms";
    public static final String GAMS_OPT2_FILENAME = "optimizador.gms";

    // file names
    public static final String GAMS_SIMUXC_FILENAME = "simuladorXC.txt";
    public static final String GAMS_RESULTGDX_FILENAME = "resultados.gdx";
    public static final String GAMS_RESULTXLSX_FILENAME = "resultados.xlsx";
    public static final String GAMS_GENXLSX_FILENAME = "generador.xlsx";
    public static final String GAMS_RESGENXLSX_FILENAME = "resgenerador.xlsx";
    public static final String GAMS_INPUTXLSX_FILENAME = "input.xlsx";
    public static final String GAMS_INPUTGDX_FILENAME = "input.gdx";
    public static final String GAMS_OPT1_GDX_FILENAME = "generador.gdx";
    public static final String GAMS_RESGENGDX_FILENAME = "resgenerador.gdx";

    //file references
    protected static final String GAMS_VAR_GENXLSX = "%generadorxlsx%";
    protected static final String GAMS_VAR_GENGDX = "%generadorgdx%";
    protected static final String GAMS_VAR_RESGENGDX = "%resgeneradorgdx%";
    protected static final String GAMS_VAR_RESGENXLSX = "%resgeneradorxlsx%";
    protected static final String GAMS_VAR_INPUTXLSX = "%inputxls%";
    protected static final String GAMS_VAR_INPUTGDX = "%inputgdx%";
    protected static final String GAMS_VAR_SIMUXC = "%simuladorXC%";
    protected static final String GAMS_VAR_RESULTGDX = "%resultadosgdx%";
    protected static final String GAMS_VAR_RESULTXLSX = "%resultadosxlsx%";


    public static String replaceNonTxt(String content, String runPath, String search, String replace) {
        return content.replace(search, runPath + File.separator + replace);
    }
}
