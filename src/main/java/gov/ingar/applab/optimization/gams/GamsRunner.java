package gov.ingar.applab.optimization.gams;

import gov.ingar.applab.domain.EnvConfig;
import gov.ingar.applab.domain.enumeration.GamsError;
import gov.ingar.applab.optimization.FileService;
import gov.ingar.applab.optimization.OptimizationConstants;
import gov.ingar.applab.repository.EnvConfigRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gams.api.gamsx;
import com.gams.api.gdx;
import com.gams.api.opt;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.List;

public abstract class GamsRunner {

    private final EnvConfigRepository envConfigRepository;
    protected final FileService fileService;
    final Logger log = LoggerFactory.getLogger(GamsRunner.class);

    protected com.gams.api.opt opt;
    protected com.gams.api.gamsx gamsx;
    protected com.gams.api.gdx gdx;

    public GamsRunner(EnvConfigRepository envConfigRepository, FileService fileService) {
        this.envConfigRepository = envConfigRepository;
        this.fileService = fileService;
    }

    /**
     * Method to initialize gams variables.
     */
    @PostConstruct
    public void init() {
        //String libraryPath = System.getProperty("java.library.path");
        //log.debug("Library Path: " + libraryPath);
        gdx = new gdx();
        gamsx = new gamsx();
        opt = new opt();
    }

    public void checkGamsConfig() throws GamsException {
        String gamsPath = getGamsPath();
        if (!fileService.existFolder(gamsPath)) {
            // Error de GAMS: La Configuración de GAMS no corresponde a una carpeta del Sistema.
            throw new GamsException(GamsError.FOLDER_PATH_CONFIG);
        }
    }

    public String getGamsPath() throws GamsException {
        return getEnvConfig(OptimizationConstants.ENV_MATH_SOFTWARE_PATH);
    }

    public String getOPT1Path() throws GamsException {
        return getEnvConfig(OptimizationConstants.ENV_OPT1_EXEC_PATH);
    }

    protected String getOPT2Path() throws GamsException {
        return getEnvConfig(OptimizationConstants.ENV_OPT2_EXEC_PATH);
    }

    private String getEnvConfig(String param) throws GamsException {
        List<EnvConfig> envConfigList = envConfigRepository.findByParam(param);
        if (envConfigList.isEmpty()) {
            // Error de GAMS: No está cargada la Configuración General de GAMS en el Sistema.
            throw new GamsException(GamsError.ENVIRONMENT_CONFIG);
        } else {
            return envConfigList.get(0).getValue();
        }
    }

    protected void initializeGamsEnv(String pathGams) throws GamsException {
        // Directorio de gams
        log.debug("pathGams: " + pathGams);

        // Preparamos un mensaje
        String[] msgError = new String[1];

        // Inicializamos el GDX. SI hay errores, lanzamos una excepción.
        if (gdx.CreateD(pathGams, msgError) != 1) {
            throw new GamsException(GamsError.GDX_EXCEPTION, msgError[0]);
        }
        log.debug("GDX created");

        // Inicializamos la ejecución de gams. Si hay errores, lanza una excepción.
        if (gamsx.CreateD(pathGams, msgError) != 1) {
            throw new GamsException(GamsError.GAMSX_EXCEPTION, msgError[0]);
        }
        log.debug("GAMSX created");

        // Inicializamos las opciones para gams. Si hay errores, lanza una excepción.
        if (opt.CreateD(pathGams, msgError) != 1) {
            throw new GamsException(GamsError.OPT_INIT_EXCEPTION, msgError[0]);
        }
        log.debug("OPT-INIT created");

        // Leemos la definición de las opciones
        if (opt.ReadDefinition(pathGams + File.separator + GamsUtil.GAMS_OPTDEF) != 0) {
            throw new GamsException(GamsError.OPT_READ_EXCEPTION);
        }
        log.debug("lectura de opciones GAMS OPT-READ OK");
    }

    protected void runGamsModel(String pathGams, String runPath, String fileModel, String logFile) throws GamsException {
        // Directorio de GAMS
        opt.SetStrStr("SysDir", pathGams);
        // Modelo a ejecutar
        opt.SetStrStr("Input", runPath + File.separator + fileModel);
        // Directorio de trabajo
        opt.SetStrStr("WorkDir", runPath + File.separator);
        // Nombre del archivo del log
        opt.SetStrStr("LogFile", logFile);
        // Para que imprima *.log y *.lst
        opt.SetIntStr("LogOption", GamsUtil.GAMS_LOGOPTIONS);

        String[] msgGams = new String[1];
        log.info("Call gamsx.RunExecDLL");
        int retgms = gamsx.RunExecDLL(opt.GetoptPtr(), pathGams, 1, msgGams);
        if (retgms != 0) {
            String msgs = "";
            for (String msg : msgGams) {
                msgs += msg + "; ";
            }
            log.error("ERROR executing gams: " + retgms);

            throw new GamsException(GamsError.EXECUTION, msgs);
        }
    }
}
