package gov.ingar.applab.optimization.gams;

import gov.ingar.applab.config.WebSocketController;
import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.domain.enumeration.GamsError;
import gov.ingar.applab.domain.enumeration.ResultStatus;
import gov.ingar.applab.domain.enumeration.Status;
import gov.ingar.applab.optimization.FileService;
import gov.ingar.applab.optimization.FormatUtil;
import gov.ingar.applab.optimization.OptimizationConstants;
import gov.ingar.applab.optimization.dto.ModelRunDefMessage;
import gov.ingar.applab.optimization.dto.OptimizationPoint2DTO;
import gov.ingar.applab.repository.EnvConfigRepository;
import gov.ingar.applab.service.ModelRunDefService;
import gov.ingar.applab.service.dto.ModelRunDefDTO;
import gov.ingar.applab.service.mapper.ModelRunDefMapper;
import gov.ingar.applab.web.rest.errors.CustomParameterizedException;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Service
public class GamsOptimizationPoint2Runner extends GamsRunner {

    private final InputExcelOptimizationPoint2 inputExcelOptimizationPoint2;
    private final ModelRunDefService modelRunDefService;
    private final ModelRunDefMapper modelRunDefMapper;
    private ModelRunDefDTO runDef = new ModelRunDefDTO();
    private final WebSocketController webSocketController;


    public GamsOptimizationPoint2Runner(InputExcelOptimizationPoint2 inputExcelOptimizationPoint2, FileService fileService, EnvConfigRepository envConfigRepository,
                                        ModelRunDefService modelRunDefService, ModelRunDefMapper modelRunDefMapper,
                                        WebSocketController webSocketController) {
        super(envConfigRepository, fileService);
        this.inputExcelOptimizationPoint2 = inputExcelOptimizationPoint2;
        this.modelRunDefService = modelRunDefService;
        this.modelRunDefMapper = modelRunDefMapper;
        this.webSocketController = webSocketController;
    }

    private void inputToExcel(OptimizationPoint2DTO op2, String runPath) {
        Workbook wb = inputExcelOptimizationPoint2.toExcel(op2);

        try (OutputStream fileOut = new FileOutputStream(runPath + File.separator + GamsUtil.GAMS_INPUTXLSX_FILENAME)) {
            wb.write(fileOut);
            wb.close();
            log.info("OptimizationPoint2 Input closed OK");
        } catch (IOException e) {
            log.error("OptimizationPoint2 Input close ERROR", e);
            throw new CustomParameterizedException("error.custom", e.getMessage());
        }
    }

    @Async
    public void runOptimizationPoint2(OptimizationPoint2DTO op2) {
        log.debug("request to execute optimization point 2");
        runDef = modelRunDefMapper.toDto(op2.getModelRunDef());

        //check configuration (gams path was loaded and exists)
        log.debug("checking configuration");
        try {
            checkGamsConfig();
        } catch (GamsException e) {
            JSONObject error = this.getError(e);
            webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), Status.ERROR, 100, error.toString()));
            throw new CustomParameterizedException("error.custom", e.getMessage());
        }

        log.debug("generate the new model run def");
        runDef.setStatus(Status.READY);
        runDef.setRunDate(LocalDate.now());
        runDef.setStartTime(FormatUtil.formatHour(LocalDateTime.now()));
        runDef.setProgress(0);

        // create and save dataSetInput
//        JSONObject dataSetInput = new JSONObject();
//        dataSetInput.put("diameters", op2.getDiameters());
//        dataSetInput.put("longs", op2.getLongs());
//        dataSetInput.put("interProducts", op2.getInterProducts());
//        dataSetInput.put("finalProducts", op2.getFinalProducts());
//        dataSetInput.put("productRelations", op2.getProductRelations());
//        dataSetInput.put("resourceCosts", op2.getResourceCosts());
//        dataSetInput.put("resourceQtys", op2.getResourceQtys());
//        runDef.setDataSetInput(dataSetInput.toString());

        runDef = modelRunDefService.save(runDef);
        webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress()));

        try {
            String runPathSufix = runDef.getUser() + FormatUtil.formatDateHour(LocalDateTime.now());
            String runPath = (getOPT2Path() + runPathSufix).replace("/", File.separator);

            //step1: folder and files
            String fileModel = prepareFolder(runPath);

            //step2: input excel files (input & resgenerador)
            inputToExcel(op2, runPath);
            FileUtils.writeByteArrayToFile(new File(runPath + File.separator + GamsUtil.GAMS_RESGENXLSX_FILENAME), op2.getOptimizationPoint1().getOtherResult());

            // step 3: initialize gams:
            String pathGams = getGamsPath().replace("/", File.separator);
            initializeGamsEnv(pathGams);

            updateProgress();

            // step 4: run gams
            runGamsModel(pathGams, runPath, fileModel, GamsUtil.GAMS_OPT2_LOGFILE);

            // update progress and set results
            runDef.setProgress(100);
            runDef.setEndTime(FormatUtil.formatHour(LocalDateTime.now()));
            runDef.setStatus(Status.FINISHED);
            runDef.setDataSetResult(runPath + File.separator + GamsUtil.GAMS_RESULTXLSX_FILENAME);
            File userResult = new File(runDef.getDataSetResult());
            runDef.setUserResult(Files.readAllBytes(userResult.toPath()));
            runDef = modelRunDefService.save(runDef);

            // save after to obtain a resume from excel data
            runDef.setResultStatus(ResultStatus.INANALYSIS);
            runDef.setResultResume(getResultResume().toString());
            modelRunDefService.save(runDef);

            webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress(), runDef.getDataSetResult()));

        } catch (IOException | GamsException e) {
            // if error set status and description
            // update progress and set results
            runDef.setProgress(100);
            runDef.setEndTime(FormatUtil.formatHour(LocalDateTime.now()));
            runDef.setStatus(Status.ERROR);
            runDef.setResultStatus(ResultStatus.DISCARDED);

            //Error object
            JSONObject error = this.getError(e);
            runDef.setComments(error.toString());
            modelRunDefService.save(runDef);

            webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress(), error.toString()));

            log.error("Error running gams program", e);
        }
    }

    /**
     * Funcion auxiliar para obtener datos del error
     *
     * @param e
     * @return
     */
    private JSONObject getError(Exception e) {
        JSONObject error = new JSONObject();
        error.put("type", e.getClass().getName());
        error.put("message", e.getMessage());

        try {
            error.put("code", ((GamsException) e).getErrorCode());
        } catch (Exception ex) {
            error.put("code", GamsError.FILE_HANDLING);
        }
        return error;
    }

    /**
     * Funcion auxiliar para obtener resumen de ejecucion (costos y beneficios) del excel de resultados
     *
     * @return JSONObject
     */
    private JSONObject getResultResume() throws IOException {
        JSONObject resume = new JSONObject();
        Sheet sheet = fileService.getResultSheet(runDef.getId(), "Resumen");

        try {
            resume.put("earnings", sheet.getRow(7).getCell(2).getNumericCellValue());
            resume.put("resourceCost", sheet.getRow(8).getCell(2).getNumericCellValue());
            resume.put("operatingCost", sheet.getRow(9).getCell(2).getNumericCellValue());
            resume.put("benefits", sheet.getRow(10).getCell(2).getNumericCellValue());
        } catch (Exception e) {
        }

        return resume;
    }

    private String prepareFolder(String runPath) throws IOException {
        String srcResource = OptimizationConstants.ENV_MODEL_SRC_RESOURCE;
        String fileModel = GamsUtil.GAMS_OPT2_FILENAME;

        log.info("Creating run folder");
        fileService.createFolder(runPath);

        log.info("Copy gms model file");
        fileService.copyFileFromResource(srcResource, runPath, fileModel);

        webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), 15));

        // load gms file and replace variables:
        String modelContent = fileService.readSimpleFile(runPath + File.separator + fileModel);

        // replace variables
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_RESULTGDX, GamsUtil.GAMS_RESULTGDX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_RESULTXLSX, GamsUtil.GAMS_RESULTXLSX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_RESGENGDX, GamsUtil.GAMS_RESGENGDX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_RESGENXLSX, GamsUtil.GAMS_RESGENXLSX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_INPUTXLSX, GamsUtil.GAMS_INPUTXLSX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_INPUTGDX, GamsUtil.GAMS_INPUTGDX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_SIMUXC, GamsUtil.GAMS_SIMUXC_FILENAME);

        // override file content
        fileService.writeSimpleFile(modelContent, runPath, fileModel);

        // update progress
        runDef.setProgress(30);
        runDef.setStatus(Status.RUNNING);
        runDef = modelRunDefService.save(runDef);
        webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress()));

        return fileModel;
    }

    @Async
    private void updateProgress() {
        TimerTask repeatedTask = new TimerTask() {
            public void run() {
                if (runDef.getProgress() < 95) {
                    runDef.setProgress(runDef.getProgress() + 2);
                    webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress()));
                } else {
                    cancel();
                }
            }
        };
        Timer timer = new Timer("Timer");

        long delay = 0;
        long period = getUpdateProgressInterval();
        timer.scheduleAtFixedRate(repeatedTask, delay, period);
    }

    private long getUpdateProgressInterval() {
        List<ModelRunDef> last10 = modelRunDefService.findLast10(runDef.getName(), Status.FINISHED);
        long ret = 0;
        if (last10.size() > 0) {
            long total = 0;
            for (ModelRunDef modelRunDef : last10) {
                total = total + modelRunDef.getDurationInSeconds();
            }
            ret = ((total / last10.size()) / 30) * 1000;
        }
        if (ret <= 0) {
            ret = 4000;
        }

        return ret;
    }
}
