package gov.ingar.applab.optimization.gams;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.optimization.dto.OptimizationPoint2DTO;
import gov.ingar.applab.optimization.dto.OptimizationPointDTO;
import gov.ingar.applab.repository.*;
import gov.ingar.applab.service.dto.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class InputExcelOptimizationPoint2 extends InputExcel{


    private final ModelObjFunctionRepository modelObjFunctionRepository;
    private final PatternExclusionRepository patternExclusionRepository;
    private final ConfigYieldPlanningRepository configYieldPlanningRepository;
    private final ConfigProccessTimeRepository configProccessTimeRepository;
    private final ConfigTableCorrectionRepository configTableCorrectionRepository;

    public InputExcelOptimizationPoint2(ModelObjFunctionRepository modelObjFunctionRepository, PatternExclusionRepository patternExclusionRepository,
                                        ConfigYieldPlanningRepository configYieldPlanningRepository, ConfigProccessTimeRepository configProccessTimeRepository,
                                        ConfigTableCorrectionRepository configTableCorrectionRepository){
        this.modelObjFunctionRepository = modelObjFunctionRepository;
        this.patternExclusionRepository = patternExclusionRepository;
        this.configProccessTimeRepository = configProccessTimeRepository;
        this.configYieldPlanningRepository = configYieldPlanningRepository;
        this.configTableCorrectionRepository = configTableCorrectionRepository;
    }

    @Override
    public Workbook toExcel(OptimizationPointDTO op){
        OptimizationPoint2DTO op2 = (OptimizationPoint2DTO) op;
        Workbook wb = new XSSFWorkbook();
        Sheet sheet1 = wb.createSheet("ConfiguracionGeneral");
        Sheet sheet2 = wb.createSheet("Productos");
        Sheet sheet3 = wb.createSheet("Troncos");
        Sheet sheet4 = wb.createSheet("Patrones");
        Sheet sheet5 = wb.createSheet("Index");

        writeConfiguracionGeneral(wb, sheet1, op2);
        writeProductos(wb, sheet2, op2);
        writeTroncos(wb, sheet3, op2);
        writePatrones(wb, sheet4, op2);

        int lastRows[] = new int[4];
        lastRows[0] = sheet1.getLastRowNum()+1;
        lastRows[1] = sheet2.getLastRowNum()+1;
        lastRows[2] = sheet3.getLastRowNum()+1;
        lastRows[3] = sheet4.getLastRowNum()+1;
        writeIndex(sheet5, lastRows);

        return wb;
    }

    @Override
    public void writeIndex(Sheet sheet, int[] lastRows){
        List<List<String>> index = new ArrayList<>();

        index.add(Arrays.asList(null,null,null,"rdim","cdim","dim","ignoreColumns"));
        index.add(Arrays.asList("set","i","Productos!G3:G"+lastRows[1],"1"));
        index.add(Arrays.asList("set","f","Productos!B3:B"+lastRows[1],"1"));
        index.add(Arrays.asList("set","RNP","Patrones!J3:J"+lastRows[3],"1"));
        index.add(Arrays.asList("set","s","Patrones!B3:B"+lastRows[3],"1"));
        index.add(Arrays.asList("par","CosTonelada","Troncos!B3:C"+lastRows[2],"1"));
        index.add(Arrays.asList("par","InvTroncoNuevo","Troncos!H3:J"+lastRows[2],"2"));
        index.add(Arrays.asList("par","DemTF","Productos!B3:D"+lastRows[1],"1",null,null,"C"));
        index.add(Arrays.asList("par","DemTFmin","Productos!B3:E"+lastRows[1],"1",null,null,"C,D"));
        index.add(Arrays.asList("par","pvTF","Productos!B3:C"+lastRows[1],"1"));
        index.add(Arrays.asList("par","RendiLimite","Troncos!E3:F"+lastRows[2],"1"));
        index.add(Arrays.asList("par","TiempProcAs","Troncos!L3:N"+lastRows[2],"2"));
        index.add(Arrays.asList("par","ajuste","Troncos!P2:AF"+lastRows[2],"1","1"));
        index.add(Arrays.asList("par","RelacionTAyTF","Patrones!D3:H"+lastRows[3],"4"));
        index.add(Arrays.asList("par","BigM","ConfiguracionGeneral!C2",null,null,"0"));
        index.add(Arrays.asList("par","Tsetupmin","ConfiguracionGeneral!C3",null,null,"0"));
        index.add(Arrays.asList("par","TMaxOperacionAse","ConfiguracionGeneral!C4",null,null,"0"));
        index.add(Arrays.asList("par","CosSetupAse","ConfiguracionGeneral!C6",null,null,"0"));
        index.add(Arrays.asList("par","CosVarAse","ConfiguracionGeneral!C5",null,null,"0"));
        index.add(Arrays.asList("par","FO","ConfiguracionGeneral!C7",null,null,"0"));
        index.add(Arrays.asList("par","UnidadD","ConfiguracionGeneral!C8",null,null,"0"));
        index.add(Arrays.asList("par","UnidadL","ConfiguracionGeneral!C10",null,null,"0"));
        index.add(Arrays.asList("par","UnidadE","ConfiguracionGeneral!C9",null,null,"0"));

        int rowNum;
        int colNum;
        Row row;
        Cell cell;
        List<String> indexRow;

        for (rowNum = 0; rowNum < index.size(); rowNum++) {
            indexRow = index.get(rowNum);
            row = sheet.createRow(rowNum);
            for (colNum = 0; colNum < indexRow.size(); colNum++) {
                cell = row.createCell(colNum);
                cell.setCellValue(indexRow.get(colNum));
            }
        }

        int columns = sheet.getRow(0).getLastCellNum();
        for (int i =0; i<=columns; i++){
            sheet.autoSizeColumn(i);
        }
    }

    private void writeConfiguracionGeneral(Workbook wb, Sheet sheet, OptimizationPoint2DTO op2) {
        List<Object> parameters = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        List<List<Object>> dataLists = new ArrayList<>();

        parameters.add("BigM");
        values.add(3000.0);

        List<ModelRunConfigDTO> configs = op2.getModelRunConfigs();
        for(ModelRunConfigDTO config : configs){
            parameters.add(config.getParam());
            values.add(Double.parseDouble(config.getValue()));
        }

        parameters.add("Costo de setup ($/segundo)");
        parameters.add("Funcion Objetivo");
        parameters.add("Unidad Diametro");
        parameters.add("Unidad Espesor");
        parameters.add("Unidad Largo");

        values.add(2.0);
        values.add(new Double(modelObjFunctionRepository.findByModelDefIdAndName(op2.getModelDefId(), op2.getModelRunDef().getObjFunction()).getCode()));
        values.add(convertUnitToCode(op2.getDiameters().get(0).getValType()));
        values.add(convertUnitToCode(op2.getInterProducts().get(0).getValType()));
        values.add(convertUnitToCode(op2.getLongs().get(0).getValType()));

        dataLists.add(parameters);
        dataLists.add(values);

        writeByColumns(wb, sheet, dataLists, false, true);
    }

    private void writeProductos(Workbook wb, Sheet sheet, OptimizationPoint2DTO op2) {
        List<Object> interProductData = interProductData(op2);
        List<List<Object>> finalProductData = finalProductData(op2);

        List<List<Object>> dataLists = new ArrayList<>(finalProductData);
        dataLists.add(new ArrayList<>());
        dataLists.add(interProductData);

        writeByColumns(wb, sheet, dataLists, true, false);
    }

    private void writeTroncos(Workbook wb, Sheet sheet, OptimizationPoint2DTO op2){
        List<List<Object>> costData = costData(op2);
        List<List<Object>> quantityData = quantityData(op2);
        List<List<Object>> performanceData = performanceData();
        List<List<Object>> processTimeData = processTimeData();
        List<List<Object>> correctionData = correctionData();

        List<List<Object>> dataLists = new ArrayList<>(costData);
        dataLists.add(new ArrayList<>());
        dataLists.addAll(performanceData);
        dataLists.add(new ArrayList<>());
        dataLists.addAll(quantityData);
        dataLists.add(new ArrayList<>());
        dataLists.addAll(processTimeData);
        dataLists.add(new ArrayList<>());
        dataLists.addAll(correctionData);

        writeByColumns(wb, sheet, dataLists, true, false);
    }

    private void writePatrones(Workbook wb, Sheet sheet, OptimizationPoint2DTO op2){
        List<List<Object>> productRelationsData = productRelationsData(op2);
        List<Object> patternExclusionData = patternExclusionData();

        List<List<Object>> dataLists = new ArrayList<>(productRelationsData);
        dataLists.add(new ArrayList<>());
        dataLists.add(patternExclusionData);


        writeByColumns(wb, sheet, dataLists, true, false);
    }

    private List<Object> interProductData(OptimizationPoint2DTO op2) {
        List<InterProductDTO> interProducts = op2.getInterProducts();
        List<Object> interProductCodeList = new ArrayList<>();

        interProductCodeList.add("Productos Intermedios");
        for(InterProductDTO interProduct : interProducts){
            interProductCodeList.add(interProduct.getCode());
        }
        interProductCodeList.add("Rendimiento");

        return interProductCodeList;
    }

    private List<List<Object>> finalProductData(OptimizationPoint2DTO op2) {
        List<FinalProductDTO> finalProducts = op2.getFinalProducts();
        List<Object> finalProductCodeList = new ArrayList<>();
        List<Object> priceList = new ArrayList<>();
        List<Object> maxDemandList = new ArrayList<>();
        List<Object> minDemandList = new ArrayList<>();

        finalProductCodeList.add("Producto Final");
        priceList.add("Precio");
        maxDemandList.add("Demanda Maxima");
        minDemandList.add("Demanda Minima");

        for(FinalProductDTO finalProduct : finalProducts){
            finalProductCodeList.add(finalProduct.getCode());
            priceList.add(finalProduct.getPrice().doubleValue());
            maxDemandList.add(new Double(finalProduct.getMaxDemand()));
            minDemandList.add(new Double(finalProduct.getMinDemand()));
        }

        List<List<Object>> lists = new ArrayList<>();
        lists.add(finalProductCodeList);
        lists.add(priceList);
        lists.add(maxDemandList);
        lists.add(minDemandList);
        return lists;
    }

    private List<List<Object>> costData(OptimizationPoint2DTO op2){
        List<ResourceCostDTO> costs = op2.getResourceCosts();
        List<Object> diameterList = new ArrayList<>();
        List<Object> costList = new ArrayList<>();

        diameterList.add("Diametro");
        costList.add("Costo");
        for(ResourceCostDTO cost : costs){
            diameterList.add(cost.getDiameterGroupCode());
            costList.add(cost.getValue().doubleValue());
        }

        List<List<Object>> lists = new ArrayList<>();
        lists.add(diameterList);
        lists.add(costList);
        return lists;
    }

    private List<List<Object>> quantityData(OptimizationPoint2DTO op2){
        List<ResourceQtyDTO> quantities = op2.getResourceQtys();
        List<Object> diameterList = new ArrayList<>();
        List<Object> longList = new ArrayList<>();
        List<Object> quantityList = new ArrayList<>();

        diameterList.add("Diametro");
        longList.add("Largo");
        quantityList.add("Disponibilidad");
        for(ResourceQtyDTO quantity : quantities){
            diameterList.add(quantity.getDiameterGroupCode());
            longList.add(quantity.getLongGroupCode());
            quantityList.add(new Double(quantity.getQuantity()));
        }

        List<List<Object>> lists = new ArrayList<>();
        lists.add(longList);
        lists.add(diameterList);
        lists.add(quantityList);
        return lists;
    }

    private List<List<Object>> performanceData(){
        List<ConfigYieldPlanning> performances = configYieldPlanningRepository.findAll();
        List<Object> diameterList = new ArrayList<>();
        List<Object> performanceList = new ArrayList<>();

        diameterList.add("Diametro");
        performanceList.add("Rendimiento");
        for(ConfigYieldPlanning performance : performances){
            diameterList.add(performance.getDiameterGroup().getCode());
            performanceList.add(performance.getValue().doubleValue()/100);
        }

        List<List<Object>> lists = new ArrayList<>();
        lists.add(diameterList);
        lists.add(performanceList);
        return lists;
    }

    private List<List<Object>> processTimeData(){
        List<ConfigProccessTime> processTimes = configProccessTimeRepository.findAll();
        List<Object> diameterList = new ArrayList<>();
        List<Object> longList = new ArrayList<>();
        List<Object> processTimeList = new ArrayList<>();

        diameterList.add("Diametro");
        longList.add("Largo");
        processTimeList.add("Tiempo de Proceso");
        for(ConfigProccessTime proccessTime : processTimes){
            diameterList.add(proccessTime.getDiameterGroup().getCode());
            longList.add(proccessTime.getLongGroup().getCode());
            processTimeList.add(proccessTime.getValue().doubleValue());
        }

        List<List<Object>> lists = new ArrayList<>();
        lists.add(diameterList);
        lists.add(longList);
        lists.add(processTimeList);
        return lists;

    }

    private List<List<Object>> correctionData(){
        List<ConfigTableCorrection> corrections = configTableCorrectionRepository.findAll();
        List<Object> diameterList = new ArrayList<Object>(){{add("Diametro");}};
        List<Object> correction1List = new ArrayList<Object>(){{add("n1");}};
        List<Object> correction2List = new ArrayList<Object>(){{add("n2");}};
        List<Object> correction3List = new ArrayList<Object>(){{add("n3");}};
        List<Object> correction4List = new ArrayList<Object>(){{add("n4");}};
        List<Object> correction5List = new ArrayList<Object>(){{add("n5");}};
        List<Object> correction6List = new ArrayList<Object>(){{add("n6");}};
        List<Object> correction7List = new ArrayList<Object>(){{add("n7");}};
        List<Object> correction8List = new ArrayList<Object>(){{add("n8");}};
        List<Object> correction9List = new ArrayList<Object>(){{add("n9");}};
        List<Object> correction10List = new ArrayList<Object>(){{add("n10");}};
        List<Object> correction11List = new ArrayList<Object>(){{add("n11");}};
        List<Object> correction12List = new ArrayList<Object>(){{add("n12");}};
        List<Object> correction13List = new ArrayList<Object>(){{add("n13");}};
        List<Object> correction14List = new ArrayList<Object>(){{add("n14");}};
        List<Object> correction15List = new ArrayList<Object>(){{add("n15");}};
        List<Object> correction16List = new ArrayList<Object>(){{add("n16");}};

        for(ConfigTableCorrection correction : corrections){
            diameterList.add(correction.getDiameterGroup().getCode());
            correction1List.add(correction.getValue1().doubleValue());
            correction2List.add(correction.getValue1().doubleValue());
            correction3List.add(correction.getValue1().doubleValue());
            correction4List.add(correction.getValue2().doubleValue());
            correction5List.add(correction.getValue2().doubleValue());
            correction6List.add(correction.getValue2().doubleValue());
            correction7List.add(correction.getValue3().doubleValue());
            correction8List.add(correction.getValue3().doubleValue());
            correction9List.add(correction.getValue3().doubleValue());
            correction10List.add(correction.getValue4().doubleValue());
            correction11List.add(correction.getValue4().doubleValue());
            correction12List.add(correction.getValue4().doubleValue());
            correction13List.add(correction.getValue5().doubleValue());
            correction14List.add(correction.getValue5().doubleValue());
            correction15List.add(correction.getValue5().doubleValue());
            correction16List.add(correction.getValue6().doubleValue());
        }

        List<List<Object>> lists = new ArrayList<>();
        lists.add(diameterList);
        lists.add(correction1List);
        lists.add(correction2List);
        lists.add(correction3List);
        lists.add(correction4List);
        lists.add(correction5List);
        lists.add(correction6List);
        lists.add(correction7List);
        lists.add(correction8List);
        lists.add(correction9List);
        lists.add(correction10List);
        lists.add(correction11List);
        lists.add(correction12List);
        lists.add(correction13List);
        lists.add(correction14List);
        lists.add(correction15List);
        lists.add(correction16List);
        return lists;
    }

    private List<List<Object>> productRelationsData(OptimizationPoint2DTO op2){
        List<ProductRelationsDTO> productRelations = op2.getProductRelations();
        List<Object> patternList = new ArrayList<>();
        List<Object> patterRelationList = new ArrayList<>();
        List<Object> interProductRelationList = new ArrayList<>();
        List<Object> longRelationList = new ArrayList<>();
        List<Object> finalProductRelationList = new ArrayList<>();
        List<Object> quantityRelationList = new ArrayList<>();

        patternList.add("Patrones Secundario");
        patterRelationList.add("Patron Secundario");
        interProductRelationList.add("Producto Intermedio");
        longRelationList.add("Largo");
        finalProductRelationList.add("Producto Final");
        quantityRelationList.add("Cantidad");

        for(ProductRelationsDTO productRelation : productRelations){
            patternList.add("s" + productRelation.getId());
            patterRelationList.add("s" + productRelation.getId());
            interProductRelationList.add(productRelation.getInterProductCode());
            longRelationList.add(productRelation.getLongGroupCode());
            finalProductRelationList.add(productRelation.getFinalProduct1Code());
            quantityRelationList.add(new Double(productRelation.getQuantity1()));

            if(productRelation.getQuantity2() != null && productRelation.getQuantity2() > 0){
                patterRelationList.add("s" + productRelation.getId());
                interProductRelationList.add(productRelation.getInterProductCode());
                longRelationList.add(productRelation.getLongGroupCode());
                finalProductRelationList.add(productRelation.getFinalProduct2Code());
                quantityRelationList.add(new Double(productRelation.getQuantity2()));
            }

            if(productRelation.getQuantity3() != null && productRelation.getQuantity3() > 0){
                patterRelationList.add("s" + productRelation.getId());
                interProductRelationList.add(productRelation.getInterProductCode());
                longRelationList.add(productRelation.getLongGroupCode());
                finalProductRelationList.add(productRelation.getFinalProduct3Code());
                quantityRelationList.add(new Double(productRelation.getQuantity3()));
            }
        }

        List<List<Object>> lists = new ArrayList<>();
        lists.add(patternList);
        lists.add(new ArrayList<>());
        lists.add(patterRelationList);
        lists.add(interProductRelationList);
        lists.add(longRelationList);
        lists.add(finalProductRelationList);
        lists.add(quantityRelationList);
        return lists;
    }

    private List<Object> patternExclusionData(){
        List<PatternExclusion> patternExclusions = patternExclusionRepository.findAll();
        List<Object> patternExclusionList = new ArrayList<>();

        patternExclusionList.add("Patrones Indeseados");
        patternExclusionList.add("p0");
        for(PatternExclusion patternExclusion : patternExclusions){
            patternExclusionList.add(patternExclusion.getName());
        }
        return patternExclusionList;
    }
}
