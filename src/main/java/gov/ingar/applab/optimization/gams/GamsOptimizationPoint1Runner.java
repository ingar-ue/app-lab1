package gov.ingar.applab.optimization.gams;

import gov.ingar.applab.config.WebSocketController;
import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.domain.enumeration.GamsError;
import gov.ingar.applab.domain.enumeration.Status;
import gov.ingar.applab.optimization.*;
import gov.ingar.applab.optimization.dto.ModelRunDefMessage;
import gov.ingar.applab.optimization.dto.OptimizationPoint1DTO;
import gov.ingar.applab.repository.EnvConfigRepository;
import gov.ingar.applab.service.ModelRunDefService;
import gov.ingar.applab.service.dto.ModelRunDefDTO;
import gov.ingar.applab.service.mapper.ModelRunDefMapper;
import gov.ingar.applab.web.rest.errors.CustomParameterizedException;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Service
public class GamsOptimizationPoint1Runner extends GamsRunner {

    private final InputExcelOptimizationPoint1 inputExcelOptimizationPoint1;
    private final ModelRunDefService modelRunDefService;
    private final ModelRunDefMapper modelRunDefMapper;
    private ModelRunDefDTO runDef = new ModelRunDefDTO();
    private final WebSocketController webSocketController;

    public GamsOptimizationPoint1Runner(InputExcelOptimizationPoint1 inputExcelOptimizationPoint1, FileService fileService, EnvConfigRepository envConfigRepository,
                                        ModelRunDefService modelRunDefService, ModelRunDefMapper modelRunDefMapper,
                                        WebSocketController webSocketController) {
        super(envConfigRepository, fileService);
        this.inputExcelOptimizationPoint1 = inputExcelOptimizationPoint1;
        this.modelRunDefService = modelRunDefService;
        this.modelRunDefMapper = modelRunDefMapper;
        this.webSocketController = webSocketController;
    }

    private void inputToExcel(OptimizationPoint1DTO op1, String runPath) {
        Workbook wb = inputExcelOptimizationPoint1.toExcel(op1);

        try (OutputStream fileOut = new FileOutputStream(runPath + File.separator + GamsUtil.GAMS_INPUTXLSX_FILENAME)) {
            wb.write(fileOut);
            wb.close();
            log.info("OptimizationPoint1 Input closed OK");
        } catch (IOException e) {
            log.error("OptimizationPoint1 Input close ERROR", e);
            throw new CustomParameterizedException("error.custom", e.getMessage());
        }
    }

    @Async
    public void runOptimizationPoint1(OptimizationPoint1DTO op1) {
        log.debug("request to execute pattern generator");
        runDef = modelRunDefMapper.toDto(op1.getModelRunDef());

        //check configuration (gams path was loaded and exists)
        log.debug("checking configuration");
        try {
            checkGamsConfig();
        } catch (GamsException e) {
            JSONObject error = this.getError(e);
            webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), Status.ERROR, 100, error.toString()));
            throw new CustomParameterizedException("error.custom", e.getMessage());
        }

        log.debug("generate the new pattern generator");
        runDef.setStatus(Status.READY);
        runDef.setRunDate(LocalDate.now());
        runDef.setStartTime(FormatUtil.formatHour(LocalDateTime.now()));
        runDef.setProgress(0);

        runDef = modelRunDefService.save(runDef);
        webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress()));

        try {
            String runPathSufix = runDef.getUser() + FormatUtil.formatDateHour(LocalDateTime.now());
            String runPath = (getOPT1Path() + runPathSufix).replace("/", File.separator);

            //step1: folder and files
            String fileModel = prepareFolder(runPath);

            //step2: excel file
            inputToExcel(op1, runPath);

            // step 3: initialize gams:
            String pathGams = getGamsPath().replace("/", File.separator);
            initializeGamsEnv(pathGams);

            updateProgress();

            // step 4: run gams
            runGamsModel(pathGams, runPath, fileModel, GamsUtil.GAMS_OPT1_LOGFILE);

            // update progress and set results
            runDef.setProgress(100);
            runDef.setEndTime(FormatUtil.formatHour(LocalDateTime.now()));
            runDef.setStatus(Status.FINISHED);
            runDef.setDataSetResult(runPath + File.separator + GamsUtil.GAMS_GENXLSX_FILENAME);
            File userResult = new File(runPath + File.separator + GamsUtil.GAMS_GENXLSX_FILENAME);
            runDef.setUserResult(Files.readAllBytes(userResult.toPath()));
            File otherResult = new File(runPath + File.separator + GamsUtil.GAMS_RESGENXLSX_FILENAME);
            runDef.setOtherResult(Files.readAllBytes(otherResult.toPath()));
            modelRunDefService.save(runDef);
            webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress(), runDef.getDataSetResult()));

        } catch (IOException | GamsException e) {
            // if error set status and description
            // update progress and set results
            runDef.setProgress(100);
            runDef.setEndTime(FormatUtil.formatHour(LocalDateTime.now()));
            runDef.setStatus(Status.ERROR);

            //Error object
            JSONObject error = this.getError(e);
            runDef.setComments(error.toString());
            modelRunDefService.save(runDef);

            webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress(), error.toString()));

            log.error("Error running gams program", e);
        }
    }

    /**
     * Funcion auxiliar para obtener datos del error
     *
     * @param e
     * @return
     */
    private JSONObject getError(Exception e) {
        JSONObject error = new JSONObject();
        error.put("type", e.getClass().getName());
        error.put("message", e.getMessage());

        try {
            error.put("code", ((GamsException) e).getErrorCode());
        } catch (Exception ex) {
            error.put("code", GamsError.FILE_HANDLING);
        }
        return error;
    }

    private String prepareFolder(String runPath) throws IOException {
        String srcResource = OptimizationConstants.ENV_MODEL_SRC_RESOURCE;
        String fileModel = GamsUtil.GAMS_OPT1_FILENAME;

        log.info("Creating run folder");
        fileService.createFolder(runPath);

        log.info("Copy gms model file");
        fileService.copyFileFromResource(srcResource, runPath, fileModel);

        webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), 15));

        // load gms file and replace variables:
        String modelContent = fileService.readSimpleFile(runPath + File.separator + fileModel);

        // replace variables
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_GENXLSX, GamsUtil.GAMS_GENXLSX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_GENGDX, GamsUtil.GAMS_OPT1_GDX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_RESGENGDX, GamsUtil.GAMS_RESGENGDX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_RESGENXLSX, GamsUtil.GAMS_RESGENXLSX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_INPUTXLSX, GamsUtil.GAMS_INPUTXLSX_FILENAME);
        modelContent = GamsUtil.replaceNonTxt(modelContent, runPath, GamsUtil.GAMS_VAR_INPUTGDX, GamsUtil.GAMS_INPUTGDX_FILENAME);

        // override file content
        fileService.writeSimpleFile(modelContent, runPath, fileModel);

        // update progress
        runDef.setProgress(30);
        runDef.setStatus(Status.RUNNING);
        runDef = modelRunDefService.save(runDef);
        webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress()));

        return fileModel;
    }

    @Async
    private void updateProgress() {
        TimerTask repeatedTask = new TimerTask() {
            public void run() {
                if (runDef.getProgress() < 95) {
                    runDef.setProgress(runDef.getProgress() + 5);
                    webSocketController.fireMessage("/messages", new ModelRunDefMessage(runDef.getName(), runDef.getId(), runDef.getStatus(), runDef.getProgress()));
                } else {
                    cancel();
                }
            }
        };
        Timer timer = new Timer("Timer");

        long delay = 0;
        long period = getUpdateProgressInterval();
        timer.scheduleAtFixedRate(repeatedTask, delay, period);
    }

    private long getUpdateProgressInterval() {
        List<ModelRunDef> last10 = modelRunDefService.findLast10(runDef.getName(), Status.FINISHED);
        if (last10.size() > 0) {
            long total = 0;
            for (ModelRunDef modelRunDef : last10) {
                total = total + modelRunDef.getDurationInSeconds();
            }
            return (long) (((total / last10.size()) / 13.0) * 1000);
        } else {
            return 7000;
        }
    }
}
