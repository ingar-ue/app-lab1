package gov.ingar.applab.optimization;

import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.domain.ResourceCost;
import gov.ingar.applab.domain.enumeration.ResultStatus;
import gov.ingar.applab.optimization.dto.OptimizationDashboardDTO;
import gov.ingar.applab.optimization.dto.ResultLineChartDTO;
import gov.ingar.applab.optimization.dto.ResultPieChartDTO;
import gov.ingar.applab.repository.*;
import gov.ingar.applab.service.dto.EnvConfigDTO;
import gov.ingar.applab.service.mapper.EnvConfigMapper;
import gov.ingar.applab.service.mapper.ModelRunDefMapper;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
@Transactional(readOnly = true)
public class OptimizationDashboardService {
    private final ModelRunDefRepository modelRunDefRepository;
    private final ModelRunDefMapper modelRunDefMapper;
    private final DiameterGroupRepository diameterGroupRepository;
    private final LongGroupRepository longGroupRepository;
    private final FinalProductRepository finalProductRepository;
    private final InterProductRepository interProductRepository;
    private final ResourceCostRepository resourceCostRepository;
    private final EnvConfigRepository envConfigRepository;
    private final EnvConfigMapper envConfigMapper;
    private final ModelDefRepository modelDefRepository;
    private final ResourceQtyRepository resourceQtyRepository;

    public OptimizationDashboardService(
        ModelRunDefRepository modelRunDefRepository, ModelRunDefMapper modelRunDefMapper, DiameterGroupRepository diameterGroupRepository,
        LongGroupRepository longGroupRepository, FinalProductRepository finalProductRepository, InterProductRepository interProductRepository,
        ResourceCostRepository resourceCostRepository, EnvConfigRepository envConfigRepository, EnvConfigMapper envConfigMapper,
        ModelDefRepository modelDefRepository, ResourceQtyRepository resourceQtyRepository
    ) {
        this.modelRunDefRepository = modelRunDefRepository;
        this.modelRunDefMapper = modelRunDefMapper;
        this.diameterGroupRepository = diameterGroupRepository;
        this.longGroupRepository = longGroupRepository;
        this.interProductRepository = interProductRepository;
        this.finalProductRepository = finalProductRepository;
        this.resourceCostRepository = resourceCostRepository;
        this.resourceQtyRepository = resourceQtyRepository;
        this.envConfigRepository = envConfigRepository;
        this.envConfigMapper = envConfigMapper;
        this.modelDefRepository = modelDefRepository;
    }

    public OptimizationDashboardDTO getDashboardData() {
        OptimizationDashboardDTO summary = new OptimizationDashboardDTO();

        EnvConfigDTO language = envConfigMapper.toDto(envConfigRepository.findOneByParam("Math-Language"));
        EnvConfigDTO solver = envConfigMapper.toDto(envConfigRepository.findOneByParam("Math-Solver"));
        if (language != null && solver != null)
            summary.setLanguageSolver(language.getValue() + " - " + solver.getValue());

        summary.setDiametersCount((int) diameterGroupRepository.count());
        summary.setLongsCount((int) longGroupRepository.count());
        summary.setInterProductsCount((int) interProductRepository.count());
        summary.setFinalProductsCount((int) finalProductRepository.count());
        summary.setModelsCount((int) modelDefRepository.count());
        summary.setResourceQty(resourceQtyRepository.countByQuantity(0));
        summary.setLastOpt1Execution(modelRunDefMapper.toDto(modelRunDefRepository.findFirstByNameOrderByIdDesc("Pattern Generator")));
        summary.setLastOpt2Execution(modelRunDefMapper.toDto(modelRunDefRepository.findFirstByNameOrderByIdDesc("Planner")));
        summary.setDailyProduction(this.getDailyProduction());
        summary.setResourceCostAvg(resourceCostRepository.findAvgCost());

        List<ResourceCost> resourceCosts = resourceCostRepository.findAll();
        Map<String, Double> resourceCostsMap = new LinkedHashMap<>();

        for(int i=0; i<resourceCosts.size(); i++){
            resourceCostsMap.put(resourceCosts.get(i).getDiameterGroup().getCode(), resourceCosts.get(i).getValue().doubleValue());
        }

        summary.setResourceCosts(new ResultPieChartDTO("", new ArrayList<>(resourceCostsMap.keySet()), new ArrayList<>(resourceCostsMap.values())));

        return summary;
    }

    public List<ResultLineChartDTO> getDailyProduction(){

        java.util.Calendar cal = Calendar.getInstance();
        LocalDate toDate = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        cal.add(Calendar.MONTH, -3);
        LocalDate fromDate = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        List<ModelRunDef> modelProductions = modelRunDefRepository.findAllByNameAndResultStatusAndRunDateBetweenOrderByIdAsc("Planner", ResultStatus.APPLIED, fromDate, toDate);

        List<String> dateValues = new ArrayList<>();
        List<Double> earningsValues = new ArrayList<>();
        List<Double> resourceCostValues = new ArrayList<>();
        List<Double> operatingCostValues = new ArrayList<>();
        List<Double> benefitsValues = new ArrayList<>();

        for(ModelRunDef model: modelProductions){
            JSONObject resultResume = new JSONObject(model.getResultResume());
            dateValues.add(model.getRunDate().toString());
            earningsValues.add(resultResume.has("earnings") ? resultResume.getDouble("earnings") : 0);
            resourceCostValues.add(resultResume.has("resourceCost") ? resultResume.getDouble("resourceCost") : 0);
            operatingCostValues.add(resultResume.has("operatingCost") ? resultResume.getDouble("operatingCost") : 0);
            benefitsValues.add(resultResume.has("benefits") ? resultResume.getDouble("benefits") : 0);
        }

        List<ResultLineChartDTO> result = new ArrayList<>();
        result.add(new ResultLineChartDTO("", "", "", dateValues, earningsValues));
        result.add(new ResultLineChartDTO("", "", "", dateValues, resourceCostValues));
        result.add(new ResultLineChartDTO("", "", "", dateValues, operatingCostValues));
        result.add(new ResultLineChartDTO("", "", "", dateValues, benefitsValues));

        return result;
    }


}
