package gov.ingar.applab.optimization;

import gov.ingar.applab.service.ModelRunDefService;
import gov.ingar.applab.service.dto.ModelRunDefDTO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.common.SystemCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.net.URL;
import java.util.Optional;

@Service
@Transactional
public class FileService {
    private final ModelRunDefService modelRunDefService;

    public FileService(ModelRunDefService modelRunDefService) {
        this.modelRunDefService = modelRunDefService;
    }

    private final Logger log = LoggerFactory.getLogger(FileService.class);

    /**
     * Test if file path exists and is a directory.
     * @param path
     * @return
     */
    public boolean existFolder(String path) {
        File f = new File(path);

        return (f.exists() && f.isDirectory());
    }

    /**
     * Create folder in localhost
     * @param path
     * @throws FileNotFoundException
     */
    public void createFolder(String path) throws FileNotFoundException {
        File folder = new File(path);
        if (!folder.exists()) folder.mkdirs();
    }

    /**
     * Copy files from classpath
     * @param srcResource ex. "spa-model/"
     * @param toPath ex. "/spa/gen/user20181010"
     * @param filename ex. "archivo.gms"
     * @throws IOException
     */
    public void copyFileFromResource(String srcResource, String toPath, String filename) throws IOException {
        //Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();

        //File source = new File(classLoader.getResource(srcResource + filename).getFile());
        URL inputUrl = classLoader.getResource(srcResource + filename);

        File dest = new File(toPath + File.separator + filename);
        //File dest = new File("/path/to/destination/file");

        log.info("inputUrl: " + inputUrl);
        log.info("dest: " + dest);

        FileUtils.copyURLToFile(inputUrl, dest);
        //FileUtils.copyFile(source, dest);
    }

    /**
     * Read a file from localhost, and return content as String, encoded as UTF8.
     *
     * @param path
     * @return
     * @throws IOException
     */
    public String readSimpleFile(String path) throws IOException {
        FileInputStream inputStream = new FileInputStream(path);

        String content = IOUtils.toString(inputStream, "UTF-8");
        inputStream.close();

        return content;
    }

    /**
     * Write a file in localhost, into destPath folder, encoded in UTF8.
     *
     * @param fileContent
     * @param destPath
     * @param filename
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public void writeSimpleFile(String fileContent, String destPath,
                                String filename)
        throws FileNotFoundException, UnsupportedEncodingException {

        File folder = new File(destPath);
        if (!folder.exists()) folder.mkdirs();

        PrintWriter writer = new PrintWriter(destPath + File.separator + filename, "UTF-8");
        writer.print(fileContent);
        writer.close();
    }

    /**
     * Funcion auxiliar para retornar la hoja de resultados de un archivo excel
     *
     * @param modelRunDefId
     * @return Sheet
     * @throws IOException
     */
    public Sheet getResultSheet(Long modelRunDefId, String sheetName) throws IOException {
        Optional<ModelRunDefDTO> o = modelRunDefService.findOne(modelRunDefId);
        File file = new File(o.get().getDataSetResult());
        FileInputStream inputStream = new FileInputStream(file);

        Workbook wb = null;
        String fileName = file.getName();
        String fileExtensionName = fileName.substring(fileName.indexOf("."));

        if (fileExtensionName.equals(".xlsx")) {
            wb = new XSSFWorkbook(inputStream);
        } else if (fileExtensionName.equals(".xls")) {
            wb = new HSSFWorkbook(inputStream);
        }

        return wb.getSheet(sheetName);
    }
}
