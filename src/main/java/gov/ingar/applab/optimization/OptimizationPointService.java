package gov.ingar.applab.optimization;

import gov.ingar.applab.optimization.dto.OptimizationPointDTO;

import java.net.URISyntaxException;

public interface OptimizationPointService {

    /**
     * Load dto from data service to run an optimization
     * @return the dto start the optimization
     */
    OptimizationPointDTO loadCase();

    /**
     * Run the optimization point
     */
    void runOptimizationPoint(OptimizationPointDTO optimizationPointDTO);

}
