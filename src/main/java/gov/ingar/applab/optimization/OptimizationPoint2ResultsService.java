package gov.ingar.applab.optimization;

import gov.ingar.applab.service.ConfigProccessTimeService;
import gov.ingar.applab.service.dto.ConfigProccessTimeDTO;
import javafx.util.Pair;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

@Service
@Transactional(readOnly = true)
public class OptimizationPoint2ResultsService {
    private final FileService fileService;
    private final ConfigProccessTimeService configProccessTimeService;

    public OptimizationPoint2ResultsService(FileService fileService, ConfigProccessTimeService configProccessTimeService) {
        this.fileService = fileService;
        this.configProccessTimeService = configProccessTimeService;
    }

    /**
     * Funcion que obtiene un resumen del resultado de la ejecucion del opt2
     *
     * @param modelRunDefId
     * @return JSONArray
     * @throws IOException
     */
    public JSONArray getResume(Long modelRunDefId) throws IOException {
        Sheet sheet = this.fileService.getResultSheet(modelRunDefId, "Resumen");
        int rowCount = sheet.getLastRowNum();
        JSONArray resume = new JSONArray();

        for (int i = 0; i < rowCount + 1; i++) {
            Row row = sheet.getRow(i);
            if (row != null) {
                List<String> resumeRow = new ArrayList<>();
                int colCount = row.getLastCellNum();
                for (int j = 0; j < colCount; j++) {
                    Cell currentCell = row.getCell(j);
                    if (currentCell != null) {
                        if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                            resumeRow.add(String.valueOf(currentCell.getNumericCellValue()));
                        } else {
                            resumeRow.add(currentCell.getStringCellValue());
                        }
                    }
                }
                resume.put(resumeRow);
            }
        }

        return resume;
    }

    /**
     * Funcion que obtiene la demanda insatisfecha por tipo de tabla (prod final)
     *
     * @param modelRunDefId
     * @return JSONObject
     * @throws IOException
     */
    public JSONObject getChart1(Long modelRunDefId) throws IOException {
        Sheet sheet = this.fileService.getResultSheet(modelRunDefId, "R-TablasFInales");
        int rowCount = sheet.getLastRowNum();
        JSONObject demand = new JSONObject();

        // 3 columnas donde estan los datos
        int[] cols = new int[]{1, 4, 10};

        Cell current;
        int value;

        for (int i = 1; i < rowCount + 1; i++) {
            Row row = sheet.getRow(i);
            if (row != null) {
                // resta tablas producidas
                current = row.getCell(cols[0]);
                if (current != null) {
                    if (demand.has(current.getStringCellValue()))
                        value = demand.getInt(current.getStringCellValue());
                    else
                        value = 0;
                    demand.put(row.getCell(cols[0]).getStringCellValue(), value - row.getCell(cols[0] + 1).getNumericCellValue());
                }

                // suma tablas entregadas
                current = row.getCell(cols[1]);
                if (current != null) {
                    value = demand.getInt(current.getStringCellValue());
                    demand.put(current.getStringCellValue(), value + row.getCell(cols[1] + 1).getNumericCellValue());
                }

                // suma demanda insatisfecha
                current = row.getCell(cols[2]);
                if (current != null) {
                    if (demand.has(current.getStringCellValue()))
                        value = demand.getInt(current.getStringCellValue());
                    else
                        value = 0;
                    demand.put(current.getStringCellValue(), value + row.getCell(cols[2] + 1).getNumericCellValue());
                }
            }
        }

        return demand;
    }

    /**
     * Funcion que obtiene un resumen de la produccion a realizar (patrones, tablas, tiempos, tipos)
     *
     * @param modelRunDefId
     * @return JSONObject
     * @throws IOException
     */
    public JSONObject getProductionData(Long modelRunDefId) throws IOException {
        Sheet sheetUsedResource = this.fileService.getResultSheet(modelRunDefId, "R-TroncosUsados");
        Sheet sheetPCprimary = this.fileService.getResultSheet(modelRunDefId, "D-PC-primario");
        Sheet sheetSetupTime = this.fileService.getResultSheet(modelRunDefId, "D-TiemposCostos");

        LinkedHashMap<Pair<String, String>, List<Pair<String, Integer>>> usedResourceList = this.getUsedResourceList(sheetUsedResource);
        LinkedHashMap<Pair<String, String>, List<Pair<String, Integer>>> pcPrimaryList = this.getPCPrimaryList(sheetPCprimary);
        LinkedHashMap<Pair<String, String>, Double> performanceList = this.getPerformanceList(sheetPCprimary);
        LinkedHashMap<Pair<String, String>, Integer> processTimeList = this.getProcessTimeList();
        int setupTime = this.getSetupTime(sheetSetupTime);

        // Armar orden de fabricacion con los datos obtenidos:
        // 1 - troncos usados por cada patron
        // 2 - tablas resultantes del corte de cada patron
        // 3 - rollos procesados por minuto (clase diametrica - largo)
        // 4 - tiempo de setup fijo (cambio de cuchillas entre cortes de distinto largo y clase diametrica)

        List<JSONObject> resumeData = new ArrayList<>();
        Iterator<Pair<String, String>> itr = usedResourceList.keySet().iterator();
        while (itr.hasNext()) {
            Pair<String, String> pattern = itr.next();
            JSONObject patternData = new JSONObject();

            // setea numero de patron
            patternData.put("name", pattern.getKey());

            // setea clase diametrica del patron
            patternData.put("diameterGroup", pattern.getValue());

            // setea cantidad de troncos procesados (segun largo) por patron
            patternData.put("resourceQty", usedResourceList.get(pattern));

            // setea rendimiento del patron
            patternData.put("performance", performanceList.get(pattern));

            // setea tipo de tablas que se obtienen del patron (definicion)
            patternData.put("tables", pcPrimaryList.get(pattern));

            // obtiene tiempo total de procesamiento segun los largos (en segundos)
            Integer patternProcessTime = 0;
            for (Pair<String, Integer> resourceQty : usedResourceList.get(pattern)) {
                Integer resourceProcessTime = processTimeList.get(new Pair<>(pattern.getValue(), resourceQty.getKey()));
                patternProcessTime = patternProcessTime + (int) java.lang.Math.floor(((double) resourceQty.getValue() / (double) resourceProcessTime * 60));
            }

            patternData.put("patternProcessTime", patternProcessTime);
            resumeData.add(patternData);
        }

        JSONObject productionData = new JSONObject();
        productionData.put("setupTime", setupTime);
        productionData.put("production", resumeData);

        return productionData;
    }

    /**
     * Funcion auxiliar que retorna el tiempo de setup en minutos
     *
     * @return
     */
    public int getSetupTime(Sheet sheet) {
        try {
            //obtiene el valor especifico en cierta fila - columna (sino obtener de modelRunDefConfig y buscar variable tiempo)
            return (int) sheet.getRow(2).getCell(2).getNumericCellValue();
        } catch (Exception e) {
            return 1;
        }
    }

    /**
     * Funcion auxiliar para obtener los tiempos de proceso
     *
     * @return
     */
    public LinkedHashMap<Pair<String, String>, Integer> getProcessTimeList() {
        LinkedHashMap<Pair<String, String>, Integer> processTimeList = new LinkedHashMap<>();

        List<ConfigProccessTimeDTO> processTime = this.configProccessTimeService.findAllList();

        for (int i = 0; i < processTime.size(); i++) {
            ConfigProccessTimeDTO item = processTime.get(i);
            processTimeList.put(new Pair<>(item.getDiameterGroupCode(), item.getLongGroupCode()), item.getValue().intValue());
        }

        return processTimeList;
    }

    /**
     * Funcion auxiliar para obtener el rendimiento de cada patron
     *
     * @return
     */
    public LinkedHashMap<Pair<String, String>, Double> getPerformanceList(Sheet sheet) {
        LinkedHashMap<Pair<String, String>, Double> performanceList = new LinkedHashMap<>();

        int rowCount = sheet.getLastRowNum();
        for (int i = 2; i < rowCount + 1; i++) {
            Row row = sheet.getRow(i);
            if (row != null) {

                Pair<String, String> pattern = null;
                //obtiene el patron
                if (row.getCell(1) != null && row.getCell(2) != null) {
                    pattern = new Pair(row.getCell(2).getStringCellValue(), row.getCell(1).getStringCellValue());
                }

                //obtiene el rendimiento del patron
                Double performance = 0.0;
                if (row.getCell(3) != null) {
                    performance = row.getCell(3).getNumericCellValue();
                }

                //inserta los datos del patron obtenido
                performanceList.put(pattern, performance);
            }
        }

        return performanceList;
    }

    /**
     * Funcion auxiliar que retorna el contenido de la hoja PC-Primario
     *
     * @param sheet
     * @return
     */
    public LinkedHashMap<Pair<String, String>, List<Pair<String, Integer>>> getPCPrimaryList(Sheet sheet) {
        LinkedHashMap<Pair<String, String>, List<Pair<String, Integer>>> pcPrimaryList = new LinkedHashMap<>();

        int rowCount = sheet.getLastRowNum();
        List<String> codeTables = new ArrayList<>();
        for (int i = 1; i < rowCount + 1; i++) {
            Row row = sheet.getRow(i);
            if (row != null) {

                //obtiene los tipos de largos
                if (i == 1) {
                    int colCount = row.getLastCellNum();
                    for (int j = 4; j < colCount; j++) {
                        Cell currentCell = row.getCell(j);
                        if (currentCell != null)
                            codeTables.add(currentCell.getStringCellValue());
                    }
                } else {
                    int colCount = row.getLastCellNum();
                    Pair<String, String> pattern = null;
                    //obtiene el patron
                    if (row.getCell(1) != null && row.getCell(2) != null) {
                        pattern = new Pair(row.getCell(2).getStringCellValue(), row.getCell(1).getStringCellValue());
                    }

                    //obtiene cantidad de tablas
                    List<Pair<String, Integer>> tablesQty = new ArrayList<>();
                    for (int j = 4; j < colCount; j++) {
                        Cell currentCell = row.getCell(j);
                        if (currentCell != null) {
                            if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                                tablesQty.add(new Pair(codeTables.get(j - 4), (int) currentCell.getNumericCellValue()));
                            }
                        }
                    }

                    //inserta los datos del patron obtenido
                    pcPrimaryList.put(pattern, tablesQty);
                }
            }
        }

        return pcPrimaryList;
    }

    /**
     * Funcion auxiliar que retorna el contenido de la hoja R-TroncosUsados
     *
     * @param sheet
     * @return
     */
    public LinkedHashMap<Pair<String, String>, List<Pair<String, Integer>>> getUsedResourceList(Sheet sheet) {
        LinkedHashMap<Pair<String, String>, List<Pair<String, Integer>>> usedResourceList = new LinkedHashMap<>();

        int rowCount = sheet.getLastRowNum();
        List<String> largeTables = new ArrayList<>();
        for (int i = 1; i < rowCount + 1; i++) {
            Row row = sheet.getRow(i);
            if (row != null) {

                //obtiene los tipos de largos
                if (i == 1) {
                    int colCount = row.getLastCellNum();
                    for (int j = 3; j < colCount; j++) {
                        Cell currentCell = row.getCell(j);
                        if (currentCell != null)
                            largeTables.add(currentCell.getStringCellValue());
                    }
                } else {
                    int colCount = row.getLastCellNum();
                    Pair<String, String> pattern = null;
                    //obtiene el patron
                    if (row.getCell(1) != null && row.getCell(2) != null) {
                        pattern = new Pair(row.getCell(1).getStringCellValue(), row.getCell(2).getStringCellValue());
                    }

                    //obtiene cantidad de troncos
                    List<Pair<String, Integer>> resourceQty = new ArrayList<>();
                    for (int j = 3; j < colCount; j++) {
                        Cell currentCell = row.getCell(j);
                        if (currentCell != null) {
                            if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                                resourceQty.add(new Pair(largeTables.get(j - 3), (int) currentCell.getNumericCellValue()));
                            }
                        }
                    }

                    //inserta los datos del patron obtenido
                    usedResourceList.put(pattern, resourceQty);
                }
            }
        }

        return usedResourceList;
    }
}
