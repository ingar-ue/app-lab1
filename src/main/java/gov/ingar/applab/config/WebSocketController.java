package gov.ingar.applab.config;


import gov.ingar.applab.domain.enumeration.Status;
import gov.ingar.applab.optimization.dto.ModelRunDefMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    @Autowired
    private SimpMessagingTemplate template;

//    @MessageMapping("/socket")
//    @SendTo("/topic/greetings")
//    public Greeting greeting(HelloMessage message) throws Exception {
//        Thread.sleep(1000); // simulated delay
//        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
//    }

    public void fireMessage(String receiver, ModelRunDefMessage message) {
        this.template.convertAndSend(receiver, message);
    }
}
