package gov.ingar.applab.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(gov.ingar.applab.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(gov.ingar.applab.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.Company.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.LongGroup.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.DiameterGroup.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.InterProduct.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.FinalProduct.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.EnvConfig.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.KerfConfig.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ConfigProccessTime.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ConfigTableCorrection.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ConfigYieldPlanning.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ProductRelations.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ResourceQty.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ResourceCost.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.PatternExclusion.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ModelDef.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ModelConfig.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ModelVersion.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ModelObjFunction.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ModelRunDef.class.getName(), jcacheConfiguration);
            cm.createCache(gov.ingar.applab.domain.ModelRunConfig.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
