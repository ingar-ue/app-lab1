import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2Webstorage, LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { AppLabSharedModule } from 'app/shared';
import { AppLabCoreModule } from 'app/core';
import { AppLabAppRoutingModule } from './app-routing.module';
import { AppLabHomeModule } from './home/home.module';
import { AppLabAccountModule } from './account/account.module';
import { AppLabEntityModule } from './entities/entity.module';
import { AppLabOptModule } from './opt/opt.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent, NavbarComponent, FooterComponent, PageRibbonComponent, ActiveMenuDirective, ErrorComponent } from './layouts';
import { StompRService } from '@stomp/ng2-stompjs';
import { AgGridModule } from 'ag-grid-angular';
import { ChartModule } from 'angular2-chartjs';
import { SuiModule } from 'ng2-semantic-ui';

// import { NumericEditorComponent } from '../custom-grid/editors/numeric-editor/numeric-editor.component';

@NgModule({
    imports: [
        BrowserModule,
        AppLabAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        AppLabSharedModule,
        AppLabCoreModule,
        AppLabHomeModule,
        AppLabAccountModule,
        AppLabEntityModule,
        AppLabOptModule,
        AgGridModule.withComponents([]),
        ChartModule,
        SuiModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    // entryComponents: [NumericEditorComponent],
    entryComponents: [],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
        // NumericEditorComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [LocalStorageService, SessionStorageService]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [Injector]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [JhiEventManager]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [Injector]
        },
        StompRService
    ],
    bootstrap: [JhiMainComponent]
})
export class AppLabAppModule {}
