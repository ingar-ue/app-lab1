import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IInterProduct } from 'app/shared/model/inter-product.model';
import { Principal } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { InterProductService } from './inter-product.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
    selector: 'jhi-inter-product',
    templateUrl: './inter-product.component.html'
})
export class InterProductComponent implements OnInit, OnDestroy {
    interProducts: IInterProduct[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    search: any;

    constructor(
        private interProductService: InterProductService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private translateService: TranslateService
    ) {
        this.interProducts = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.search = { keyword: '' };
        this.setSearchLanguage();

        this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            this.setSearchLanguage();
        });
    }

    setSearchLanguage() {
        this.search.inputPlaceholder = this.translateService.instant('global.searchKeywordPlaceholder');
        this.search.selectPlaceholder = this.translateService.instant('global.searchByPlaceholder');
        this.search.fields = [
            { value: 'code', label: this.translateService.instant('appLabApp.interProduct.code') },
            { value: 'tickness', label: this.translateService.instant('appLabApp.interProduct.tickness') },
            { value: 'width', label: this.translateService.instant('appLabApp.interProduct.width') },
            { value: 'longGroups', label: this.translateService.instant('appLabApp.interProduct.longGroups') }
        ];
    }

    loadAll() {
        this.interProductService
            .query({
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort(),
                type: this.search.field,
                keyword: this.search.keyword
            })
            .subscribe(
                (res: HttpResponse<IInterProduct[]>) => this.paginateInterProducts(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    reset() {
        this.page = 0;
        this.interProducts = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInInterProducts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IInterProduct) {
        return item.id;
    }

    registerChangeInInterProducts() {
        this.eventSubscriber = this.eventManager.subscribe('interProductListModification', response => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private paginateInterProducts(data: IInterProduct[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        for (let i = 0; i < data.length; i++) {
            this.interProducts.push(data[i]);
        }
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
