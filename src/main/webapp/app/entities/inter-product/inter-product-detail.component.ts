import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInterProduct } from 'app/shared/model/inter-product.model';

@Component({
    selector: 'jhi-inter-product-detail',
    templateUrl: './inter-product-detail.component.html'
})
export class InterProductDetailComponent implements OnInit {
    interProduct: IInterProduct;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ interProduct }) => {
            this.interProduct = interProduct;
        });
    }

    previousState() {
        window.history.back();
    }
}
