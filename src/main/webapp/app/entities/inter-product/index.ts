export * from './inter-product.service';
export * from './inter-product-update.component';
export * from './inter-product-delete-dialog.component';
export * from './inter-product-detail.component';
export * from './inter-product.component';
export * from './inter-product.route';
