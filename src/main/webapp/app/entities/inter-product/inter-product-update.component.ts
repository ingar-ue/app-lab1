import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IInterProduct } from 'app/shared/model/inter-product.model';
import { InterProductService } from './inter-product.service';
import { SummaryService } from '../../opt/summary/summary.service';

@Component({
    selector: 'jhi-inter-product-update',
    templateUrl: './inter-product-update.component.html'
})
export class InterProductUpdateComponent implements OnInit {
    private _interProduct: IInterProduct;
    isSaving: boolean;
    longGroupSummary: any = {};

    constructor(
        private interProductService: InterProductService,
        private activatedRoute: ActivatedRoute,
        private summaryService: SummaryService
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.loadSummaryData();
        this.activatedRoute.data.subscribe(({ interProduct }) => {
            this.interProduct = interProduct;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.interProduct.id !== undefined) {
            this.subscribeToSaveResponse(this.interProductService.update(this.interProduct));
        } else {
            this.subscribeToSaveResponse(this.interProductService.create(this.interProduct));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IInterProduct>>) {
        result.subscribe((res: HttpResponse<IInterProduct>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get interProduct() {
        return this._interProduct;
    }

    set interProduct(interProduct: IInterProduct) {
        this._interProduct = interProduct;
    }

    loadSummaryData() {
        // get long group summary
        this.summaryService.longGroupSummary().subscribe((longGroupSummary: any) => {
            console.log('longGroupSummary');
            this.longGroupSummary = longGroupSummary.body;
        });
    }
}
