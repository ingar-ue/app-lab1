import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IInterProduct } from 'app/shared/model/inter-product.model';

type EntityResponseType = HttpResponse<IInterProduct>;
type EntityArrayResponseType = HttpResponse<IInterProduct[]>;

@Injectable({ providedIn: 'root' })
export class InterProductService {
    private resourceUrl = SERVER_API_URL + 'api/inter-products';

    constructor(private http: HttpClient) {}

    create(interProduct: IInterProduct): Observable<EntityResponseType> {
        return this.http.post<IInterProduct>(this.resourceUrl, interProduct, { observe: 'response' });
    }

    update(interProduct: IInterProduct): Observable<EntityResponseType> {
        return this.http.put<IInterProduct>(this.resourceUrl, interProduct, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IInterProduct>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IInterProduct[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
