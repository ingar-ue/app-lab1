import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SuiModule } from 'ng2-semantic-ui';

import { AppLabSharedModule } from 'app/shared';
import {
    InterProductComponent,
    InterProductDetailComponent,
    InterProductUpdateComponent,
    InterProductDeletePopupComponent,
    InterProductDeleteDialogComponent,
    interProductRoute,
    interProductPopupRoute
} from './';

const ENTITY_STATES = [...interProductRoute, ...interProductPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES), SuiModule],
    declarations: [
        InterProductComponent,
        InterProductDetailComponent,
        InterProductUpdateComponent,
        InterProductDeleteDialogComponent,
        InterProductDeletePopupComponent
    ],
    entryComponents: [
        InterProductComponent,
        InterProductUpdateComponent,
        InterProductDeleteDialogComponent,
        InterProductDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabInterProductModule {}
