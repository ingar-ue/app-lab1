import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { InterProduct } from 'app/shared/model/inter-product.model';
import { InterProductService } from './inter-product.service';
import { InterProductComponent } from './inter-product.component';
import { InterProductDetailComponent } from './inter-product-detail.component';
import { InterProductUpdateComponent } from './inter-product-update.component';
import { InterProductDeletePopupComponent } from './inter-product-delete-dialog.component';
import { IInterProduct } from 'app/shared/model/inter-product.model';

@Injectable({ providedIn: 'root' })
export class InterProductResolve implements Resolve<IInterProduct> {
    constructor(private service: InterProductService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((interProduct: HttpResponse<InterProduct>) => interProduct.body));
        }
        return of(new InterProduct());
    }
}

export const interProductRoute: Routes = [
    {
        path: 'inter-product',
        component: InterProductComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.interProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'inter-product/:id/view',
        component: InterProductDetailComponent,
        resolve: {
            interProduct: InterProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.interProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'inter-product/new',
        component: InterProductUpdateComponent,
        resolve: {
            interProduct: InterProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.interProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'inter-product/:id/edit',
        component: InterProductUpdateComponent,
        resolve: {
            interProduct: InterProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.interProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const interProductPopupRoute: Routes = [
    {
        path: 'inter-product/:id/delete',
        component: InterProductDeletePopupComponent,
        resolve: {
            interProduct: InterProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.interProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
