import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IResourceQty } from 'app/shared/model/resource-qty.model';

type EntityResponseType = HttpResponse<IResourceQty>;
type EntityArrayResponseType = HttpResponse<IResourceQty[]>;

@Injectable({ providedIn: 'root' })
export class ResourceQtyService {
    private resourceUrl = SERVER_API_URL + 'api/resource-qties';

    constructor(private http: HttpClient) {}

    create(resourceQty: IResourceQty): Observable<EntityResponseType> {
        return this.http.post<IResourceQty>(this.resourceUrl, resourceQty, { observe: 'response' });
    }

    update(resourceQty: IResourceQty): Observable<EntityResponseType> {
        return this.http.put<IResourceQty>(this.resourceUrl, resourceQty, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IResourceQty>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IResourceQty[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
