import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ResourceQty } from 'app/shared/model/resource-qty.model';
import { ResourceQtyService } from './resource-qty.service';
import { ResourceQtyComponent } from './resource-qty.component';
import { ResourceQtyDetailComponent } from './resource-qty-detail.component';
import { ResourceQtyUpdateComponent } from './resource-qty-update.component';
import { ResourceQtyDeletePopupComponent } from './resource-qty-delete-dialog.component';
import { IResourceQty } from 'app/shared/model/resource-qty.model';

@Injectable({ providedIn: 'root' })
export class ResourceQtyResolve implements Resolve<IResourceQty> {
    constructor(private service: ResourceQtyService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((resourceQty: HttpResponse<ResourceQty>) => resourceQty.body));
        }
        return of(new ResourceQty());
    }
}

export const resourceQtyRoute: Routes = [
    {
        path: 'resource-qty',
        component: ResourceQtyComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.resourceQty.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'resource-qty/:id/view',
        component: ResourceQtyDetailComponent,
        resolve: {
            resourceQty: ResourceQtyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.resourceQty.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'resource-qty/new',
        component: ResourceQtyUpdateComponent,
        resolve: {
            resourceQty: ResourceQtyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.resourceQty.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'resource-qty/:id/edit',
        component: ResourceQtyUpdateComponent,
        resolve: {
            resourceQty: ResourceQtyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.resourceQty.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const resourceQtyPopupRoute: Routes = [
    {
        path: 'resource-qty/:id/delete',
        component: ResourceQtyDeletePopupComponent,
        resolve: {
            resourceQty: ResourceQtyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.resourceQty.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
