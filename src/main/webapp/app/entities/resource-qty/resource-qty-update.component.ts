import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IResourceQty } from 'app/shared/model/resource-qty.model';
import { ResourceQtyService } from './resource-qty.service';
import { IDiameterGroup } from 'app/shared/model/diameter-group.model';
import { DiameterGroupService } from 'app/entities/diameter-group';
import { ILongGroup } from 'app/shared/model/long-group.model';
import { LongGroupService } from 'app/entities/long-group';

@Component({
    selector: 'jhi-resource-qty-update',
    templateUrl: './resource-qty-update.component.html'
})
export class ResourceQtyUpdateComponent implements OnInit {
    private _resourceQty: IResourceQty;
    isSaving: boolean;

    diametergroups: IDiameterGroup[];

    longgroups: ILongGroup[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private resourceQtyService: ResourceQtyService,
        private diameterGroupService: DiameterGroupService,
        private longGroupService: LongGroupService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ resourceQty }) => {
            this.resourceQty = resourceQty;
        });
        this.diameterGroupService.query().subscribe(
            (res: HttpResponse<IDiameterGroup[]>) => {
                this.diametergroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.longGroupService.query().subscribe(
            (res: HttpResponse<ILongGroup[]>) => {
                this.longgroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.resourceQty.id !== undefined) {
            this.subscribeToSaveResponse(this.resourceQtyService.update(this.resourceQty));
        } else {
            this.subscribeToSaveResponse(this.resourceQtyService.create(this.resourceQty));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IResourceQty>>) {
        result.subscribe((res: HttpResponse<IResourceQty>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackDiameterGroupById(index: number, item: IDiameterGroup) {
        return item.id;
    }

    trackLongGroupById(index: number, item: ILongGroup) {
        return item.id;
    }
    get resourceQty() {
        return this._resourceQty;
    }

    set resourceQty(resourceQty: IResourceQty) {
        this._resourceQty = resourceQty;
    }
}
