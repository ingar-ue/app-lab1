import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IResourceQty } from 'app/shared/model/resource-qty.model';
import { ResourceQtyService } from './resource-qty.service';

@Component({
    selector: 'jhi-resource-qty-delete-dialog',
    templateUrl: './resource-qty-delete-dialog.component.html'
})
export class ResourceQtyDeleteDialogComponent {
    resourceQty: IResourceQty;

    constructor(
        private resourceQtyService: ResourceQtyService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.resourceQtyService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'resourceQtyListModification',
                content: 'Deleted an resourceQty'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-resource-qty-delete-popup',
    template: ''
})
export class ResourceQtyDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ resourceQty }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ResourceQtyDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.resourceQty = resourceQty;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
