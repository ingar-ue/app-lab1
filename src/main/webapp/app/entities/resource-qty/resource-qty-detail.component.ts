import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IResourceQty } from 'app/shared/model/resource-qty.model';

@Component({
    selector: 'jhi-resource-qty-detail',
    templateUrl: './resource-qty-detail.component.html'
})
export class ResourceQtyDetailComponent implements OnInit {
    resourceQty: IResourceQty;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ resourceQty }) => {
            this.resourceQty = resourceQty;
        });
    }

    previousState() {
        window.history.back();
    }
}
