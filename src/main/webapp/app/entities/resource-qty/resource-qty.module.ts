import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ResourceQtyComponent,
    ResourceQtyDetailComponent,
    ResourceQtyUpdateComponent,
    ResourceQtyDeletePopupComponent,
    ResourceQtyDeleteDialogComponent,
    resourceQtyRoute,
    resourceQtyPopupRoute
} from './';

const ENTITY_STATES = [...resourceQtyRoute, ...resourceQtyPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ResourceQtyComponent,
        ResourceQtyDetailComponent,
        ResourceQtyUpdateComponent,
        ResourceQtyDeleteDialogComponent,
        ResourceQtyDeletePopupComponent
    ],
    entryComponents: [ResourceQtyComponent, ResourceQtyUpdateComponent, ResourceQtyDeleteDialogComponent, ResourceQtyDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabResourceQtyModule {}
