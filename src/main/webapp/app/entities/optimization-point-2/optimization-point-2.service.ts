import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { IOptimizationPoint2 } from 'app/shared/model/optimization-point-2.model';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';

type EntityResponseType = HttpResponse<IOptimizationPoint2>;
type EntityArrayResponseType = HttpResponse<IOptimizationPoint2[]>;

@Injectable({ providedIn: 'root' })
export class OptimizationPoint2Service {
    private resourceUrl = SERVER_API_URL + 'api/optimization-point-2';

    constructor(private http: HttpClient) {}

    loadCase(): Observable<HttpResponse<IOptimizationPoint2>> {
        return this.http
            .get(this.resourceUrl + '-load', { observe: 'response' })
            .pipe(map((res: HttpResponse<IOptimizationPoint2>) => res));
    }

    run(optimizationPoint2: IOptimizationPoint2): Observable<HttpResponse<any>> {
        return this.http.put(this.resourceUrl + '-execute', optimizationPoint2, { observe: 'response' }).pipe(map((res: any) => res));
    }

    loadObjFunctions(modelDefId: number): Observable<HttpResponse<IModelObjFunction[]>> {
        return this.http
            .get(this.resourceUrl + '-load-obj-functions/' + modelDefId, { observe: 'response' })
            .pipe(map((res: HttpResponse<IModelObjFunction[]>) => res));
    }

    loadConfigs(modelDefId: number): Observable<HttpResponse<IModelRunConfig[]>> {
        return this.http
            .get(this.resourceUrl + '-load-configs/' + modelDefId, { observe: 'response' })
            .pipe(map((res: HttpResponse<IModelRunConfig[]>) => res));
    }

    checkRunning(): Observable<HttpResponse<IModelRunDef>> {
        return this.http
            .get(this.resourceUrl + '-check-running', { observe: 'response' })
            .pipe(map((res: HttpResponse<IModelRunDef>) => res));
    }

    private convertDateFromClient(optimizationPoint2: IOptimizationPoint2): IOptimizationPoint2 {
        const copy: IOptimizationPoint2 = Object.assign({}, optimizationPoint2, {
            // runDate:
            //     optimizationPoint2.runDate != null && optimizationPoint2.runDate.isValid()
            //         ? optimizationPoint2.runDate.format(DATE_FORMAT)
            //         : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        // res.body.runDate = res.body.runDate != null ? moment(res.body.runDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        // res.body.forEach((optimizationPoint2: IOptimizationPoint2) => {
        //     optimizationPoint2.runDate = optimizationPoint2.runDate != null ? moment(optimizationPoint2.runDate) : null;
        // });
        return res;
    }

    getChartData(modelRunDefId, chart): Observable<HttpResponse<any>> {
        return this.http
            .get(this.resourceUrl + `-results-${chart}/${modelRunDefId}`, { observe: 'response' })
            .pipe(map((res: HttpResponse<any>) => res.body));
    }
}
