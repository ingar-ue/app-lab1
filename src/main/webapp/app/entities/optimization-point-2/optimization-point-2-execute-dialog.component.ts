import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOptimizationPoint2 } from 'app/shared/model/optimization-point-2.model';
import { OptimizationPoint2Service } from './optimization-point-2.service';

@Component({
    selector: 'jhi-optimization-point-2-execute-dialog',
    templateUrl: './optimization-point-2-execute-dialog.component.html'
})
export class OptimizationPoint2ExecuteDialogComponent {
    optimizationPoint2: IOptimizationPoint2;

    constructor(
        private optimizationPoint2Service: OptimizationPoint2Service,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    optimizationPoint2Execute() {
        console.log(this.optimizationPoint2);
        this.optimizationPoint2Service.run(this.optimizationPoint2).subscribe(response => {
            this.eventManager.broadcast({
                name: 'optimizationPoint2ListModification',
                content: 'OptimizationPoint2 generated'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-optimization-point-2-execute-popup',
    template: ''
})
export class OptimizationPoint2ExecutePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ optimizationPoint2 }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(OptimizationPoint2ExecuteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.optimizationPoint2 = optimizationPoint2;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
