import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { OptimizationPoint2 } from 'app/shared/model/optimization-point-2.model';
import { OptimizationPoint2Service } from './optimization-point-2.service';
import { OptimizationPoint2Component } from './optimization-point-2.component';
import { OptimizationPoint2ExecutePopupComponent } from './optimization-point-2-execute-dialog.component';
import { IOptimizationPoint2 } from 'app/shared/model/optimization-point-2.model';

@Injectable({ providedIn: 'root' })
export class OptimizationPoint2Resolve implements Resolve<IOptimizationPoint2> {
    constructor(private service: OptimizationPoint2Service) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        // if (id) {
        //     return this.service.find(id).pipe(map((optimizationPoint2: HttpResponse<OptimizationPoint2>) => optimizationPoint2.body));
        // }
        return of(new OptimizationPoint2());
    }
}

export const optimizationPoint2Route: Routes = [
    {
        path: 'optimization-point-2',
        component: OptimizationPoint2Component,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.optimization.op2.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const optimizationPoint2PopupRoute: Routes = [
    {
        path: 'optimization-point-2/execute/:op2',
        component: OptimizationPoint2ExecutePopupComponent,
        resolve: {
            optimizationPoint2: OptimizationPoint2Resolve
        },
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.optimization.op2.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
