import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    OptimizationPoint2Component,
    OptimizationPoint2ExecutePopupComponent,
    OptimizationPoint2ExecuteDialogComponent,
    optimizationPoint2Route,
    optimizationPoint2PopupRoute
} from './';

import { AgGridModule } from 'ag-grid-angular';

import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-balham.css';

import { ChartModule } from 'angular2-chartjs';

const ENTITY_STATES = [...optimizationPoint2Route, ...optimizationPoint2PopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES), AgGridModule.withComponents([]), ChartModule],
    declarations: [OptimizationPoint2Component, OptimizationPoint2ExecutePopupComponent, OptimizationPoint2ExecuteDialogComponent],
    entryComponents: [OptimizationPoint2Component, OptimizationPoint2ExecutePopupComponent, OptimizationPoint2ExecuteDialogComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabOptimizationPoint2Module {}
