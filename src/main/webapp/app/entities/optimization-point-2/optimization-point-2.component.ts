import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IOptimizationPoint2 } from 'app/shared/model/optimization-point-2.model';
import { Principal } from 'app/core';

import { OptimizationPoint2Service } from './optimization-point-2.service';

import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-balham.css';
import { IModelRunDef, IModelRunDefMessage } from 'app/shared/model/model-run-def.model';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';

import { Message } from '@stomp/stompjs';
import { WebSocketService } from 'app/shared/web-socket/web-socket.service';

@Component({
    selector: 'jhi-optimization-point-2',
    templateUrl: './optimization-point-2.component.html'
})
export class OptimizationPoint2Component implements OnInit, OnDestroy {
    currentAccount: any;
    eventSubscriber: Subscription;
    optimizationPoint2: IOptimizationPoint2;
    modelRunDef: IModelRunDef;
    modelRunDefChange = 0;
    objFunctions: IModelObjFunction[];
    configs: IModelRunConfig[];
    configValuesLoaded = true;
    messageSubscription: Subscription;
    executingOP1: boolean;
    noOP1: boolean;
    activeTabId: string;
    alertClosed: boolean;
    alertMessage = '';

    /**************** RANDOMIZATION PARAMS ********************/
    MIN_RESOURCEQTY_QUANTITY = 50;
    MAX_RESOURCEQTY_QUANTITY = 300;
    MIN_RESOURCECOST_VALUE = 200;
    MAX_RESOURCECOST_VALUE = 300;
    MIN_FINALPRODUCT_PRICE = 50;
    MAX_FINALPRODUCT_PRICE = 100;
    MIN_FINALPRODUCT_MINDEMAND = 0;
    MAX_FINALPRODUCT_MINDEMAND = 0;
    MIN_FINALPRODUCT_DEMAND_RANGE = 0;
    MAX_FINALPRODUCT_DEMAND_RANGE = 100;

    /******************* TABLE DATA **************************/
    // 1 - final products
    columnDefs1 = [
        { headerName: 'Código', field: 'code', editable: false },
        { headerName: 'Nombre', field: 'name', editable: false },
        { headerName: 'Precio', field: 'price', editable: true },
        { headerName: 'Demanda mínima', field: 'minDemand', editable: true },
        { headerName: 'Demanda máxima', field: 'maxDemand', editable: true }
    ];
    // 2 - long groups
    columnDefs2 = [
        { headerName: 'Código', field: 'code', editable: false },
        { headerName: 'Valor', field: 'value', editable: false },
        { headerName: 'Unidad', field: 'valType', editable: false }
    ];
    // 3 - diameter classes
    columnDefs3 = [
        { headerName: 'Código', field: 'code', editable: false },
        { headerName: 'Diámetro mínimo', field: 'minValue', editable: false },
        { headerName: 'Diámetro máximo', field: 'maxValue', editable: false },
        { headerName: 'Unidad', field: 'valType', editable: false }
    ];
    // 4 - inter products
    columnDefs4 = [
        { headerName: 'Código', field: 'code', editable: false },
        { headerName: 'Espesor', field: 'tickness', editable: false },
        { headerName: 'Ancho', field: 'width', editable: false },
        { headerName: 'Unidad', field: 'valType', editable: false }
    ];
    // 5 - product relations
    columnDefs5 = [
        { headerName: 'Producto intermedio', field: 'interProductCode', editable: false },
        { headerName: 'Largo', field: 'longGroupCode', editable: false },
        { headerName: 'Producto final 1', field: 'finalProduct1Code', editable: false },
        { headerName: 'Cantidad 1', field: 'quantity1', editable: false },
        { headerName: 'Producto final 2', field: 'finalProduct2Code', editable: false },
        { headerName: 'Cantidad 2', field: 'quantity2', editable: false },
        { headerName: 'Producto final 3', field: 'finalProduct3Code', editable: false },
        { headerName: 'Cantidad 3', field: 'quantity3', editable: false }
    ];
    // 6 - quantities
    columnDefs6 = [
        { headerName: 'Clase diamétrica', field: 'diameterGroupCode', editable: false },
        { headerName: 'Largo', field: 'longGroupCode', editable: false },
        { headerName: 'Cantidad', field: 'quantity', editable: true }
    ];
    // 7 - costs
    columnDefs7 = [
        { headerName: 'Clase diamétrica', field: 'diameterGroupCode', editable: false },
        { headerName: 'Costo', field: 'value', editable: true }
    ];

    rowData1: any;
    rowData2: any;
    rowData3: any;
    rowData4: any;
    rowData5: any;
    rowData6: any;
    rowData7: any;

    grid1IsValid = false;
    grid2IsValid = false;
    grid3IsValid = false;
    grid4IsValid = false;
    grid5IsValid = false;
    grid6IsValid = false;
    grid7IsValid = false;

    grid1ValidMsg: String;
    grid2ValidMsg: String;
    grid3ValidMsg: String;
    grid4ValidMsg: String;
    grid5ValidMsg: String;
    grid6ValidMsg: String;
    grid7ValidMsg: String;

    gridApi: any;

    /************************ CLASS **************************/
    constructor(
        private optimizationPoint2Service: OptimizationPoint2Service,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private service: WebSocketService
    ) {
        this.executingOP1 = false;
        this.noOP1 = false;
        this.activeTabId = 'dataTab';
        this.alertClosed = true;
        this.modelRunDef = {};
        this.gridApi = {};
    }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.loadCase();
        this.registerChangeInOptimizationPoint2s();
        this.subscribeToSocket();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.messageSubscription.unsubscribe();
    }

    registerChangeInOptimizationPoint2s() {
        this.eventSubscriber = this.eventManager.subscribe('optimizationPoint2ListModification', response => this.reset());
    }

    reset() {
        this.loadCase();
        this.registerChangeInOptimizationPoint2s();
    }

    loadCase() {
        this.optimizationPoint2Service.loadCase().subscribe((res: HttpResponse<IOptimizationPoint2>) => {
            this.optimizationPoint2 = res.body;
            this.optimizationPoint2Service.checkRunning().subscribe((res1: HttpResponse<IModelRunDef>) => {
                if (res1.body) {
                    if (res1.body.name === 'Planner') {
                        this.modelRunDef = res1.body;
                        this.modelRunDefChange++;
                        this.activeTabId = 'processTab';
                    } else {
                        this.executingOP1 = true;
                        this.modelRunDef = this.optimizationPoint2.modelRunDef;
                        this.modelRunDefChange++;
                    }
                } else {
                    this.modelRunDef = this.optimizationPoint2.modelRunDef;
                    this.modelRunDefChange++;
                }

                if (!this.optimizationPoint2.optimizationPoint1) {
                    this.noOP1 = true;
                }
            });

            // Populate tables
            this.rowData1 = this.optimizationPoint2.finalProducts;
            this.rowData2 = this.optimizationPoint2.longs;
            this.rowData3 = this.optimizationPoint2.diameters;
            this.rowData4 = this.optimizationPoint2.interProducts;
            this.rowData5 = this.optimizationPoint2.productRelations;
            this.rowData6 = this.optimizationPoint2.resourceQtys;
            this.rowData7 = this.optimizationPoint2.resourceCosts;

            // Validate data grids and set message
            this.validateCaseData();

            this.optimizationPoint2Service
                .loadObjFunctions(this.optimizationPoint2.modelDefId)
                .subscribe((res1: HttpResponse<IModelObjFunction[]>) => {
                    this.objFunctions = res1.body;
                });
            this.optimizationPoint2Service
                .loadConfigs(this.optimizationPoint2.modelDefId)
                .subscribe((res2: HttpResponse<IModelRunConfig[]>) => {
                    this.configs = res2.body;
                });
        });
    }

    randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    randomizeParameters() {
        // FinalProduct.price minDemand maxDemand randomization
        this.rowData1.forEach(Row => {
            Row.price = this.randomIntFromInterval(this.MIN_FINALPRODUCT_PRICE, this.MAX_FINALPRODUCT_PRICE);
            const md = this.randomIntFromInterval(this.MIN_FINALPRODUCT_MINDEMAND, this.MAX_FINALPRODUCT_MINDEMAND);
            const rd = this.randomIntFromInterval(this.MIN_FINALPRODUCT_DEMAND_RANGE, this.MAX_FINALPRODUCT_DEMAND_RANGE);
            Row.minDemand = md;
            Row.maxDemand = md + rd;
        });
        // ResourceQty.quantity randomization
        this.rowData6.forEach(Row => {
            Row.quantity = this.randomIntFromInterval(this.MIN_RESOURCEQTY_QUANTITY, this.MAX_RESOURCEQTY_QUANTITY);
        });
        // ResourceCost.value randomization
        this.rowData7.forEach(Row => {
            Row.value = this.randomIntFromInterval(this.MIN_RESOURCECOST_VALUE, this.MAX_RESOURCECOST_VALUE);
        });
        // this.registerChangeInOptimizationPoint2s();
        // this.gridApi.forEachNode();
        const params = { force: false };
        for (const grid of Object.keys(this.gridApi)) {
            this.gridApi[grid].refreshCells(params);
        }
    }

    subscribeToSocket() {
        this.messageSubscription = this.service.subscribe().subscribe((message: Message) => {
            const msg: IModelRunDefMessage = JSON.parse(message.body);
            if (msg.name === 'Planner') {
                this.modelRunDef.status = msg.status;
                this.modelRunDef.progress = msg.progress;
                this.modelRunDef.dataSetResult = msg.result;
                this.modelRunDef.id = msg.id;
                this.modelRunDefChange++;

                if (msg.progress === 100 && msg.status === 'FINISHED') {
                    this.fireSuccessAlert('La ejecución ha finalizado, puede visualizar los resultados en la pestaña correspondiente');
                    this.activeTabId = 'resultsTab';
                }

                if (msg.progress === 100 && msg.status === 'ERROR') {
                    this.modelRunDef.comments = msg.result;
                    this.activeTabId = 'resultsTab';
                }
            } else if (msg.progress === 100) {
                this.executingOP1 = false;
                if (this.noOP1) {
                    this.noOP1 = false;
                    this.fireSuccessAlert('Nuevo Generador de Patrones encontrado');
                    this.reset();
                } else {
                    this.fireSuccessAlert('La ejecución del otro proceso ha finalizado');
                }
            }
        });
    }

    run() {
        this.optimizationPoint2.modelRunConfigs = this.configs;
        this.optimizationPoint2Service.run(this.optimizationPoint2).subscribe(res => console.log(res));
    }

    fireSuccessAlert(msg: string) {
        this.alertMessage = msg;
        this.alertClosed = false;
        setTimeout(() => (this.alertClosed = true), 10000);
    }

    configChange() {
        this.configValuesLoaded = true;
        for (const config of this.configs) {
            if (!config.value) {
                this.configValuesLoaded = false;
            }
        }
    }

    tabChange(e) {
        this.activeTabId = e.nextId;
    }

    /******************* TABLE FUNCTIONS **************************/
    validateCaseData() {
        // grid 1
        if (this.rowData1.length > 0) {
            this.grid1IsValid = true;
            this.grid1ValidMsg = 'Datos correctos';
        } else {
            this.grid1IsValid = false;
            this.grid1ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 2
        if (this.rowData2.length > 0) {
            this.grid2IsValid = true;
            this.grid2ValidMsg = 'Datos correctos';
        } else {
            this.grid2IsValid = false;
            this.grid2ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 3
        if (this.rowData3.length > 0) {
            this.grid3IsValid = true;
            this.grid3ValidMsg = 'Datos correctos';
        } else {
            this.grid3IsValid = false;
            this.grid3ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 4
        if (this.rowData4.length > 0) {
            this.grid4IsValid = true;
            this.grid4ValidMsg = 'Datos correctos';
        } else {
            this.grid4IsValid = false;
            this.grid4ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 5
        if (this.rowData5.length > 0) {
            this.grid5IsValid = true;
            this.grid5ValidMsg = 'Datos correctos';
        } else {
            this.grid5IsValid = false;
            this.grid5ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 6
        if (this.rowData6.length > 0) {
            this.grid6IsValid = true;
            this.grid6ValidMsg = 'Datos correctos';
        } else {
            this.grid6IsValid = false;
            this.grid6ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 7
        if (this.rowData7.length > 0) {
            this.grid7IsValid = true;
            this.grid7ValidMsg = 'Datos correctos';
        } else {
            this.grid7IsValid = false;
            this.grid7ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
    }

    onGridReady(params, id) {
        this.gridApi[id] = params.api;
        // Setea ancho de las columnas para llenar ancho de la tabla
        params.api.sizeColumnsToFit();
        // Si la cantidad de filas a mostrar es mayor que 25 recorta el largo de la tabla
        console.log('params.api.getDisplayedRowCount() ' + params.api.getDisplayedRowCount());
        if (params.api.getDisplayedRowCount() > 10) {
            params.api.setGridAutoHeight(false);
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '300px';
        } else {
            params.api.setGridAutoHeight(true);
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '';
        }
    }
}
