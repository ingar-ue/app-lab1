export * from './config-table-correction.service';
export * from './config-table-correction-update.component';
export * from './config-table-correction-delete-dialog.component';
export * from './config-table-correction-detail.component';
export * from './config-table-correction.component';
export * from './config-table-correction.route';
