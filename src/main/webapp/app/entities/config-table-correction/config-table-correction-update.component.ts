import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IConfigTableCorrection } from 'app/shared/model/config-table-correction.model';
import { ConfigTableCorrectionService } from './config-table-correction.service';
import { IDiameterGroup } from 'app/shared/model/diameter-group.model';
import { DiameterGroupService } from 'app/entities/diameter-group';

@Component({
    selector: 'jhi-config-table-correction-update',
    templateUrl: './config-table-correction-update.component.html'
})
export class ConfigTableCorrectionUpdateComponent implements OnInit {
    private _configTableCorrection: IConfigTableCorrection;
    isSaving: boolean;

    diametergroups: IDiameterGroup[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private configTableCorrectionService: ConfigTableCorrectionService,
        private diameterGroupService: DiameterGroupService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ configTableCorrection }) => {
            this.configTableCorrection = configTableCorrection;
        });
        this.diameterGroupService.query().subscribe(
            (res: HttpResponse<IDiameterGroup[]>) => {
                this.diametergroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.configTableCorrection.id !== undefined) {
            this.subscribeToSaveResponse(this.configTableCorrectionService.update(this.configTableCorrection));
        } else {
            this.subscribeToSaveResponse(this.configTableCorrectionService.create(this.configTableCorrection));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IConfigTableCorrection>>) {
        result.subscribe(
            (res: HttpResponse<IConfigTableCorrection>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackDiameterGroupById(index: number, item: IDiameterGroup) {
        return item.id;
    }
    get configTableCorrection() {
        return this._configTableCorrection;
    }

    set configTableCorrection(configTableCorrection: IConfigTableCorrection) {
        this._configTableCorrection = configTableCorrection;
    }
}
