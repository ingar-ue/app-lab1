import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigTableCorrection } from 'app/shared/model/config-table-correction.model';
import { ConfigTableCorrectionService } from './config-table-correction.service';
import { ConfigTableCorrectionComponent } from './config-table-correction.component';
import { ConfigTableCorrectionDetailComponent } from './config-table-correction-detail.component';
import { ConfigTableCorrectionUpdateComponent } from './config-table-correction-update.component';
import { ConfigTableCorrectionDeletePopupComponent } from './config-table-correction-delete-dialog.component';
import { IConfigTableCorrection } from 'app/shared/model/config-table-correction.model';

@Injectable({ providedIn: 'root' })
export class ConfigTableCorrectionResolve implements Resolve<IConfigTableCorrection> {
    constructor(private service: ConfigTableCorrectionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service
                .find(id)
                .pipe(map((configTableCorrection: HttpResponse<ConfigTableCorrection>) => configTableCorrection.body));
        }
        return of(new ConfigTableCorrection());
    }
}

export const configTableCorrectionRoute: Routes = [
    {
        path: 'config-table-correction',
        component: ConfigTableCorrectionComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.configTableCorrection.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'config-table-correction/:id/view',
        component: ConfigTableCorrectionDetailComponent,
        resolve: {
            configTableCorrection: ConfigTableCorrectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configTableCorrection.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'config-table-correction/new',
        component: ConfigTableCorrectionUpdateComponent,
        resolve: {
            configTableCorrection: ConfigTableCorrectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configTableCorrection.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'config-table-correction/:id/edit',
        component: ConfigTableCorrectionUpdateComponent,
        resolve: {
            configTableCorrection: ConfigTableCorrectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configTableCorrection.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const configTableCorrectionPopupRoute: Routes = [
    {
        path: 'config-table-correction/:id/delete',
        component: ConfigTableCorrectionDeletePopupComponent,
        resolve: {
            configTableCorrection: ConfigTableCorrectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configTableCorrection.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
