import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IConfigTableCorrection } from 'app/shared/model/config-table-correction.model';
import { ConfigTableCorrectionService } from './config-table-correction.service';

@Component({
    selector: 'jhi-config-table-correction-delete-dialog',
    templateUrl: './config-table-correction-delete-dialog.component.html'
})
export class ConfigTableCorrectionDeleteDialogComponent {
    configTableCorrection: IConfigTableCorrection;

    constructor(
        private configTableCorrectionService: ConfigTableCorrectionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.configTableCorrectionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'configTableCorrectionListModification',
                content: 'Deleted an configTableCorrection'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-config-table-correction-delete-popup',
    template: ''
})
export class ConfigTableCorrectionDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ configTableCorrection }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ConfigTableCorrectionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.configTableCorrection = configTableCorrection;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
