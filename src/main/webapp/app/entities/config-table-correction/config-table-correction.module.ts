import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ConfigTableCorrectionComponent,
    ConfigTableCorrectionDetailComponent,
    ConfigTableCorrectionUpdateComponent,
    ConfigTableCorrectionDeletePopupComponent,
    ConfigTableCorrectionDeleteDialogComponent,
    configTableCorrectionRoute,
    configTableCorrectionPopupRoute
} from './';

const ENTITY_STATES = [...configTableCorrectionRoute, ...configTableCorrectionPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ConfigTableCorrectionComponent,
        ConfigTableCorrectionDetailComponent,
        ConfigTableCorrectionUpdateComponent,
        ConfigTableCorrectionDeleteDialogComponent,
        ConfigTableCorrectionDeletePopupComponent
    ],
    entryComponents: [
        ConfigTableCorrectionComponent,
        ConfigTableCorrectionUpdateComponent,
        ConfigTableCorrectionDeleteDialogComponent,
        ConfigTableCorrectionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabConfigTableCorrectionModule {}
