import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IConfigTableCorrection } from 'app/shared/model/config-table-correction.model';

@Component({
    selector: 'jhi-config-table-correction-detail',
    templateUrl: './config-table-correction-detail.component.html'
})
export class ConfigTableCorrectionDetailComponent implements OnInit {
    configTableCorrection: IConfigTableCorrection;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ configTableCorrection }) => {
            this.configTableCorrection = configTableCorrection;
        });
    }

    previousState() {
        window.history.back();
    }
}
