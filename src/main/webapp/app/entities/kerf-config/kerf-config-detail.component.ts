import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IKerfConfig } from 'app/shared/model/kerf-config.model';

@Component({
    selector: 'jhi-kerf-config-detail',
    templateUrl: './kerf-config-detail.component.html'
})
export class KerfConfigDetailComponent implements OnInit {
    kerfConfig: IKerfConfig;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ kerfConfig }) => {
            this.kerfConfig = kerfConfig;
        });
    }

    previousState() {
        window.history.back();
    }
}
