import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { KerfConfig } from 'app/shared/model/kerf-config.model';
import { KerfConfigService } from './kerf-config.service';
import { KerfConfigComponent } from './kerf-config.component';
import { KerfConfigDetailComponent } from './kerf-config-detail.component';
import { KerfConfigUpdateComponent } from './kerf-config-update.component';
import { KerfConfigDeletePopupComponent } from './kerf-config-delete-dialog.component';
import { IKerfConfig } from 'app/shared/model/kerf-config.model';

@Injectable({ providedIn: 'root' })
export class KerfConfigResolve implements Resolve<IKerfConfig> {
    constructor(private service: KerfConfigService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((kerfConfig: HttpResponse<KerfConfig>) => kerfConfig.body));
        }
        return of(new KerfConfig());
    }
}

export const kerfConfigRoute: Routes = [
    {
        path: 'kerf-config',
        component: KerfConfigComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.kerfConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'kerf-config/:id/view',
        component: KerfConfigDetailComponent,
        resolve: {
            kerfConfig: KerfConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.kerfConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'kerf-config/new',
        component: KerfConfigUpdateComponent,
        resolve: {
            kerfConfig: KerfConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.kerfConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'kerf-config/:id/edit',
        component: KerfConfigUpdateComponent,
        resolve: {
            kerfConfig: KerfConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.kerfConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const kerfConfigPopupRoute: Routes = [
    {
        path: 'kerf-config/:id/delete',
        component: KerfConfigDeletePopupComponent,
        resolve: {
            kerfConfig: KerfConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.kerfConfig.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
