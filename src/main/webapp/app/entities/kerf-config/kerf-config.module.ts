import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    KerfConfigComponent,
    KerfConfigDetailComponent,
    KerfConfigUpdateComponent,
    KerfConfigDeletePopupComponent,
    KerfConfigDeleteDialogComponent,
    kerfConfigRoute,
    kerfConfigPopupRoute
} from './';

const ENTITY_STATES = [...kerfConfigRoute, ...kerfConfigPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        KerfConfigComponent,
        KerfConfigDetailComponent,
        KerfConfigUpdateComponent,
        KerfConfigDeleteDialogComponent,
        KerfConfigDeletePopupComponent
    ],
    entryComponents: [KerfConfigComponent, KerfConfigUpdateComponent, KerfConfigDeleteDialogComponent, KerfConfigDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabKerfConfigModule {}
