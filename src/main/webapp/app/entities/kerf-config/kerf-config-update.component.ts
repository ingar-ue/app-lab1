import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IKerfConfig } from 'app/shared/model/kerf-config.model';
import { KerfConfigService } from './kerf-config.service';

@Component({
    selector: 'jhi-kerf-config-update',
    templateUrl: './kerf-config-update.component.html'
})
export class KerfConfigUpdateComponent implements OnInit {
    private _kerfConfig: IKerfConfig;
    isSaving: boolean;

    constructor(private kerfConfigService: KerfConfigService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ kerfConfig }) => {
            this.kerfConfig = kerfConfig;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.kerfConfig.id !== undefined) {
            this.subscribeToSaveResponse(this.kerfConfigService.update(this.kerfConfig));
        } else {
            this.subscribeToSaveResponse(this.kerfConfigService.create(this.kerfConfig));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IKerfConfig>>) {
        result.subscribe((res: HttpResponse<IKerfConfig>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get kerfConfig() {
        return this._kerfConfig;
    }

    set kerfConfig(kerfConfig: IKerfConfig) {
        this._kerfConfig = kerfConfig;
    }
}
