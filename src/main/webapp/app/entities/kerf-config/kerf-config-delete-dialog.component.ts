import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IKerfConfig } from 'app/shared/model/kerf-config.model';
import { KerfConfigService } from './kerf-config.service';

@Component({
    selector: 'jhi-kerf-config-delete-dialog',
    templateUrl: './kerf-config-delete-dialog.component.html'
})
export class KerfConfigDeleteDialogComponent {
    kerfConfig: IKerfConfig;

    constructor(private kerfConfigService: KerfConfigService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.kerfConfigService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'kerfConfigListModification',
                content: 'Deleted an kerfConfig'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-kerf-config-delete-popup',
    template: ''
})
export class KerfConfigDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ kerfConfig }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(KerfConfigDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.kerfConfig = kerfConfig;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
