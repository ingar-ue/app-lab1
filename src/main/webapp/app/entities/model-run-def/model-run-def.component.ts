import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IModelRunDef, ResultStatus } from 'app/shared/model/model-run-def.model';
import { Principal } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { ModelRunDefService } from './model-run-def.service';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Moment } from 'moment';
import { Router } from '@angular/router';

@Component({
    selector: 'jhi-model-run-def',
    templateUrl: './model-run-def.component.html',
    styleUrls: ['model-run-def.css']
})
export class ModelRunDefComponent implements OnInit, OnDestroy {
    modelRunDefs: IModelRunDef[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    searchNameString = 'Planner';

    /******DATE PICKER VARIABLES*******/
    hoveredDate: NgbDate;
    fromDate: NgbDate;
    toDate: NgbDate;
    selectedDate: Moment;
    range_date = '';

    constructor(
        private modelRunDefService: ModelRunDefService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private calendar: NgbCalendar,
        private router: Router
    ) {
        this.modelRunDefs = [];
        this.itemsPerPage = 100;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;

        /******DATE PICKER DECLARATIONS*******/
        this.toDate = calendar.getToday();
        this.fromDate = calendar.getPrev(calendar.getToday(), 'd', 28);
        this.range_date = this.formatDate(this.fromDate) + ' - ' + this.formatDate(this.toDate);
    }

    loadAll() {
        this.modelRunDefService
            .query({
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort(),
                name: this.searchNameString,
                begin: this.fromDate,
                end: this.toDate
            })
            .subscribe(
                (res: HttpResponse<IModelRunDef[]>) => this.paginateModelRunDefs(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    reset() {
        this.page = 0;
        this.modelRunDefs = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInModelRunDefs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IModelRunDef) {
        return item.id;
    }

    registerChangeInModelRunDefs() {
        this.eventSubscriber = this.eventManager.subscribe('modelRunDefListModification', response => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private paginateModelRunDefs(data: IModelRunDef[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        for (let i = 0; i < data.length; i++) {
            this.modelRunDefs.push(data[i]);
        }

        const aux = Object.assign([], this.modelRunDefs);
        this.modelRunDefs = [];
        setTimeout(() => {
            this.modelRunDefs = aux;
        }, 10);
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    /******COMPARE FUNCTIONS***********/
    changeCompareList(modelGroup, modelRunDef) {
        if (typeof modelGroup.compareList === 'undefined') {
            modelGroup.compareList = [];
        }

        if (!modelGroup.compareList.length) {
            modelGroup.compareList.push(modelRunDef.id);
        } else {
            const index = modelGroup.compareList.indexOf(modelRunDef.id);
            if (index === -1) {
                modelGroup.compareList.push(modelRunDef.id);
            } else {
                modelGroup.compareList.splice(index, 1);
            }
        }
    }

    checkModelList(modelGroup, modelRunDef) {
        return modelGroup.compareList && modelGroup.compareList.indexOf(modelRunDef.id) !== -1;
    }

    compare(e, modelGroup) {
        if (modelGroup.compareList && modelGroup.compareList.length > 1) {
            this.router.navigate([
                '/model-run-def/compare',
                modelGroup.compareList[0] || 0,
                modelGroup.compareList[1] || 0,
                modelGroup.compareList[2] || 0
            ]);
        } else {
            e.stopPropagation();
            e.preventDefault();
        }
    }

    /******CHANGE RESULT STATUS******/

    changeResultStatus(e, modelRunDef, modelGroup) {
        const previus = modelRunDef.resultStatus;
        modelRunDef.resultStatus = e.target.value;
        this.modelRunDefService.update(modelRunDef).subscribe(
            res => {
                if (modelRunDef.resultStatus === ResultStatus.APPLIED) {
                    for (const modelRunDefItem of modelGroup.value) {
                        if (modelRunDefItem.id !== modelRunDef.id && modelRunDefItem.resultStatus === ResultStatus.APPLIED) {
                            modelRunDefItem.resultStatus = ResultStatus.INANALYSIS;
                            this.modelRunDefService.update(modelRunDefItem).subscribe(() => {}, error => error);
                        }
                    }
                }
            },
            error => {
                // retorna el valor anterior
                this.onError(error.message);

                e.target.value = previus || ResultStatus.INANALYSIS;
                modelRunDef.resultStatus = previus || ResultStatus.INANALYSIS;
            }
        );
    }

    /******DATE PICKER FUNCTIONS*******/
    onDateSelection(d) {
        const date = new NgbDate(this.selectedDate.year(), this.selectedDate.month() + 1, this.selectedDate.date());
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        } else if (
            this.fromDate &&
            this.toDate &&
            this.dateEqualsDate(this.fromDate, this.toDate) &&
            this.dateIsAfterDate(date, this.fromDate)
        ) {
            this.toDate = date;
            this.range_date = this.formatDate(this.fromDate) + ' - ' + this.formatDate(this.toDate);
            d.close();
            this.reset();
        } else {
            this.toDate = date;
            this.fromDate = date;
        }
    }

    dateIsAfterDate(date1: NgbDate, date2: NgbDate): boolean {
        if (date2 === null || date1 === null) {
            return false;
        } else if (date1.year === date2.year) {
            if (date1.month === date2.month) {
                return date1.day === date2.day ? false : date1.day > date2.day;
            } else {
                return date1.month > date2.month;
            }
        } else {
            return date1.year > date2.year;
        }
    }

    dateIsBeforeDate(date1: NgbDate, date2: NgbDate): boolean {
        if (date2 === null || date1 === null) {
            return false;
        } else if (date1.year === date2.year) {
            if (date1.month === date2.month) {
                return date1.day === date2.day ? false : date1.day < date2.day;
            } else {
                return date1.month < date2.month;
            }
        } else {
            return date1.year < date2.year;
        }
    }

    dateEqualsDate(date1: NgbDate, date2: NgbDate): boolean {
        if (date2 === null || date1 === null) {
            return false;
        } else {
            return date1.year === date2.year && date1.month === date2.month && date1.day === date2.day;
        }
    }

    isHovered(date: NgbDate) {
        return (
            this.fromDate &&
            this.dateEqualsDate(this.fromDate, this.toDate) &&
            this.hoveredDate &&
            this.dateIsAfterDate(date, this.fromDate) &&
            this.dateIsBeforeDate(date, this.hoveredDate)
        );
    }

    isInside(date: NgbDate) {
        return this.dateIsAfterDate(date, this.fromDate) && this.dateIsBeforeDate(date, this.toDate);
    }

    isRange(date: NgbDate) {
        return (
            this.dateEqualsDate(date, this.fromDate) ||
            this.dateEqualsDate(date, this.toDate) ||
            this.isInside(date) ||
            this.isHovered(date)
        );
    }

    formatDate(date: NgbDate): string {
        return date.day + '/' + date.month + '/' + date.year;
    }

    /**** OBTENER RESULTADO DIARIO ****/
    getResultStatus(modelRunDefList): string {
        for (const modelRunDef of modelRunDefList) {
            if (modelRunDef.resultStatus === 'APPLIED') {
                return 'APPLIED';
            }
        }
        return 'INANALYSIS';
    }
}
