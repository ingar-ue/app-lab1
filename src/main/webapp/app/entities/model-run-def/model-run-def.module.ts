import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ModelRunDefComponent,
    ModelRunDefDetailComponent,
    ModelRunDefDeletePopupComponent,
    ModelRunDefDeleteDialogComponent,
    modelRunDefRoute,
    modelRunDefPopupRoute
} from './';

import { ChartModule } from 'angular2-chartjs';
import { ModelRunDefCompareComponent } from 'app/entities/model-run-def/model-run-def-compare.component';
import { DecimalPipe } from '@angular/common';

const ENTITY_STATES = [...modelRunDefRoute, ...modelRunDefPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES), ChartModule],
    declarations: [
        ModelRunDefComponent,
        ModelRunDefDetailComponent,
        ModelRunDefDeleteDialogComponent,
        ModelRunDefDeletePopupComponent,
        ModelRunDefCompareComponent
    ],
    entryComponents: [ModelRunDefComponent, ModelRunDefDeleteDialogComponent, ModelRunDefDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [DecimalPipe]
})
export class AppLabModelRunDefModule {}
