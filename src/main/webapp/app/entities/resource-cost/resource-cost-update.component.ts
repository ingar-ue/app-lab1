import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IResourceCost } from 'app/shared/model/resource-cost.model';
import { ResourceCostService } from './resource-cost.service';
import { IDiameterGroup } from 'app/shared/model/diameter-group.model';
import { DiameterGroupService } from 'app/entities/diameter-group';

@Component({
    selector: 'jhi-resource-cost-update',
    templateUrl: './resource-cost-update.component.html'
})
export class ResourceCostUpdateComponent implements OnInit {
    private _resourceCost: IResourceCost;
    isSaving: boolean;

    diametergroups: IDiameterGroup[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private resourceCostService: ResourceCostService,
        private diameterGroupService: DiameterGroupService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ resourceCost }) => {
            this.resourceCost = resourceCost;
        });
        this.diameterGroupService.query().subscribe(
            (res: HttpResponse<IDiameterGroup[]>) => {
                this.diametergroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.resourceCost.id !== undefined) {
            this.subscribeToSaveResponse(this.resourceCostService.update(this.resourceCost));
        } else {
            this.subscribeToSaveResponse(this.resourceCostService.create(this.resourceCost));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IResourceCost>>) {
        result.subscribe((res: HttpResponse<IResourceCost>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackDiameterGroupById(index: number, item: IDiameterGroup) {
        return item.id;
    }
    get resourceCost() {
        return this._resourceCost;
    }

    set resourceCost(resourceCost: IResourceCost) {
        this._resourceCost = resourceCost;
    }
}
