import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    LongGroupComponent,
    LongGroupDetailComponent,
    LongGroupUpdateComponent,
    LongGroupDeletePopupComponent,
    LongGroupDeleteDialogComponent,
    longGroupRoute,
    longGroupPopupRoute
} from './';

const ENTITY_STATES = [...longGroupRoute, ...longGroupPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        LongGroupComponent,
        LongGroupDetailComponent,
        LongGroupUpdateComponent,
        LongGroupDeleteDialogComponent,
        LongGroupDeletePopupComponent
    ],
    entryComponents: [LongGroupComponent, LongGroupUpdateComponent, LongGroupDeleteDialogComponent, LongGroupDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabLongGroupModule {}
