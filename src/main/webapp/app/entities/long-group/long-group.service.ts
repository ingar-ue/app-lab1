import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ILongGroup } from 'app/shared/model/long-group.model';

type EntityResponseType = HttpResponse<ILongGroup>;
type EntityArrayResponseType = HttpResponse<ILongGroup[]>;

@Injectable({ providedIn: 'root' })
export class LongGroupService {
    private resourceUrl = SERVER_API_URL + 'api/long-groups';

    constructor(private http: HttpClient) {}

    create(longGroup: ILongGroup): Observable<EntityResponseType> {
        return this.http.post<ILongGroup>(this.resourceUrl, longGroup, { observe: 'response' });
    }

    update(longGroup: ILongGroup): Observable<EntityResponseType> {
        return this.http.put<ILongGroup>(this.resourceUrl, longGroup, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ILongGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ILongGroup[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
