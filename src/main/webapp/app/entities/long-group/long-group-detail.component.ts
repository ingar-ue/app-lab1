import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILongGroup } from 'app/shared/model/long-group.model';

@Component({
    selector: 'jhi-long-group-detail',
    templateUrl: './long-group-detail.component.html'
})
export class LongGroupDetailComponent implements OnInit {
    longGroup: ILongGroup;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ longGroup }) => {
            this.longGroup = longGroup;
        });
    }

    previousState() {
        window.history.back();
    }
}
