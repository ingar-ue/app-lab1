import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ILongGroup } from 'app/shared/model/long-group.model';
import { LongGroupService } from './long-group.service';

@Component({
    selector: 'jhi-long-group-update',
    templateUrl: './long-group-update.component.html'
})
export class LongGroupUpdateComponent implements OnInit {
    private _longGroup: ILongGroup;
    isSaving: boolean;

    constructor(private longGroupService: LongGroupService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ longGroup }) => {
            this.longGroup = longGroup;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.longGroup.id !== undefined) {
            this.subscribeToSaveResponse(this.longGroupService.update(this.longGroup));
        } else {
            this.subscribeToSaveResponse(this.longGroupService.create(this.longGroup));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ILongGroup>>) {
        result.subscribe((res: HttpResponse<ILongGroup>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get longGroup() {
        return this._longGroup;
    }

    set longGroup(longGroup: ILongGroup) {
        this._longGroup = longGroup;
    }
}
