import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { LongGroup } from 'app/shared/model/long-group.model';
import { LongGroupService } from './long-group.service';
import { LongGroupComponent } from './long-group.component';
import { LongGroupDetailComponent } from './long-group-detail.component';
import { LongGroupUpdateComponent } from './long-group-update.component';
import { LongGroupDeletePopupComponent } from './long-group-delete-dialog.component';
import { ILongGroup } from 'app/shared/model/long-group.model';

@Injectable({ providedIn: 'root' })
export class LongGroupResolve implements Resolve<ILongGroup> {
    constructor(private service: LongGroupService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((longGroup: HttpResponse<LongGroup>) => longGroup.body));
        }
        return of(new LongGroup());
    }
}

export const longGroupRoute: Routes = [
    {
        path: 'long-group',
        component: LongGroupComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.longGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'long-group/:id/view',
        component: LongGroupDetailComponent,
        resolve: {
            longGroup: LongGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.longGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'long-group/new',
        component: LongGroupUpdateComponent,
        resolve: {
            longGroup: LongGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.longGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'long-group/:id/edit',
        component: LongGroupUpdateComponent,
        resolve: {
            longGroup: LongGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.longGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const longGroupPopupRoute: Routes = [
    {
        path: 'long-group/:id/delete',
        component: LongGroupDeletePopupComponent,
        resolve: {
            longGroup: LongGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.longGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
