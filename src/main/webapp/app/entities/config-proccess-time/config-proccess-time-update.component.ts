import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IConfigProccessTime } from 'app/shared/model/config-proccess-time.model';
import { ConfigProccessTimeService } from './config-proccess-time.service';
import { IDiameterGroup } from 'app/shared/model/diameter-group.model';
import { DiameterGroupService } from 'app/entities/diameter-group';
import { ILongGroup } from 'app/shared/model/long-group.model';
import { LongGroupService } from 'app/entities/long-group';

@Component({
    selector: 'jhi-config-proccess-time-update',
    templateUrl: './config-proccess-time-update.component.html'
})
export class ConfigProccessTimeUpdateComponent implements OnInit {
    private _configProccessTime: IConfigProccessTime;
    isSaving: boolean;

    diametergroups: IDiameterGroup[];

    longgroups: ILongGroup[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private configProccessTimeService: ConfigProccessTimeService,
        private diameterGroupService: DiameterGroupService,
        private longGroupService: LongGroupService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ configProccessTime }) => {
            this.configProccessTime = configProccessTime;
        });
        this.diameterGroupService.query().subscribe(
            (res: HttpResponse<IDiameterGroup[]>) => {
                this.diametergroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.longGroupService.query().subscribe(
            (res: HttpResponse<ILongGroup[]>) => {
                this.longgroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.configProccessTime.id !== undefined) {
            this.subscribeToSaveResponse(this.configProccessTimeService.update(this.configProccessTime));
        } else {
            this.subscribeToSaveResponse(this.configProccessTimeService.create(this.configProccessTime));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IConfigProccessTime>>) {
        result.subscribe((res: HttpResponse<IConfigProccessTime>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackDiameterGroupById(index: number, item: IDiameterGroup) {
        return item.id;
    }

    trackLongGroupById(index: number, item: ILongGroup) {
        return item.id;
    }
    get configProccessTime() {
        return this._configProccessTime;
    }

    set configProccessTime(configProccessTime: IConfigProccessTime) {
        this._configProccessTime = configProccessTime;
    }
}
