export * from './config-proccess-time.service';
export * from './config-proccess-time-update.component';
export * from './config-proccess-time-delete-dialog.component';
export * from './config-proccess-time-detail.component';
export * from './config-proccess-time.component';
export * from './config-proccess-time.route';
