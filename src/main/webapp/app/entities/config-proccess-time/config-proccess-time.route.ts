import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigProccessTime } from 'app/shared/model/config-proccess-time.model';
import { ConfigProccessTimeService } from './config-proccess-time.service';
import { ConfigProccessTimeComponent } from './config-proccess-time.component';
import { ConfigProccessTimeDetailComponent } from './config-proccess-time-detail.component';
import { ConfigProccessTimeUpdateComponent } from './config-proccess-time-update.component';
import { ConfigProccessTimeDeletePopupComponent } from './config-proccess-time-delete-dialog.component';
import { IConfigProccessTime } from 'app/shared/model/config-proccess-time.model';

@Injectable({ providedIn: 'root' })
export class ConfigProccessTimeResolve implements Resolve<IConfigProccessTime> {
    constructor(private service: ConfigProccessTimeService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((configProccessTime: HttpResponse<ConfigProccessTime>) => configProccessTime.body));
        }
        return of(new ConfigProccessTime());
    }
}

export const configProccessTimeRoute: Routes = [
    {
        path: 'config-proccess-time',
        component: ConfigProccessTimeComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.configProccessTime.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'config-proccess-time/:id/view',
        component: ConfigProccessTimeDetailComponent,
        resolve: {
            configProccessTime: ConfigProccessTimeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configProccessTime.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'config-proccess-time/new',
        component: ConfigProccessTimeUpdateComponent,
        resolve: {
            configProccessTime: ConfigProccessTimeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configProccessTime.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'config-proccess-time/:id/edit',
        component: ConfigProccessTimeUpdateComponent,
        resolve: {
            configProccessTime: ConfigProccessTimeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configProccessTime.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const configProccessTimePopupRoute: Routes = [
    {
        path: 'config-proccess-time/:id/delete',
        component: ConfigProccessTimeDeletePopupComponent,
        resolve: {
            configProccessTime: ConfigProccessTimeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configProccessTime.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
