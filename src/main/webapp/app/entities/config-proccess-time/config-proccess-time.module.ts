import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ConfigProccessTimeComponent,
    ConfigProccessTimeDetailComponent,
    ConfigProccessTimeUpdateComponent,
    ConfigProccessTimeDeletePopupComponent,
    ConfigProccessTimeDeleteDialogComponent,
    configProccessTimeRoute,
    configProccessTimePopupRoute
} from './';

const ENTITY_STATES = [...configProccessTimeRoute, ...configProccessTimePopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ConfigProccessTimeComponent,
        ConfigProccessTimeDetailComponent,
        ConfigProccessTimeUpdateComponent,
        ConfigProccessTimeDeleteDialogComponent,
        ConfigProccessTimeDeletePopupComponent
    ],
    entryComponents: [
        ConfigProccessTimeComponent,
        ConfigProccessTimeUpdateComponent,
        ConfigProccessTimeDeleteDialogComponent,
        ConfigProccessTimeDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabConfigProccessTimeModule {}
