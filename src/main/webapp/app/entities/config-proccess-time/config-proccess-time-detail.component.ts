import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IConfigProccessTime } from 'app/shared/model/config-proccess-time.model';

@Component({
    selector: 'jhi-config-proccess-time-detail',
    templateUrl: './config-proccess-time-detail.component.html'
})
export class ConfigProccessTimeDetailComponent implements OnInit {
    configProccessTime: IConfigProccessTime;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ configProccessTime }) => {
            this.configProccessTime = configProccessTime;
        });
    }

    previousState() {
        window.history.back();
    }
}
