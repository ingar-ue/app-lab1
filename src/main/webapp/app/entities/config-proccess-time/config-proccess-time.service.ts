import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IConfigProccessTime } from 'app/shared/model/config-proccess-time.model';

type EntityResponseType = HttpResponse<IConfigProccessTime>;
type EntityArrayResponseType = HttpResponse<IConfigProccessTime[]>;

@Injectable({ providedIn: 'root' })
export class ConfigProccessTimeService {
    private resourceUrl = SERVER_API_URL + 'api/config-proccess-times';

    constructor(private http: HttpClient) {}

    create(configProccessTime: IConfigProccessTime): Observable<EntityResponseType> {
        return this.http.post<IConfigProccessTime>(this.resourceUrl, configProccessTime, { observe: 'response' });
    }

    update(configProccessTime: IConfigProccessTime): Observable<EntityResponseType> {
        return this.http.put<IConfigProccessTime>(this.resourceUrl, configProccessTime, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IConfigProccessTime>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IConfigProccessTime[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
