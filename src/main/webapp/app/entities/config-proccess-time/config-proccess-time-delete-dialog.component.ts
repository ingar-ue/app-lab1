import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IConfigProccessTime } from 'app/shared/model/config-proccess-time.model';
import { ConfigProccessTimeService } from './config-proccess-time.service';

@Component({
    selector: 'jhi-config-proccess-time-delete-dialog',
    templateUrl: './config-proccess-time-delete-dialog.component.html'
})
export class ConfigProccessTimeDeleteDialogComponent {
    configProccessTime: IConfigProccessTime;

    constructor(
        private configProccessTimeService: ConfigProccessTimeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.configProccessTimeService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'configProccessTimeListModification',
                content: 'Deleted an configProccessTime'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-config-proccess-time-delete-popup',
    template: ''
})
export class ConfigProccessTimeDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ configProccessTime }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ConfigProccessTimeDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.configProccessTime = configProccessTime;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
