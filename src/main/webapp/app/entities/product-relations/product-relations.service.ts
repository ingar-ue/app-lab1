import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IProductRelations } from 'app/shared/model/product-relations.model';

type EntityResponseType = HttpResponse<IProductRelations>;
type EntityArrayResponseType = HttpResponse<IProductRelations[]>;

@Injectable({ providedIn: 'root' })
export class ProductRelationsService {
    private resourceUrl = SERVER_API_URL + 'api/product-relations';

    constructor(private http: HttpClient) {}

    create(productRelations: IProductRelations): Observable<EntityResponseType> {
        return this.http.post<IProductRelations>(this.resourceUrl, productRelations, { observe: 'response' });
    }

    update(productRelations: IProductRelations): Observable<EntityResponseType> {
        return this.http.put<IProductRelations>(this.resourceUrl, productRelations, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IProductRelations>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IProductRelations[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
