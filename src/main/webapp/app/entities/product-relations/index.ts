export * from './product-relations.service';
export * from './product-relations-update.component';
export * from './product-relations-delete-dialog.component';
export * from './product-relations-detail.component';
export * from './product-relations.component';
export * from './product-relations.route';
