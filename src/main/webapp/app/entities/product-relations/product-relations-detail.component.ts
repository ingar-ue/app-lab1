import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductRelations } from 'app/shared/model/product-relations.model';

@Component({
    selector: 'jhi-product-relations-detail',
    templateUrl: './product-relations-detail.component.html'
})
export class ProductRelationsDetailComponent implements OnInit {
    productRelations: IProductRelations;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ productRelations }) => {
            this.productRelations = productRelations;
        });
    }

    previousState() {
        window.history.back();
    }
}
