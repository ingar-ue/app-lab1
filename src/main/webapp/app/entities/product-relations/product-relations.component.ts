import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IProductRelations } from 'app/shared/model/product-relations.model';
import { Principal } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { ProductRelationsService } from './product-relations.service';
import { LongGroupService } from '../long-group/long-group.service';
import { ILongGroup } from 'app/shared/model/long-group.model';

import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
    selector: 'jhi-product-relations',
    templateUrl: './product-relations.component.html'
})
export class ProductRelationsComponent implements OnInit, OnDestroy {
    productRelations: IProductRelations[];
    longGroups: ILongGroup[];
    longGroup: ILongGroup;
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    search: any;
    sortType: string;

    constructor(
        private productRelationsService: ProductRelationsService,
        private longGroupService: LongGroupService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private translateService: TranslateService
    ) {
        this.productRelations = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.search = { keyword: '' };
        this.setSearchLanguage();

        this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            this.setSearchLanguage();
        });
    }

    setSearchLanguage() {
        this.search.inputPlaceholder = this.translateService.instant('global.searchKeywordPlaceholder');
        this.search.selectPlaceholder = this.translateService.instant('global.searchByPlaceholder');
        this.search.fields = [
            { value: 'interProduct', label: this.translateService.instant('appLabApp.productRelations.interProduct') },
            { value: 'longGroup', label: this.translateService.instant('appLabApp.productRelations.longGroup') },
            { value: 'finalProduct1', label: this.translateService.instant('appLabApp.productRelations.finalProduct1') },
            { value: 'finalProduct2', label: this.translateService.instant('appLabApp.productRelations.finalProduct2') },
            { value: 'finalProduct3', label: this.translateService.instant('appLabApp.productRelations.finalProduct3') }
        ];
    }

    loadAll() {
        this.productRelationsService
            .query({
                size: this.itemsPerPage,
                sort: this.sort(),
                page: this.page,
                type: this.search.field,
                keyword: this.search.keyword
            })
            .subscribe(
                (res: HttpResponse<IProductRelations[]>) => {
                    this.paginateProductRelations(res.body, res.headers);
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    reset() {
        this.page = 0;
        this.productRelations = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInProductRelations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IProductRelations) {
        return item.id;
    }

    registerChangeInProductRelations() {
        this.eventSubscriber = this.eventManager.subscribe('productRelationsListModification', response => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private getLongGroup(id) {
        this.longGroupService.find(id).subscribe(
            (res: HttpResponse<ILongGroup>) => {
                this.longGroup = res.body;
                console.log('found this ', this.longGroup);
                return this.longGroup;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private paginateProductRelations(data: IProductRelations[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        for (let i = 0; i < data.length; i++) {
            this.productRelations.push(data[i]);
        }
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
