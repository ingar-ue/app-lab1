import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductRelations } from 'app/shared/model/product-relations.model';
import { ProductRelationsService } from './product-relations.service';

@Component({
    selector: 'jhi-product-relations-delete-dialog',
    templateUrl: './product-relations-delete-dialog.component.html'
})
export class ProductRelationsDeleteDialogComponent {
    productRelations: IProductRelations;

    constructor(
        private productRelationsService: ProductRelationsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.productRelationsService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'productRelationsListModification',
                content: 'Deleted an productRelations'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-product-relations-delete-popup',
    template: ''
})
export class ProductRelationsDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ productRelations }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ProductRelationsDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.productRelations = productRelations;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
