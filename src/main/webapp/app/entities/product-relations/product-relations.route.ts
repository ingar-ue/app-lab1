import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProductRelations } from 'app/shared/model/product-relations.model';
import { ProductRelationsService } from './product-relations.service';
import { ProductRelationsComponent } from './product-relations.component';
import { ProductRelationsDetailComponent } from './product-relations-detail.component';
import { ProductRelationsUpdateComponent } from './product-relations-update.component';
import { ProductRelationsDeletePopupComponent } from './product-relations-delete-dialog.component';
import { IProductRelations } from 'app/shared/model/product-relations.model';

@Injectable({ providedIn: 'root' })
export class ProductRelationsResolve implements Resolve<IProductRelations> {
    constructor(private service: ProductRelationsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((productRelations: HttpResponse<ProductRelations>) => productRelations.body));
        }
        return of(new ProductRelations());
    }
}

export const productRelationsRoute: Routes = [
    {
        path: 'product-relations',
        component: ProductRelationsComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.productRelations.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-relations/:id/view',
        component: ProductRelationsDetailComponent,
        resolve: {
            productRelations: ProductRelationsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.productRelations.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-relations/new',
        component: ProductRelationsUpdateComponent,
        resolve: {
            productRelations: ProductRelationsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.productRelations.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'product-relations/:id/edit',
        component: ProductRelationsUpdateComponent,
        resolve: {
            productRelations: ProductRelationsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.productRelations.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productRelationsPopupRoute: Routes = [
    {
        path: 'product-relations/:id/delete',
        component: ProductRelationsDeletePopupComponent,
        resolve: {
            productRelations: ProductRelationsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.productRelations.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
