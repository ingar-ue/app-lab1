import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IProductRelations } from 'app/shared/model/product-relations.model';
import { ProductRelationsService } from './product-relations.service';
import { IFinalProduct } from 'app/shared/model/final-product.model';
import { FinalProductService } from 'app/entities/final-product';
import { IInterProduct } from 'app/shared/model/inter-product.model';
import { InterProductService } from 'app/entities/inter-product';
import { ILongGroup } from 'app/shared/model/long-group.model';
import { LongGroupService } from 'app/entities/long-group';

@Component({
    selector: 'jhi-product-relations-update',
    templateUrl: './product-relations-update.component.html'
})
export class ProductRelationsUpdateComponent implements OnInit {
    private _productRelations: IProductRelations;
    isSaving: boolean;

    finalproducts: IFinalProduct[];

    interproducts: IInterProduct[];

    longgroups: ILongGroup[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private productRelationsService: ProductRelationsService,
        private finalProductService: FinalProductService,
        private interProductService: InterProductService,
        private longGroupService: LongGroupService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ productRelations }) => {
            this.productRelations = productRelations;
        });
        this.finalProductService.query({ size: 100000 }).subscribe(
            (res: HttpResponse<IFinalProduct[]>) => {
                this.finalproducts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.interProductService.query({ size: 100000 }).subscribe(
            (res: HttpResponse<IInterProduct[]>) => {
                this.interproducts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.longGroupService.query().subscribe(
            (res: HttpResponse<ILongGroup[]>) => {
                this.longgroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.productRelations.id !== undefined) {
            this.subscribeToSaveResponse(this.productRelationsService.update(this.productRelations));
        } else {
            this.subscribeToSaveResponse(this.productRelationsService.create(this.productRelations));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IProductRelations>>) {
        result.subscribe((res: HttpResponse<IProductRelations>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    get productRelations() {
        return this._productRelations;
    }

    set productRelations(productRelations: IProductRelations) {
        this._productRelations = productRelations;
    }
}
