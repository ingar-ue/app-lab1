import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SuiModule } from 'ng2-semantic-ui';

import { AppLabSharedModule } from 'app/shared';
import {
    ProductRelationsComponent,
    ProductRelationsDetailComponent,
    ProductRelationsUpdateComponent,
    ProductRelationsDeletePopupComponent,
    ProductRelationsDeleteDialogComponent,
    productRelationsRoute,
    productRelationsPopupRoute
} from './';

import { CustomFilterPipe } from 'app/shared/pipes/custom-filter.pipe';
import { SortPipe } from 'app/shared/pipes/sort.pipe';

const ENTITY_STATES = [...productRelationsRoute, ...productRelationsPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES), SuiModule],
    declarations: [
        ProductRelationsComponent,
        ProductRelationsDetailComponent,
        ProductRelationsUpdateComponent,
        ProductRelationsDeleteDialogComponent,
        ProductRelationsDeletePopupComponent
    ],
    entryComponents: [
        ProductRelationsComponent,
        ProductRelationsUpdateComponent,
        ProductRelationsDeleteDialogComponent,
        ProductRelationsDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabProductRelationsModule {}
