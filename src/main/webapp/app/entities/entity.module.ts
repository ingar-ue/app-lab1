import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppLabCompanyModule } from './company/company.module';
import { AppLabLongGroupModule } from './long-group/long-group.module';
import { AppLabDiameterGroupModule } from './diameter-group/diameter-group.module';
import { AppLabInterProductModule } from './inter-product/inter-product.module';
import { AppLabFinalProductModule } from './final-product/final-product.module';
import { AppLabEnvConfigModule } from './env-config/env-config.module';
import { AppLabKerfConfigModule } from './kerf-config/kerf-config.module';
import { AppLabConfigProccessTimeModule } from './config-proccess-time/config-proccess-time.module';
import { AppLabConfigTableCorrectionModule } from './config-table-correction/config-table-correction.module';
import { AppLabConfigYieldPlanningModule } from './config-yield-planning/config-yield-planning.module';
import { AppLabProductRelationsModule } from './product-relations/product-relations.module';
import { AppLabResourceQtyModule } from './resource-qty/resource-qty.module';
import { AppLabResourceCostModule } from './resource-cost/resource-cost.module';
import { AppLabPatternExclusionModule } from './pattern-exclusion/pattern-exclusion.module';
import { AppLabModelDefModule } from './model-def/model-def.module';
import { AppLabModelConfigModule } from './model-config/model-config.module';
import { AppLabModelVersionModule } from './model-version/model-version.module';
import { AppLabModelObjFunctionModule } from './model-obj-function/model-obj-function.module';
import { AppLabModelRunDefModule } from './model-run-def/model-run-def.module';
import { AppLabModelRunConfigModule } from './model-run-config/model-run-config.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */
import { AppLabOptimizationPoint1Module } from './optimization-point-1/optimization-point-1.module';
import { AppLabOptimizationPoint2Module } from './optimization-point-2/optimization-point-2.module';

@NgModule({
    // prettier-ignore
    imports: [
        AppLabCompanyModule,
        AppLabLongGroupModule,
        AppLabDiameterGroupModule,
        AppLabInterProductModule,
        AppLabFinalProductModule,
        AppLabEnvConfigModule,
        AppLabKerfConfigModule,
        AppLabConfigProccessTimeModule,
        AppLabConfigTableCorrectionModule,
        AppLabConfigYieldPlanningModule,
        AppLabProductRelationsModule,
        AppLabResourceQtyModule,
        AppLabResourceCostModule,
        AppLabPatternExclusionModule,
        AppLabModelDefModule,
        AppLabModelConfigModule,
        AppLabModelVersionModule,
        AppLabModelObjFunctionModule,
        AppLabModelRunDefModule,
        AppLabModelRunConfigModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
        AppLabOptimizationPoint1Module,
        AppLabOptimizationPoint2Module,
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabEntityModule {}
