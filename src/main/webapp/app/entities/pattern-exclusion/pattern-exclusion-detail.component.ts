import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPatternExclusion } from 'app/shared/model/pattern-exclusion.model';

@Component({
    selector: 'jhi-pattern-exclusion-detail',
    templateUrl: './pattern-exclusion-detail.component.html'
})
export class PatternExclusionDetailComponent implements OnInit {
    patternExclusion: IPatternExclusion;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ patternExclusion }) => {
            this.patternExclusion = patternExclusion;
        });
    }

    previousState() {
        window.history.back();
    }
}
