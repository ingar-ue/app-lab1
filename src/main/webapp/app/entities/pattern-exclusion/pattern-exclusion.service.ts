import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPatternExclusion } from 'app/shared/model/pattern-exclusion.model';

type EntityResponseType = HttpResponse<IPatternExclusion>;
type EntityArrayResponseType = HttpResponse<IPatternExclusion[]>;

@Injectable({ providedIn: 'root' })
export class PatternExclusionService {
    private resourceUrl = SERVER_API_URL + 'api/pattern-exclusions';

    constructor(private http: HttpClient) {}

    create(patternExclusion: IPatternExclusion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(patternExclusion);
        return this.http
            .post<IPatternExclusion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(patternExclusion: IPatternExclusion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(patternExclusion);
        return this.http
            .put<IPatternExclusion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IPatternExclusion>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IPatternExclusion[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(patternExclusion: IPatternExclusion): IPatternExclusion {
        const copy: IPatternExclusion = Object.assign({}, patternExclusion, {
            exclusionDate:
                patternExclusion.exclusionDate != null && patternExclusion.exclusionDate.isValid()
                    ? patternExclusion.exclusionDate.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.exclusionDate = res.body.exclusionDate != null ? moment(res.body.exclusionDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((patternExclusion: IPatternExclusion) => {
            patternExclusion.exclusionDate = patternExclusion.exclusionDate != null ? moment(patternExclusion.exclusionDate) : null;
        });
        return res;
    }
}
