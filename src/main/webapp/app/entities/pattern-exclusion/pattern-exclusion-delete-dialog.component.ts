import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPatternExclusion } from 'app/shared/model/pattern-exclusion.model';
import { PatternExclusionService } from './pattern-exclusion.service';

@Component({
    selector: 'jhi-pattern-exclusion-delete-dialog',
    templateUrl: './pattern-exclusion-delete-dialog.component.html'
})
export class PatternExclusionDeleteDialogComponent {
    patternExclusion: IPatternExclusion;

    constructor(
        private patternExclusionService: PatternExclusionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.patternExclusionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'patternExclusionListModification',
                content: 'Deleted an patternExclusion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pattern-exclusion-delete-popup',
    template: ''
})
export class PatternExclusionDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ patternExclusion }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PatternExclusionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.patternExclusion = patternExclusion;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
