import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PatternExclusion } from 'app/shared/model/pattern-exclusion.model';
import { PatternExclusionService } from './pattern-exclusion.service';
import { PatternExclusionComponent } from './pattern-exclusion.component';
import { PatternExclusionDetailComponent } from './pattern-exclusion-detail.component';
import { PatternExclusionUpdateComponent } from './pattern-exclusion-update.component';
import { PatternExclusionDeletePopupComponent } from './pattern-exclusion-delete-dialog.component';
import { IPatternExclusion } from 'app/shared/model/pattern-exclusion.model';

@Injectable({ providedIn: 'root' })
export class PatternExclusionResolve implements Resolve<IPatternExclusion> {
    constructor(private service: PatternExclusionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((patternExclusion: HttpResponse<PatternExclusion>) => patternExclusion.body));
        }
        return of(new PatternExclusion());
    }
}

export const patternExclusionRoute: Routes = [
    {
        path: 'pattern-exclusion',
        component: PatternExclusionComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.patternExclusion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pattern-exclusion/:id/view',
        component: PatternExclusionDetailComponent,
        resolve: {
            patternExclusion: PatternExclusionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.patternExclusion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pattern-exclusion/new',
        component: PatternExclusionUpdateComponent,
        resolve: {
            patternExclusion: PatternExclusionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.patternExclusion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pattern-exclusion/:id/edit',
        component: PatternExclusionUpdateComponent,
        resolve: {
            patternExclusion: PatternExclusionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.patternExclusion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const patternExclusionPopupRoute: Routes = [
    {
        path: 'pattern-exclusion/:id/delete',
        component: PatternExclusionDeletePopupComponent,
        resolve: {
            patternExclusion: PatternExclusionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.patternExclusion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
