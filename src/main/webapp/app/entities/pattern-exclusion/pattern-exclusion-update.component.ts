import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IPatternExclusion } from 'app/shared/model/pattern-exclusion.model';
import { PatternExclusionService } from './pattern-exclusion.service';

@Component({
    selector: 'jhi-pattern-exclusion-update',
    templateUrl: './pattern-exclusion-update.component.html'
})
export class PatternExclusionUpdateComponent implements OnInit {
    private _patternExclusion: IPatternExclusion;
    isSaving: boolean;
    exclusionDateDp: any;

    constructor(private patternExclusionService: PatternExclusionService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ patternExclusion }) => {
            this.patternExclusion = patternExclusion;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.patternExclusion.id !== undefined) {
            this.subscribeToSaveResponse(this.patternExclusionService.update(this.patternExclusion));
        } else {
            this.subscribeToSaveResponse(this.patternExclusionService.create(this.patternExclusion));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPatternExclusion>>) {
        result.subscribe((res: HttpResponse<IPatternExclusion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get patternExclusion() {
        return this._patternExclusion;
    }

    set patternExclusion(patternExclusion: IPatternExclusion) {
        this._patternExclusion = patternExclusion;
    }
}
