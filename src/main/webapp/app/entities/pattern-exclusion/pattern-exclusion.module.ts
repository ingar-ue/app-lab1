import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    PatternExclusionComponent,
    PatternExclusionDetailComponent,
    PatternExclusionUpdateComponent,
    PatternExclusionDeletePopupComponent,
    PatternExclusionDeleteDialogComponent,
    patternExclusionRoute,
    patternExclusionPopupRoute
} from './';

const ENTITY_STATES = [...patternExclusionRoute, ...patternExclusionPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PatternExclusionComponent,
        PatternExclusionDetailComponent,
        PatternExclusionUpdateComponent,
        PatternExclusionDeleteDialogComponent,
        PatternExclusionDeletePopupComponent
    ],
    entryComponents: [
        PatternExclusionComponent,
        PatternExclusionUpdateComponent,
        PatternExclusionDeleteDialogComponent,
        PatternExclusionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabPatternExclusionModule {}
