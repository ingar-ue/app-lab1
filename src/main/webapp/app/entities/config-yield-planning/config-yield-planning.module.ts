import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ConfigYieldPlanningComponent,
    ConfigYieldPlanningDetailComponent,
    ConfigYieldPlanningUpdateComponent,
    ConfigYieldPlanningDeletePopupComponent,
    ConfigYieldPlanningDeleteDialogComponent,
    configYieldPlanningRoute,
    configYieldPlanningPopupRoute
} from './';

const ENTITY_STATES = [...configYieldPlanningRoute, ...configYieldPlanningPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ConfigYieldPlanningComponent,
        ConfigYieldPlanningDetailComponent,
        ConfigYieldPlanningUpdateComponent,
        ConfigYieldPlanningDeleteDialogComponent,
        ConfigYieldPlanningDeletePopupComponent
    ],
    entryComponents: [
        ConfigYieldPlanningComponent,
        ConfigYieldPlanningUpdateComponent,
        ConfigYieldPlanningDeleteDialogComponent,
        ConfigYieldPlanningDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabConfigYieldPlanningModule {}
