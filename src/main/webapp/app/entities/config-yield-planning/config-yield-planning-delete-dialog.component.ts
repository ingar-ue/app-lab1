import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IConfigYieldPlanning } from 'app/shared/model/config-yield-planning.model';
import { ConfigYieldPlanningService } from './config-yield-planning.service';

@Component({
    selector: 'jhi-config-yield-planning-delete-dialog',
    templateUrl: './config-yield-planning-delete-dialog.component.html'
})
export class ConfigYieldPlanningDeleteDialogComponent {
    configYieldPlanning: IConfigYieldPlanning;

    constructor(
        private configYieldPlanningService: ConfigYieldPlanningService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.configYieldPlanningService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'configYieldPlanningListModification',
                content: 'Deleted an configYieldPlanning'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-config-yield-planning-delete-popup',
    template: ''
})
export class ConfigYieldPlanningDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ configYieldPlanning }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ConfigYieldPlanningDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.configYieldPlanning = configYieldPlanning;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
