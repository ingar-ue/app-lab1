import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IConfigYieldPlanning } from 'app/shared/model/config-yield-planning.model';

type EntityResponseType = HttpResponse<IConfigYieldPlanning>;
type EntityArrayResponseType = HttpResponse<IConfigYieldPlanning[]>;

@Injectable({ providedIn: 'root' })
export class ConfigYieldPlanningService {
    private resourceUrl = SERVER_API_URL + 'api/config-yield-plannings';

    constructor(private http: HttpClient) {}

    create(configYieldPlanning: IConfigYieldPlanning): Observable<EntityResponseType> {
        return this.http.post<IConfigYieldPlanning>(this.resourceUrl, configYieldPlanning, { observe: 'response' });
    }

    update(configYieldPlanning: IConfigYieldPlanning): Observable<EntityResponseType> {
        return this.http.put<IConfigYieldPlanning>(this.resourceUrl, configYieldPlanning, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IConfigYieldPlanning>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IConfigYieldPlanning[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
