import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigYieldPlanning } from 'app/shared/model/config-yield-planning.model';
import { ConfigYieldPlanningService } from './config-yield-planning.service';
import { ConfigYieldPlanningComponent } from './config-yield-planning.component';
import { ConfigYieldPlanningDetailComponent } from './config-yield-planning-detail.component';
import { ConfigYieldPlanningUpdateComponent } from './config-yield-planning-update.component';
import { ConfigYieldPlanningDeletePopupComponent } from './config-yield-planning-delete-dialog.component';
import { IConfigYieldPlanning } from 'app/shared/model/config-yield-planning.model';

@Injectable({ providedIn: 'root' })
export class ConfigYieldPlanningResolve implements Resolve<IConfigYieldPlanning> {
    constructor(private service: ConfigYieldPlanningService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((configYieldPlanning: HttpResponse<ConfigYieldPlanning>) => configYieldPlanning.body));
        }
        return of(new ConfigYieldPlanning());
    }
}

export const configYieldPlanningRoute: Routes = [
    {
        path: 'config-yield-planning',
        component: ConfigYieldPlanningComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.configYieldPlanning.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'config-yield-planning/:id/view',
        component: ConfigYieldPlanningDetailComponent,
        resolve: {
            configYieldPlanning: ConfigYieldPlanningResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configYieldPlanning.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'config-yield-planning/new',
        component: ConfigYieldPlanningUpdateComponent,
        resolve: {
            configYieldPlanning: ConfigYieldPlanningResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configYieldPlanning.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'config-yield-planning/:id/edit',
        component: ConfigYieldPlanningUpdateComponent,
        resolve: {
            configYieldPlanning: ConfigYieldPlanningResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configYieldPlanning.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const configYieldPlanningPopupRoute: Routes = [
    {
        path: 'config-yield-planning/:id/delete',
        component: ConfigYieldPlanningDeletePopupComponent,
        resolve: {
            configYieldPlanning: ConfigYieldPlanningResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.configYieldPlanning.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
