import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IConfigYieldPlanning } from 'app/shared/model/config-yield-planning.model';
import { ConfigYieldPlanningService } from './config-yield-planning.service';
import { IDiameterGroup } from 'app/shared/model/diameter-group.model';
import { DiameterGroupService } from 'app/entities/diameter-group';

@Component({
    selector: 'jhi-config-yield-planning-update',
    templateUrl: './config-yield-planning-update.component.html'
})
export class ConfigYieldPlanningUpdateComponent implements OnInit {
    private _configYieldPlanning: IConfigYieldPlanning;
    isSaving: boolean;

    diametergroups: IDiameterGroup[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private configYieldPlanningService: ConfigYieldPlanningService,
        private diameterGroupService: DiameterGroupService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ configYieldPlanning }) => {
            this.configYieldPlanning = configYieldPlanning;
        });
        this.diameterGroupService.query().subscribe(
            (res: HttpResponse<IDiameterGroup[]>) => {
                this.diametergroups = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.configYieldPlanning.id !== undefined) {
            this.subscribeToSaveResponse(this.configYieldPlanningService.update(this.configYieldPlanning));
        } else {
            this.subscribeToSaveResponse(this.configYieldPlanningService.create(this.configYieldPlanning));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IConfigYieldPlanning>>) {
        result.subscribe((res: HttpResponse<IConfigYieldPlanning>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackDiameterGroupById(index: number, item: IDiameterGroup) {
        return item.id;
    }
    get configYieldPlanning() {
        return this._configYieldPlanning;
    }

    set configYieldPlanning(configYieldPlanning: IConfigYieldPlanning) {
        this._configYieldPlanning = configYieldPlanning;
    }
}
