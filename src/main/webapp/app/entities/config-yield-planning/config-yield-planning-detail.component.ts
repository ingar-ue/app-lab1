import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IConfigYieldPlanning } from 'app/shared/model/config-yield-planning.model';

@Component({
    selector: 'jhi-config-yield-planning-detail',
    templateUrl: './config-yield-planning-detail.component.html'
})
export class ConfigYieldPlanningDetailComponent implements OnInit {
    configYieldPlanning: IConfigYieldPlanning;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ configYieldPlanning }) => {
            this.configYieldPlanning = configYieldPlanning;
        });
    }

    previousState() {
        window.history.back();
    }
}
