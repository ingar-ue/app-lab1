import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DiameterGroup } from 'app/shared/model/diameter-group.model';
import { DiameterGroupService } from './diameter-group.service';
import { DiameterGroupComponent } from './diameter-group.component';
import { DiameterGroupDetailComponent } from './diameter-group-detail.component';
import { DiameterGroupUpdateComponent } from './diameter-group-update.component';
import { DiameterGroupDeletePopupComponent } from './diameter-group-delete-dialog.component';
import { IDiameterGroup } from 'app/shared/model/diameter-group.model';

@Injectable({ providedIn: 'root' })
export class DiameterGroupResolve implements Resolve<IDiameterGroup> {
    constructor(private service: DiameterGroupService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((diameterGroup: HttpResponse<DiameterGroup>) => diameterGroup.body));
        }
        return of(new DiameterGroup());
    }
}

export const diameterGroupRoute: Routes = [
    {
        path: 'diameter-group',
        component: DiameterGroupComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.diameterGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'diameter-group/:id/view',
        component: DiameterGroupDetailComponent,
        resolve: {
            diameterGroup: DiameterGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.diameterGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'diameter-group/new',
        component: DiameterGroupUpdateComponent,
        resolve: {
            diameterGroup: DiameterGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.diameterGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'diameter-group/:id/edit',
        component: DiameterGroupUpdateComponent,
        resolve: {
            diameterGroup: DiameterGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.diameterGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const diameterGroupPopupRoute: Routes = [
    {
        path: 'diameter-group/:id/delete',
        component: DiameterGroupDeletePopupComponent,
        resolve: {
            diameterGroup: DiameterGroupResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.diameterGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
