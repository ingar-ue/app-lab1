import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDiameterGroup } from 'app/shared/model/diameter-group.model';

@Component({
    selector: 'jhi-diameter-group-detail',
    templateUrl: './diameter-group-detail.component.html'
})
export class DiameterGroupDetailComponent implements OnInit {
    diameterGroup: IDiameterGroup;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ diameterGroup }) => {
            this.diameterGroup = diameterGroup;
        });
    }

    previousState() {
        window.history.back();
    }
}
