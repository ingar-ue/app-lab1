import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDiameterGroup } from 'app/shared/model/diameter-group.model';

type EntityResponseType = HttpResponse<IDiameterGroup>;
type EntityArrayResponseType = HttpResponse<IDiameterGroup[]>;

@Injectable({ providedIn: 'root' })
export class DiameterGroupService {
    private resourceUrl = SERVER_API_URL + 'api/diameter-groups';

    constructor(private http: HttpClient) {}

    create(diameterGroup: IDiameterGroup): Observable<EntityResponseType> {
        return this.http.post<IDiameterGroup>(this.resourceUrl, diameterGroup, { observe: 'response' });
    }

    update(diameterGroup: IDiameterGroup): Observable<EntityResponseType> {
        return this.http.put<IDiameterGroup>(this.resourceUrl, diameterGroup, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IDiameterGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IDiameterGroup[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
