import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDiameterGroup } from 'app/shared/model/diameter-group.model';
import { DiameterGroupService } from './diameter-group.service';

@Component({
    selector: 'jhi-diameter-group-delete-dialog',
    templateUrl: './diameter-group-delete-dialog.component.html'
})
export class DiameterGroupDeleteDialogComponent {
    diameterGroup: IDiameterGroup;

    constructor(
        private diameterGroupService: DiameterGroupService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.diameterGroupService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'diameterGroupListModification',
                content: 'Deleted an diameterGroup'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-diameter-group-delete-popup',
    template: ''
})
export class DiameterGroupDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ diameterGroup }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DiameterGroupDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.diameterGroup = diameterGroup;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
