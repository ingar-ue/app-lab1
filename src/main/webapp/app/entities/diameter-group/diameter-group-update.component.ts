import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IDiameterGroup } from 'app/shared/model/diameter-group.model';
import { DiameterGroupService } from './diameter-group.service';

@Component({
    selector: 'jhi-diameter-group-update',
    templateUrl: './diameter-group-update.component.html'
})
export class DiameterGroupUpdateComponent implements OnInit {
    private _diameterGroup: IDiameterGroup;
    isSaving: boolean;

    constructor(private diameterGroupService: DiameterGroupService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ diameterGroup }) => {
            this.diameterGroup = diameterGroup;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.diameterGroup.id !== undefined) {
            this.subscribeToSaveResponse(this.diameterGroupService.update(this.diameterGroup));
        } else {
            this.subscribeToSaveResponse(this.diameterGroupService.create(this.diameterGroup));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDiameterGroup>>) {
        result.subscribe((res: HttpResponse<IDiameterGroup>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get diameterGroup() {
        return this._diameterGroup;
    }

    set diameterGroup(diameterGroup: IDiameterGroup) {
        this._diameterGroup = diameterGroup;
    }
}
