import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    DiameterGroupComponent,
    DiameterGroupDetailComponent,
    DiameterGroupUpdateComponent,
    DiameterGroupDeletePopupComponent,
    DiameterGroupDeleteDialogComponent,
    diameterGroupRoute,
    diameterGroupPopupRoute
} from './';

const ENTITY_STATES = [...diameterGroupRoute, ...diameterGroupPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DiameterGroupComponent,
        DiameterGroupDetailComponent,
        DiameterGroupUpdateComponent,
        DiameterGroupDeleteDialogComponent,
        DiameterGroupDeletePopupComponent
    ],
    entryComponents: [
        DiameterGroupComponent,
        DiameterGroupUpdateComponent,
        DiameterGroupDeleteDialogComponent,
        DiameterGroupDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabDiameterGroupModule {}
