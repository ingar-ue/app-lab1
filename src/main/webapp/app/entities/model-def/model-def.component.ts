import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IModelDef } from 'app/shared/model/model-def.model';
import { Principal } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { ModelDefService } from './model-def.service';

@Component({
    selector: 'jhi-model-def',
    templateUrl: './model-def.component.html'
})
export class ModelDefComponent implements OnInit, OnDestroy {
    modelDefs: IModelDef[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    isCollapsed: boolean[];
    collapse: boolean;

    constructor(
        private modelDefService: ModelDefService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal
    ) {
        this.modelDefs = [];
        this.isCollapsed = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.collapse = true;
    }

    loadAll() {
        this.modelDefService
            .query({
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IModelDef[]>) => this.paginateModelDefs(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    reset() {
        this.page = 0;
        this.modelDefs = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInModelDefs();
        this.registerChangeInModelConfs();
        this.registerChangeInModelObjFunctions();
        this.registerChangeInModelVersions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IModelDef) {
        return item.id;
    }

    registerChangeInModelDefs() {
        this.eventSubscriber = this.eventManager.subscribe('modelDefListModification', response => this.reset());
    }
    registerChangeInModelConfs() {
        this.eventSubscriber = this.eventManager.subscribe('modelConfigListModification', response => this.reset());
    }
    registerChangeInModelObjFunctions() {
        this.eventSubscriber = this.eventManager.subscribe('modelObjFunctionListModification', response => this.reset());
    }
    registerChangeInModelVersions() {
        this.eventSubscriber = this.eventManager.subscribe('modelVersionListModification', response => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private paginateModelDefs(data: IModelDef[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        for (let i = 0; i < data.length; i++) {
            this.modelDefs.push(data[i]);
            if (this.collapse) {
                this.isCollapsed[this.modelDefs[i].id] = true;
            }
        }
        this.collapse = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
