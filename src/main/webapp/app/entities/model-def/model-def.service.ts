import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IModelDef } from 'app/shared/model/model-def.model';

type EntityResponseType = HttpResponse<IModelDef>;
type EntityArrayResponseType = HttpResponse<IModelDef[]>;

@Injectable({ providedIn: 'root' })
export class ModelDefService {
    private resourceUrl = SERVER_API_URL + 'api/model-defs';

    constructor(private http: HttpClient) {}

    create(modelDef: IModelDef): Observable<EntityResponseType> {
        return this.http.post<IModelDef>(this.resourceUrl, modelDef, { observe: 'response' });
    }

    update(modelDef: IModelDef): Observable<EntityResponseType> {
        return this.http.put<IModelDef>(this.resourceUrl, modelDef, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IModelDef>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IModelDef[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
