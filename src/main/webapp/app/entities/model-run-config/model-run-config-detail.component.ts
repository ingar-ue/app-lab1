import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IModelRunConfig } from 'app/shared/model/model-run-config.model';

@Component({
    selector: 'jhi-model-run-config-detail',
    templateUrl: './model-run-config-detail.component.html'
})
export class ModelRunConfigDetailComponent implements OnInit {
    modelRunConfig: IModelRunConfig;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelRunConfig }) => {
            this.modelRunConfig = modelRunConfig;
        });
    }

    previousState() {
        window.history.back();
    }
}
