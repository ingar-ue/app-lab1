import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ModelRunConfigComponent,
    ModelRunConfigDetailComponent,
    ModelRunConfigUpdateComponent,
    ModelRunConfigDeletePopupComponent,
    ModelRunConfigDeleteDialogComponent,
    modelRunConfigRoute,
    modelRunConfigPopupRoute
} from './';

const ENTITY_STATES = [...modelRunConfigRoute, ...modelRunConfigPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelRunConfigComponent,
        ModelRunConfigDetailComponent,
        ModelRunConfigUpdateComponent,
        ModelRunConfigDeleteDialogComponent,
        ModelRunConfigDeletePopupComponent
    ],
    entryComponents: [
        ModelRunConfigComponent,
        ModelRunConfigUpdateComponent,
        ModelRunConfigDeleteDialogComponent,
        ModelRunConfigDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabModelRunConfigModule {}
