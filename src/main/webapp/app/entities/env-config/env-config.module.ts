import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    EnvConfigComponent,
    EnvConfigDetailComponent,
    EnvConfigUpdateComponent,
    EnvConfigDeletePopupComponent,
    EnvConfigDeleteDialogComponent,
    envConfigRoute,
    envConfigPopupRoute
} from './';

const ENTITY_STATES = [...envConfigRoute, ...envConfigPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        EnvConfigComponent,
        EnvConfigDetailComponent,
        EnvConfigUpdateComponent,
        EnvConfigDeleteDialogComponent,
        EnvConfigDeletePopupComponent
    ],
    entryComponents: [EnvConfigComponent, EnvConfigUpdateComponent, EnvConfigDeleteDialogComponent, EnvConfigDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabEnvConfigModule {}
