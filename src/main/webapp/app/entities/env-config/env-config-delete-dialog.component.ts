import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEnvConfig } from 'app/shared/model/env-config.model';
import { EnvConfigService } from './env-config.service';

@Component({
    selector: 'jhi-env-config-delete-dialog',
    templateUrl: './env-config-delete-dialog.component.html'
})
export class EnvConfigDeleteDialogComponent {
    envConfig: IEnvConfig;

    constructor(private envConfigService: EnvConfigService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.envConfigService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'envConfigListModification',
                content: 'Deleted an envConfig'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-env-config-delete-popup',
    template: ''
})
export class EnvConfigDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ envConfig }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(EnvConfigDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.envConfig = envConfig;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
