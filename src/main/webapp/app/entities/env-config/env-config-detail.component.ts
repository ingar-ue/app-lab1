import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEnvConfig } from 'app/shared/model/env-config.model';

@Component({
    selector: 'jhi-env-config-detail',
    templateUrl: './env-config-detail.component.html'
})
export class EnvConfigDetailComponent implements OnInit {
    envConfig: IEnvConfig;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ envConfig }) => {
            this.envConfig = envConfig;
        });
    }

    previousState() {
        window.history.back();
    }
}
