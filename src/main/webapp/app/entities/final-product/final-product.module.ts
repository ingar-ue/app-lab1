import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SuiModule } from 'ng2-semantic-ui';

import { AppLabSharedModule } from 'app/shared';
import {
    FinalProductComponent,
    FinalProductDetailComponent,
    FinalProductUpdateComponent,
    FinalProductDeletePopupComponent,
    FinalProductDeleteDialogComponent,
    finalProductRoute,
    finalProductPopupRoute,
    FinalProductDemandResetPopupComponent,
    FinalProductDemandResetDialogComponent,
    FinalProductPriceResetPopupComponent,
    FinalProductPriceResetDialogComponent,
    FinalProductPriceIncreasePopupComponent,
    FinalProductPriceIncreaseDialogComponent
} from './';

const ENTITY_STATES = [...finalProductRoute, ...finalProductPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES), SuiModule],
    declarations: [
        FinalProductComponent,
        FinalProductDetailComponent,
        FinalProductUpdateComponent,
        FinalProductDeleteDialogComponent,
        FinalProductDeletePopupComponent,
        FinalProductDemandResetPopupComponent,
        FinalProductDemandResetDialogComponent,
        FinalProductPriceResetPopupComponent,
        FinalProductPriceResetDialogComponent,
        FinalProductPriceIncreasePopupComponent,
        FinalProductPriceIncreaseDialogComponent
    ],
    entryComponents: [
        FinalProductComponent,
        FinalProductUpdateComponent,
        FinalProductDeleteDialogComponent,
        FinalProductDeletePopupComponent,
        FinalProductDemandResetPopupComponent,
        FinalProductDemandResetDialogComponent,
        FinalProductPriceResetPopupComponent,
        FinalProductPriceResetDialogComponent,
        FinalProductPriceIncreasePopupComponent,
        FinalProductPriceIncreaseDialogComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabFinalProductModule {}
