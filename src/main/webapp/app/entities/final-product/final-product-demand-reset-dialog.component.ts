import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFinalProduct } from 'app/shared/model/final-product.model';
import { FinalProductPopupService } from './final-product-popup.service';
import { FinalProductService } from './final-product.service';

@Component({
    selector: 'jhi-final-product-demand-reset-dialog',
    templateUrl: './final-product-demand-reset-dialog.component.html'
})
export class FinalProductDemandResetDialogComponent {
    finalProduct: IFinalProduct;

    constructor(
        private finalProductService: FinalProductService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    demandReset() {
        this.finalProductService.demandReset().subscribe(response => {
            this.eventManager.broadcast({
                name: 'finalProductListModification',
                content: 'FinalProduct generated'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-final-product-demand-reset-popup',
    template: ''
})
export class FinalProductDemandResetPopupComponent implements OnInit, OnDestroy {
    routeSub: any;

    constructor(private route: ActivatedRoute, private finalProductPopupService: FinalProductPopupService) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.finalProductPopupService.open(FinalProductDemandResetDialogComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
