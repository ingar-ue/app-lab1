import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FinalProduct } from 'app/shared/model/final-product.model';
import { FinalProductService } from './final-product.service';

@Injectable({ providedIn: 'root' })
export class FinalProductPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(private modalService: NgbModal, private router: Router, private finalProductService: FinalProductService) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.finalProductService.find(id).subscribe(finalProduct => {
                    this.ngbModalRef = this.finalProductModalRef(component, finalProduct.body);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    // create new object
                    const newObject = new FinalProduct();
                    // set default values
                    newObject.minDemand = 0;
                    newObject.maxDemand = 0;
                    newObject.price = 0;

                    this.ngbModalRef = this.finalProductModalRef(component, newObject);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    finalProductModalRef(component: Component, finalProduct: FinalProduct): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.finalProduct = finalProduct;
        modalRef.result.then(
            result => {
                this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                this.ngbModalRef = null;
            },
            reason => {
                this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                this.ngbModalRef = null;
            }
        );
        return modalRef;
    }
}
