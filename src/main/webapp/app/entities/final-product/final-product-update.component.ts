import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IFinalProduct } from 'app/shared/model/final-product.model';
import { FinalProductService } from './final-product.service';

@Component({
    selector: 'jhi-final-product-update',
    templateUrl: './final-product-update.component.html'
})
export class FinalProductUpdateComponent implements OnInit {
    private _finalProduct: IFinalProduct;
    isSaving: boolean;

    constructor(private finalProductService: FinalProductService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ finalProduct }) => {
            this.finalProduct = finalProduct;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.finalProduct.id !== undefined) {
            this.subscribeToSaveResponse(this.finalProductService.update(this.finalProduct));
        } else {
            this.subscribeToSaveResponse(this.finalProductService.create(this.finalProduct));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IFinalProduct>>) {
        result.subscribe((res: HttpResponse<IFinalProduct>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get finalProduct() {
        return this._finalProduct;
    }

    set finalProduct(finalProduct: IFinalProduct) {
        this._finalProduct = finalProduct;
    }
}
