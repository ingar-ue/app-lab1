import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { FinalProduct } from 'app/shared/model/final-product.model';
import { FinalProductService } from './final-product.service';
import { FinalProductComponent } from './final-product.component';
import { FinalProductDetailComponent } from './final-product-detail.component';
import { FinalProductUpdateComponent } from './final-product-update.component';
import { FinalProductDeletePopupComponent } from './final-product-delete-dialog.component';
import { FinalProductDemandResetPopupComponent } from './final-product-demand-reset-dialog.component';
import { FinalProductPriceResetPopupComponent } from './final-product-price-reset-dialog.component';
import { FinalProductPriceIncreasePopupComponent } from './final-product-price-increase-dialog.component';
import { IFinalProduct } from 'app/shared/model/final-product.model';

@Injectable({ providedIn: 'root' })
export class FinalProductResolve implements Resolve<IFinalProduct> {
    constructor(private service: FinalProductService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((finalProduct: HttpResponse<FinalProduct>) => finalProduct.body));
        }
        return of(new FinalProduct());
    }
}

export const finalProductRoute: Routes = [
    {
        path: 'final-product',
        component: FinalProductComponent,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLabApp.finalProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'final-product/:id/view',
        component: FinalProductDetailComponent,
        resolve: {
            finalProduct: FinalProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.finalProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'final-product/new',
        component: FinalProductUpdateComponent,
        resolve: {
            finalProduct: FinalProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.finalProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'final-product/:id/edit',
        component: FinalProductUpdateComponent,
        resolve: {
            finalProduct: FinalProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.finalProduct.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const finalProductPopupRoute: Routes = [
    {
        path: 'final-product/:id/delete',
        component: FinalProductDeletePopupComponent,
        resolve: {
            finalProduct: FinalProductResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.finalProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'final-product-price-reset',
        component: FinalProductPriceResetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.finalProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'final-product-price-increase',
        component: FinalProductPriceIncreasePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.finalProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'final-product-demand-reset',
        component: FinalProductDemandResetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.finalProduct.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
