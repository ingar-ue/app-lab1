import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFinalProduct } from 'app/shared/model/final-product.model';
import { FinalProductPopupService } from './final-product-popup.service';
import { FinalProductService } from './final-product.service';

@Component({
    selector: 'jhi-final-product-price-increase-dialog',
    templateUrl: './final-product-price-increase-dialog.component.html'
})
export class FinalProductPriceIncreaseDialogComponent {
    finalProduct: IFinalProduct;
    pricePercentage: number;

    constructor(
        private finalProductService: FinalProductService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
        // set default value to 0
        this.pricePercentage = 0;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    priceIncrease() {
        this.finalProductService.priceIncrease(this.pricePercentage).subscribe(response => {
            this.eventManager.broadcast({
                name: 'finalProductListModification',
                content: 'FinalProduct generated'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-final-product-price-increase-popup',
    template: ''
})
export class FinalProductPriceIncreasePopupComponent implements OnInit, OnDestroy {
    routeSub: any;

    constructor(private route: ActivatedRoute, private finalProductPopupService: FinalProductPopupService) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.finalProductPopupService.open(FinalProductPriceIncreaseDialogComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
