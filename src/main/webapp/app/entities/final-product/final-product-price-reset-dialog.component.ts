import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFinalProduct } from 'app/shared/model/final-product.model';
import { FinalProductPopupService } from './final-product-popup.service';
import { FinalProductService } from './final-product.service';

@Component({
    selector: 'jhi-final-product-price-reset-dialog',
    templateUrl: './final-product-price-reset-dialog.component.html'
})
export class FinalProductPriceResetDialogComponent {
    finalProduct: IFinalProduct;

    constructor(
        private finalProductService: FinalProductService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    priceReset() {
        this.finalProductService.priceReset().subscribe(response => {
            this.eventManager.broadcast({
                name: 'finalProductListModification',
                content: 'FinalProduct generated'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-final-product-price-reset-popup',
    template: ''
})
export class FinalProductPriceResetPopupComponent implements OnInit, OnDestroy {
    routeSub: any;

    constructor(private route: ActivatedRoute, private finalProductPopupService: FinalProductPopupService) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.finalProductPopupService.open(FinalProductPriceResetDialogComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
