import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IFinalProduct } from 'app/shared/model/final-product.model';
import { Principal } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { FinalProductService } from './final-product.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
    selector: 'jhi-final-product',
    templateUrl: './final-product.component.html'
})
export class FinalProductComponent implements OnInit, OnDestroy {
    finalProducts: IFinalProduct[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    search: any;

    constructor(
        private finalProductService: FinalProductService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private translateService: TranslateService
    ) {
        this.finalProducts = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.search = { keyword: '' };
        this.setSearchLanguage();

        this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            this.setSearchLanguage();
        });
    }

    setSearchLanguage() {
        this.search.inputPlaceholder = this.translateService.instant('global.searchKeywordPlaceholder');
        this.search.selectPlaceholder = this.translateService.instant('global.searchByPlaceholder');
        this.search.fields = [
            { value: 'code', label: this.translateService.instant('appLabApp.finalProduct.code') },
            { value: 'name', label: this.translateService.instant('appLabApp.finalProduct.name') },
            { value: 'price', label: this.translateService.instant('appLabApp.finalProduct.price') },
            { value: 'minDemand', label: this.translateService.instant('appLabApp.finalProduct.minDemand') },
            { value: 'maxDemand', label: this.translateService.instant('appLabApp.finalProduct.maxDemand') }
        ];
    }

    loadAll() {
        this.finalProductService
            .query({
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort(),
                type: this.search.field,
                keyword: this.search.keyword
            })
            .subscribe(
                (res: HttpResponse<IFinalProduct[]>) => this.paginateFinalProducts(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    reset() {
        this.page = 0;
        this.finalProducts = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInFinalProducts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IFinalProduct) {
        return item.id;
    }

    registerChangeInFinalProducts() {
        this.eventSubscriber = this.eventManager.subscribe('finalProductListModification', response => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private paginateFinalProducts(data: IFinalProduct[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        for (let i = 0; i < data.length; i++) {
            this.finalProducts.push(data[i]);
        }
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
