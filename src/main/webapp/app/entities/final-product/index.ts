export * from './final-product.service';
export * from './final-product-update.component';
export * from './final-product-delete-dialog.component';
export * from './final-product-detail.component';
export * from './final-product.component';
export * from './final-product.route';

export * from './final-product-popup.service';
export * from './final-product-price-increase-dialog.component';
export * from './final-product-demand-reset-dialog.component';
export * from './final-product-price-reset-dialog.component';
