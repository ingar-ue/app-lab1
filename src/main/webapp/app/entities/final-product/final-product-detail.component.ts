import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFinalProduct } from 'app/shared/model/final-product.model';

@Component({
    selector: 'jhi-final-product-detail',
    templateUrl: './final-product-detail.component.html'
})
export class FinalProductDetailComponent implements OnInit {
    finalProduct: IFinalProduct;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ finalProduct }) => {
            this.finalProduct = finalProduct;
        });
    }

    previousState() {
        window.history.back();
    }
}
