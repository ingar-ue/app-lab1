import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IFinalProduct } from 'app/shared/model/final-product.model';

type EntityResponseType = HttpResponse<IFinalProduct>;
type EntityArrayResponseType = HttpResponse<IFinalProduct[]>;

@Injectable({ providedIn: 'root' })
export class FinalProductService {
    private resourceUrl = SERVER_API_URL + 'api/final-products';

    constructor(private http: HttpClient) {}

    create(finalProduct: IFinalProduct): Observable<EntityResponseType> {
        return this.http.post<IFinalProduct>(this.resourceUrl, finalProduct, { observe: 'response' });
    }

    update(finalProduct: IFinalProduct): Observable<EntityResponseType> {
        return this.http.put<IFinalProduct>(this.resourceUrl, finalProduct, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IFinalProduct>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IFinalProduct[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    demandReset(): Observable<HttpResponse<any>> {
        return this.http.put<any>(this.resourceUrl + '-demand-reset', { observe: 'response' });
    }

    priceReset(): Observable<HttpResponse<any>> {
        return this.http.put<any>(this.resourceUrl + '-price-reset', { observe: 'response' });
    }

    priceIncrease(pricePercentage: number): Observable<HttpResponse<any>> {
        return this.http.put<any>(this.resourceUrl + '-price-increase/' + pricePercentage, { observe: 'response' });
    }
}
