import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';
import { Principal } from 'app/core';

import { OptimizationPoint1Service } from './optimization-point-1.service';

import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-balham.css';
import { IModelRunDef, IModelRunDefMessage } from 'app/shared/model/model-run-def.model';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';

import { Message } from '@stomp/stompjs';
import { WebSocketService } from 'app/shared';

@Component({
    selector: 'jhi-optimization-point-1',
    templateUrl: './optimization-point-1.component.html'
})
export class OptimizationPoint1Component implements OnInit, OnDestroy {
    currentAccount: any;
    demoUser = false;
    eventSubscriber: Subscription;
    optimizationPoint1: IOptimizationPoint1;
    modelRunDef: IModelRunDef;
    modelRunDefChange = 0;
    objFunctions: IModelObjFunction[];
    configs: IModelRunConfig[];
    configValuesLoaded = true;
    messageSubscription: Subscription;
    executingOP2: boolean;
    activeTabId: string;
    alertClosed: boolean;
    alertMessage = '';

    /******************* TABLE DATA **************************/
    // 1 - long groups
    columnDefs1 = [
        { headerName: 'Código', field: 'code', editable: false },
        { headerName: 'Valor', field: 'value', editable: false },
        { headerName: 'Unidad', field: 'valType', editable: false }
    ];
    // 2 - diameter classes
    columnDefs2 = [
        { headerName: 'Código', field: 'code', editable: false },
        { headerName: 'Diámetro Mínimo', field: 'minValue', editable: false },
        { headerName: 'Diámetro Máximo', field: 'maxValue', editable: false },
        { headerName: 'Unidad', field: 'valType', editable: false }
    ];
    // 3 - kerfs
    columnDefs3 = [{ headerName: 'Tipo', field: 'param', editable: false }, { headerName: 'Valor (mm)', field: 'value', editable: true }];
    // 4 - inter products
    columnDefs4 = [
        { headerName: 'Código', field: 'code', editable: false },
        { headerName: 'Espesor', field: 'tickness', editable: false },
        { headerName: 'Ancho', field: 'width', editable: false },
        { headerName: 'Unidad', field: 'valType', editable: false }
    ];

    rowData1: any;
    rowData2: any;
    rowData3: any;
    rowData4: any;

    grid1IsValid = false;
    grid2IsValid = false;
    grid3IsValid = false;
    grid4IsValid = false;

    grid1ValidMsg: String;
    grid2ValidMsg: String;
    grid3ValidMsg: String;
    grid4ValidMsg: String;

    gridApi: any;
    gridColumnApi: any;

    /************************ CLASS **************************/
    constructor(
        private optimizationPoint1Service: OptimizationPoint1Service,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private service: WebSocketService
    ) {
        this.modelRunDef = {};
        this.executingOP2 = false;
        this.activeTabId = 'dataTab';
        this.alertClosed = true;
    }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.principal.hasAuthority('ROLE_DEMO').then(demo => {
            this.demoUser = demo;
        });
        this.loadCase();
        this.registerChangeInOptimizationPoint1s();
        this.subscribeToSocket();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.messageSubscription.unsubscribe();
    }

    registerChangeInOptimizationPoint1s() {
        this.eventSubscriber = this.eventManager.subscribe('optimizationPoint1ListModification', response => this.reset());
    }

    reset() {
        this.loadCase();
        this.registerChangeInOptimizationPoint1s();
        // this.chartInit();
    }

    loadCase() {
        this.optimizationPoint1Service.loadCase().subscribe((res: HttpResponse<IOptimizationPoint1>) => {
            this.optimizationPoint1 = res.body;
            this.optimizationPoint1Service.checkRunning().subscribe((res1: HttpResponse<IModelRunDef>) => {
                if (res1.body) {
                    if (res1.body.name === 'Pattern Generator') {
                        this.modelRunDef = res1.body;
                        this.modelRunDefChange++;
                        this.activeTabId = 'processTab';
                    } else {
                        this.executingOP2 = true;
                        this.modelRunDef = this.optimizationPoint1.modelRunDef;
                        this.modelRunDefChange++;
                    }
                } else {
                    this.modelRunDef = this.optimizationPoint1.modelRunDef;
                    this.modelRunDefChange++;
                }
            });

            // Populate tables
            this.rowData1 = this.optimizationPoint1.longs;
            this.rowData2 = this.optimizationPoint1.diameters;
            this.rowData3 = this.optimizationPoint1.kerfConfigs;
            this.rowData4 = this.optimizationPoint1.interProducts;

            // Validate data grids and set message
            this.validateCaseData();

            this.optimizationPoint1Service
                .loadObjFunctions(this.optimizationPoint1.modelDefId)
                .subscribe((res1: HttpResponse<IModelObjFunction[]>) => {
                    this.objFunctions = res1.body;
                });
            this.optimizationPoint1Service
                .loadConfigs(this.optimizationPoint1.modelDefId)
                .subscribe((res2: HttpResponse<IModelRunConfig[]>) => {
                    this.configs = res2.body;
                });
        });
    }

    subscribeToSocket() {
        this.messageSubscription = this.service.subscribe().subscribe((message: Message) => {
            const msg: IModelRunDefMessage = JSON.parse(message.body);
            if (msg.name === 'Pattern Generator') {
                this.modelRunDef.status = msg.status;
                this.modelRunDef.progress = msg.progress;
                this.modelRunDef.dataSetResult = msg.result;
                this.modelRunDef.id = msg.id;
                this.modelRunDefChange++;

                if (msg.progress === 100 && msg.status === 'FINISHED') {
                    this.fireSuccessAlert('La ejecución ha finalizado, puede visualizar los resultados en la pestaña correspondiente');
                    this.activeTabId = 'resultsTab';
                }

                if (msg.progress === 100 && msg.status === 'ERROR') {
                    this.modelRunDef.comments = msg.result;
                    this.activeTabId = 'resultsTab';
                }
            } else if (msg.progress === 100) {
                this.executingOP2 = false;
                this.fireSuccessAlert('La ejecución del otro proceso ha finalizado');
            }
        });
    }

    run() {
        this.optimizationPoint1Service.run(this.optimizationPoint1).subscribe(res => console.log(res));
    }

    fireSuccessAlert(msg: string) {
        this.alertMessage = msg;
        this.alertClosed = false;
        setTimeout(() => (this.alertClosed = true), 10000);
    }

    configChange() {
        this.configValuesLoaded = true;
        for (const config of this.configs) {
            if (!config.value) {
                this.configValuesLoaded = false;
            }
        }
    }

    tabChange(e) {
        this.activeTabId = e.nextId;
    }

    /******************* TABLE FUNCTIONS **************************/
    validateCaseData() {
        // grid 1
        if (this.rowData1.length > 0) {
            this.grid1IsValid = true;
            this.grid1ValidMsg = 'Datos correctos';
        } else {
            this.grid1IsValid = false;
            this.grid1ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 2
        if (this.rowData2.length > 0) {
            this.grid2IsValid = true;
            this.grid2ValidMsg = 'Datos correctos';
        } else {
            this.grid2IsValid = false;
            this.grid2ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 3
        if (this.rowData3.length > 0) {
            this.grid3IsValid = true;
            this.grid3ValidMsg = 'Datos correctos';
        } else {
            this.grid3IsValid = false;
            this.grid3ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 4
        if (this.rowData4.length > 0) {
            this.grid4IsValid = true;
            this.grid4ValidMsg = 'Datos correctos';
        } else {
            this.grid4IsValid = false;
            this.grid4ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
    }

    onGridReady(params, id) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        // Setea ancho de las columnas para llenar ancho de la tabla
        params.api.sizeColumnsToFit();
        // Si la cantidad de filas a mostrar es mayor que 25 recorta el largo de la tabla
        console.log('params.api.getDisplayedRowCount() ' + params.api.getDisplayedRowCount());
        if (params.api.getDisplayedRowCount() > 10) {
            params.api.setGridAutoHeight(false);
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '300px';
        } else {
            params.api.setGridAutoHeight(true);
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '';
        }
    }
}
