import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';

type EntityResponseType = HttpResponse<IOptimizationPoint1>;
type EntityArrayResponseType = HttpResponse<IOptimizationPoint1[]>;

@Injectable({ providedIn: 'root' })
export class OptimizationPoint1Service {
    private resourceUrl = SERVER_API_URL + 'api/optimization-point-1';

    constructor(private http: HttpClient) {}

    loadCase(): Observable<HttpResponse<IOptimizationPoint1>> {
        return this.http
            .get(this.resourceUrl + '-load', { observe: 'response' })
            .pipe(map((res: HttpResponse<IOptimizationPoint1>) => res));
    }

    run(optimizationPoint1: IOptimizationPoint1): Observable<HttpResponse<any>> {
        return this.http.put(this.resourceUrl + '-execute', optimizationPoint1, { observe: 'response' }).pipe(map((res: any) => res));
    }

    loadObjFunctions(modelDefId: number): Observable<HttpResponse<IModelObjFunction[]>> {
        return this.http
            .get(this.resourceUrl + '-load-obj-functions/' + modelDefId, { observe: 'response' })
            .pipe(map((res: HttpResponse<IModelObjFunction[]>) => res));
    }

    loadConfigs(modelDefId: number): Observable<HttpResponse<IModelRunConfig[]>> {
        return this.http
            .get(this.resourceUrl + '-load-configs/' + modelDefId, { observe: 'response' })
            .pipe(map((res: HttpResponse<IModelRunConfig[]>) => res));
    }

    checkRunning(): Observable<HttpResponse<IModelRunDef>> {
        return this.http
            .get(this.resourceUrl + '-check-running', { observe: 'response' })
            .pipe(map((res: HttpResponse<IModelRunDef>) => res));
    }

    private convertDateFromClient(optimizationPoint1: IOptimizationPoint1): IOptimizationPoint1 {
        const copy: IOptimizationPoint1 = Object.assign({}, optimizationPoint1, {
            // runDate:
            //     optimizationPoint1.runDate != null && optimizationPoint1.runDate.isValid()
            //         ? optimizationPoint1.runDate.format(DATE_FORMAT)
            //         : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        // res.body.runDate = res.body.runDate != null ? moment(res.body.runDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        // res.body.forEach((optimizationPoint1: IOptimizationPoint1) => {
        //     optimizationPoint1.runDate = optimizationPoint1.runDate != null ? moment(optimizationPoint1.runDate) : null;
        // });
        return res;
    }

    getChartData(modelRunDefId, chart): Observable<HttpResponse<any>> {
        return this.http
            .get(this.resourceUrl + `-results-${chart}/${modelRunDefId}`, { observe: 'response' })
            .pipe(map((res: HttpResponse<any>) => res.body));
    }
}
