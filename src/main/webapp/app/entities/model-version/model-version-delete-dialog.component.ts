import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IModelVersion } from 'app/shared/model/model-version.model';
import { ModelVersionService } from './model-version.service';

@Component({
    selector: 'jhi-model-version-delete-dialog',
    templateUrl: './model-version-delete-dialog.component.html'
})
export class ModelVersionDeleteDialogComponent {
    modelVersion: IModelVersion;

    constructor(
        private modelVersionService: ModelVersionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.modelVersionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'modelVersionListModification',
                content: 'Deleted an modelVersion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-model-version-delete-popup',
    template: ''
})
export class ModelVersionDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelVersion }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ModelVersionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.modelVersion = modelVersion;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
