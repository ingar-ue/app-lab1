import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ModelVersionUpdateDialogComponent,
    ModelVersionUpdatePopupComponent,
    ModelVersionDeletePopupComponent,
    ModelVersionDeleteDialogComponent,
    modelVersionRoute,
    modelVersionPopupRoute
} from './';

const ENTITY_STATES = [...modelVersionRoute, ...modelVersionPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelVersionUpdateDialogComponent,
        ModelVersionUpdatePopupComponent,
        ModelVersionDeleteDialogComponent,
        ModelVersionDeletePopupComponent
    ],
    entryComponents: [
        ModelVersionUpdateDialogComponent,
        ModelVersionUpdatePopupComponent,
        ModelVersionDeleteDialogComponent,
        ModelVersionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabModelVersionModule {}
