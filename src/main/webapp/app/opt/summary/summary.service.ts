import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable({ providedIn: 'root' })
export class SummaryService {
    private resourceUrl = SERVER_API_URL + 'api/';

    constructor(private http: HttpClient) {}

    optimizationSummary(): Observable<HttpResponse<any>> {
        return this.http.get(this.resourceUrl + 'optimization-summary', { observe: 'response' });
    }

    longGroupSummary(): Observable<HttpResponse<any>> {
        return this.http.get(this.resourceUrl + 'long-group-summary', { observe: 'response' });
    }
}
