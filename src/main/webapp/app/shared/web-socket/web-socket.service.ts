import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { StompConfig, StompRService } from '@stomp/ng2-stompjs';
import * as SockJS from 'sockjs-client';
import { Message } from '@stomp/stompjs';
import { SERVER_API_URL } from 'app/app.constants';

@Injectable({ providedIn: 'root' })
export class WebSocketService {
    stompConfig: StompConfig = {
        // Which server?
        url: new SockJS(SERVER_API_URL + '/socket'),
        headers: {
            login: 'guest',
            passcode: 'guest'
        },
        heartbeat_in: 0, // Typical value 0 - disabled
        heartbeat_out: 20000, // Typical value 20000 - every 20 seconds
        reconnect_delay: 5000, // Typical value 5000 (5 seconds)
        debug: true // Will log diagnostics on console
    };

    constructor(private http: HttpClient, private _stompService: StompRService) {}

    connect() {
        this._stompService.config = this.stompConfig;
        this._stompService.initAndConnect();
    }

    subscribe(): Observable<Message> {
        const stomp_subscription = this._stompService.subscribe('/messages');
        return stomp_subscription;
    }
}
