import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'stringFilter'
})
@Injectable()
export class StringFilterPipe implements PipeTransform {
    transform(items: any[], field: string, value: string) {
        if (items && items.length) {
            return items.filter(item => {
                if (value && item[field].toLowerCase().indexOf(value.toLowerCase()) === -1) {
                    return false;
                }
                return true;
            });
        } else {
            return items;
        }
    }
}
