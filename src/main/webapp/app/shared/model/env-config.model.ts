export interface IEnvConfig {
    id?: number;
    param?: string;
    value?: string;
    description?: string;
}

export class EnvConfig implements IEnvConfig {
    constructor(public id?: number, public param?: string, public value?: string, public description?: string) {}
}
