export const enum Unit {
    CM = 'CM',
    MM = 'MM',
    M = 'M',
    PIE = 'PIE',
    PULGADA = 'PULGADA'
}

export interface ILongGroup {
    id?: number;
    code?: string;
    value?: number;
    valType?: Unit;
    description?: string;
}

export class LongGroup implements ILongGroup {
    constructor(public id?: number, public code?: string, public value?: number, public valType?: Unit, public description?: string) {}
}
