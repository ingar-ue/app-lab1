export interface IResourceQty {
    id?: number;
    quantity?: number;
    diameterGroupId?: number;
    longGroupId?: number;
    diameterGroupCode?: number;
    longGroupCode?: number;
}

export class ResourceQty implements IResourceQty {
    constructor(public id?: number, public quantity?: number, public diameterGroupId?: number, public longGroupId?: number) {}
}
