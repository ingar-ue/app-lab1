import { DiameterGroup, IDiameterGroup } from './diameter-group.model';
import { ILongGroup, LongGroup } from './long-group.model';
import { IInterProduct, InterProduct } from './inter-product.model';
import { IKerfConfig, KerfConfig } from './kerf-config.model';
import { IModelRunDef, ModelRunDef } from 'app/shared/model/model-run-def.model';

export interface IOptimizationPoint1 {
    diameters?: IDiameterGroup[];
    longs?: ILongGroup[];
    interProducts?: IInterProduct[];
    kerfConfigs?: IKerfConfig[];
    modelRunDef?: IModelRunDef;
    modelDefId?: number;
}

export class OptimizationPoint1 implements IOptimizationPoint1 {
    constructor(
        public diameters?: DiameterGroup[],
        public longs?: LongGroup[],
        public interProducts?: InterProduct[],
        public kerfConfigs?: KerfConfig[],
        public modelRunDef?: ModelRunDef,
        public modelDefId?: number
    ) {}
}
