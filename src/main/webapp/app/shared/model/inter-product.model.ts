export const enum Unit {
    CM = 'CM',
    MM = 'MM',
    M = 'M',
    PIE = 'PIE',
    PULGADA = 'PULGADA'
}

export interface IInterProduct {
    id?: number;
    code?: string;
    tickness?: number;
    width?: number;
    valType?: Unit;
    longGroups?: string;
    longGroupsArray?: string[];
    description?: string;
}

export class InterProduct implements IInterProduct {
    constructor(
        public id?: number,
        public code?: string,
        public tickness?: number,
        public width?: number,
        public valType?: Unit,
        public longGroups?: string,
        public longGroupsArray?: string[],
        public description?: string
    ) {}
}
