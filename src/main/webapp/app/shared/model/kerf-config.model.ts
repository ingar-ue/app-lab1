export interface IKerfConfig {
    id?: number;
    param?: string;
    value?: number;
}

export class KerfConfig implements IKerfConfig {
    constructor(public id?: number, public param?: string, public value?: number) {}
}
