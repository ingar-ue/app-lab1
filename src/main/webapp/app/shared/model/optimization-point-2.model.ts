import { FinalProduct, IFinalProduct } from './final-product.model';
import { IModelRunDef, ModelRunDef } from 'app/shared/model/model-run-def.model';
import { IDiameterGroup } from 'app/shared/model/diameter-group.model';
import { ILongGroup } from 'app/shared/model/long-group.model';
import { IInterProduct } from 'app/shared/model/inter-product.model';
import { IProductRelations } from 'app/shared/model/product-relations.model';
import { IResourceCost } from 'app/shared/model/resource-cost.model';
import { IResourceQty } from 'app/shared/model/resource-qty.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';

export interface IOptimizationPoint2 {
    finalProducts?: IFinalProduct[];
    diameters?: IDiameterGroup[];
    longs?: ILongGroup[];
    interProducts?: IInterProduct[];
    productRelations?: IProductRelations[];
    resourceCosts?: IResourceCost[];
    resourceQtys?: IResourceQty[];
    modelRunDef?: IModelRunDef;
    modelDefId?: number;
    modelRunConfigs?: IModelRunConfig[];
    optimizationPoint1?: IModelRunDef;
}

export class OptimizationPoint2 implements IOptimizationPoint2 {
    constructor(
        public finalProducts?: FinalProduct[],
        public modelRunDef?: ModelRunDef,
        public diameters?: IDiameterGroup[],
        public longs?: ILongGroup[],
        public interProducts?: IInterProduct[],
        public productRelations?: IProductRelations[],
        public resourceCosts?: IResourceCost[],
        public resourceQtys?: IResourceQty[],
        public modelDefId?: number,
        public modelRunConfigs?: IModelRunConfig[],
        public optimizationPoint1?: IModelRunDef
    ) {}
}
