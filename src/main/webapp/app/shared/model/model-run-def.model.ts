import { Moment } from 'moment';

export const enum Language {
    GAMS = 'GAMS',
    PYOMO = 'PYOMO',
    ORTOOLS = 'ORTOOLS'
}

export const enum Solver {
    CPLEX = 'CPLEX',
    GUROBI = 'GUROBI',
    GLPK = 'GLPK',
    CBC = 'CBC'
}

export const enum Status {
    READY = 'READY',
    RUNNING = 'RUNNING',
    FINISHED = 'FINISHED',
    ERROR = 'ERROR'
}

export const enum ResultStatus {
    DISCARDED = 'DISCARDED',
    INANALYSIS = 'INANALYSIS',
    APPLIED = 'APPLIED'
}

export interface IModelRunDefMessage {
    name?: string;
    id?: number;
    status?: string;
    progress?: number;
    result?: string;
}

export interface IModelRunDef {
    id?: number;
    name?: string;
    language?: Language;
    solver?: Solver;
    version?: number;
    objFunction?: string;
    objFunctionCode?: number;
    runDate?: Moment;
    startTime?: string;
    endTime?: string;
    status?: string;
    progress?: number;
    dataSetInput?: string;
    dataSetResult?: string;
    userResultContentType?: string;
    userResult?: any;
    otherResultContentType?: string;
    otherResult?: any;
    comments?: string;
    user?: string;
    duration?: string;
    resultStatus?: ResultStatus;
    resultResume?: string;
}

export class ModelRunDef implements IModelRunDef {
    constructor(
        public id?: number,
        public name?: string,
        public language?: Language,
        public solver?: Solver,
        public version?: number,
        public objFunction?: string,
        public runDate?: Moment,
        public startTime?: string,
        public endTime?: string,
        public status?: Status,
        public progress?: number,
        public dataSetInput?: string,
        public dataSetResult?: string,
        public userResultContentType?: string,
        public userResult?: any,
        public otherResultContentType?: string,
        public otherResult?: any,
        public comments?: string,
        public user?: string,
        public duration?: string,
        public resultStatus?: ResultStatus,
        public resultResume?: string
    ) {}
}
