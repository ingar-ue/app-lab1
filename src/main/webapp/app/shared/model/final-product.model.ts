export interface IFinalProduct {
    id?: number;
    code?: string;
    name?: string;
    minDemand?: number;
    maxDemand?: number;
    price?: number;
}

export class FinalProduct implements IFinalProduct {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public minDemand?: number,
        public maxDemand?: number,
        public price?: number
    ) {}
}
