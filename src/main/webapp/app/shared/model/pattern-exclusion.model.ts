import { Moment } from 'moment';

export interface IPatternExclusion {
    id?: number;
    name?: string;
    description?: string;
    exclusionDate?: Moment;
}

export class PatternExclusion implements IPatternExclusion {
    constructor(public id?: number, public name?: string, public description?: string, public exclusionDate?: Moment) {}
}
