export interface IProductRelations {
    id?: number;
    quantity1?: number;
    quantity2?: number;
    quantity3?: number;
    finalProduct1Id?: number;
    finalProduct2Id?: number;
    finalProduct3Id?: number;
    finalProduct1Code?: number;
    finalProduct2Code?: number;
    finalProduct3Code?: number;
    interProductId?: number;
    interProductCode?: number;
    longGroupId?: number;
    longGroupCode?: number;
}

export class ProductRelations implements IProductRelations {
    constructor(
        public id?: number,
        public quantity1?: number,
        public quantity2?: number,
        public quantity3?: number,
        public finalProduct1Id?: number,
        public finalProduct2Id?: number,
        public finalProduct3Id?: number,
        public finalProduct1Code?: number,
        public finalProduct2Code?: number,
        public finalProduct3Code?: number,
        public interProductId?: number,
        public interProductCode?: number,
        public longGroupId?: number,
        public longGroupCode?: number
    ) {}
}
