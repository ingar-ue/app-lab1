export interface IConfigProccessTime {
    id?: number;
    value?: number;
    diameterGroupId?: number;
    longGroupId?: number;
    diameterGroupCode?: string;
    longGroupCode?: string;
}

export class ConfigProccessTime implements IConfigProccessTime {
    constructor(public id?: number, public value?: number, public diameterGroupId?: number, public longGroupId?: number) {}
}
