export interface IConfigYieldPlanning {
    id?: number;
    value?: number;
    diameterGroupId?: number;
    diameterGroupCode?: string;
}

export class ConfigYieldPlanning implements IConfigYieldPlanning {
    constructor(public id?: number, public value?: number, public diameterGroupId?: number) {}
}
