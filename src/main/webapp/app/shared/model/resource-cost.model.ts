export interface IResourceCost {
    id?: number;
    value?: number;
    diameterGroupId?: number;
}

export class ResourceCost implements IResourceCost {
    constructor(public id?: number, public value?: number, public diameterGroupId?: number) {}
}
