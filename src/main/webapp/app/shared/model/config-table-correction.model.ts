export interface IConfigTableCorrection {
    id?: number;
    value1?: number;
    value2?: number;
    value3?: number;
    value4?: number;
    value5?: number;
    value6?: number;
    diameterGroupId?: number;
    diameterGroupCode?: string;
}

export class ConfigTableCorrection implements IConfigTableCorrection {
    constructor(
        public id?: number,
        public value1?: number,
        public value2?: number,
        public value3?: number,
        public value4?: number,
        public value5?: number,
        public value6?: number,
        public diameterGroupId?: number,
        public diameterGroupCode?: string
    ) {}
}
