export interface ICompany {
    id?: number;
    name?: string;
    address?: string;
    website?: string;
    email?: string;
    phone?: string;
    description?: string;
}

export class Company implements ICompany {
    constructor(
        public id?: number,
        public name?: string,
        public address?: string,
        public website?: string,
        public email?: string,
        public phone?: string,
        public description?: string
    ) {}
}
