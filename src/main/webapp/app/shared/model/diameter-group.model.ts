export const enum Unit {
    CM = 'CM',
    MM = 'MM',
    M = 'M',
    PIE = 'PIE',
    PULGADA = 'PULGADA'
}

export interface IDiameterGroup {
    id?: number;
    code?: string;
    minValue?: number;
    maxValue?: number;
    valType?: Unit;
    schema?: string;
    tablesCentral?: number;
    tablesSide?: number;
    tablesTop?: number;
    longGroups?: string;
    yieldIndex?: number;
}

export class DiameterGroup implements IDiameterGroup {
    constructor(
        public id?: number,
        public code?: string,
        public minValue?: number,
        public maxValue?: number,
        public valType?: Unit,
        public schema?: string,
        public tablesCentral?: number,
        public tablesSide?: number,
        public tablesTop?: number,
        public longGroups?: string,
        public yieldIndex?: number
    ) {}
}
