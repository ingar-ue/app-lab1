import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { AppLabSharedLibsModule, AppLabSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { StringFilterPipe } from 'app/shared/pipes/string-filter.pipe';
import { CustomFilterPipe } from 'app/shared/pipes/custom-filter.pipe';
import { SortPipe } from 'app/shared/pipes/sort.pipe';
import { GroupByPipe } from 'app/shared/pipes/group-by.pipe';
import { OptimizationPoint1ChartsComponent } from 'app/shared/chart/optimization-point-1-charts.component';
import { ChartModule } from 'angular2-chartjs';
import { OptimizationPoint2ChartsComponent } from 'app/shared/chart/optimization-point-2-charts.component';
import { SuiModule } from 'ng2-semantic-ui';

@NgModule({
    imports: [AppLabSharedLibsModule, AppLabSharedCommonModule, ChartModule, SuiModule],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        StringFilterPipe,
        CustomFilterPipe,
        SortPipe,
        GroupByPipe,
        OptimizationPoint1ChartsComponent,
        OptimizationPoint2ChartsComponent
    ],
    providers: [
        { provide: NgbDateAdapter, useClass: NgbDateMomentAdapter },
        OptimizationPoint1ChartsComponent,
        OptimizationPoint2ChartsComponent
    ],
    entryComponents: [JhiLoginModalComponent, OptimizationPoint1ChartsComponent, OptimizationPoint2ChartsComponent],
    exports: [
        AppLabSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        StringFilterPipe,
        CustomFilterPipe,
        SortPipe,
        GroupByPipe,
        OptimizationPoint1ChartsComponent,
        OptimizationPoint2ChartsComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabSharedModule {}
