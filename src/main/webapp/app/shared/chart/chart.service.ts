import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ChartService {
    opt2DemandFilter: any;

    constructor() {}

    setFilters(filters) {
        this.opt2DemandFilter = filters;
    }

    objectToArray(obj) {
        const array = [];
        for (const key of Object.keys(obj)) {
            array.push({ product: key, demand: obj[key] });
        }
        return array;
    }

    filterByProduction = item => {
        switch (this.opt2DemandFilter.production) {
            case 'null':
                return true;
            case 'positive':
                return item.demand < 0;
            case 'negative':
                return item.demand > 0;
            case 'satisfied':
                return item.demand === 0;
        }
    };

    filterByLarge = item => this.opt2DemandFilter.large === 'null' || item.product.split('x')[2] === this.opt2DemandFilter.large;

    filterByType = item => this.opt2DemandFilter.type === 'null' || item.product.indexOf(this.opt2DemandFilter.type) !== -1;

    sortByKey = (a, b) => {
        return a.product < b.product ? -1 : a.product > b.product ? 1 : 0;
    };

    applyFilters(data, filters) {
        this.setFilters(filters);
        return data
            .filter(this.filterByProduction)
            .filter(this.filterByType)
            .filter(this.filterByLarge)
            .sort(this.sortByKey);
    }

    getArrayByKey(array, key) {
        const result = [];
        array.map(obj => {
            result.push(obj[key]);
        });
        return result;
    }

    setChartConfig(dataset: object, title, labelX, labelY, aspectRatio = true, showLegend = false, isStacked = false, tooltip = null) {
        const chartConfig = {
            data: dataset,
            options: {
                tooltips: {},
                legend: {},
                // responsive: true,
                maintainAspectRatio: aspectRatio,
                scales: {
                    xAxes: {},
                    yAxes: {}
                },
                title: {}
            }
        };

        if (title) {
            chartConfig.options.title = {
                display: true,
                position: 'bottom',
                fontSize: 14,
                text: title
            };
        }

        if (showLegend) {
            chartConfig.options.legend = {
                display: true
            };
        } else {
            chartConfig.options.legend = false;
        }

        if (labelX && !isStacked) {
            chartConfig.options.scales.xAxes = [
                {
                    scaleLabel: {
                        display: true,
                        labelString: labelX
                    }
                }
            ];
        }

        if (labelY && !isStacked) {
            chartConfig.options.scales.yAxes = [
                {
                    scaleLabel: {
                        display: true,
                        labelString: labelY
                    }
                }
            ];
        }

        if (isStacked) {
            // gantt chart
            chartConfig.options.scales = {
                xAxes: [
                    {
                        stacked: isStacked,
                        scaleLabel: {
                            display: true,
                            labelString: labelX
                        }
                    }
                ],
                yAxes: [{ stacked: isStacked }]
            };

            chartConfig.options['tooltips']['callbacks'] = {
                title: (tooltipItem, data) => {
                    return data['labels'][tooltipItem[0]['index']];
                },
                label: (tooltipItem, data) => {
                    if (tooltipItem['datasetIndex']) {
                        return tooltip[tooltipItem['index']];
                    } else {
                        return '';
                    }
                },
                afterLabel: (tooltipItem, data) => {
                    return '';
                }
            };
        }

        return chartConfig;
    }
}
