import { Component, Input, OnChanges } from '@angular/core';
import { OptimizationPoint1Service } from 'app/entities/optimization-point-1/optimization-point-1.service';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { ChartService } from 'app/shared/chart/chart.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
    selector: 'jhi-optimization-point-1-charts',
    templateUrl: './optimization-point-1-charts.component.html'
})
export class OptimizationPoint1ChartsComponent implements OnChanges {
    charts: any;
    chartsLoaded = false;
    chartLabels: any;
    json = JSON;

    @Input() currentChart: number;
    @Input() change: number;
    @Input() showSidebar: boolean;
    @Input() chartColumnSize: number;
    @Input() modelRunDef: IModelRunDef;

    constructor(
        private optimizationPoint1Service: OptimizationPoint1Service,
        private chartService: ChartService,
        private translateService: TranslateService
    ) {
        this.chartInit();
        this.showSidebar = true;
        this.chartColumnSize = 8;

        this.setLanguageValues();
        this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            this.setLanguageValues();
            if (this.chartsLoaded) {
                this.getChartData('chart1');
                this.getChartData('chart2');
                this.getChartData('chart3');
            }
        });
    }

    setLanguageValues() {
        this.chartLabels = {
            qty: this.translateService.instant('appLabApp.optimization.op1.qty'),
            interProducts: this.translateService.instant('appLabApp.optimization.op1.interProducts'),
            diameterClasses: this.translateService.instant('appLabApp.optimization.op1.diameterClasses'),
            performance: this.translateService.instant('appLabApp.optimization.op1.performance'),
            patternQty: this.translateService.instant('appLabApp.optimization.op1.patternQty')
        };
    }

    ngOnChanges() {
        if (!this.chartsLoaded && this.modelRunDef.status === 'FINISHED') {
            this.getChartData('chart1');
            this.getChartData('chart2');
            this.getChartData('chart3');
            this.chartsLoaded = true;
        }
    }

    chartInit() {
        this.charts = {};
        this.currentChart = 1;
        if (!this.chartsLoaded && this.modelRunDef && this.modelRunDef.status === 'FINISHED') {
            this.getChartData('chart1');
            this.getChartData('chart2');
            this.getChartData('chart3');
            this.chartsLoaded = true;
        }
    }

    getChartData(chart) {
        this.optimizationPoint1Service.getChartData(this.modelRunDef.id, chart).subscribe(
            data => {
                this.setData(chart, data);
            },
            err => {
                console.log(err);
            }
        );
    }

    setData(chart, data) {
        this.charts[chart] = this.chartService.setChartConfig(
            {
                labels: data.valuesX,
                datasets: [
                    {
                        data: data.valuesY,
                        backgroundColor: 'rgba(0,166,90,.5)'
                    }
                ]
            },
            null,
            this.getChartLabel(chart, true),
            this.getChartLabel(chart, false)
        );
    }

    getChartLabel(chart, xAxis) {
        switch (chart) {
            case 'chart1':
                if (xAxis) {
                    return '% ' + this.chartLabels.performance;
                } else {
                    return this.chartLabels.patternQty;
                }
            case 'chart2':
                if (xAxis) {
                    return this.chartLabels.diameterClasses;
                } else {
                    return '% ' + this.chartLabels.performance;
                }
            case 'chart3':
                if (xAxis) {
                    return this.chartLabels.interProducts;
                } else {
                    return this.chartLabels.qty;
                }
        }
    }
}
