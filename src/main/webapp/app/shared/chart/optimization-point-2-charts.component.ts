import { Component, Input, OnChanges } from '@angular/core';
import { ChartService } from 'app/shared/chart/chart.service';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { OptimizationPoint2Service } from 'app/entities/optimization-point-2';
import { Moment } from 'moment';
import * as moment from 'moment';
import { DecimalPipe } from '@angular/common';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
    selector: 'jhi-optimization-point-2-charts',
    templateUrl: './optimization-point-2-charts.component.html'
})
export class OptimizationPoint2ChartsComponent implements OnChanges {
    charts: any;
    chartsLoaded = false;
    demandChartData: any;
    resumeData: any;
    filters: any;
    manufactoringOrder: any;
    moment = moment;
    chartLabels: any;
    json = JSON;

    @Input() change: number;
    @Input() currentChart: number;
    @Input() showSidebar: boolean;
    @Input() chartColumnSize: number;
    @Input() modelRunDef: IModelRunDef;

    constructor(
        private optimizationPoint2Service: OptimizationPoint2Service,
        private chartService: ChartService,
        private decimalPipe: DecimalPipe,
        private translateService: TranslateService
    ) {
        this.chartInit();
        this.showSidebar = true;
        this.chartColumnSize = 8;

        this.setLanguageValues();
        this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            this.setLanguageValues();
            if (this.demandChartData) {
                this.setChart1();
            }
            if (this.resumeData) {
                this.setResume(this.resumeData);
            }
            if (this.manufactoringOrder) {
                this.setManufactoringOrderGantt();
            }
        });
    }

    setLanguageValues() {
        this.chartLabels = {
            performance: this.translateService.instant('appLabApp.optimization.op2.performance'),
            satisfiedShort: this.translateService.instant('appLabApp.optimization.op2.satisfiedShort'),
            unsatisfiedShort: this.translateService.instant('appLabApp.optimization.op2.unsatisfiedShort'),
            satisfied: this.translateService.instant('appLabApp.optimization.op2.satisfied'),
            unsatisfied: this.translateService.instant('appLabApp.optimization.op2.unsatisfied'),
            demand: this.translateService.instant('appLabApp.optimization.op2.demand'),
            waste: this.translateService.instant('appLabApp.optimization.op2.waste'),
            finalProduct: this.translateService.instant('appLabApp.optimization.op2.finalProduct'),
            demandTables: this.translateService.instant('appLabApp.optimization.op2.demandTables'),
            minutes: this.translateService.instant('appLabApp.optimization.op2.minutes'),
            hours: this.translateService.instant('appLabApp.optimization.op2.hours')
        };
    }

    ngOnChanges() {
        if (!this.chartsLoaded && this.modelRunDef.status === 'FINISHED') {
            this.getChartData('resume');
            this.getChartData('chart1');
            this.getChartData('production-data');
            this.chartsLoaded = true;
        }
    }

    chartInit() {
        this.charts = {};
        this.currentChart = 1;
        this.resetFilters();
        if (!this.chartsLoaded && this.modelRunDef && this.modelRunDef.status === 'FINISHED') {
            this.getChartData('resume');
            this.getChartData('chart1');
            this.getChartData('production-data');
            this.chartsLoaded = true;
        }
    }

    resetFilters(reload?) {
        this.filters = {
            production: 'null',
            large: 'null',
            type: 'null'
        };

        if (reload) {
            this.setChart1();
        }
    }

    getChartData(chart) {
        this.optimizationPoint2Service.getChartData(this.modelRunDef.id, chart).subscribe(
            data => {
                switch (chart) {
                    case 'resume':
                        this.resumeData = data;
                        this.setResume(this.resumeData);
                        break;
                    case 'chart1':
                        this.demandChartData = this.chartService.objectToArray(data);
                        this.setChart1();
                        break;
                    case 'production-data':
                        this.manufactoringOrder = data;
                        this.setManufactoringOrderGantt();
                        break;
                }
            },
            err => {
                console.log(err);
            }
        );
    }

    setManufactoringOrderGantt() {
        let totalTime = 0;
        const beginData = [];
        const endData = [];
        const gantLabels = [];
        const colors = [];
        const minutes = [];

        for (const pattern of this.manufactoringOrder.production) {
            gantLabels.push('setup');
            colors.push('rgba(0,0,0,.2)');
            beginData.push(totalTime);
            const beginTime = parseFloat(this.decimalPipe.transform(this.manufactoringOrder.setupTime / 60, '1.2-2', 'en'));
            totalTime += beginTime;
            endData.push(beginTime);
            minutes.push(this.manufactoringOrder.setupTime + ' ' + this.chartLabels.minutes);

            gantLabels.push(pattern.name);
            colors.push('rgba(0,166,90,.5)');
            beginData.push(totalTime);
            const endTime = parseFloat(this.decimalPipe.transform(pattern.patternProcessTime / 3600, '1.2-2', 'en'));
            totalTime += endTime;
            endData.push(endTime);
            minutes.push(moment.duration(pattern.patternProcessTime, 's').minutes() + ' ' + this.chartLabels.minutes);
        }

        this.manufactoringOrder.gantt = this.chartService.setChartConfig(
            {
                labels: gantLabels,
                datasets: [
                    {
                        data: beginData,
                        backgroundColor: 'rgba(63,103,126,0)',
                        hoverBackgroundColor: 'rgba(50,90,100,0)'
                    },
                    {
                        data: endData,
                        backgroundColor: colors
                    }
                ]
            },
            null,
            this.chartLabels.hours,
            null,
            false,
            false,
            true,
            minutes
        );
    }

    setResume(data) {
        const chartData = data.slice(-2);
        this.charts.resume = {
            info: data,
            chart1: this.chartService.setChartConfig(
                {
                    labels: [this.chartLabels.satisfiedShort, this.chartLabels.unsatisfiedShort],
                    datasets: [
                        {
                            backgroundColor: ['rgba(0,166,90,.5)', '#f0f0f0'],
                            data: [100 - chartData[0][1], chartData[0][1]]
                        }
                    ]
                },
                this.chartLabels.satisfied + ' (%)',
                null,
                null
            ),
            chart2: this.chartService.setChartConfig(
                {
                    labels: [this.chartLabels.performance, this.chartLabels.waste],
                    datasets: [
                        {
                            backgroundColor: ['rgba(0,166,90,.5)', '#f0f0f0'],
                            data: [chartData[1][1], 100 - chartData[1][1]]
                        }
                    ]
                },
                this.chartLabels.performance + ' (%)',
                null,
                null
            )
        };
    }

    setChart1() {
        const filterData = this.applyFilters();

        this.setData('chart1', {
            valuesX: this.chartService.getArrayByKey(filterData, 'product'),
            valuesY: this.chartService.getArrayByKey(filterData, 'demand'),
            labelX: this.chartLabels.finalProduct,
            labelY: this.chartLabels.demandTables
        });
    }

    setData(chart, data) {
        this.charts[chart] = this.chartService.setChartConfig(
            {
                labels: data.valuesX,
                datasets: [
                    {
                        data: data.valuesY,
                        backgroundColor: 'rgba(0,166,90,.5)',
                        borderWidth: 1,
                        borderColor: 'rgba(0,166,90,1)'
                    }
                ]
            },
            null,
            data.labelX,
            data.labelY
        );
    }

    applyFilters() {
        return this.chartService.applyFilters(this.demandChartData, this.filters);
    }

    getTotalResourceUsed(resourceQty): number {
        let total = 0;
        for (const resource of resourceQty) {
            total += resource.value;
        }
        return total;
    }

    getLargeOptions() {
        return this.demandChartData
            .map(item => item.product.split('x')[2])
            .filter((x, i, a) => x && a.indexOf(x) === i)
            .sort();
    }

    getTypeOptions() {
        return this.demandChartData
            .map(item => item.product.split('x')[0])
            .filter((x, i, a) => x && a.indexOf(x) === i)
            .sort();
    }

    formatResume(col) {
        return isNaN(col) ? col : this.decimalPipe.transform(col, '1.0-2');
    }
}
