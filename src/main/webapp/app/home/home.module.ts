import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';

import { ChartModule } from 'angular2-chartjs';
import { SuiModule } from 'ng2-semantic-ui';

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild([HOME_ROUTE]), ChartModule, SuiModule],
    declarations: [HomeComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabHomeModule {}
