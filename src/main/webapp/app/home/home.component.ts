import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LoginModalService, Principal, Account } from 'app/core';
import { SummaryService } from '../opt/summary/summary.service';

import { ChartService } from 'app/shared';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.css']
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    optSummary: any;
    charts: any = {};
    chartProductionOptions: any;
    chartLabels: any;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private summaryService: SummaryService,
        private chartService: ChartService,
        private translateService: TranslateService
    ) {
        this.setLanguageValues();
        this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            this.setLanguageValues();
            if (this.optSummary) {
                this.setChart1();
                this.setChart2();
            }
        });
    }

    setLanguageValues() {
        this.chartProductionOptions = [
            { value: 'earnings', label: this.translateService.instant('home.earnings') },
            { value: 'resourceCost', label: this.translateService.instant('home.resourceCost') },
            { value: 'operatingCost', label: this.translateService.instant('home.operatingCost') },
            { value: 'benefits', label: this.translateService.instant('home.benefits') }
        ];

        this.chartLabels = {
            xLabel: this.translateService.instant('home.date'),
            yLabel: this.translateService.instant('home.ammount'),
            chartCost: this.translateService.instant('home.chartCost'),
            earnings: this.translateService.instant('home.earnings'),
            operatingCost: this.translateService.instant('home.operatingCost'),
            resourceCost: this.translateService.instant('home.resourceCost'),
            benefits: this.translateService.instant('home.benefits'),
            select: this.translateService.instant('home.select')
        };
    }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.account = account;
            if (this.isAuthenticated()) {
                this.getOptSummary();
            }
        });
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.principal.identity().then(account => {
                this.account = account;
                this.getOptSummary();
            });
        });
    }

    getOptSummary() {
        // Get Optimization summary
        this.summaryService.optimizationSummary().subscribe(
            (optSummary: any) => {
                this.optSummary = optSummary.body;
                if (this.optSummary.dailyProduction) {
                    this.setChart1();
                }
                if (this.optSummary.resourceCosts) {
                    this.setChart2();
                }
            },
            err => {
                console.log(err);
            }
        );
    }

    setChart1() {
        this.charts.chart1 = this.chartService.setChartConfig(
            {
                labels: this.optSummary.dailyProduction[0].valuesX,
                datasets: [
                    {
                        data: this.optSummary.dailyProduction[0].valuesY,
                        backgroundColor: 'rgba(0,166,90,.5)',
                        label: this.chartProductionOptions[0].label
                    },
                    {
                        data: this.optSummary.dailyProduction[1].valuesY,
                        backgroundColor: 'rgba(166,175,24,.5)',
                        label: this.chartProductionOptions[1].label
                    },
                    {
                        data: this.optSummary.dailyProduction[2].valuesY,
                        backgroundColor: 'rgba(243,156,18,.5)',
                        label: this.chartProductionOptions[2].label
                    },
                    {
                        data: this.optSummary.dailyProduction[3].valuesY,
                        backgroundColor: 'rgba(0,192,239,.5)',
                        label: this.chartProductionOptions[3].label
                    }
                ]
            },
            null,
            this.chartLabels.xLabel,
            this.chartLabels.yLabel,
            false,
            true
        );
    }

    setChart2() {
        const colors = ['#f39c12', '#a6af18', '#00a65a', '#00c0ef', '#4b94c0', '#d2d6de', '#39cccc', '#932ab6'];

        this.charts.chart2 = this.chartService.setChartConfig(
            {
                labels: this.optSummary.resourceCosts.labels,
                datasets: [
                    {
                        data: this.optSummary.resourceCosts.values,
                        backgroundColor: colors
                    }
                ]
            },
            this.chartLabels.chartCost,
            null,
            null,
            false,
            true
        );
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
}
