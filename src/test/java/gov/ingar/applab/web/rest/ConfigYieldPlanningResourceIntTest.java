package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.ConfigYieldPlanning;
import gov.ingar.applab.repository.ConfigYieldPlanningRepository;
import gov.ingar.applab.service.ConfigYieldPlanningService;
import gov.ingar.applab.service.dto.ConfigYieldPlanningDTO;
import gov.ingar.applab.service.mapper.ConfigYieldPlanningMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConfigYieldPlanningResource REST controller.
 *
 * @see ConfigYieldPlanningResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class ConfigYieldPlanningResourceIntTest {

    private static final BigDecimal DEFAULT_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE = new BigDecimal(2);

    @Autowired
    private ConfigYieldPlanningRepository configYieldPlanningRepository;


    @Autowired
    private ConfigYieldPlanningMapper configYieldPlanningMapper;
    

    @Autowired
    private ConfigYieldPlanningService configYieldPlanningService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restConfigYieldPlanningMockMvc;

    private ConfigYieldPlanning configYieldPlanning;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConfigYieldPlanningResource configYieldPlanningResource = new ConfigYieldPlanningResource(configYieldPlanningService);
        this.restConfigYieldPlanningMockMvc = MockMvcBuilders.standaloneSetup(configYieldPlanningResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ConfigYieldPlanning createEntity(EntityManager em) {
        ConfigYieldPlanning configYieldPlanning = new ConfigYieldPlanning()
            .value(DEFAULT_VALUE);
        return configYieldPlanning;
    }

    @Before
    public void initTest() {
        configYieldPlanning = createEntity(em);
    }

    @Test
    @Transactional
    public void createConfigYieldPlanning() throws Exception {
        int databaseSizeBeforeCreate = configYieldPlanningRepository.findAll().size();

        // Create the ConfigYieldPlanning
        ConfigYieldPlanningDTO configYieldPlanningDTO = configYieldPlanningMapper.toDto(configYieldPlanning);
        restConfigYieldPlanningMockMvc.perform(post("/api/config-yield-plannings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configYieldPlanningDTO)))
            .andExpect(status().isCreated());

        // Validate the ConfigYieldPlanning in the database
        List<ConfigYieldPlanning> configYieldPlanningList = configYieldPlanningRepository.findAll();
        assertThat(configYieldPlanningList).hasSize(databaseSizeBeforeCreate + 1);
        ConfigYieldPlanning testConfigYieldPlanning = configYieldPlanningList.get(configYieldPlanningList.size() - 1);
        assertThat(testConfigYieldPlanning.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createConfigYieldPlanningWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = configYieldPlanningRepository.findAll().size();

        // Create the ConfigYieldPlanning with an existing ID
        configYieldPlanning.setId(1L);
        ConfigYieldPlanningDTO configYieldPlanningDTO = configYieldPlanningMapper.toDto(configYieldPlanning);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConfigYieldPlanningMockMvc.perform(post("/api/config-yield-plannings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configYieldPlanningDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ConfigYieldPlanning in the database
        List<ConfigYieldPlanning> configYieldPlanningList = configYieldPlanningRepository.findAll();
        assertThat(configYieldPlanningList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = configYieldPlanningRepository.findAll().size();
        // set the field null
        configYieldPlanning.setValue(null);

        // Create the ConfigYieldPlanning, which fails.
        ConfigYieldPlanningDTO configYieldPlanningDTO = configYieldPlanningMapper.toDto(configYieldPlanning);

        restConfigYieldPlanningMockMvc.perform(post("/api/config-yield-plannings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configYieldPlanningDTO)))
            .andExpect(status().isBadRequest());

        List<ConfigYieldPlanning> configYieldPlanningList = configYieldPlanningRepository.findAll();
        assertThat(configYieldPlanningList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllConfigYieldPlannings() throws Exception {
        // Initialize the database
        configYieldPlanningRepository.saveAndFlush(configYieldPlanning);

        // Get all the configYieldPlanningList
        restConfigYieldPlanningMockMvc.perform(get("/api/config-yield-plannings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(configYieldPlanning.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.intValue())));
    }
    

    @Test
    @Transactional
    public void getConfigYieldPlanning() throws Exception {
        // Initialize the database
        configYieldPlanningRepository.saveAndFlush(configYieldPlanning);

        // Get the configYieldPlanning
        restConfigYieldPlanningMockMvc.perform(get("/api/config-yield-plannings/{id}", configYieldPlanning.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(configYieldPlanning.getId().intValue()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingConfigYieldPlanning() throws Exception {
        // Get the configYieldPlanning
        restConfigYieldPlanningMockMvc.perform(get("/api/config-yield-plannings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConfigYieldPlanning() throws Exception {
        // Initialize the database
        configYieldPlanningRepository.saveAndFlush(configYieldPlanning);

        int databaseSizeBeforeUpdate = configYieldPlanningRepository.findAll().size();

        // Update the configYieldPlanning
        ConfigYieldPlanning updatedConfigYieldPlanning = configYieldPlanningRepository.findById(configYieldPlanning.getId()).get();
        // Disconnect from session so that the updates on updatedConfigYieldPlanning are not directly saved in db
        em.detach(updatedConfigYieldPlanning);
        updatedConfigYieldPlanning
            .value(UPDATED_VALUE);
        ConfigYieldPlanningDTO configYieldPlanningDTO = configYieldPlanningMapper.toDto(updatedConfigYieldPlanning);

        restConfigYieldPlanningMockMvc.perform(put("/api/config-yield-plannings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configYieldPlanningDTO)))
            .andExpect(status().isOk());

        // Validate the ConfigYieldPlanning in the database
        List<ConfigYieldPlanning> configYieldPlanningList = configYieldPlanningRepository.findAll();
        assertThat(configYieldPlanningList).hasSize(databaseSizeBeforeUpdate);
        ConfigYieldPlanning testConfigYieldPlanning = configYieldPlanningList.get(configYieldPlanningList.size() - 1);
        assertThat(testConfigYieldPlanning.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingConfigYieldPlanning() throws Exception {
        int databaseSizeBeforeUpdate = configYieldPlanningRepository.findAll().size();

        // Create the ConfigYieldPlanning
        ConfigYieldPlanningDTO configYieldPlanningDTO = configYieldPlanningMapper.toDto(configYieldPlanning);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConfigYieldPlanningMockMvc.perform(put("/api/config-yield-plannings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configYieldPlanningDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ConfigYieldPlanning in the database
        List<ConfigYieldPlanning> configYieldPlanningList = configYieldPlanningRepository.findAll();
        assertThat(configYieldPlanningList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteConfigYieldPlanning() throws Exception {
        // Initialize the database
        configYieldPlanningRepository.saveAndFlush(configYieldPlanning);

        int databaseSizeBeforeDelete = configYieldPlanningRepository.findAll().size();

        // Get the configYieldPlanning
        restConfigYieldPlanningMockMvc.perform(delete("/api/config-yield-plannings/{id}", configYieldPlanning.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ConfigYieldPlanning> configYieldPlanningList = configYieldPlanningRepository.findAll();
        assertThat(configYieldPlanningList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigYieldPlanning.class);
        ConfigYieldPlanning configYieldPlanning1 = new ConfigYieldPlanning();
        configYieldPlanning1.setId(1L);
        ConfigYieldPlanning configYieldPlanning2 = new ConfigYieldPlanning();
        configYieldPlanning2.setId(configYieldPlanning1.getId());
        assertThat(configYieldPlanning1).isEqualTo(configYieldPlanning2);
        configYieldPlanning2.setId(2L);
        assertThat(configYieldPlanning1).isNotEqualTo(configYieldPlanning2);
        configYieldPlanning1.setId(null);
        assertThat(configYieldPlanning1).isNotEqualTo(configYieldPlanning2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigYieldPlanningDTO.class);
        ConfigYieldPlanningDTO configYieldPlanningDTO1 = new ConfigYieldPlanningDTO();
        configYieldPlanningDTO1.setId(1L);
        ConfigYieldPlanningDTO configYieldPlanningDTO2 = new ConfigYieldPlanningDTO();
        assertThat(configYieldPlanningDTO1).isNotEqualTo(configYieldPlanningDTO2);
        configYieldPlanningDTO2.setId(configYieldPlanningDTO1.getId());
        assertThat(configYieldPlanningDTO1).isEqualTo(configYieldPlanningDTO2);
        configYieldPlanningDTO2.setId(2L);
        assertThat(configYieldPlanningDTO1).isNotEqualTo(configYieldPlanningDTO2);
        configYieldPlanningDTO1.setId(null);
        assertThat(configYieldPlanningDTO1).isNotEqualTo(configYieldPlanningDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(configYieldPlanningMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(configYieldPlanningMapper.fromId(null)).isNull();
    }
}
