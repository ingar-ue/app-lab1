package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.KerfConfig;
import gov.ingar.applab.repository.KerfConfigRepository;
import gov.ingar.applab.service.KerfConfigService;
import gov.ingar.applab.service.dto.KerfConfigDTO;
import gov.ingar.applab.service.mapper.KerfConfigMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the KerfConfigResource REST controller.
 *
 * @see KerfConfigResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class KerfConfigResourceIntTest {

    private static final String DEFAULT_PARAM = "AAAAAAAAAA";
    private static final String UPDATED_PARAM = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE = new BigDecimal(2);

    @Autowired
    private KerfConfigRepository kerfConfigRepository;


    @Autowired
    private KerfConfigMapper kerfConfigMapper;
    

    @Autowired
    private KerfConfigService kerfConfigService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restKerfConfigMockMvc;

    private KerfConfig kerfConfig;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KerfConfigResource kerfConfigResource = new KerfConfigResource(kerfConfigService);
        this.restKerfConfigMockMvc = MockMvcBuilders.standaloneSetup(kerfConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KerfConfig createEntity(EntityManager em) {
        KerfConfig kerfConfig = new KerfConfig()
            .param(DEFAULT_PARAM)
            .value(DEFAULT_VALUE);
        return kerfConfig;
    }

    @Before
    public void initTest() {
        kerfConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createKerfConfig() throws Exception {
        int databaseSizeBeforeCreate = kerfConfigRepository.findAll().size();

        // Create the KerfConfig
        KerfConfigDTO kerfConfigDTO = kerfConfigMapper.toDto(kerfConfig);
        restKerfConfigMockMvc.perform(post("/api/kerf-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kerfConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the KerfConfig in the database
        List<KerfConfig> kerfConfigList = kerfConfigRepository.findAll();
        assertThat(kerfConfigList).hasSize(databaseSizeBeforeCreate + 1);
        KerfConfig testKerfConfig = kerfConfigList.get(kerfConfigList.size() - 1);
        assertThat(testKerfConfig.getParam()).isEqualTo(DEFAULT_PARAM);
        assertThat(testKerfConfig.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createKerfConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = kerfConfigRepository.findAll().size();

        // Create the KerfConfig with an existing ID
        kerfConfig.setId(1L);
        KerfConfigDTO kerfConfigDTO = kerfConfigMapper.toDto(kerfConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKerfConfigMockMvc.perform(post("/api/kerf-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kerfConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the KerfConfig in the database
        List<KerfConfig> kerfConfigList = kerfConfigRepository.findAll();
        assertThat(kerfConfigList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkParamIsRequired() throws Exception {
        int databaseSizeBeforeTest = kerfConfigRepository.findAll().size();
        // set the field null
        kerfConfig.setParam(null);

        // Create the KerfConfig, which fails.
        KerfConfigDTO kerfConfigDTO = kerfConfigMapper.toDto(kerfConfig);

        restKerfConfigMockMvc.perform(post("/api/kerf-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kerfConfigDTO)))
            .andExpect(status().isBadRequest());

        List<KerfConfig> kerfConfigList = kerfConfigRepository.findAll();
        assertThat(kerfConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = kerfConfigRepository.findAll().size();
        // set the field null
        kerfConfig.setValue(null);

        // Create the KerfConfig, which fails.
        KerfConfigDTO kerfConfigDTO = kerfConfigMapper.toDto(kerfConfig);

        restKerfConfigMockMvc.perform(post("/api/kerf-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kerfConfigDTO)))
            .andExpect(status().isBadRequest());

        List<KerfConfig> kerfConfigList = kerfConfigRepository.findAll();
        assertThat(kerfConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllKerfConfigs() throws Exception {
        // Initialize the database
        kerfConfigRepository.saveAndFlush(kerfConfig);

        // Get all the kerfConfigList
        restKerfConfigMockMvc.perform(get("/api/kerf-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kerfConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].param").value(hasItem(DEFAULT_PARAM.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.intValue())));
    }
    

    @Test
    @Transactional
    public void getKerfConfig() throws Exception {
        // Initialize the database
        kerfConfigRepository.saveAndFlush(kerfConfig);

        // Get the kerfConfig
        restKerfConfigMockMvc.perform(get("/api/kerf-configs/{id}", kerfConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(kerfConfig.getId().intValue()))
            .andExpect(jsonPath("$.param").value(DEFAULT_PARAM.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingKerfConfig() throws Exception {
        // Get the kerfConfig
        restKerfConfigMockMvc.perform(get("/api/kerf-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKerfConfig() throws Exception {
        // Initialize the database
        kerfConfigRepository.saveAndFlush(kerfConfig);

        int databaseSizeBeforeUpdate = kerfConfigRepository.findAll().size();

        // Update the kerfConfig
        KerfConfig updatedKerfConfig = kerfConfigRepository.findById(kerfConfig.getId()).get();
        // Disconnect from session so that the updates on updatedKerfConfig are not directly saved in db
        em.detach(updatedKerfConfig);
        updatedKerfConfig
            .param(UPDATED_PARAM)
            .value(UPDATED_VALUE);
        KerfConfigDTO kerfConfigDTO = kerfConfigMapper.toDto(updatedKerfConfig);

        restKerfConfigMockMvc.perform(put("/api/kerf-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kerfConfigDTO)))
            .andExpect(status().isOk());

        // Validate the KerfConfig in the database
        List<KerfConfig> kerfConfigList = kerfConfigRepository.findAll();
        assertThat(kerfConfigList).hasSize(databaseSizeBeforeUpdate);
        KerfConfig testKerfConfig = kerfConfigList.get(kerfConfigList.size() - 1);
        assertThat(testKerfConfig.getParam()).isEqualTo(UPDATED_PARAM);
        assertThat(testKerfConfig.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingKerfConfig() throws Exception {
        int databaseSizeBeforeUpdate = kerfConfigRepository.findAll().size();

        // Create the KerfConfig
        KerfConfigDTO kerfConfigDTO = kerfConfigMapper.toDto(kerfConfig);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restKerfConfigMockMvc.perform(put("/api/kerf-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(kerfConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the KerfConfig in the database
        List<KerfConfig> kerfConfigList = kerfConfigRepository.findAll();
        assertThat(kerfConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKerfConfig() throws Exception {
        // Initialize the database
        kerfConfigRepository.saveAndFlush(kerfConfig);

        int databaseSizeBeforeDelete = kerfConfigRepository.findAll().size();

        // Get the kerfConfig
        restKerfConfigMockMvc.perform(delete("/api/kerf-configs/{id}", kerfConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<KerfConfig> kerfConfigList = kerfConfigRepository.findAll();
        assertThat(kerfConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KerfConfig.class);
        KerfConfig kerfConfig1 = new KerfConfig();
        kerfConfig1.setId(1L);
        KerfConfig kerfConfig2 = new KerfConfig();
        kerfConfig2.setId(kerfConfig1.getId());
        assertThat(kerfConfig1).isEqualTo(kerfConfig2);
        kerfConfig2.setId(2L);
        assertThat(kerfConfig1).isNotEqualTo(kerfConfig2);
        kerfConfig1.setId(null);
        assertThat(kerfConfig1).isNotEqualTo(kerfConfig2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KerfConfigDTO.class);
        KerfConfigDTO kerfConfigDTO1 = new KerfConfigDTO();
        kerfConfigDTO1.setId(1L);
        KerfConfigDTO kerfConfigDTO2 = new KerfConfigDTO();
        assertThat(kerfConfigDTO1).isNotEqualTo(kerfConfigDTO2);
        kerfConfigDTO2.setId(kerfConfigDTO1.getId());
        assertThat(kerfConfigDTO1).isEqualTo(kerfConfigDTO2);
        kerfConfigDTO2.setId(2L);
        assertThat(kerfConfigDTO1).isNotEqualTo(kerfConfigDTO2);
        kerfConfigDTO1.setId(null);
        assertThat(kerfConfigDTO1).isNotEqualTo(kerfConfigDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(kerfConfigMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(kerfConfigMapper.fromId(null)).isNull();
    }
}
