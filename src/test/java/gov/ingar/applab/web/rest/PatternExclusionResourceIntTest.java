package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.PatternExclusion;
import gov.ingar.applab.repository.PatternExclusionRepository;
import gov.ingar.applab.service.PatternExclusionService;
import gov.ingar.applab.service.dto.PatternExclusionDTO;
import gov.ingar.applab.service.mapper.PatternExclusionMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PatternExclusionResource REST controller.
 *
 * @see PatternExclusionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class PatternExclusionResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_EXCLUSION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXCLUSION_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PatternExclusionRepository patternExclusionRepository;


    @Autowired
    private PatternExclusionMapper patternExclusionMapper;
    

    @Autowired
    private PatternExclusionService patternExclusionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPatternExclusionMockMvc;

    private PatternExclusion patternExclusion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PatternExclusionResource patternExclusionResource = new PatternExclusionResource(patternExclusionService);
        this.restPatternExclusionMockMvc = MockMvcBuilders.standaloneSetup(patternExclusionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PatternExclusion createEntity(EntityManager em) {
        PatternExclusion patternExclusion = new PatternExclusion()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .exclusionDate(DEFAULT_EXCLUSION_DATE);
        return patternExclusion;
    }

    @Before
    public void initTest() {
        patternExclusion = createEntity(em);
    }

    @Test
    @Transactional
    public void createPatternExclusion() throws Exception {
        int databaseSizeBeforeCreate = patternExclusionRepository.findAll().size();

        // Create the PatternExclusion
        PatternExclusionDTO patternExclusionDTO = patternExclusionMapper.toDto(patternExclusion);
        restPatternExclusionMockMvc.perform(post("/api/pattern-exclusions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(patternExclusionDTO)))
            .andExpect(status().isCreated());

        // Validate the PatternExclusion in the database
        List<PatternExclusion> patternExclusionList = patternExclusionRepository.findAll();
        assertThat(patternExclusionList).hasSize(databaseSizeBeforeCreate + 1);
        PatternExclusion testPatternExclusion = patternExclusionList.get(patternExclusionList.size() - 1);
        assertThat(testPatternExclusion.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPatternExclusion.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPatternExclusion.getExclusionDate()).isEqualTo(DEFAULT_EXCLUSION_DATE);
    }

    @Test
    @Transactional
    public void createPatternExclusionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = patternExclusionRepository.findAll().size();

        // Create the PatternExclusion with an existing ID
        patternExclusion.setId(1L);
        PatternExclusionDTO patternExclusionDTO = patternExclusionMapper.toDto(patternExclusion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPatternExclusionMockMvc.perform(post("/api/pattern-exclusions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(patternExclusionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PatternExclusion in the database
        List<PatternExclusion> patternExclusionList = patternExclusionRepository.findAll();
        assertThat(patternExclusionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = patternExclusionRepository.findAll().size();
        // set the field null
        patternExclusion.setName(null);

        // Create the PatternExclusion, which fails.
        PatternExclusionDTO patternExclusionDTO = patternExclusionMapper.toDto(patternExclusion);

        restPatternExclusionMockMvc.perform(post("/api/pattern-exclusions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(patternExclusionDTO)))
            .andExpect(status().isBadRequest());

        List<PatternExclusion> patternExclusionList = patternExclusionRepository.findAll();
        assertThat(patternExclusionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPatternExclusions() throws Exception {
        // Initialize the database
        patternExclusionRepository.saveAndFlush(patternExclusion);

        // Get all the patternExclusionList
        restPatternExclusionMockMvc.perform(get("/api/pattern-exclusions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(patternExclusion.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].exclusionDate").value(hasItem(DEFAULT_EXCLUSION_DATE.toString())));
    }
    

    @Test
    @Transactional
    public void getPatternExclusion() throws Exception {
        // Initialize the database
        patternExclusionRepository.saveAndFlush(patternExclusion);

        // Get the patternExclusion
        restPatternExclusionMockMvc.perform(get("/api/pattern-exclusions/{id}", patternExclusion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(patternExclusion.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.exclusionDate").value(DEFAULT_EXCLUSION_DATE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingPatternExclusion() throws Exception {
        // Get the patternExclusion
        restPatternExclusionMockMvc.perform(get("/api/pattern-exclusions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePatternExclusion() throws Exception {
        // Initialize the database
        patternExclusionRepository.saveAndFlush(patternExclusion);

        int databaseSizeBeforeUpdate = patternExclusionRepository.findAll().size();

        // Update the patternExclusion
        PatternExclusion updatedPatternExclusion = patternExclusionRepository.findById(patternExclusion.getId()).get();
        // Disconnect from session so that the updates on updatedPatternExclusion are not directly saved in db
        em.detach(updatedPatternExclusion);
        updatedPatternExclusion
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .exclusionDate(UPDATED_EXCLUSION_DATE);
        PatternExclusionDTO patternExclusionDTO = patternExclusionMapper.toDto(updatedPatternExclusion);

        restPatternExclusionMockMvc.perform(put("/api/pattern-exclusions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(patternExclusionDTO)))
            .andExpect(status().isOk());

        // Validate the PatternExclusion in the database
        List<PatternExclusion> patternExclusionList = patternExclusionRepository.findAll();
        assertThat(patternExclusionList).hasSize(databaseSizeBeforeUpdate);
        PatternExclusion testPatternExclusion = patternExclusionList.get(patternExclusionList.size() - 1);
        assertThat(testPatternExclusion.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPatternExclusion.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPatternExclusion.getExclusionDate()).isEqualTo(UPDATED_EXCLUSION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPatternExclusion() throws Exception {
        int databaseSizeBeforeUpdate = patternExclusionRepository.findAll().size();

        // Create the PatternExclusion
        PatternExclusionDTO patternExclusionDTO = patternExclusionMapper.toDto(patternExclusion);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPatternExclusionMockMvc.perform(put("/api/pattern-exclusions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(patternExclusionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PatternExclusion in the database
        List<PatternExclusion> patternExclusionList = patternExclusionRepository.findAll();
        assertThat(patternExclusionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePatternExclusion() throws Exception {
        // Initialize the database
        patternExclusionRepository.saveAndFlush(patternExclusion);

        int databaseSizeBeforeDelete = patternExclusionRepository.findAll().size();

        // Get the patternExclusion
        restPatternExclusionMockMvc.perform(delete("/api/pattern-exclusions/{id}", patternExclusion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PatternExclusion> patternExclusionList = patternExclusionRepository.findAll();
        assertThat(patternExclusionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PatternExclusion.class);
        PatternExclusion patternExclusion1 = new PatternExclusion();
        patternExclusion1.setId(1L);
        PatternExclusion patternExclusion2 = new PatternExclusion();
        patternExclusion2.setId(patternExclusion1.getId());
        assertThat(patternExclusion1).isEqualTo(patternExclusion2);
        patternExclusion2.setId(2L);
        assertThat(patternExclusion1).isNotEqualTo(patternExclusion2);
        patternExclusion1.setId(null);
        assertThat(patternExclusion1).isNotEqualTo(patternExclusion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PatternExclusionDTO.class);
        PatternExclusionDTO patternExclusionDTO1 = new PatternExclusionDTO();
        patternExclusionDTO1.setId(1L);
        PatternExclusionDTO patternExclusionDTO2 = new PatternExclusionDTO();
        assertThat(patternExclusionDTO1).isNotEqualTo(patternExclusionDTO2);
        patternExclusionDTO2.setId(patternExclusionDTO1.getId());
        assertThat(patternExclusionDTO1).isEqualTo(patternExclusionDTO2);
        patternExclusionDTO2.setId(2L);
        assertThat(patternExclusionDTO1).isNotEqualTo(patternExclusionDTO2);
        patternExclusionDTO1.setId(null);
        assertThat(patternExclusionDTO1).isNotEqualTo(patternExclusionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(patternExclusionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(patternExclusionMapper.fromId(null)).isNull();
    }
}
