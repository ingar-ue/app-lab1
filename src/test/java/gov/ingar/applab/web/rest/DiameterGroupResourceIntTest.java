package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.DiameterGroup;
import gov.ingar.applab.repository.DiameterGroupRepository;
import gov.ingar.applab.service.DiameterGroupService;
import gov.ingar.applab.service.dto.DiameterGroupDTO;
import gov.ingar.applab.service.mapper.DiameterGroupMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import gov.ingar.applab.domain.enumeration.Unit;
/**
 * Test class for the DiameterGroupResource REST controller.
 *
 * @see DiameterGroupResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class DiameterGroupResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_MIN_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_MIN_VALUE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_MAX_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_MAX_VALUE = new BigDecimal(2);

    private static final Unit DEFAULT_VAL_TYPE = Unit.CM;
    private static final Unit UPDATED_VAL_TYPE = Unit.MM;

    private static final String DEFAULT_SCHEMA = "AAAAAAAAAA";
    private static final String UPDATED_SCHEMA = "BBBBBBBBBB";

    private static final Integer DEFAULT_TABLES_CENTRAL = 1;
    private static final Integer UPDATED_TABLES_CENTRAL = 2;

    private static final Integer DEFAULT_TABLES_SIDE = 1;
    private static final Integer UPDATED_TABLES_SIDE = 2;

    private static final Integer DEFAULT_TABLES_TOP = 1;
    private static final Integer UPDATED_TABLES_TOP = 2;

    private static final String DEFAULT_LONG_GROUPS = "AAAAAAAAAA";
    private static final String UPDATED_LONG_GROUPS = "BBBBBBBBBB";

    private static final Integer DEFAULT_YIELD_INDEX = 0;
    private static final Integer UPDATED_YIELD_INDEX = 1;

    @Autowired
    private DiameterGroupRepository diameterGroupRepository;


    @Autowired
    private DiameterGroupMapper diameterGroupMapper;
    

    @Autowired
    private DiameterGroupService diameterGroupService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDiameterGroupMockMvc;

    private DiameterGroup diameterGroup;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DiameterGroupResource diameterGroupResource = new DiameterGroupResource(diameterGroupService);
        this.restDiameterGroupMockMvc = MockMvcBuilders.standaloneSetup(diameterGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DiameterGroup createEntity(EntityManager em) {
        DiameterGroup diameterGroup = new DiameterGroup()
            .code(DEFAULT_CODE)
            .minValue(DEFAULT_MIN_VALUE)
            .maxValue(DEFAULT_MAX_VALUE)
            .valType(DEFAULT_VAL_TYPE)
            .schema(DEFAULT_SCHEMA)
            .tablesCentral(DEFAULT_TABLES_CENTRAL)
            .tablesSide(DEFAULT_TABLES_SIDE)
            .tablesTop(DEFAULT_TABLES_TOP)
            .longGroups(DEFAULT_LONG_GROUPS)
            .yieldIndex(DEFAULT_YIELD_INDEX);
        return diameterGroup;
    }

    @Before
    public void initTest() {
        diameterGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createDiameterGroup() throws Exception {
        int databaseSizeBeforeCreate = diameterGroupRepository.findAll().size();

        // Create the DiameterGroup
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);
        restDiameterGroupMockMvc.perform(post("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isCreated());

        // Validate the DiameterGroup in the database
        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeCreate + 1);
        DiameterGroup testDiameterGroup = diameterGroupList.get(diameterGroupList.size() - 1);
        assertThat(testDiameterGroup.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testDiameterGroup.getMinValue()).isEqualTo(DEFAULT_MIN_VALUE);
        assertThat(testDiameterGroup.getMaxValue()).isEqualTo(DEFAULT_MAX_VALUE);
        assertThat(testDiameterGroup.getValType()).isEqualTo(DEFAULT_VAL_TYPE);
        assertThat(testDiameterGroup.getSchema()).isEqualTo(DEFAULT_SCHEMA);
        assertThat(testDiameterGroup.getTablesCentral()).isEqualTo(DEFAULT_TABLES_CENTRAL);
        assertThat(testDiameterGroup.getTablesSide()).isEqualTo(DEFAULT_TABLES_SIDE);
        assertThat(testDiameterGroup.getTablesTop()).isEqualTo(DEFAULT_TABLES_TOP);
        assertThat(testDiameterGroup.getLongGroups()).isEqualTo(DEFAULT_LONG_GROUPS);
        assertThat(testDiameterGroup.getYieldIndex()).isEqualTo(DEFAULT_YIELD_INDEX);
    }

    @Test
    @Transactional
    public void createDiameterGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = diameterGroupRepository.findAll().size();

        // Create the DiameterGroup with an existing ID
        diameterGroup.setId(1L);
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDiameterGroupMockMvc.perform(post("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DiameterGroup in the database
        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = diameterGroupRepository.findAll().size();
        // set the field null
        diameterGroup.setCode(null);

        // Create the DiameterGroup, which fails.
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);

        restDiameterGroupMockMvc.perform(post("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isBadRequest());

        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMinValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = diameterGroupRepository.findAll().size();
        // set the field null
        diameterGroup.setMinValue(null);

        // Create the DiameterGroup, which fails.
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);

        restDiameterGroupMockMvc.perform(post("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isBadRequest());

        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = diameterGroupRepository.findAll().size();
        // set the field null
        diameterGroup.setMaxValue(null);

        // Create the DiameterGroup, which fails.
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);

        restDiameterGroupMockMvc.perform(post("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isBadRequest());

        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = diameterGroupRepository.findAll().size();
        // set the field null
        diameterGroup.setValType(null);

        // Create the DiameterGroup, which fails.
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);

        restDiameterGroupMockMvc.perform(post("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isBadRequest());

        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSchemaIsRequired() throws Exception {
        int databaseSizeBeforeTest = diameterGroupRepository.findAll().size();
        // set the field null
        diameterGroup.setSchema(null);

        // Create the DiameterGroup, which fails.
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);

        restDiameterGroupMockMvc.perform(post("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isBadRequest());

        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTablesCentralIsRequired() throws Exception {
        int databaseSizeBeforeTest = diameterGroupRepository.findAll().size();
        // set the field null
        diameterGroup.setTablesCentral(null);

        // Create the DiameterGroup, which fails.
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);

        restDiameterGroupMockMvc.perform(post("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isBadRequest());

        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYieldIndexIsRequired() throws Exception {
        int databaseSizeBeforeTest = diameterGroupRepository.findAll().size();
        // set the field null
        diameterGroup.setYieldIndex(null);

        // Create the DiameterGroup, which fails.
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);

        restDiameterGroupMockMvc.perform(post("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isBadRequest());

        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDiameterGroups() throws Exception {
        // Initialize the database
        diameterGroupRepository.saveAndFlush(diameterGroup);

        // Get all the diameterGroupList
        restDiameterGroupMockMvc.perform(get("/api/diameter-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(diameterGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].minValue").value(hasItem(DEFAULT_MIN_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].maxValue").value(hasItem(DEFAULT_MAX_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].valType").value(hasItem(DEFAULT_VAL_TYPE.toString())))
            .andExpect(jsonPath("$.[*].schema").value(hasItem(DEFAULT_SCHEMA.toString())))
            .andExpect(jsonPath("$.[*].tablesCentral").value(hasItem(DEFAULT_TABLES_CENTRAL)))
            .andExpect(jsonPath("$.[*].tablesSide").value(hasItem(DEFAULT_TABLES_SIDE)))
            .andExpect(jsonPath("$.[*].tablesTop").value(hasItem(DEFAULT_TABLES_TOP)))
            .andExpect(jsonPath("$.[*].longGroups").value(hasItem(DEFAULT_LONG_GROUPS.toString())))
            .andExpect(jsonPath("$.[*].yieldIndex").value(hasItem(DEFAULT_YIELD_INDEX)));
    }
    

    @Test
    @Transactional
    public void getDiameterGroup() throws Exception {
        // Initialize the database
        diameterGroupRepository.saveAndFlush(diameterGroup);

        // Get the diameterGroup
        restDiameterGroupMockMvc.perform(get("/api/diameter-groups/{id}", diameterGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(diameterGroup.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.minValue").value(DEFAULT_MIN_VALUE.intValue()))
            .andExpect(jsonPath("$.maxValue").value(DEFAULT_MAX_VALUE.intValue()))
            .andExpect(jsonPath("$.valType").value(DEFAULT_VAL_TYPE.toString()))
            .andExpect(jsonPath("$.schema").value(DEFAULT_SCHEMA.toString()))
            .andExpect(jsonPath("$.tablesCentral").value(DEFAULT_TABLES_CENTRAL))
            .andExpect(jsonPath("$.tablesSide").value(DEFAULT_TABLES_SIDE))
            .andExpect(jsonPath("$.tablesTop").value(DEFAULT_TABLES_TOP))
            .andExpect(jsonPath("$.longGroups").value(DEFAULT_LONG_GROUPS.toString()))
            .andExpect(jsonPath("$.yieldIndex").value(DEFAULT_YIELD_INDEX));
    }
    @Test
    @Transactional
    public void getNonExistingDiameterGroup() throws Exception {
        // Get the diameterGroup
        restDiameterGroupMockMvc.perform(get("/api/diameter-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDiameterGroup() throws Exception {
        // Initialize the database
        diameterGroupRepository.saveAndFlush(diameterGroup);

        int databaseSizeBeforeUpdate = diameterGroupRepository.findAll().size();

        // Update the diameterGroup
        DiameterGroup updatedDiameterGroup = diameterGroupRepository.findById(diameterGroup.getId()).get();
        // Disconnect from session so that the updates on updatedDiameterGroup are not directly saved in db
        em.detach(updatedDiameterGroup);
        updatedDiameterGroup
            .code(UPDATED_CODE)
            .minValue(UPDATED_MIN_VALUE)
            .maxValue(UPDATED_MAX_VALUE)
            .valType(UPDATED_VAL_TYPE)
            .schema(UPDATED_SCHEMA)
            .tablesCentral(UPDATED_TABLES_CENTRAL)
            .tablesSide(UPDATED_TABLES_SIDE)
            .tablesTop(UPDATED_TABLES_TOP)
            .longGroups(UPDATED_LONG_GROUPS)
            .yieldIndex(UPDATED_YIELD_INDEX);
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(updatedDiameterGroup);

        restDiameterGroupMockMvc.perform(put("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isOk());

        // Validate the DiameterGroup in the database
        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeUpdate);
        DiameterGroup testDiameterGroup = diameterGroupList.get(diameterGroupList.size() - 1);
        assertThat(testDiameterGroup.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testDiameterGroup.getMinValue()).isEqualTo(UPDATED_MIN_VALUE);
        assertThat(testDiameterGroup.getMaxValue()).isEqualTo(UPDATED_MAX_VALUE);
        assertThat(testDiameterGroup.getValType()).isEqualTo(UPDATED_VAL_TYPE);
        assertThat(testDiameterGroup.getSchema()).isEqualTo(UPDATED_SCHEMA);
        assertThat(testDiameterGroup.getTablesCentral()).isEqualTo(UPDATED_TABLES_CENTRAL);
        assertThat(testDiameterGroup.getTablesSide()).isEqualTo(UPDATED_TABLES_SIDE);
        assertThat(testDiameterGroup.getTablesTop()).isEqualTo(UPDATED_TABLES_TOP);
        assertThat(testDiameterGroup.getLongGroups()).isEqualTo(UPDATED_LONG_GROUPS);
        assertThat(testDiameterGroup.getYieldIndex()).isEqualTo(UPDATED_YIELD_INDEX);
    }

    @Test
    @Transactional
    public void updateNonExistingDiameterGroup() throws Exception {
        int databaseSizeBeforeUpdate = diameterGroupRepository.findAll().size();

        // Create the DiameterGroup
        DiameterGroupDTO diameterGroupDTO = diameterGroupMapper.toDto(diameterGroup);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDiameterGroupMockMvc.perform(put("/api/diameter-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diameterGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DiameterGroup in the database
        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDiameterGroup() throws Exception {
        // Initialize the database
        diameterGroupRepository.saveAndFlush(diameterGroup);

        int databaseSizeBeforeDelete = diameterGroupRepository.findAll().size();

        // Get the diameterGroup
        restDiameterGroupMockMvc.perform(delete("/api/diameter-groups/{id}", diameterGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DiameterGroup> diameterGroupList = diameterGroupRepository.findAll();
        assertThat(diameterGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DiameterGroup.class);
        DiameterGroup diameterGroup1 = new DiameterGroup();
        diameterGroup1.setId(1L);
        DiameterGroup diameterGroup2 = new DiameterGroup();
        diameterGroup2.setId(diameterGroup1.getId());
        assertThat(diameterGroup1).isEqualTo(diameterGroup2);
        diameterGroup2.setId(2L);
        assertThat(diameterGroup1).isNotEqualTo(diameterGroup2);
        diameterGroup1.setId(null);
        assertThat(diameterGroup1).isNotEqualTo(diameterGroup2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DiameterGroupDTO.class);
        DiameterGroupDTO diameterGroupDTO1 = new DiameterGroupDTO();
        diameterGroupDTO1.setId(1L);
        DiameterGroupDTO diameterGroupDTO2 = new DiameterGroupDTO();
        assertThat(diameterGroupDTO1).isNotEqualTo(diameterGroupDTO2);
        diameterGroupDTO2.setId(diameterGroupDTO1.getId());
        assertThat(diameterGroupDTO1).isEqualTo(diameterGroupDTO2);
        diameterGroupDTO2.setId(2L);
        assertThat(diameterGroupDTO1).isNotEqualTo(diameterGroupDTO2);
        diameterGroupDTO1.setId(null);
        assertThat(diameterGroupDTO1).isNotEqualTo(diameterGroupDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(diameterGroupMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(diameterGroupMapper.fromId(null)).isNull();
    }
}
