package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.LongGroup;
import gov.ingar.applab.repository.LongGroupRepository;
import gov.ingar.applab.service.LongGroupService;
import gov.ingar.applab.service.dto.LongGroupDTO;
import gov.ingar.applab.service.mapper.LongGroupMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import gov.ingar.applab.domain.enumeration.Unit;
/**
 * Test class for the LongGroupResource REST controller.
 *
 * @see LongGroupResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class LongGroupResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE = new BigDecimal(2);

    private static final Unit DEFAULT_VAL_TYPE = Unit.CM;
    private static final Unit UPDATED_VAL_TYPE = Unit.MM;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private LongGroupRepository longGroupRepository;


    @Autowired
    private LongGroupMapper longGroupMapper;
    

    @Autowired
    private LongGroupService longGroupService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLongGroupMockMvc;

    private LongGroup longGroup;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LongGroupResource longGroupResource = new LongGroupResource(longGroupService);
        this.restLongGroupMockMvc = MockMvcBuilders.standaloneSetup(longGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LongGroup createEntity(EntityManager em) {
        LongGroup longGroup = new LongGroup()
            .code(DEFAULT_CODE)
            .value(DEFAULT_VALUE)
            .valType(DEFAULT_VAL_TYPE)
            .description(DEFAULT_DESCRIPTION);
        return longGroup;
    }

    @Before
    public void initTest() {
        longGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createLongGroup() throws Exception {
        int databaseSizeBeforeCreate = longGroupRepository.findAll().size();

        // Create the LongGroup
        LongGroupDTO longGroupDTO = longGroupMapper.toDto(longGroup);
        restLongGroupMockMvc.perform(post("/api/long-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(longGroupDTO)))
            .andExpect(status().isCreated());

        // Validate the LongGroup in the database
        List<LongGroup> longGroupList = longGroupRepository.findAll();
        assertThat(longGroupList).hasSize(databaseSizeBeforeCreate + 1);
        LongGroup testLongGroup = longGroupList.get(longGroupList.size() - 1);
        assertThat(testLongGroup.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testLongGroup.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testLongGroup.getValType()).isEqualTo(DEFAULT_VAL_TYPE);
        assertThat(testLongGroup.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createLongGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = longGroupRepository.findAll().size();

        // Create the LongGroup with an existing ID
        longGroup.setId(1L);
        LongGroupDTO longGroupDTO = longGroupMapper.toDto(longGroup);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLongGroupMockMvc.perform(post("/api/long-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(longGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LongGroup in the database
        List<LongGroup> longGroupList = longGroupRepository.findAll();
        assertThat(longGroupList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = longGroupRepository.findAll().size();
        // set the field null
        longGroup.setCode(null);

        // Create the LongGroup, which fails.
        LongGroupDTO longGroupDTO = longGroupMapper.toDto(longGroup);

        restLongGroupMockMvc.perform(post("/api/long-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(longGroupDTO)))
            .andExpect(status().isBadRequest());

        List<LongGroup> longGroupList = longGroupRepository.findAll();
        assertThat(longGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = longGroupRepository.findAll().size();
        // set the field null
        longGroup.setValue(null);

        // Create the LongGroup, which fails.
        LongGroupDTO longGroupDTO = longGroupMapper.toDto(longGroup);

        restLongGroupMockMvc.perform(post("/api/long-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(longGroupDTO)))
            .andExpect(status().isBadRequest());

        List<LongGroup> longGroupList = longGroupRepository.findAll();
        assertThat(longGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = longGroupRepository.findAll().size();
        // set the field null
        longGroup.setValType(null);

        // Create the LongGroup, which fails.
        LongGroupDTO longGroupDTO = longGroupMapper.toDto(longGroup);

        restLongGroupMockMvc.perform(post("/api/long-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(longGroupDTO)))
            .andExpect(status().isBadRequest());

        List<LongGroup> longGroupList = longGroupRepository.findAll();
        assertThat(longGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLongGroups() throws Exception {
        // Initialize the database
        longGroupRepository.saveAndFlush(longGroup);

        // Get all the longGroupList
        restLongGroupMockMvc.perform(get("/api/long-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(longGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].valType").value(hasItem(DEFAULT_VAL_TYPE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    

    @Test
    @Transactional
    public void getLongGroup() throws Exception {
        // Initialize the database
        longGroupRepository.saveAndFlush(longGroup);

        // Get the longGroup
        restLongGroupMockMvc.perform(get("/api/long-groups/{id}", longGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(longGroup.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.intValue()))
            .andExpect(jsonPath("$.valType").value(DEFAULT_VAL_TYPE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingLongGroup() throws Exception {
        // Get the longGroup
        restLongGroupMockMvc.perform(get("/api/long-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLongGroup() throws Exception {
        // Initialize the database
        longGroupRepository.saveAndFlush(longGroup);

        int databaseSizeBeforeUpdate = longGroupRepository.findAll().size();

        // Update the longGroup
        LongGroup updatedLongGroup = longGroupRepository.findById(longGroup.getId()).get();
        // Disconnect from session so that the updates on updatedLongGroup are not directly saved in db
        em.detach(updatedLongGroup);
        updatedLongGroup
            .code(UPDATED_CODE)
            .value(UPDATED_VALUE)
            .valType(UPDATED_VAL_TYPE)
            .description(UPDATED_DESCRIPTION);
        LongGroupDTO longGroupDTO = longGroupMapper.toDto(updatedLongGroup);

        restLongGroupMockMvc.perform(put("/api/long-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(longGroupDTO)))
            .andExpect(status().isOk());

        // Validate the LongGroup in the database
        List<LongGroup> longGroupList = longGroupRepository.findAll();
        assertThat(longGroupList).hasSize(databaseSizeBeforeUpdate);
        LongGroup testLongGroup = longGroupList.get(longGroupList.size() - 1);
        assertThat(testLongGroup.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testLongGroup.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testLongGroup.getValType()).isEqualTo(UPDATED_VAL_TYPE);
        assertThat(testLongGroup.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingLongGroup() throws Exception {
        int databaseSizeBeforeUpdate = longGroupRepository.findAll().size();

        // Create the LongGroup
        LongGroupDTO longGroupDTO = longGroupMapper.toDto(longGroup);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLongGroupMockMvc.perform(put("/api/long-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(longGroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LongGroup in the database
        List<LongGroup> longGroupList = longGroupRepository.findAll();
        assertThat(longGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLongGroup() throws Exception {
        // Initialize the database
        longGroupRepository.saveAndFlush(longGroup);

        int databaseSizeBeforeDelete = longGroupRepository.findAll().size();

        // Get the longGroup
        restLongGroupMockMvc.perform(delete("/api/long-groups/{id}", longGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LongGroup> longGroupList = longGroupRepository.findAll();
        assertThat(longGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LongGroup.class);
        LongGroup longGroup1 = new LongGroup();
        longGroup1.setId(1L);
        LongGroup longGroup2 = new LongGroup();
        longGroup2.setId(longGroup1.getId());
        assertThat(longGroup1).isEqualTo(longGroup2);
        longGroup2.setId(2L);
        assertThat(longGroup1).isNotEqualTo(longGroup2);
        longGroup1.setId(null);
        assertThat(longGroup1).isNotEqualTo(longGroup2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LongGroupDTO.class);
        LongGroupDTO longGroupDTO1 = new LongGroupDTO();
        longGroupDTO1.setId(1L);
        LongGroupDTO longGroupDTO2 = new LongGroupDTO();
        assertThat(longGroupDTO1).isNotEqualTo(longGroupDTO2);
        longGroupDTO2.setId(longGroupDTO1.getId());
        assertThat(longGroupDTO1).isEqualTo(longGroupDTO2);
        longGroupDTO2.setId(2L);
        assertThat(longGroupDTO1).isNotEqualTo(longGroupDTO2);
        longGroupDTO1.setId(null);
        assertThat(longGroupDTO1).isNotEqualTo(longGroupDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(longGroupMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(longGroupMapper.fromId(null)).isNull();
    }
}
