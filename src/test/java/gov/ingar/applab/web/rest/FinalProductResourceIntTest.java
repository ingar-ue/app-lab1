package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.FinalProduct;
import gov.ingar.applab.repository.FinalProductRepository;
import gov.ingar.applab.service.FinalProductService;
import gov.ingar.applab.service.dto.FinalProductDTO;
import gov.ingar.applab.service.mapper.FinalProductMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FinalProductResource REST controller.
 *
 * @see FinalProductResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class FinalProductResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_MIN_DEMAND = 0;
    private static final Integer UPDATED_MIN_DEMAND = 1;

    private static final Integer DEFAULT_MAX_DEMAND = 0;
    private static final Integer UPDATED_MAX_DEMAND = 1;

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(1);

    @Autowired
    private FinalProductRepository finalProductRepository;


    @Autowired
    private FinalProductMapper finalProductMapper;
    

    @Autowired
    private FinalProductService finalProductService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFinalProductMockMvc;

    private FinalProduct finalProduct;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FinalProductResource finalProductResource = new FinalProductResource(finalProductService);
        this.restFinalProductMockMvc = MockMvcBuilders.standaloneSetup(finalProductResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FinalProduct createEntity(EntityManager em) {
        FinalProduct finalProduct = new FinalProduct()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .minDemand(DEFAULT_MIN_DEMAND)
            .maxDemand(DEFAULT_MAX_DEMAND)
            .price(DEFAULT_PRICE);
        return finalProduct;
    }

    @Before
    public void initTest() {
        finalProduct = createEntity(em);
    }

    @Test
    @Transactional
    public void createFinalProduct() throws Exception {
        int databaseSizeBeforeCreate = finalProductRepository.findAll().size();

        // Create the FinalProduct
        FinalProductDTO finalProductDTO = finalProductMapper.toDto(finalProduct);
        restFinalProductMockMvc.perform(post("/api/final-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(finalProductDTO)))
            .andExpect(status().isCreated());

        // Validate the FinalProduct in the database
        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeCreate + 1);
        FinalProduct testFinalProduct = finalProductList.get(finalProductList.size() - 1);
        assertThat(testFinalProduct.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testFinalProduct.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFinalProduct.getMinDemand()).isEqualTo(DEFAULT_MIN_DEMAND);
        assertThat(testFinalProduct.getMaxDemand()).isEqualTo(DEFAULT_MAX_DEMAND);
        assertThat(testFinalProduct.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    public void createFinalProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = finalProductRepository.findAll().size();

        // Create the FinalProduct with an existing ID
        finalProduct.setId(1L);
        FinalProductDTO finalProductDTO = finalProductMapper.toDto(finalProduct);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFinalProductMockMvc.perform(post("/api/final-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(finalProductDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FinalProduct in the database
        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = finalProductRepository.findAll().size();
        // set the field null
        finalProduct.setCode(null);

        // Create the FinalProduct, which fails.
        FinalProductDTO finalProductDTO = finalProductMapper.toDto(finalProduct);

        restFinalProductMockMvc.perform(post("/api/final-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(finalProductDTO)))
            .andExpect(status().isBadRequest());

        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = finalProductRepository.findAll().size();
        // set the field null
        finalProduct.setName(null);

        // Create the FinalProduct, which fails.
        FinalProductDTO finalProductDTO = finalProductMapper.toDto(finalProduct);

        restFinalProductMockMvc.perform(post("/api/final-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(finalProductDTO)))
            .andExpect(status().isBadRequest());

        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMinDemandIsRequired() throws Exception {
        int databaseSizeBeforeTest = finalProductRepository.findAll().size();
        // set the field null
        finalProduct.setMinDemand(null);

        // Create the FinalProduct, which fails.
        FinalProductDTO finalProductDTO = finalProductMapper.toDto(finalProduct);

        restFinalProductMockMvc.perform(post("/api/final-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(finalProductDTO)))
            .andExpect(status().isBadRequest());

        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxDemandIsRequired() throws Exception {
        int databaseSizeBeforeTest = finalProductRepository.findAll().size();
        // set the field null
        finalProduct.setMaxDemand(null);

        // Create the FinalProduct, which fails.
        FinalProductDTO finalProductDTO = finalProductMapper.toDto(finalProduct);

        restFinalProductMockMvc.perform(post("/api/final-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(finalProductDTO)))
            .andExpect(status().isBadRequest());

        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = finalProductRepository.findAll().size();
        // set the field null
        finalProduct.setPrice(null);

        // Create the FinalProduct, which fails.
        FinalProductDTO finalProductDTO = finalProductMapper.toDto(finalProduct);

        restFinalProductMockMvc.perform(post("/api/final-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(finalProductDTO)))
            .andExpect(status().isBadRequest());

        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFinalProducts() throws Exception {
        // Initialize the database
        finalProductRepository.saveAndFlush(finalProduct);

        // Get all the finalProductList
        restFinalProductMockMvc.perform(get("/api/final-products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(finalProduct.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].minDemand").value(hasItem(DEFAULT_MIN_DEMAND)))
            .andExpect(jsonPath("$.[*].maxDemand").value(hasItem(DEFAULT_MAX_DEMAND)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())));
    }
    

    @Test
    @Transactional
    public void getFinalProduct() throws Exception {
        // Initialize the database
        finalProductRepository.saveAndFlush(finalProduct);

        // Get the finalProduct
        restFinalProductMockMvc.perform(get("/api/final-products/{id}", finalProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(finalProduct.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.minDemand").value(DEFAULT_MIN_DEMAND))
            .andExpect(jsonPath("$.maxDemand").value(DEFAULT_MAX_DEMAND))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFinalProduct() throws Exception {
        // Get the finalProduct
        restFinalProductMockMvc.perform(get("/api/final-products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFinalProduct() throws Exception {
        // Initialize the database
        finalProductRepository.saveAndFlush(finalProduct);

        int databaseSizeBeforeUpdate = finalProductRepository.findAll().size();

        // Update the finalProduct
        FinalProduct updatedFinalProduct = finalProductRepository.findById(finalProduct.getId()).get();
        // Disconnect from session so that the updates on updatedFinalProduct are not directly saved in db
        em.detach(updatedFinalProduct);
        updatedFinalProduct
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .minDemand(UPDATED_MIN_DEMAND)
            .maxDemand(UPDATED_MAX_DEMAND)
            .price(UPDATED_PRICE);
        FinalProductDTO finalProductDTO = finalProductMapper.toDto(updatedFinalProduct);

        restFinalProductMockMvc.perform(put("/api/final-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(finalProductDTO)))
            .andExpect(status().isOk());

        // Validate the FinalProduct in the database
        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeUpdate);
        FinalProduct testFinalProduct = finalProductList.get(finalProductList.size() - 1);
        assertThat(testFinalProduct.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testFinalProduct.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFinalProduct.getMinDemand()).isEqualTo(UPDATED_MIN_DEMAND);
        assertThat(testFinalProduct.getMaxDemand()).isEqualTo(UPDATED_MAX_DEMAND);
        assertThat(testFinalProduct.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void updateNonExistingFinalProduct() throws Exception {
        int databaseSizeBeforeUpdate = finalProductRepository.findAll().size();

        // Create the FinalProduct
        FinalProductDTO finalProductDTO = finalProductMapper.toDto(finalProduct);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFinalProductMockMvc.perform(put("/api/final-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(finalProductDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FinalProduct in the database
        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFinalProduct() throws Exception {
        // Initialize the database
        finalProductRepository.saveAndFlush(finalProduct);

        int databaseSizeBeforeDelete = finalProductRepository.findAll().size();

        // Get the finalProduct
        restFinalProductMockMvc.perform(delete("/api/final-products/{id}", finalProduct.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FinalProduct> finalProductList = finalProductRepository.findAll();
        assertThat(finalProductList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FinalProduct.class);
        FinalProduct finalProduct1 = new FinalProduct();
        finalProduct1.setId(1L);
        FinalProduct finalProduct2 = new FinalProduct();
        finalProduct2.setId(finalProduct1.getId());
        assertThat(finalProduct1).isEqualTo(finalProduct2);
        finalProduct2.setId(2L);
        assertThat(finalProduct1).isNotEqualTo(finalProduct2);
        finalProduct1.setId(null);
        assertThat(finalProduct1).isNotEqualTo(finalProduct2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FinalProductDTO.class);
        FinalProductDTO finalProductDTO1 = new FinalProductDTO();
        finalProductDTO1.setId(1L);
        FinalProductDTO finalProductDTO2 = new FinalProductDTO();
        assertThat(finalProductDTO1).isNotEqualTo(finalProductDTO2);
        finalProductDTO2.setId(finalProductDTO1.getId());
        assertThat(finalProductDTO1).isEqualTo(finalProductDTO2);
        finalProductDTO2.setId(2L);
        assertThat(finalProductDTO1).isNotEqualTo(finalProductDTO2);
        finalProductDTO1.setId(null);
        assertThat(finalProductDTO1).isNotEqualTo(finalProductDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(finalProductMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(finalProductMapper.fromId(null)).isNull();
    }
}
