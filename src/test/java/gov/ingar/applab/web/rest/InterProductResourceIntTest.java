package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.InterProduct;
import gov.ingar.applab.repository.InterProductRepository;
import gov.ingar.applab.service.InterProductService;
import gov.ingar.applab.service.dto.InterProductDTO;
import gov.ingar.applab.service.mapper.InterProductMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import gov.ingar.applab.domain.enumeration.Unit;
/**
 * Test class for the InterProductResource REST controller.
 *
 * @see InterProductResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class InterProductResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TICKNESS = new BigDecimal(0);
    private static final BigDecimal UPDATED_TICKNESS = new BigDecimal(1);

    private static final BigDecimal DEFAULT_WIDTH = new BigDecimal(0);
    private static final BigDecimal UPDATED_WIDTH = new BigDecimal(1);

    private static final Unit DEFAULT_VAL_TYPE = Unit.CM;
    private static final Unit UPDATED_VAL_TYPE = Unit.MM;

    private static final String DEFAULT_LONG_GROUPS = "AAAAAAAAAA";
    private static final String UPDATED_LONG_GROUPS = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private InterProductRepository interProductRepository;


    @Autowired
    private InterProductMapper interProductMapper;
    

    @Autowired
    private InterProductService interProductService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInterProductMockMvc;

    private InterProduct interProduct;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InterProductResource interProductResource = new InterProductResource(interProductService);
        this.restInterProductMockMvc = MockMvcBuilders.standaloneSetup(interProductResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InterProduct createEntity(EntityManager em) {
        InterProduct interProduct = new InterProduct()
            .code(DEFAULT_CODE)
            .tickness(DEFAULT_TICKNESS)
            .width(DEFAULT_WIDTH)
            .valType(DEFAULT_VAL_TYPE)
            .longGroups(DEFAULT_LONG_GROUPS)
            .description(DEFAULT_DESCRIPTION);
        return interProduct;
    }

    @Before
    public void initTest() {
        interProduct = createEntity(em);
    }

    @Test
    @Transactional
    public void createInterProduct() throws Exception {
        int databaseSizeBeforeCreate = interProductRepository.findAll().size();

        // Create the InterProduct
        InterProductDTO interProductDTO = interProductMapper.toDto(interProduct);
        restInterProductMockMvc.perform(post("/api/inter-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interProductDTO)))
            .andExpect(status().isCreated());

        // Validate the InterProduct in the database
        List<InterProduct> interProductList = interProductRepository.findAll();
        assertThat(interProductList).hasSize(databaseSizeBeforeCreate + 1);
        InterProduct testInterProduct = interProductList.get(interProductList.size() - 1);
        assertThat(testInterProduct.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testInterProduct.getTickness()).isEqualTo(DEFAULT_TICKNESS);
        assertThat(testInterProduct.getWidth()).isEqualTo(DEFAULT_WIDTH);
        assertThat(testInterProduct.getValType()).isEqualTo(DEFAULT_VAL_TYPE);
        assertThat(testInterProduct.getLongGroups()).isEqualTo(DEFAULT_LONG_GROUPS);
        assertThat(testInterProduct.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createInterProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = interProductRepository.findAll().size();

        // Create the InterProduct with an existing ID
        interProduct.setId(1L);
        InterProductDTO interProductDTO = interProductMapper.toDto(interProduct);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInterProductMockMvc.perform(post("/api/inter-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interProductDTO)))
            .andExpect(status().isBadRequest());

        // Validate the InterProduct in the database
        List<InterProduct> interProductList = interProductRepository.findAll();
        assertThat(interProductList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = interProductRepository.findAll().size();
        // set the field null
        interProduct.setCode(null);

        // Create the InterProduct, which fails.
        InterProductDTO interProductDTO = interProductMapper.toDto(interProduct);

        restInterProductMockMvc.perform(post("/api/inter-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interProductDTO)))
            .andExpect(status().isBadRequest());

        List<InterProduct> interProductList = interProductRepository.findAll();
        assertThat(interProductList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTicknessIsRequired() throws Exception {
        int databaseSizeBeforeTest = interProductRepository.findAll().size();
        // set the field null
        interProduct.setTickness(null);

        // Create the InterProduct, which fails.
        InterProductDTO interProductDTO = interProductMapper.toDto(interProduct);

        restInterProductMockMvc.perform(post("/api/inter-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interProductDTO)))
            .andExpect(status().isBadRequest());

        List<InterProduct> interProductList = interProductRepository.findAll();
        assertThat(interProductList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkWidthIsRequired() throws Exception {
        int databaseSizeBeforeTest = interProductRepository.findAll().size();
        // set the field null
        interProduct.setWidth(null);

        // Create the InterProduct, which fails.
        InterProductDTO interProductDTO = interProductMapper.toDto(interProduct);

        restInterProductMockMvc.perform(post("/api/inter-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interProductDTO)))
            .andExpect(status().isBadRequest());

        List<InterProduct> interProductList = interProductRepository.findAll();
        assertThat(interProductList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = interProductRepository.findAll().size();
        // set the field null
        interProduct.setValType(null);

        // Create the InterProduct, which fails.
        InterProductDTO interProductDTO = interProductMapper.toDto(interProduct);

        restInterProductMockMvc.perform(post("/api/inter-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interProductDTO)))
            .andExpect(status().isBadRequest());

        List<InterProduct> interProductList = interProductRepository.findAll();
        assertThat(interProductList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInterProducts() throws Exception {
        // Initialize the database
        interProductRepository.saveAndFlush(interProduct);

        // Get all the interProductList
        restInterProductMockMvc.perform(get("/api/inter-products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(interProduct.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].tickness").value(hasItem(DEFAULT_TICKNESS.intValue())))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.intValue())))
            .andExpect(jsonPath("$.[*].valType").value(hasItem(DEFAULT_VAL_TYPE.toString())))
            .andExpect(jsonPath("$.[*].longGroups").value(hasItem(DEFAULT_LONG_GROUPS.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    

    @Test
    @Transactional
    public void getInterProduct() throws Exception {
        // Initialize the database
        interProductRepository.saveAndFlush(interProduct);

        // Get the interProduct
        restInterProductMockMvc.perform(get("/api/inter-products/{id}", interProduct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(interProduct.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.tickness").value(DEFAULT_TICKNESS.intValue()))
            .andExpect(jsonPath("$.width").value(DEFAULT_WIDTH.intValue()))
            .andExpect(jsonPath("$.valType").value(DEFAULT_VAL_TYPE.toString()))
            .andExpect(jsonPath("$.longGroups").value(DEFAULT_LONG_GROUPS.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingInterProduct() throws Exception {
        // Get the interProduct
        restInterProductMockMvc.perform(get("/api/inter-products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInterProduct() throws Exception {
        // Initialize the database
        interProductRepository.saveAndFlush(interProduct);

        int databaseSizeBeforeUpdate = interProductRepository.findAll().size();

        // Update the interProduct
        InterProduct updatedInterProduct = interProductRepository.findById(interProduct.getId()).get();
        // Disconnect from session so that the updates on updatedInterProduct are not directly saved in db
        em.detach(updatedInterProduct);
        updatedInterProduct
            .code(UPDATED_CODE)
            .tickness(UPDATED_TICKNESS)
            .width(UPDATED_WIDTH)
            .valType(UPDATED_VAL_TYPE)
            .longGroups(UPDATED_LONG_GROUPS)
            .description(UPDATED_DESCRIPTION);
        InterProductDTO interProductDTO = interProductMapper.toDto(updatedInterProduct);

        restInterProductMockMvc.perform(put("/api/inter-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interProductDTO)))
            .andExpect(status().isOk());

        // Validate the InterProduct in the database
        List<InterProduct> interProductList = interProductRepository.findAll();
        assertThat(interProductList).hasSize(databaseSizeBeforeUpdate);
        InterProduct testInterProduct = interProductList.get(interProductList.size() - 1);
        assertThat(testInterProduct.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testInterProduct.getTickness()).isEqualTo(UPDATED_TICKNESS);
        assertThat(testInterProduct.getWidth()).isEqualTo(UPDATED_WIDTH);
        assertThat(testInterProduct.getValType()).isEqualTo(UPDATED_VAL_TYPE);
        assertThat(testInterProduct.getLongGroups()).isEqualTo(UPDATED_LONG_GROUPS);
        assertThat(testInterProduct.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingInterProduct() throws Exception {
        int databaseSizeBeforeUpdate = interProductRepository.findAll().size();

        // Create the InterProduct
        InterProductDTO interProductDTO = interProductMapper.toDto(interProduct);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInterProductMockMvc.perform(put("/api/inter-products")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(interProductDTO)))
            .andExpect(status().isBadRequest());

        // Validate the InterProduct in the database
        List<InterProduct> interProductList = interProductRepository.findAll();
        assertThat(interProductList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInterProduct() throws Exception {
        // Initialize the database
        interProductRepository.saveAndFlush(interProduct);

        int databaseSizeBeforeDelete = interProductRepository.findAll().size();

        // Get the interProduct
        restInterProductMockMvc.perform(delete("/api/inter-products/{id}", interProduct.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InterProduct> interProductList = interProductRepository.findAll();
        assertThat(interProductList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InterProduct.class);
        InterProduct interProduct1 = new InterProduct();
        interProduct1.setId(1L);
        InterProduct interProduct2 = new InterProduct();
        interProduct2.setId(interProduct1.getId());
        assertThat(interProduct1).isEqualTo(interProduct2);
        interProduct2.setId(2L);
        assertThat(interProduct1).isNotEqualTo(interProduct2);
        interProduct1.setId(null);
        assertThat(interProduct1).isNotEqualTo(interProduct2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InterProductDTO.class);
        InterProductDTO interProductDTO1 = new InterProductDTO();
        interProductDTO1.setId(1L);
        InterProductDTO interProductDTO2 = new InterProductDTO();
        assertThat(interProductDTO1).isNotEqualTo(interProductDTO2);
        interProductDTO2.setId(interProductDTO1.getId());
        assertThat(interProductDTO1).isEqualTo(interProductDTO2);
        interProductDTO2.setId(2L);
        assertThat(interProductDTO1).isNotEqualTo(interProductDTO2);
        interProductDTO1.setId(null);
        assertThat(interProductDTO1).isNotEqualTo(interProductDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(interProductMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(interProductMapper.fromId(null)).isNull();
    }
}
