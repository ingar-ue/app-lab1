package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.ConfigTableCorrection;
import gov.ingar.applab.repository.ConfigTableCorrectionRepository;
import gov.ingar.applab.service.ConfigTableCorrectionService;
import gov.ingar.applab.service.dto.ConfigTableCorrectionDTO;
import gov.ingar.applab.service.mapper.ConfigTableCorrectionMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConfigTableCorrectionResource REST controller.
 *
 * @see ConfigTableCorrectionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class ConfigTableCorrectionResourceIntTest {

    private static final BigDecimal DEFAULT_VALUE_1 = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE_1 = new BigDecimal(2);

    private static final BigDecimal DEFAULT_VALUE_2 = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE_2 = new BigDecimal(2);

    private static final BigDecimal DEFAULT_VALUE_3 = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE_3 = new BigDecimal(2);

    private static final BigDecimal DEFAULT_VALUE_4 = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE_4 = new BigDecimal(2);

    private static final BigDecimal DEFAULT_VALUE_5 = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE_5 = new BigDecimal(2);

    private static final BigDecimal DEFAULT_VALUE_6 = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE_6 = new BigDecimal(2);

    @Autowired
    private ConfigTableCorrectionRepository configTableCorrectionRepository;


    @Autowired
    private ConfigTableCorrectionMapper configTableCorrectionMapper;
    

    @Autowired
    private ConfigTableCorrectionService configTableCorrectionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restConfigTableCorrectionMockMvc;

    private ConfigTableCorrection configTableCorrection;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConfigTableCorrectionResource configTableCorrectionResource = new ConfigTableCorrectionResource(configTableCorrectionService);
        this.restConfigTableCorrectionMockMvc = MockMvcBuilders.standaloneSetup(configTableCorrectionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ConfigTableCorrection createEntity(EntityManager em) {
        ConfigTableCorrection configTableCorrection = new ConfigTableCorrection()
            .value1(DEFAULT_VALUE_1)
            .value2(DEFAULT_VALUE_2)
            .value3(DEFAULT_VALUE_3)
            .value4(DEFAULT_VALUE_4)
            .value5(DEFAULT_VALUE_5)
            .value6(DEFAULT_VALUE_6);
        return configTableCorrection;
    }

    @Before
    public void initTest() {
        configTableCorrection = createEntity(em);
    }

    @Test
    @Transactional
    public void createConfigTableCorrection() throws Exception {
        int databaseSizeBeforeCreate = configTableCorrectionRepository.findAll().size();

        // Create the ConfigTableCorrection
        ConfigTableCorrectionDTO configTableCorrectionDTO = configTableCorrectionMapper.toDto(configTableCorrection);
        restConfigTableCorrectionMockMvc.perform(post("/api/config-table-corrections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configTableCorrectionDTO)))
            .andExpect(status().isCreated());

        // Validate the ConfigTableCorrection in the database
        List<ConfigTableCorrection> configTableCorrectionList = configTableCorrectionRepository.findAll();
        assertThat(configTableCorrectionList).hasSize(databaseSizeBeforeCreate + 1);
        ConfigTableCorrection testConfigTableCorrection = configTableCorrectionList.get(configTableCorrectionList.size() - 1);
        assertThat(testConfigTableCorrection.getValue1()).isEqualTo(DEFAULT_VALUE_1);
        assertThat(testConfigTableCorrection.getValue2()).isEqualTo(DEFAULT_VALUE_2);
        assertThat(testConfigTableCorrection.getValue3()).isEqualTo(DEFAULT_VALUE_3);
        assertThat(testConfigTableCorrection.getValue4()).isEqualTo(DEFAULT_VALUE_4);
        assertThat(testConfigTableCorrection.getValue5()).isEqualTo(DEFAULT_VALUE_5);
        assertThat(testConfigTableCorrection.getValue6()).isEqualTo(DEFAULT_VALUE_6);
    }

    @Test
    @Transactional
    public void createConfigTableCorrectionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = configTableCorrectionRepository.findAll().size();

        // Create the ConfigTableCorrection with an existing ID
        configTableCorrection.setId(1L);
        ConfigTableCorrectionDTO configTableCorrectionDTO = configTableCorrectionMapper.toDto(configTableCorrection);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConfigTableCorrectionMockMvc.perform(post("/api/config-table-corrections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configTableCorrectionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ConfigTableCorrection in the database
        List<ConfigTableCorrection> configTableCorrectionList = configTableCorrectionRepository.findAll();
        assertThat(configTableCorrectionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllConfigTableCorrections() throws Exception {
        // Initialize the database
        configTableCorrectionRepository.saveAndFlush(configTableCorrection);

        // Get all the configTableCorrectionList
        restConfigTableCorrectionMockMvc.perform(get("/api/config-table-corrections?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(configTableCorrection.getId().intValue())))
            .andExpect(jsonPath("$.[*].value1").value(hasItem(DEFAULT_VALUE_1.intValue())))
            .andExpect(jsonPath("$.[*].value2").value(hasItem(DEFAULT_VALUE_2.intValue())))
            .andExpect(jsonPath("$.[*].value3").value(hasItem(DEFAULT_VALUE_3.intValue())))
            .andExpect(jsonPath("$.[*].value4").value(hasItem(DEFAULT_VALUE_4.intValue())))
            .andExpect(jsonPath("$.[*].value5").value(hasItem(DEFAULT_VALUE_5.intValue())))
            .andExpect(jsonPath("$.[*].value6").value(hasItem(DEFAULT_VALUE_6.intValue())));
    }
    

    @Test
    @Transactional
    public void getConfigTableCorrection() throws Exception {
        // Initialize the database
        configTableCorrectionRepository.saveAndFlush(configTableCorrection);

        // Get the configTableCorrection
        restConfigTableCorrectionMockMvc.perform(get("/api/config-table-corrections/{id}", configTableCorrection.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(configTableCorrection.getId().intValue()))
            .andExpect(jsonPath("$.value1").value(DEFAULT_VALUE_1.intValue()))
            .andExpect(jsonPath("$.value2").value(DEFAULT_VALUE_2.intValue()))
            .andExpect(jsonPath("$.value3").value(DEFAULT_VALUE_3.intValue()))
            .andExpect(jsonPath("$.value4").value(DEFAULT_VALUE_4.intValue()))
            .andExpect(jsonPath("$.value5").value(DEFAULT_VALUE_5.intValue()))
            .andExpect(jsonPath("$.value6").value(DEFAULT_VALUE_6.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingConfigTableCorrection() throws Exception {
        // Get the configTableCorrection
        restConfigTableCorrectionMockMvc.perform(get("/api/config-table-corrections/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConfigTableCorrection() throws Exception {
        // Initialize the database
        configTableCorrectionRepository.saveAndFlush(configTableCorrection);

        int databaseSizeBeforeUpdate = configTableCorrectionRepository.findAll().size();

        // Update the configTableCorrection
        ConfigTableCorrection updatedConfigTableCorrection = configTableCorrectionRepository.findById(configTableCorrection.getId()).get();
        // Disconnect from session so that the updates on updatedConfigTableCorrection are not directly saved in db
        em.detach(updatedConfigTableCorrection);
        updatedConfigTableCorrection
            .value1(UPDATED_VALUE_1)
            .value2(UPDATED_VALUE_2)
            .value3(UPDATED_VALUE_3)
            .value4(UPDATED_VALUE_4)
            .value5(UPDATED_VALUE_5)
            .value6(UPDATED_VALUE_6);
        ConfigTableCorrectionDTO configTableCorrectionDTO = configTableCorrectionMapper.toDto(updatedConfigTableCorrection);

        restConfigTableCorrectionMockMvc.perform(put("/api/config-table-corrections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configTableCorrectionDTO)))
            .andExpect(status().isOk());

        // Validate the ConfigTableCorrection in the database
        List<ConfigTableCorrection> configTableCorrectionList = configTableCorrectionRepository.findAll();
        assertThat(configTableCorrectionList).hasSize(databaseSizeBeforeUpdate);
        ConfigTableCorrection testConfigTableCorrection = configTableCorrectionList.get(configTableCorrectionList.size() - 1);
        assertThat(testConfigTableCorrection.getValue1()).isEqualTo(UPDATED_VALUE_1);
        assertThat(testConfigTableCorrection.getValue2()).isEqualTo(UPDATED_VALUE_2);
        assertThat(testConfigTableCorrection.getValue3()).isEqualTo(UPDATED_VALUE_3);
        assertThat(testConfigTableCorrection.getValue4()).isEqualTo(UPDATED_VALUE_4);
        assertThat(testConfigTableCorrection.getValue5()).isEqualTo(UPDATED_VALUE_5);
        assertThat(testConfigTableCorrection.getValue6()).isEqualTo(UPDATED_VALUE_6);
    }

    @Test
    @Transactional
    public void updateNonExistingConfigTableCorrection() throws Exception {
        int databaseSizeBeforeUpdate = configTableCorrectionRepository.findAll().size();

        // Create the ConfigTableCorrection
        ConfigTableCorrectionDTO configTableCorrectionDTO = configTableCorrectionMapper.toDto(configTableCorrection);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConfigTableCorrectionMockMvc.perform(put("/api/config-table-corrections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configTableCorrectionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ConfigTableCorrection in the database
        List<ConfigTableCorrection> configTableCorrectionList = configTableCorrectionRepository.findAll();
        assertThat(configTableCorrectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteConfigTableCorrection() throws Exception {
        // Initialize the database
        configTableCorrectionRepository.saveAndFlush(configTableCorrection);

        int databaseSizeBeforeDelete = configTableCorrectionRepository.findAll().size();

        // Get the configTableCorrection
        restConfigTableCorrectionMockMvc.perform(delete("/api/config-table-corrections/{id}", configTableCorrection.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ConfigTableCorrection> configTableCorrectionList = configTableCorrectionRepository.findAll();
        assertThat(configTableCorrectionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigTableCorrection.class);
        ConfigTableCorrection configTableCorrection1 = new ConfigTableCorrection();
        configTableCorrection1.setId(1L);
        ConfigTableCorrection configTableCorrection2 = new ConfigTableCorrection();
        configTableCorrection2.setId(configTableCorrection1.getId());
        assertThat(configTableCorrection1).isEqualTo(configTableCorrection2);
        configTableCorrection2.setId(2L);
        assertThat(configTableCorrection1).isNotEqualTo(configTableCorrection2);
        configTableCorrection1.setId(null);
        assertThat(configTableCorrection1).isNotEqualTo(configTableCorrection2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigTableCorrectionDTO.class);
        ConfigTableCorrectionDTO configTableCorrectionDTO1 = new ConfigTableCorrectionDTO();
        configTableCorrectionDTO1.setId(1L);
        ConfigTableCorrectionDTO configTableCorrectionDTO2 = new ConfigTableCorrectionDTO();
        assertThat(configTableCorrectionDTO1).isNotEqualTo(configTableCorrectionDTO2);
        configTableCorrectionDTO2.setId(configTableCorrectionDTO1.getId());
        assertThat(configTableCorrectionDTO1).isEqualTo(configTableCorrectionDTO2);
        configTableCorrectionDTO2.setId(2L);
        assertThat(configTableCorrectionDTO1).isNotEqualTo(configTableCorrectionDTO2);
        configTableCorrectionDTO1.setId(null);
        assertThat(configTableCorrectionDTO1).isNotEqualTo(configTableCorrectionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(configTableCorrectionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(configTableCorrectionMapper.fromId(null)).isNull();
    }
}
