package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.ProductRelations;
import gov.ingar.applab.repository.ProductRelationsRepository;
import gov.ingar.applab.service.ProductRelationsService;
import gov.ingar.applab.service.dto.ProductRelationsDTO;
import gov.ingar.applab.service.mapper.ProductRelationsMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProductRelationsResource REST controller.
 *
 * @see ProductRelationsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class ProductRelationsResourceIntTest {

    private static final Integer DEFAULT_QUANTITY_1 = 1;
    private static final Integer UPDATED_QUANTITY_1 = 2;

    private static final Integer DEFAULT_QUANTITY_2 = 1;
    private static final Integer UPDATED_QUANTITY_2 = 2;

    private static final Integer DEFAULT_QUANTITY_3 = 1;
    private static final Integer UPDATED_QUANTITY_3 = 2;

    @Autowired
    private ProductRelationsRepository productRelationsRepository;


    @Autowired
    private ProductRelationsMapper productRelationsMapper;
    

    @Autowired
    private ProductRelationsService productRelationsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProductRelationsMockMvc;

    private ProductRelations productRelations;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProductRelationsResource productRelationsResource = new ProductRelationsResource(productRelationsService);
        this.restProductRelationsMockMvc = MockMvcBuilders.standaloneSetup(productRelationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductRelations createEntity(EntityManager em) {
        ProductRelations productRelations = new ProductRelations()
            .quantity1(DEFAULT_QUANTITY_1)
            .quantity2(DEFAULT_QUANTITY_2)
            .quantity3(DEFAULT_QUANTITY_3);
        return productRelations;
    }

    @Before
    public void initTest() {
        productRelations = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductRelations() throws Exception {
        int databaseSizeBeforeCreate = productRelationsRepository.findAll().size();

        // Create the ProductRelations
        ProductRelationsDTO productRelationsDTO = productRelationsMapper.toDto(productRelations);
        restProductRelationsMockMvc.perform(post("/api/product-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRelationsDTO)))
            .andExpect(status().isCreated());

        // Validate the ProductRelations in the database
        List<ProductRelations> productRelationsList = productRelationsRepository.findAll();
        assertThat(productRelationsList).hasSize(databaseSizeBeforeCreate + 1);
        ProductRelations testProductRelations = productRelationsList.get(productRelationsList.size() - 1);
        assertThat(testProductRelations.getQuantity1()).isEqualTo(DEFAULT_QUANTITY_1);
        assertThat(testProductRelations.getQuantity2()).isEqualTo(DEFAULT_QUANTITY_2);
        assertThat(testProductRelations.getQuantity3()).isEqualTo(DEFAULT_QUANTITY_3);
    }

    @Test
    @Transactional
    public void createProductRelationsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productRelationsRepository.findAll().size();

        // Create the ProductRelations with an existing ID
        productRelations.setId(1L);
        ProductRelationsDTO productRelationsDTO = productRelationsMapper.toDto(productRelations);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductRelationsMockMvc.perform(post("/api/product-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRelationsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProductRelations in the database
        List<ProductRelations> productRelationsList = productRelationsRepository.findAll();
        assertThat(productRelationsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkQuantity1IsRequired() throws Exception {
        int databaseSizeBeforeTest = productRelationsRepository.findAll().size();
        // set the field null
        productRelations.setQuantity1(null);

        // Create the ProductRelations, which fails.
        ProductRelationsDTO productRelationsDTO = productRelationsMapper.toDto(productRelations);

        restProductRelationsMockMvc.perform(post("/api/product-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRelationsDTO)))
            .andExpect(status().isBadRequest());

        List<ProductRelations> productRelationsList = productRelationsRepository.findAll();
        assertThat(productRelationsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProductRelations() throws Exception {
        // Initialize the database
        productRelationsRepository.saveAndFlush(productRelations);

        // Get all the productRelationsList
        restProductRelationsMockMvc.perform(get("/api/product-relations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productRelations.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity1").value(hasItem(DEFAULT_QUANTITY_1)))
            .andExpect(jsonPath("$.[*].quantity2").value(hasItem(DEFAULT_QUANTITY_2)))
            .andExpect(jsonPath("$.[*].quantity3").value(hasItem(DEFAULT_QUANTITY_3)));
    }
    

    @Test
    @Transactional
    public void getProductRelations() throws Exception {
        // Initialize the database
        productRelationsRepository.saveAndFlush(productRelations);

        // Get the productRelations
        restProductRelationsMockMvc.perform(get("/api/product-relations/{id}", productRelations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(productRelations.getId().intValue()))
            .andExpect(jsonPath("$.quantity1").value(DEFAULT_QUANTITY_1))
            .andExpect(jsonPath("$.quantity2").value(DEFAULT_QUANTITY_2))
            .andExpect(jsonPath("$.quantity3").value(DEFAULT_QUANTITY_3));
    }
    @Test
    @Transactional
    public void getNonExistingProductRelations() throws Exception {
        // Get the productRelations
        restProductRelationsMockMvc.perform(get("/api/product-relations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductRelations() throws Exception {
        // Initialize the database
        productRelationsRepository.saveAndFlush(productRelations);

        int databaseSizeBeforeUpdate = productRelationsRepository.findAll().size();

        // Update the productRelations
        ProductRelations updatedProductRelations = productRelationsRepository.findById(productRelations.getId()).get();
        // Disconnect from session so that the updates on updatedProductRelations are not directly saved in db
        em.detach(updatedProductRelations);
        updatedProductRelations
            .quantity1(UPDATED_QUANTITY_1)
            .quantity2(UPDATED_QUANTITY_2)
            .quantity3(UPDATED_QUANTITY_3);
        ProductRelationsDTO productRelationsDTO = productRelationsMapper.toDto(updatedProductRelations);

        restProductRelationsMockMvc.perform(put("/api/product-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRelationsDTO)))
            .andExpect(status().isOk());

        // Validate the ProductRelations in the database
        List<ProductRelations> productRelationsList = productRelationsRepository.findAll();
        assertThat(productRelationsList).hasSize(databaseSizeBeforeUpdate);
        ProductRelations testProductRelations = productRelationsList.get(productRelationsList.size() - 1);
        assertThat(testProductRelations.getQuantity1()).isEqualTo(UPDATED_QUANTITY_1);
        assertThat(testProductRelations.getQuantity2()).isEqualTo(UPDATED_QUANTITY_2);
        assertThat(testProductRelations.getQuantity3()).isEqualTo(UPDATED_QUANTITY_3);
    }

    @Test
    @Transactional
    public void updateNonExistingProductRelations() throws Exception {
        int databaseSizeBeforeUpdate = productRelationsRepository.findAll().size();

        // Create the ProductRelations
        ProductRelationsDTO productRelationsDTO = productRelationsMapper.toDto(productRelations);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProductRelationsMockMvc.perform(put("/api/product-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productRelationsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProductRelations in the database
        List<ProductRelations> productRelationsList = productRelationsRepository.findAll();
        assertThat(productRelationsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductRelations() throws Exception {
        // Initialize the database
        productRelationsRepository.saveAndFlush(productRelations);

        int databaseSizeBeforeDelete = productRelationsRepository.findAll().size();

        // Get the productRelations
        restProductRelationsMockMvc.perform(delete("/api/product-relations/{id}", productRelations.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ProductRelations> productRelationsList = productRelationsRepository.findAll();
        assertThat(productRelationsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductRelations.class);
        ProductRelations productRelations1 = new ProductRelations();
        productRelations1.setId(1L);
        ProductRelations productRelations2 = new ProductRelations();
        productRelations2.setId(productRelations1.getId());
        assertThat(productRelations1).isEqualTo(productRelations2);
        productRelations2.setId(2L);
        assertThat(productRelations1).isNotEqualTo(productRelations2);
        productRelations1.setId(null);
        assertThat(productRelations1).isNotEqualTo(productRelations2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductRelationsDTO.class);
        ProductRelationsDTO productRelationsDTO1 = new ProductRelationsDTO();
        productRelationsDTO1.setId(1L);
        ProductRelationsDTO productRelationsDTO2 = new ProductRelationsDTO();
        assertThat(productRelationsDTO1).isNotEqualTo(productRelationsDTO2);
        productRelationsDTO2.setId(productRelationsDTO1.getId());
        assertThat(productRelationsDTO1).isEqualTo(productRelationsDTO2);
        productRelationsDTO2.setId(2L);
        assertThat(productRelationsDTO1).isNotEqualTo(productRelationsDTO2);
        productRelationsDTO1.setId(null);
        assertThat(productRelationsDTO1).isNotEqualTo(productRelationsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(productRelationsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(productRelationsMapper.fromId(null)).isNull();
    }
}
