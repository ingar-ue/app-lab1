package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.ConfigProccessTime;
import gov.ingar.applab.repository.ConfigProccessTimeRepository;
import gov.ingar.applab.service.ConfigProccessTimeService;
import gov.ingar.applab.service.dto.ConfigProccessTimeDTO;
import gov.ingar.applab.service.mapper.ConfigProccessTimeMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConfigProccessTimeResource REST controller.
 *
 * @see ConfigProccessTimeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class ConfigProccessTimeResourceIntTest {

    private static final BigDecimal DEFAULT_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE = new BigDecimal(2);

    @Autowired
    private ConfigProccessTimeRepository configProccessTimeRepository;


    @Autowired
    private ConfigProccessTimeMapper configProccessTimeMapper;
    

    @Autowired
    private ConfigProccessTimeService configProccessTimeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restConfigProccessTimeMockMvc;

    private ConfigProccessTime configProccessTime;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConfigProccessTimeResource configProccessTimeResource = new ConfigProccessTimeResource(configProccessTimeService);
        this.restConfigProccessTimeMockMvc = MockMvcBuilders.standaloneSetup(configProccessTimeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ConfigProccessTime createEntity(EntityManager em) {
        ConfigProccessTime configProccessTime = new ConfigProccessTime()
            .value(DEFAULT_VALUE);
        return configProccessTime;
    }

    @Before
    public void initTest() {
        configProccessTime = createEntity(em);
    }

    @Test
    @Transactional
    public void createConfigProccessTime() throws Exception {
        int databaseSizeBeforeCreate = configProccessTimeRepository.findAll().size();

        // Create the ConfigProccessTime
        ConfigProccessTimeDTO configProccessTimeDTO = configProccessTimeMapper.toDto(configProccessTime);
        restConfigProccessTimeMockMvc.perform(post("/api/config-proccess-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configProccessTimeDTO)))
            .andExpect(status().isCreated());

        // Validate the ConfigProccessTime in the database
        List<ConfigProccessTime> configProccessTimeList = configProccessTimeRepository.findAll();
        assertThat(configProccessTimeList).hasSize(databaseSizeBeforeCreate + 1);
        ConfigProccessTime testConfigProccessTime = configProccessTimeList.get(configProccessTimeList.size() - 1);
        assertThat(testConfigProccessTime.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createConfigProccessTimeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = configProccessTimeRepository.findAll().size();

        // Create the ConfigProccessTime with an existing ID
        configProccessTime.setId(1L);
        ConfigProccessTimeDTO configProccessTimeDTO = configProccessTimeMapper.toDto(configProccessTime);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConfigProccessTimeMockMvc.perform(post("/api/config-proccess-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configProccessTimeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ConfigProccessTime in the database
        List<ConfigProccessTime> configProccessTimeList = configProccessTimeRepository.findAll();
        assertThat(configProccessTimeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = configProccessTimeRepository.findAll().size();
        // set the field null
        configProccessTime.setValue(null);

        // Create the ConfigProccessTime, which fails.
        ConfigProccessTimeDTO configProccessTimeDTO = configProccessTimeMapper.toDto(configProccessTime);

        restConfigProccessTimeMockMvc.perform(post("/api/config-proccess-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configProccessTimeDTO)))
            .andExpect(status().isBadRequest());

        List<ConfigProccessTime> configProccessTimeList = configProccessTimeRepository.findAll();
        assertThat(configProccessTimeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllConfigProccessTimes() throws Exception {
        // Initialize the database
        configProccessTimeRepository.saveAndFlush(configProccessTime);

        // Get all the configProccessTimeList
        restConfigProccessTimeMockMvc.perform(get("/api/config-proccess-times?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(configProccessTime.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.intValue())));
    }
    

    @Test
    @Transactional
    public void getConfigProccessTime() throws Exception {
        // Initialize the database
        configProccessTimeRepository.saveAndFlush(configProccessTime);

        // Get the configProccessTime
        restConfigProccessTimeMockMvc.perform(get("/api/config-proccess-times/{id}", configProccessTime.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(configProccessTime.getId().intValue()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingConfigProccessTime() throws Exception {
        // Get the configProccessTime
        restConfigProccessTimeMockMvc.perform(get("/api/config-proccess-times/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConfigProccessTime() throws Exception {
        // Initialize the database
        configProccessTimeRepository.saveAndFlush(configProccessTime);

        int databaseSizeBeforeUpdate = configProccessTimeRepository.findAll().size();

        // Update the configProccessTime
        ConfigProccessTime updatedConfigProccessTime = configProccessTimeRepository.findById(configProccessTime.getId()).get();
        // Disconnect from session so that the updates on updatedConfigProccessTime are not directly saved in db
        em.detach(updatedConfigProccessTime);
        updatedConfigProccessTime
            .value(UPDATED_VALUE);
        ConfigProccessTimeDTO configProccessTimeDTO = configProccessTimeMapper.toDto(updatedConfigProccessTime);

        restConfigProccessTimeMockMvc.perform(put("/api/config-proccess-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configProccessTimeDTO)))
            .andExpect(status().isOk());

        // Validate the ConfigProccessTime in the database
        List<ConfigProccessTime> configProccessTimeList = configProccessTimeRepository.findAll();
        assertThat(configProccessTimeList).hasSize(databaseSizeBeforeUpdate);
        ConfigProccessTime testConfigProccessTime = configProccessTimeList.get(configProccessTimeList.size() - 1);
        assertThat(testConfigProccessTime.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingConfigProccessTime() throws Exception {
        int databaseSizeBeforeUpdate = configProccessTimeRepository.findAll().size();

        // Create the ConfigProccessTime
        ConfigProccessTimeDTO configProccessTimeDTO = configProccessTimeMapper.toDto(configProccessTime);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConfigProccessTimeMockMvc.perform(put("/api/config-proccess-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(configProccessTimeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ConfigProccessTime in the database
        List<ConfigProccessTime> configProccessTimeList = configProccessTimeRepository.findAll();
        assertThat(configProccessTimeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteConfigProccessTime() throws Exception {
        // Initialize the database
        configProccessTimeRepository.saveAndFlush(configProccessTime);

        int databaseSizeBeforeDelete = configProccessTimeRepository.findAll().size();

        // Get the configProccessTime
        restConfigProccessTimeMockMvc.perform(delete("/api/config-proccess-times/{id}", configProccessTime.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ConfigProccessTime> configProccessTimeList = configProccessTimeRepository.findAll();
        assertThat(configProccessTimeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigProccessTime.class);
        ConfigProccessTime configProccessTime1 = new ConfigProccessTime();
        configProccessTime1.setId(1L);
        ConfigProccessTime configProccessTime2 = new ConfigProccessTime();
        configProccessTime2.setId(configProccessTime1.getId());
        assertThat(configProccessTime1).isEqualTo(configProccessTime2);
        configProccessTime2.setId(2L);
        assertThat(configProccessTime1).isNotEqualTo(configProccessTime2);
        configProccessTime1.setId(null);
        assertThat(configProccessTime1).isNotEqualTo(configProccessTime2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigProccessTimeDTO.class);
        ConfigProccessTimeDTO configProccessTimeDTO1 = new ConfigProccessTimeDTO();
        configProccessTimeDTO1.setId(1L);
        ConfigProccessTimeDTO configProccessTimeDTO2 = new ConfigProccessTimeDTO();
        assertThat(configProccessTimeDTO1).isNotEqualTo(configProccessTimeDTO2);
        configProccessTimeDTO2.setId(configProccessTimeDTO1.getId());
        assertThat(configProccessTimeDTO1).isEqualTo(configProccessTimeDTO2);
        configProccessTimeDTO2.setId(2L);
        assertThat(configProccessTimeDTO1).isNotEqualTo(configProccessTimeDTO2);
        configProccessTimeDTO1.setId(null);
        assertThat(configProccessTimeDTO1).isNotEqualTo(configProccessTimeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(configProccessTimeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(configProccessTimeMapper.fromId(null)).isNull();
    }
}
