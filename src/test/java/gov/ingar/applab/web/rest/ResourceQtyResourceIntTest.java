package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.ResourceQty;
import gov.ingar.applab.repository.ResourceQtyRepository;
import gov.ingar.applab.service.ResourceQtyService;
import gov.ingar.applab.service.dto.ResourceQtyDTO;
import gov.ingar.applab.service.mapper.ResourceQtyMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ResourceQtyResource REST controller.
 *
 * @see ResourceQtyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class ResourceQtyResourceIntTest {

    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;

    @Autowired
    private ResourceQtyRepository resourceQtyRepository;


    @Autowired
    private ResourceQtyMapper resourceQtyMapper;
    

    @Autowired
    private ResourceQtyService resourceQtyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restResourceQtyMockMvc;

    private ResourceQty resourceQty;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ResourceQtyResource resourceQtyResource = new ResourceQtyResource(resourceQtyService);
        this.restResourceQtyMockMvc = MockMvcBuilders.standaloneSetup(resourceQtyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResourceQty createEntity(EntityManager em) {
        ResourceQty resourceQty = new ResourceQty()
            .quantity(DEFAULT_QUANTITY);
        return resourceQty;
    }

    @Before
    public void initTest() {
        resourceQty = createEntity(em);
    }

    @Test
    @Transactional
    public void createResourceQty() throws Exception {
        int databaseSizeBeforeCreate = resourceQtyRepository.findAll().size();

        // Create the ResourceQty
        ResourceQtyDTO resourceQtyDTO = resourceQtyMapper.toDto(resourceQty);
        restResourceQtyMockMvc.perform(post("/api/resource-qties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resourceQtyDTO)))
            .andExpect(status().isCreated());

        // Validate the ResourceQty in the database
        List<ResourceQty> resourceQtyList = resourceQtyRepository.findAll();
        assertThat(resourceQtyList).hasSize(databaseSizeBeforeCreate + 1);
        ResourceQty testResourceQty = resourceQtyList.get(resourceQtyList.size() - 1);
        assertThat(testResourceQty.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
    }

    @Test
    @Transactional
    public void createResourceQtyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resourceQtyRepository.findAll().size();

        // Create the ResourceQty with an existing ID
        resourceQty.setId(1L);
        ResourceQtyDTO resourceQtyDTO = resourceQtyMapper.toDto(resourceQty);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResourceQtyMockMvc.perform(post("/api/resource-qties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resourceQtyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ResourceQty in the database
        List<ResourceQty> resourceQtyList = resourceQtyRepository.findAll();
        assertThat(resourceQtyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkQuantityIsRequired() throws Exception {
        int databaseSizeBeforeTest = resourceQtyRepository.findAll().size();
        // set the field null
        resourceQty.setQuantity(null);

        // Create the ResourceQty, which fails.
        ResourceQtyDTO resourceQtyDTO = resourceQtyMapper.toDto(resourceQty);

        restResourceQtyMockMvc.perform(post("/api/resource-qties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resourceQtyDTO)))
            .andExpect(status().isBadRequest());

        List<ResourceQty> resourceQtyList = resourceQtyRepository.findAll();
        assertThat(resourceQtyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllResourceQties() throws Exception {
        // Initialize the database
        resourceQtyRepository.saveAndFlush(resourceQty);

        // Get all the resourceQtyList
        restResourceQtyMockMvc.perform(get("/api/resource-qties?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resourceQty.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)));
    }
    

    @Test
    @Transactional
    public void getResourceQty() throws Exception {
        // Initialize the database
        resourceQtyRepository.saveAndFlush(resourceQty);

        // Get the resourceQty
        restResourceQtyMockMvc.perform(get("/api/resource-qties/{id}", resourceQty.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(resourceQty.getId().intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY));
    }
    @Test
    @Transactional
    public void getNonExistingResourceQty() throws Exception {
        // Get the resourceQty
        restResourceQtyMockMvc.perform(get("/api/resource-qties/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResourceQty() throws Exception {
        // Initialize the database
        resourceQtyRepository.saveAndFlush(resourceQty);

        int databaseSizeBeforeUpdate = resourceQtyRepository.findAll().size();

        // Update the resourceQty
        ResourceQty updatedResourceQty = resourceQtyRepository.findById(resourceQty.getId()).get();
        // Disconnect from session so that the updates on updatedResourceQty are not directly saved in db
        em.detach(updatedResourceQty);
        updatedResourceQty
            .quantity(UPDATED_QUANTITY);
        ResourceQtyDTO resourceQtyDTO = resourceQtyMapper.toDto(updatedResourceQty);

        restResourceQtyMockMvc.perform(put("/api/resource-qties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resourceQtyDTO)))
            .andExpect(status().isOk());

        // Validate the ResourceQty in the database
        List<ResourceQty> resourceQtyList = resourceQtyRepository.findAll();
        assertThat(resourceQtyList).hasSize(databaseSizeBeforeUpdate);
        ResourceQty testResourceQty = resourceQtyList.get(resourceQtyList.size() - 1);
        assertThat(testResourceQty.getQuantity()).isEqualTo(UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void updateNonExistingResourceQty() throws Exception {
        int databaseSizeBeforeUpdate = resourceQtyRepository.findAll().size();

        // Create the ResourceQty
        ResourceQtyDTO resourceQtyDTO = resourceQtyMapper.toDto(resourceQty);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restResourceQtyMockMvc.perform(put("/api/resource-qties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resourceQtyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ResourceQty in the database
        List<ResourceQty> resourceQtyList = resourceQtyRepository.findAll();
        assertThat(resourceQtyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteResourceQty() throws Exception {
        // Initialize the database
        resourceQtyRepository.saveAndFlush(resourceQty);

        int databaseSizeBeforeDelete = resourceQtyRepository.findAll().size();

        // Get the resourceQty
        restResourceQtyMockMvc.perform(delete("/api/resource-qties/{id}", resourceQty.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ResourceQty> resourceQtyList = resourceQtyRepository.findAll();
        assertThat(resourceQtyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ResourceQty.class);
        ResourceQty resourceQty1 = new ResourceQty();
        resourceQty1.setId(1L);
        ResourceQty resourceQty2 = new ResourceQty();
        resourceQty2.setId(resourceQty1.getId());
        assertThat(resourceQty1).isEqualTo(resourceQty2);
        resourceQty2.setId(2L);
        assertThat(resourceQty1).isNotEqualTo(resourceQty2);
        resourceQty1.setId(null);
        assertThat(resourceQty1).isNotEqualTo(resourceQty2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ResourceQtyDTO.class);
        ResourceQtyDTO resourceQtyDTO1 = new ResourceQtyDTO();
        resourceQtyDTO1.setId(1L);
        ResourceQtyDTO resourceQtyDTO2 = new ResourceQtyDTO();
        assertThat(resourceQtyDTO1).isNotEqualTo(resourceQtyDTO2);
        resourceQtyDTO2.setId(resourceQtyDTO1.getId());
        assertThat(resourceQtyDTO1).isEqualTo(resourceQtyDTO2);
        resourceQtyDTO2.setId(2L);
        assertThat(resourceQtyDTO1).isNotEqualTo(resourceQtyDTO2);
        resourceQtyDTO1.setId(null);
        assertThat(resourceQtyDTO1).isNotEqualTo(resourceQtyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(resourceQtyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(resourceQtyMapper.fromId(null)).isNull();
    }
}
