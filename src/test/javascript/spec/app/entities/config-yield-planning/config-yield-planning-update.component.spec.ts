/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ConfigYieldPlanningUpdateComponent } from 'app/entities/config-yield-planning/config-yield-planning-update.component';
import { ConfigYieldPlanningService } from 'app/entities/config-yield-planning/config-yield-planning.service';
import { ConfigYieldPlanning } from 'app/shared/model/config-yield-planning.model';

describe('Component Tests', () => {
    describe('ConfigYieldPlanning Management Update Component', () => {
        let comp: ConfigYieldPlanningUpdateComponent;
        let fixture: ComponentFixture<ConfigYieldPlanningUpdateComponent>;
        let service: ConfigYieldPlanningService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ConfigYieldPlanningUpdateComponent]
            })
                .overrideTemplate(ConfigYieldPlanningUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ConfigYieldPlanningUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConfigYieldPlanningService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ConfigYieldPlanning(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.configYieldPlanning = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ConfigYieldPlanning();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.configYieldPlanning = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
