/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ConfigYieldPlanningDetailComponent } from 'app/entities/config-yield-planning/config-yield-planning-detail.component';
import { ConfigYieldPlanning } from 'app/shared/model/config-yield-planning.model';

describe('Component Tests', () => {
    describe('ConfigYieldPlanning Management Detail Component', () => {
        let comp: ConfigYieldPlanningDetailComponent;
        let fixture: ComponentFixture<ConfigYieldPlanningDetailComponent>;
        const route = ({ data: of({ configYieldPlanning: new ConfigYieldPlanning(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ConfigYieldPlanningDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ConfigYieldPlanningDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ConfigYieldPlanningDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.configYieldPlanning).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
