/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLabTestModule } from '../../../test.module';
import { ConfigTableCorrectionDeleteDialogComponent } from 'app/entities/config-table-correction/config-table-correction-delete-dialog.component';
import { ConfigTableCorrectionService } from 'app/entities/config-table-correction/config-table-correction.service';

describe('Component Tests', () => {
    describe('ConfigTableCorrection Management Delete Component', () => {
        let comp: ConfigTableCorrectionDeleteDialogComponent;
        let fixture: ComponentFixture<ConfigTableCorrectionDeleteDialogComponent>;
        let service: ConfigTableCorrectionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ConfigTableCorrectionDeleteDialogComponent]
            })
                .overrideTemplate(ConfigTableCorrectionDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ConfigTableCorrectionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConfigTableCorrectionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
