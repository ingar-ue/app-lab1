/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ConfigTableCorrectionUpdateComponent } from 'app/entities/config-table-correction/config-table-correction-update.component';
import { ConfigTableCorrectionService } from 'app/entities/config-table-correction/config-table-correction.service';
import { ConfigTableCorrection } from 'app/shared/model/config-table-correction.model';

describe('Component Tests', () => {
    describe('ConfigTableCorrection Management Update Component', () => {
        let comp: ConfigTableCorrectionUpdateComponent;
        let fixture: ComponentFixture<ConfigTableCorrectionUpdateComponent>;
        let service: ConfigTableCorrectionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ConfigTableCorrectionUpdateComponent]
            })
                .overrideTemplate(ConfigTableCorrectionUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ConfigTableCorrectionUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConfigTableCorrectionService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ConfigTableCorrection(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.configTableCorrection = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ConfigTableCorrection();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.configTableCorrection = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
