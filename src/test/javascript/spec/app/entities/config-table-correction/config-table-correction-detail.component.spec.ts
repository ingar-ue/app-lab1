/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ConfigTableCorrectionDetailComponent } from 'app/entities/config-table-correction/config-table-correction-detail.component';
import { ConfigTableCorrection } from 'app/shared/model/config-table-correction.model';

describe('Component Tests', () => {
    describe('ConfigTableCorrection Management Detail Component', () => {
        let comp: ConfigTableCorrectionDetailComponent;
        let fixture: ComponentFixture<ConfigTableCorrectionDetailComponent>;
        const route = ({ data: of({ configTableCorrection: new ConfigTableCorrection(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ConfigTableCorrectionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ConfigTableCorrectionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ConfigTableCorrectionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.configTableCorrection).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
