/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLabTestModule } from '../../../test.module';
import { DiameterGroupDeleteDialogComponent } from 'app/entities/diameter-group/diameter-group-delete-dialog.component';
import { DiameterGroupService } from 'app/entities/diameter-group/diameter-group.service';

describe('Component Tests', () => {
    describe('DiameterGroup Management Delete Component', () => {
        let comp: DiameterGroupDeleteDialogComponent;
        let fixture: ComponentFixture<DiameterGroupDeleteDialogComponent>;
        let service: DiameterGroupService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [DiameterGroupDeleteDialogComponent]
            })
                .overrideTemplate(DiameterGroupDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DiameterGroupDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DiameterGroupService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
