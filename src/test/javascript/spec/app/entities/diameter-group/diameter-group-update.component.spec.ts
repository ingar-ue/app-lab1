/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { DiameterGroupUpdateComponent } from 'app/entities/diameter-group/diameter-group-update.component';
import { DiameterGroupService } from 'app/entities/diameter-group/diameter-group.service';
import { DiameterGroup } from 'app/shared/model/diameter-group.model';

describe('Component Tests', () => {
    describe('DiameterGroup Management Update Component', () => {
        let comp: DiameterGroupUpdateComponent;
        let fixture: ComponentFixture<DiameterGroupUpdateComponent>;
        let service: DiameterGroupService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [DiameterGroupUpdateComponent]
            })
                .overrideTemplate(DiameterGroupUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DiameterGroupUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DiameterGroupService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DiameterGroup(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.diameterGroup = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DiameterGroup();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.diameterGroup = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
