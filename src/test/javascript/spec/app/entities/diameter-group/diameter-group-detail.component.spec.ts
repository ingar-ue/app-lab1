/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { DiameterGroupDetailComponent } from 'app/entities/diameter-group/diameter-group-detail.component';
import { DiameterGroup } from 'app/shared/model/diameter-group.model';

describe('Component Tests', () => {
    describe('DiameterGroup Management Detail Component', () => {
        let comp: DiameterGroupDetailComponent;
        let fixture: ComponentFixture<DiameterGroupDetailComponent>;
        const route = ({ data: of({ diameterGroup: new DiameterGroup(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [DiameterGroupDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(DiameterGroupDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DiameterGroupDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.diameterGroup).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
