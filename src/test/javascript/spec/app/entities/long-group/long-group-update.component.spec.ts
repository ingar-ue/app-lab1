/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { LongGroupUpdateComponent } from 'app/entities/long-group/long-group-update.component';
import { LongGroupService } from 'app/entities/long-group/long-group.service';
import { LongGroup } from 'app/shared/model/long-group.model';

describe('Component Tests', () => {
    describe('LongGroup Management Update Component', () => {
        let comp: LongGroupUpdateComponent;
        let fixture: ComponentFixture<LongGroupUpdateComponent>;
        let service: LongGroupService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [LongGroupUpdateComponent]
            })
                .overrideTemplate(LongGroupUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(LongGroupUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LongGroupService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new LongGroup(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.longGroup = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new LongGroup();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.longGroup = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
