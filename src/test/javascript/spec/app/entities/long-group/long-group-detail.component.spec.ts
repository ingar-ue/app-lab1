/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { LongGroupDetailComponent } from 'app/entities/long-group/long-group-detail.component';
import { LongGroup } from 'app/shared/model/long-group.model';

describe('Component Tests', () => {
    describe('LongGroup Management Detail Component', () => {
        let comp: LongGroupDetailComponent;
        let fixture: ComponentFixture<LongGroupDetailComponent>;
        const route = ({ data: of({ longGroup: new LongGroup(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [LongGroupDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(LongGroupDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LongGroupDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.longGroup).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
