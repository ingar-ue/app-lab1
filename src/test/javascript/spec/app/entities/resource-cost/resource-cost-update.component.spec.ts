/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ResourceCostUpdateComponent } from 'app/entities/resource-cost/resource-cost-update.component';
import { ResourceCostService } from 'app/entities/resource-cost/resource-cost.service';
import { ResourceCost } from 'app/shared/model/resource-cost.model';

describe('Component Tests', () => {
    describe('ResourceCost Management Update Component', () => {
        let comp: ResourceCostUpdateComponent;
        let fixture: ComponentFixture<ResourceCostUpdateComponent>;
        let service: ResourceCostService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ResourceCostUpdateComponent]
            })
                .overrideTemplate(ResourceCostUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ResourceCostUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ResourceCostService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ResourceCost(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.resourceCost = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ResourceCost();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.resourceCost = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
