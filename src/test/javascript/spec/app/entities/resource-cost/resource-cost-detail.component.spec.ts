/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ResourceCostDetailComponent } from 'app/entities/resource-cost/resource-cost-detail.component';
import { ResourceCost } from 'app/shared/model/resource-cost.model';

describe('Component Tests', () => {
    describe('ResourceCost Management Detail Component', () => {
        let comp: ResourceCostDetailComponent;
        let fixture: ComponentFixture<ResourceCostDetailComponent>;
        const route = ({ data: of({ resourceCost: new ResourceCost(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ResourceCostDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ResourceCostDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ResourceCostDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.resourceCost).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
