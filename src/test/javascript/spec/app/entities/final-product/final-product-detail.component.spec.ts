/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { FinalProductDetailComponent } from 'app/entities/final-product/final-product-detail.component';
import { FinalProduct } from 'app/shared/model/final-product.model';

describe('Component Tests', () => {
    describe('FinalProduct Management Detail Component', () => {
        let comp: FinalProductDetailComponent;
        let fixture: ComponentFixture<FinalProductDetailComponent>;
        const route = ({ data: of({ finalProduct: new FinalProduct(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [FinalProductDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(FinalProductDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(FinalProductDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.finalProduct).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
