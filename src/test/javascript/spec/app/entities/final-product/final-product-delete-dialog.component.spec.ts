/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLabTestModule } from '../../../test.module';
import { FinalProductDeleteDialogComponent } from 'app/entities/final-product/final-product-delete-dialog.component';
import { FinalProductService } from 'app/entities/final-product/final-product.service';

describe('Component Tests', () => {
    describe('FinalProduct Management Delete Component', () => {
        let comp: FinalProductDeleteDialogComponent;
        let fixture: ComponentFixture<FinalProductDeleteDialogComponent>;
        let service: FinalProductService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [FinalProductDeleteDialogComponent]
            })
                .overrideTemplate(FinalProductDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(FinalProductDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FinalProductService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
