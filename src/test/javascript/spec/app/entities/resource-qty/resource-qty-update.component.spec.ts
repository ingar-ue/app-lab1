/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ResourceQtyUpdateComponent } from 'app/entities/resource-qty/resource-qty-update.component';
import { ResourceQtyService } from 'app/entities/resource-qty/resource-qty.service';
import { ResourceQty } from 'app/shared/model/resource-qty.model';

describe('Component Tests', () => {
    describe('ResourceQty Management Update Component', () => {
        let comp: ResourceQtyUpdateComponent;
        let fixture: ComponentFixture<ResourceQtyUpdateComponent>;
        let service: ResourceQtyService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ResourceQtyUpdateComponent]
            })
                .overrideTemplate(ResourceQtyUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ResourceQtyUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ResourceQtyService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ResourceQty(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.resourceQty = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ResourceQty();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.resourceQty = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
