/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ResourceQtyDetailComponent } from 'app/entities/resource-qty/resource-qty-detail.component';
import { ResourceQty } from 'app/shared/model/resource-qty.model';

describe('Component Tests', () => {
    describe('ResourceQty Management Detail Component', () => {
        let comp: ResourceQtyDetailComponent;
        let fixture: ComponentFixture<ResourceQtyDetailComponent>;
        const route = ({ data: of({ resourceQty: new ResourceQty(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ResourceQtyDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ResourceQtyDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ResourceQtyDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.resourceQty).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
