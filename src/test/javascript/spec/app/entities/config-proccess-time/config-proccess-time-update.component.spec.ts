/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ConfigProccessTimeUpdateComponent } from 'app/entities/config-proccess-time/config-proccess-time-update.component';
import { ConfigProccessTimeService } from 'app/entities/config-proccess-time/config-proccess-time.service';
import { ConfigProccessTime } from 'app/shared/model/config-proccess-time.model';

describe('Component Tests', () => {
    describe('ConfigProccessTime Management Update Component', () => {
        let comp: ConfigProccessTimeUpdateComponent;
        let fixture: ComponentFixture<ConfigProccessTimeUpdateComponent>;
        let service: ConfigProccessTimeService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ConfigProccessTimeUpdateComponent]
            })
                .overrideTemplate(ConfigProccessTimeUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ConfigProccessTimeUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConfigProccessTimeService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ConfigProccessTime(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.configProccessTime = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ConfigProccessTime();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.configProccessTime = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
