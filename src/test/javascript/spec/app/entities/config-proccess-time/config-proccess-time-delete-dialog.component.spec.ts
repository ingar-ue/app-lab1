/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLabTestModule } from '../../../test.module';
import { ConfigProccessTimeDeleteDialogComponent } from 'app/entities/config-proccess-time/config-proccess-time-delete-dialog.component';
import { ConfigProccessTimeService } from 'app/entities/config-proccess-time/config-proccess-time.service';

describe('Component Tests', () => {
    describe('ConfigProccessTime Management Delete Component', () => {
        let comp: ConfigProccessTimeDeleteDialogComponent;
        let fixture: ComponentFixture<ConfigProccessTimeDeleteDialogComponent>;
        let service: ConfigProccessTimeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ConfigProccessTimeDeleteDialogComponent]
            })
                .overrideTemplate(ConfigProccessTimeDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ConfigProccessTimeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConfigProccessTimeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
