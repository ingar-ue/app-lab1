/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ConfigProccessTimeDetailComponent } from 'app/entities/config-proccess-time/config-proccess-time-detail.component';
import { ConfigProccessTime } from 'app/shared/model/config-proccess-time.model';

describe('Component Tests', () => {
    describe('ConfigProccessTime Management Detail Component', () => {
        let comp: ConfigProccessTimeDetailComponent;
        let fixture: ComponentFixture<ConfigProccessTimeDetailComponent>;
        const route = ({ data: of({ configProccessTime: new ConfigProccessTime(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ConfigProccessTimeDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ConfigProccessTimeDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ConfigProccessTimeDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.configProccessTime).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
