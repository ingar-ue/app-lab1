/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { KerfConfigDetailComponent } from 'app/entities/kerf-config/kerf-config-detail.component';
import { KerfConfig } from 'app/shared/model/kerf-config.model';

describe('Component Tests', () => {
    describe('KerfConfig Management Detail Component', () => {
        let comp: KerfConfigDetailComponent;
        let fixture: ComponentFixture<KerfConfigDetailComponent>;
        const route = ({ data: of({ kerfConfig: new KerfConfig(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [KerfConfigDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(KerfConfigDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(KerfConfigDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.kerfConfig).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
