/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { KerfConfigUpdateComponent } from 'app/entities/kerf-config/kerf-config-update.component';
import { KerfConfigService } from 'app/entities/kerf-config/kerf-config.service';
import { KerfConfig } from 'app/shared/model/kerf-config.model';

describe('Component Tests', () => {
    describe('KerfConfig Management Update Component', () => {
        let comp: KerfConfigUpdateComponent;
        let fixture: ComponentFixture<KerfConfigUpdateComponent>;
        let service: KerfConfigService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [KerfConfigUpdateComponent]
            })
                .overrideTemplate(KerfConfigUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(KerfConfigUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(KerfConfigService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new KerfConfig(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.kerfConfig = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new KerfConfig();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.kerfConfig = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
