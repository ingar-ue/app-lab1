/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLabTestModule } from '../../../test.module';
import { KerfConfigDeleteDialogComponent } from 'app/entities/kerf-config/kerf-config-delete-dialog.component';
import { KerfConfigService } from 'app/entities/kerf-config/kerf-config.service';

describe('Component Tests', () => {
    describe('KerfConfig Management Delete Component', () => {
        let comp: KerfConfigDeleteDialogComponent;
        let fixture: ComponentFixture<KerfConfigDeleteDialogComponent>;
        let service: KerfConfigService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [KerfConfigDeleteDialogComponent]
            })
                .overrideTemplate(KerfConfigDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(KerfConfigDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(KerfConfigService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
