/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { InterProductUpdateComponent } from 'app/entities/inter-product/inter-product-update.component';
import { InterProductService } from 'app/entities/inter-product/inter-product.service';
import { InterProduct } from 'app/shared/model/inter-product.model';

describe('Component Tests', () => {
    describe('InterProduct Management Update Component', () => {
        let comp: InterProductUpdateComponent;
        let fixture: ComponentFixture<InterProductUpdateComponent>;
        let service: InterProductService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [InterProductUpdateComponent]
            })
                .overrideTemplate(InterProductUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(InterProductUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InterProductService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InterProduct(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.interProduct = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new InterProduct();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.interProduct = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
