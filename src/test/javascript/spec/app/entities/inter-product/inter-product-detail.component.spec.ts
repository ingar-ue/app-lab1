/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { InterProductDetailComponent } from 'app/entities/inter-product/inter-product-detail.component';
import { InterProduct } from 'app/shared/model/inter-product.model';

describe('Component Tests', () => {
    describe('InterProduct Management Detail Component', () => {
        let comp: InterProductDetailComponent;
        let fixture: ComponentFixture<InterProductDetailComponent>;
        const route = ({ data: of({ interProduct: new InterProduct(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [InterProductDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(InterProductDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(InterProductDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.interProduct).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
