/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { PatternExclusionUpdateComponent } from 'app/entities/pattern-exclusion/pattern-exclusion-update.component';
import { PatternExclusionService } from 'app/entities/pattern-exclusion/pattern-exclusion.service';
import { PatternExclusion } from 'app/shared/model/pattern-exclusion.model';

describe('Component Tests', () => {
    describe('PatternExclusion Management Update Component', () => {
        let comp: PatternExclusionUpdateComponent;
        let fixture: ComponentFixture<PatternExclusionUpdateComponent>;
        let service: PatternExclusionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [PatternExclusionUpdateComponent]
            })
                .overrideTemplate(PatternExclusionUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PatternExclusionUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PatternExclusionService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PatternExclusion(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.patternExclusion = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PatternExclusion();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.patternExclusion = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
