/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLabTestModule } from '../../../test.module';
import { PatternExclusionDeleteDialogComponent } from 'app/entities/pattern-exclusion/pattern-exclusion-delete-dialog.component';
import { PatternExclusionService } from 'app/entities/pattern-exclusion/pattern-exclusion.service';

describe('Component Tests', () => {
    describe('PatternExclusion Management Delete Component', () => {
        let comp: PatternExclusionDeleteDialogComponent;
        let fixture: ComponentFixture<PatternExclusionDeleteDialogComponent>;
        let service: PatternExclusionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [PatternExclusionDeleteDialogComponent]
            })
                .overrideTemplate(PatternExclusionDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PatternExclusionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PatternExclusionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
