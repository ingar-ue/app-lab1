/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { PatternExclusionDetailComponent } from 'app/entities/pattern-exclusion/pattern-exclusion-detail.component';
import { PatternExclusion } from 'app/shared/model/pattern-exclusion.model';

describe('Component Tests', () => {
    describe('PatternExclusion Management Detail Component', () => {
        let comp: PatternExclusionDetailComponent;
        let fixture: ComponentFixture<PatternExclusionDetailComponent>;
        const route = ({ data: of({ patternExclusion: new PatternExclusion(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [PatternExclusionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PatternExclusionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PatternExclusionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.patternExclusion).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
