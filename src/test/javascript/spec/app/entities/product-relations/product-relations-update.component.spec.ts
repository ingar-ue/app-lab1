/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ProductRelationsUpdateComponent } from 'app/entities/product-relations/product-relations-update.component';
import { ProductRelationsService } from 'app/entities/product-relations/product-relations.service';
import { ProductRelations } from 'app/shared/model/product-relations.model';

describe('Component Tests', () => {
    describe('ProductRelations Management Update Component', () => {
        let comp: ProductRelationsUpdateComponent;
        let fixture: ComponentFixture<ProductRelationsUpdateComponent>;
        let service: ProductRelationsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ProductRelationsUpdateComponent]
            })
                .overrideTemplate(ProductRelationsUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ProductRelationsUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductRelationsService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ProductRelations(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.productRelations = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ProductRelations();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.productRelations = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
