/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ProductRelationsDetailComponent } from 'app/entities/product-relations/product-relations-detail.component';
import { ProductRelations } from 'app/shared/model/product-relations.model';

describe('Component Tests', () => {
    describe('ProductRelations Management Detail Component', () => {
        let comp: ProductRelationsDetailComponent;
        let fixture: ComponentFixture<ProductRelationsDetailComponent>;
        const route = ({ data: of({ productRelations: new ProductRelations(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ProductRelationsDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ProductRelationsDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProductRelationsDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.productRelations).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
