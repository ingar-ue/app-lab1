/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLabTestModule } from '../../../test.module';
import { ProductRelationsDeleteDialogComponent } from 'app/entities/product-relations/product-relations-delete-dialog.component';
import { ProductRelationsService } from 'app/entities/product-relations/product-relations.service';

describe('Component Tests', () => {
    describe('ProductRelations Management Delete Component', () => {
        let comp: ProductRelationsDeleteDialogComponent;
        let fixture: ComponentFixture<ProductRelationsDeleteDialogComponent>;
        let service: ProductRelationsService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ProductRelationsDeleteDialogComponent]
            })
                .overrideTemplate(ProductRelationsDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProductRelationsDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductRelationsService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
